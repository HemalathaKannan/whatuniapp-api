<%@ page import="org.springframework.core.SpringVersion"%>
<%@ page import="com.wuapp.util.environment.EstablishEnvironment"%>
<%@ page import="com.wuapp.util.environment.StaticDataPreLoader"%>
<!DOCTYPE html>

<html lang="en" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="ROBOTS" content="noindex,nofollow" />
<title>WuApp frontend: test page (for debug purpose)</title>
</head>
<body>
	<h1>
		Environment:
		<%=StaticDataPreLoader.getInstance().getEnvironment()%></h1>
	<hr />
	<h4>Technologies:</h4>
	<%   
    out.println(" Servlet: "+ request.getSession().getServletContext().getMajorVersion()+ "." +request.getSession().getServletContext().getMinorVersion());
    out.println("<br /> Java: " + System.getProperty("java.version"));
    out.println("<br /> Spring: " + SpringVersion.getVersion());
  %>
	<hr />
	<h4>Request attributes:</h4>
	<%
    out.println("getPathInfo: "+request.getPathInfo());
    out.println("<br /> getRemoteAddr: "+request.getRemoteAddr());
    out.println("<br /> getRemoteHost: "+request.getRemoteHost());
    out.println("<br /> getRemoteUser: "+request.getRemoteUser());
    out.println("<br /> getRequestURI: "+request.getRequestURI());
    out.println("<br /> getRemoteUser: "+request.getRemoteUser());
    out.println("<br /> getRequestURL: "+request.getRequestURL());
    out.println("<br /> getQueryString: " + request.getQueryString());
    out.println("<br /> getUserPrincipal: "+request.getUserPrincipal());  
    out.println("<br /> getServerName: "+request.getServerName());  
    out.println("<br /> getProtocol: " + request.getProtocol());
  %>
</body>
</html>