<!DOCTtruePE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//Efalse" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<style type="text/css">
.debugger {
	width: 70%;
	font-size: 13px;
	margin-bottom: 20px;
}

.debugger td {
	padding: 3px;
}
</style>

<br />
<br />
<div id="wrp">
	<div id="con">
		<form:form
			action="/floodlight/developer/debug/view-modify-debugger-flags.html"
			modelAttribute="debugBean" acceptCharset="UTF-8">

			<table class="debugger">
				<tbody>
					<tr>
						<td colspan="2"><strong>Flags related to the entire
								page build:</strong></td>
					</tr>
					<tr>
						<td>Debug page time?</td>
						<td width="150">Yes: <form:radiobutton path="debugPageTime"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="debugPageTime" value="false" /></td>
					</tr>
					<tr>
						<td>Print page time details in JSP?</td>
						<td>Yes: <form:radiobutton path="printPageTimeInJsp"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="printPageTimeInJsp" value="false" /></td>
					</tr>
					<tr>
						<td>Print page time details in appserver log?</td>
						<td>Yes: <form:radiobutton path="printPageTimeInAppServerLog"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="printPageTimeInAppServerLog" value="false" /></td>
					</tr>
				</tbody>
			</table>

			<table class="debugger">
				<tbody>
					<tr>
						<td colspan="2"><strong>Flags related to the DB
								parameters/execution time:</strong></td>
					</tr>
					<tr>
						<td>Debug DB details?</td>
						<td width="150">Yes: <form:radiobutton path="debugDbDetails"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="debugDbDetails" value="false" /></td>
					</tr>
					<tr>
						<td>Print DB details in appserver?</td>
						<td>Yes: <form:radiobutton
								path="printDbDetailsInAppServerLog" value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No:
							<form:radiobutton path="printDbDetailsInAppServerLog"
								value="false" /></td>
					</tr>
				</tbody>
			</table>


			<table class="debugger">
				<tbody>
					<tr>
						<td colspan="2"><strong>Flags related to the Request
								Object:</strong></td>
					</tr>
					<tr>
						<td>Debug Request details?</td>
						<td width="150">Yes: <form:radiobutton path="debugRequest"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="debugRequest" value="false" /></td>
					</tr>
					<tr>
						<td>Print Request details in appserver?</td>
						<td>Yes: <form:radiobutton
								path="printRequestDetailsInAppServerLog" value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No:
							<form:radiobutton path="printRequestDetailsInAppServerLog"
								value="false" /></td>
					</tr>
				</tbody>
			</table>

			<table class="debugger">
				<tbody>
					<tr>
						<td colspan="2"><strong>Flags related to the URL
								structure:</strong></td>
					</tr>
					<tr>
						<td>Debug URL structure?</td>
						<td width="150">Yes: <form:radiobutton path="debugUrl"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="debugUrl" value="false" /></td>
					</tr>
					<tr>
						<td>Print URL structure in appserver log?</td>
						<td>Yes: <form:radiobutton path="printUrlInAppServerLog"
								value="true" />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;No: <form:radiobutton
								path="printUrlInAppServerLog" value="false" /></td>
					</tr>
				</tbody>
			</table>


			<table class="debugger">
				<tbody>
					<tr>
						<td colspan="2"><strong>Flags related to General
								purpose SOP:</strong></td>
					</tr>
				</tbody>
			</table>


			<table class="debugger">
				<tbody>
					<tr>
						<td>Update flags:</td>
						<td width="150"><input type="submit" class="blu_btn btn"
							value="Update flags" /></td>
					</tr>
				</tbody>
			</table>

			<input type="hidden" name="FORM_SUBMIT" value="TRUE" />
		</form:form>
	</div>
</div>
