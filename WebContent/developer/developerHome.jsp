<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="com.wuapp.util.environment.EstablishEnvironment"%>
<%@ page import="com.wuapp.util.environment.StaticDataPreLoader"%>


<%  
  EstablishEnvironment env = EstablishEnvironment.getInstance();
  String contextPath = env.getContextPath();
  String portNo = env.getPortNumber();
  String domain = env.getMainDomain();
  String environment = StaticDataPreLoader.getInstance().getEnvironment();
  String domainWithContextPath = domain;
  if("DEVELOPMENT_ECLIPSE".equals(environment)) {
     domainWithContextPath += portNo + contextPath;
  } else {
     domainWithContextPath += contextPath;
  }
  String pageName = (String)request.getAttribute("DEVELOPER_PAGE");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="ROBOTS" content="noindex,nofollow" />
<title>WuApp-API...</title>
</head>
<body>
	<br />
	<br />
	<div>
		<div>

			<div>
				<ul id="ModifyDebuggerFlags">
					<li><h2 style="color: red"><%=environment%></h2></li>
					<%--
					<li id="div1Li"><a href="<%=domainWithContextPath%>/developer/home.html" target="_self">Developer home</a></li>
					<li id="div2Li"><a href="<%=domainWithContextPath%>/developer/debug/view-modify-debugger-flags.html" target="_self">Modify debugger flags</a></li>
					 --%>
				</ul>
			</div>

			<%if("HOME".equals(pageName)) {%>
			<div id="div1" class="bholder">
				<h2 class="head">Developer home</h2>
				<div class="hr">
					This developer page can be seen only by us. We can do below options
					through this developer page.
					<ul>
						<li>Developer. Now you are the developer and you are here.</li>
						<li>Enable or disbale debugger flags</li>
					</ul>
				</div>
			</div>
			<%}%>

			<%if("MODIFY_DEBUG_FLAGS".equals(pageName)) {%>
			<div id="div2" class="bholder">
				<h2 class="head">Modify debugger flags</h2>
				<div class="hr">
					<jsp:include page="/developer/modifyDebuggerFlags.jsp" />
				</div>
			</div>
			<%}%>

		</div>
	</div>

</body>
</html>