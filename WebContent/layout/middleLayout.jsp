<%--
 *
 *
 * LAYOUT to difine single-middle-layout like below (middle will ocupy full width, no left/right columns)
 *
 *  -----------------------
 *  | Header              |
 *  -----------------------
 *  |     Middile         |
 *  |                     |
 *  |                     |
 *  |                     |
 *  -----------------------
 *  | Footer              |
 *  -----------------------
 *
 * @author   Mohamed Syed
 * @since    eduk101_20130724 - intial draft
 * @version  1.1
 *
--%>

<!DOCTYPE html>
<html lang="en">

  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%-- oracle.jsp.util.PublicUtil.setWriterEncoding(out, "UTF-8"); --%>
  <% request.setCharacterEncoding("UTF-8"); %>
  <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

  <head>
       
    <meta charset="UTF-8">    
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
    
    <tiles:insertAttribute name="seoMetaDetails"/>
    <tiles:insertAttribute name="includeCSS"/>
    <tiles:insertAttribute name="includeJS"/>
    <tiles:insertAttribute name="GACode"/>
    <tiles:insertAttribute name="miscellaneousTracking1"/>
  </head>

  <body id="body">
	
	
    <div id="track2" class="dpn" > <!-- div 0 -->
      <tiles:insertAttribute name="miscellaneousTracking2"/>
    </div>
	
	
	<div id="header">
		
		<!--HEADER STARTS-->
	    <header class="hdr"> <!-- div 1 -->
	      <div class="hdwrp">
	      	<tiles:insertAttribute name="header-branding"/>
	      	<tiles:insertAttribute name="header-navigation"/>
	      	<div class="fltr-ovly"></div> 	
	      </div>
	    </header>
	    <!--HEADER ENDS-->
	    
	    <!--SEARCH BOX STARTS-->
	    <div class="srbocnt">
	    	<tiles:insertAttribute name="header-search"/>
	    	<tiles:insertAttribute name="header-suggestion"/>
	    </div>
		<!--SEARCH BOX ENDS-->
	
	</div>
	
	<div id="bodyContent">
		<tiles:insertAttribute name="middle"/>
	</div>
	
	<div id="footer">
		<!--FOOTER STARTS-->
		<footer class="ftrwrp">
			<tiles:insertAttribute name="footer-content"/>
			<tiles:insertAttribute name="footer-legal"/>
		</footer>
		<!--FOOTER ENDS-->
	</div>
	
	<div id="track3" class="dpn" > <!-- div 0 -->
		<tiles:insertAttribute name="miscellaneousTracking3"/>
	</div>
	

  </body>

</html>


`