<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.DataSource"%>
<%@ page import="oracle.jdbc.OracleTypes"%>
<%@ page import="com.wuapp.config.SpringContextInjector"%>
<%
Connection connection = null;
CallableStatement cstmt = null;
DataSource datasource = null;
try{
  datasource = (DataSource) SpringContextInjector.getBeanFromSpringContext("DataSource");
  if(datasource != null){
    connection = datasource.getConnection();
    if(connection != null){
      cstmt = connection.prepareCall("{call hot_monitor.test_ssl(?)}");
      if(cstmt != null){
        //
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
        cstmt.execute();
        //
        String result = (String)cstmt.getObject(1);
        if(result != null && result.trim().length() > 0) {
          out.print("<h3> " + result + "</h3>");
        }else {
          out.print("<h3> DB returns NULL</h3>");
          response.setStatus(500); 
        }
      }
    }
  }
}catch(Exception e){
  response.setStatus(500); 
  out.print("<h3> Got below Exception: </h3>");
  out.print("<p> " + e + "</p>");  
}finally{
  if(connection != null){
    connection.close();
  }
  datasource = null;
  if(cstmt != null){
     cstmt.close();
   }
  cstmt = null;
}
%>