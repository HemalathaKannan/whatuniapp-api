<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.wuapp.util.environment.EmbeddedObject" %>
<%@ page import="com.wuapp.util.environment.EstablishEnvironment"%>
<%@ page import="com.wuapp.util.environment.StaticDataPreLoader"%>

<%
	EmbeddedObject embeddedObject = EmbeddedObject.getInstance();
%>

<section class="dummy_home">  
	<%--
	<c:if test="${not empty HOME_PAGE}">
	   <div style="width:300px; margin:200px 340px; float:left;">
	   		<h3>Welcome to WuApp-API</h3>
	   		<p>Home: <spring:message code="text.home" /></p>
	   		<p>Country: <spring:message code="text.country" /></p>
	   		<p>Language: <spring:message code="text.lang" /></p>
	   </div>
	</c:if> --%>
	<div><h2 style="font-weight:bold;">ENVIRONMENT:</h2> <h2 style="color: red"><%=StaticDataPreLoader.getInstance().getEnvironment()%></h2></div>
	<img class="dummy_img" src="<%=embeddedObject.getImgPath(request, 0, "/misc/underconstruction.jpg")%>" alt="under construction">
</section>