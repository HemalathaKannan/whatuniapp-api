<%@ page import="com.wuapp.util.environment.EmbeddedObject" %>

<%
	EmbeddedObject embeddedObject = EmbeddedObject.getInstance();
%>

<!--NAV STARTS-->
 <nav class="navwr">
     <ul class="nmnu">
         <li class="mlist"><a class="manc" href="#">Subjects</a></li>
         <li class="mlist"><a class="manc" href="#">Qualifications</a></li>
         <li class="mlist"><a class="manc" href="#">Advice</a></li>
         <li id="search_call" class="mlist srbt"><a class="manc" href="#"><span class="srt fl">Search</span><img class="fr sricg" src="<%=embeddedObject.getImgPath(request, 0, "/icons/search_icon_grey.svg")%>" width="18" height="18" alt="search icon"/></a></li>
         <li class="mlist sgnup"><a class="manc" href="#">Sign Up</a></li>
         <li class="mlist"><a class="manc" href="#">Log In</a></li>
     </ul>
     <a id="cls_msrch" class="clswr mcls">
         <img class="mcls" src="<%=embeddedObject.getImgPath(request, 0, "/icons/x_white.svg")%>" width="14" height="14" alt="close icon" />
     </a>
 </nav>
 <!--tab view overlay below-->
 <div class="fltr-ovly"></div>
 <!--tab view overlay above-->
 <!--NAV ENDS-->