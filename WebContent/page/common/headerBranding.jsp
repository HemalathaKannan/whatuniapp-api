<%@ page import="com.wuapp.util.environment.EmbeddedObject" %>

<%
	EmbeddedObject embeddedObject = EmbeddedObject.getInstance();
%>

<!--MOBILE HAMBURGER BELOW-->
<div class="mham fl">
    <a href="#" id="mob_menu">
        <span class="hline"></span>
        <span class="hline"></span>
        <span class="hline mbz"></span>
    </a>
</div>
<!--MOBILE HAMBURGER ABOVE-->
<!--LOGO STARTS-->
<div class="lgwr">
    <a href="#">
        <img class="fllg" src="<%=embeddedObject.getImgPath(request, 0, "/logo/floodlight_logo.svg")%>" width="136" height="31" alt="Floodlight logo" />
    </a>
</div>
<!--LOGO ENDS-->
<!--MOBILE SEARCH ICON BELOW-->
<a class="mosrch" href="#"><img class="fr" src="<%=embeddedObject.getImgPath(request, 0, "/icons/search_icon.svg")%>" width="18" height="18" alt="search icon" /></a>
<!--MOBILE SEARCH ICON ABOVE-->