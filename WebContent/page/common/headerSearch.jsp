<%@ page import="com.wuapp.util.environment.EmbeddedObject" %>

<%
	EmbeddedObject embeddedObject = EmbeddedObject.getInstance();
%>


<!--SEARCH AREA WRAPPER BELOW-->
 <div class="bxwrp">
     <div class="mainsrch">
         <form method="post" class="clr">
             <fieldset class="srchfld">
                 <img class="srcic" src="<%=embeddedObject.getImgPath(request, 0, "/icons/search_icon_grey.svg")%>" width="18" height="18" alt="search icon" />
                 <input class="subsrch" type="text" name="Course search" placeholder="What do you want to learn?" />
             </fieldset>
             <fieldset class="srchfld2">
                 <input class="locsrch" type="text" name="location search" placeholder="Postcode / location" />
             </fieldset>
             <!--search icon for devices below-->
             <button class="chsum" type="button">Search</button>
             <!--search icon for devices above-->
         </form>

     </div>
     <a id="cls_srch" class="clswr">
         <img class="clsicn" src="<%=embeddedObject.getImgPath(request, 0, "/icons/x.svg")%>" width="14" height="14" alt="close icon" />
     </a>
 </div>
 <!--SEARCH AREA WRAPPER ABOVE-->