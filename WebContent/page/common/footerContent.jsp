<% Long startExecuteTime1 = new Long(System.currentTimeMillis()); %>
<% long startExecuteTime2 = System.currentTimeMillis(); %>

<div class="ftsec">

	<div class="ftrcnt clr">
		<div class="sgnara fl">
			<div class="nwsup">Sign up to our newsletter</div>
			<div class="nwstxt">Receive a regular newsletter packed with useful tips and updates</div>
			<form method="post" class="clr nwsfrm">
				<fieldset class="nwswrp">
					<input class="fl nwsip" type="email" name="Newsletter sign up" placeholder="Email address" />
					<button type="submit" class="fl nwsbtn sgnup">Sign Up</button>
				</fieldset>
			</form>
		</div>
		<div class="socmd fl">
			<ul class="smedia">
				<li>Follow us</li>
				<li>
					<a href="#" class="twt"></a>
				</li>
				<li>
					<a href="#" class="fbk"></a>
				</li>
				<li>
					<a href="#" class="insg"></a>
				</li>
			</ul>
		</div>
		<div class="stclnk fl">
			<ul>
				<li class="static">Site links</li>
                		<li><a href="#">About us</a></li>
                        	<li><a href="#">Cookies</a></li>
                        	<li><a href="#">Privacy policy</a></li>
				<li><a href="#" title="Terms and conditions">Terms and conditions</a></li>
			</ul>
		</div>
	</div>
	
</div>

<div style="display:none;" id="footerContent_jsp">
	Time taken 1 = <%=(new Long((new Long(System.currentTimeMillis())) - startExecuteTime1))%> ms
	Time taken 2 = <%=System.currentTimeMillis() - startExecuteTime2%> ms
</div>	
