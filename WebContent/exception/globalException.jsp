<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.io.StringWriter,java.io.PrintWriter"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- response.setStatus(500); --%>
<%-- response.sendError(500, "HE/FE course: (1) may be expired/deleted | (2) courseId may be invalid"); --%>

<c:set value="${exception}" var="exp"/>

<%
	Exception exp=(Exception)pageContext.getAttribute("exp"); 
	exp.printStackTrace();
	StringWriter sw = new StringWriter();
	exp.printStackTrace(new PrintWriter(sw));
	String errorMessage    = sw !=null ? sw.toString() : "";
 %>
     
<h3>Global Exception</h3> 
<div style="margin: 8% 0 0 25%;" id="errorDetails">
  <p><h2><font color="red">Sorry! Error has occured. Something went wrong</font></h2></p>
  <p style="display:block;"><font color="red">Reason:</font> <%=errorMessage %></p>
</div>
   
   