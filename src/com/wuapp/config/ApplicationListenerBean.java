package com.wuapp.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

@SuppressWarnings("rawtypes")
public class ApplicationListenerBean implements ApplicationListener {

   private static ApplicationContext context;

   @Override
   public void onApplicationEvent(ApplicationEvent event) {
	if (event instanceof ContextRefreshedEvent) {
	   context = ((ContextRefreshedEvent) event).getApplicationContext();
	}
   }

   public static ApplicationContext getContext() {
	return context;
   }
}
