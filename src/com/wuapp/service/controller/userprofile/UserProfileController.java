package com.wuapp.service.controller.userprofile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IUserProfileBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.userprofile.CountryListVO;
import com.wuapp.service.dao.util.valueobject.userprofile.PreviousQualVO;
import com.wuapp.service.dao.util.valueobject.userprofile.PrivacyCAFlagVO;
import com.wuapp.service.dao.util.valueobject.userprofile.QualificationVO;
import com.wuapp.service.dao.util.valueobject.userprofile.SchoolInfoVO;
import com.wuapp.service.dao.util.valueobject.userprofile.UserClientConsentVO;
import com.wuapp.service.dao.util.valueobject.userprofile.UserFlagVO;
import com.wuapp.service.dao.util.valueobject.userprofile.UserInfoVO;
import com.wuapp.service.dao.util.valueobject.userprofile.YearOfEntryVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * USER PROFILE PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Class : UserProfileController.java 
 * Change Log :
 * ***************************************************************************************************************************
 * Author          version      Modified On     Modification Details                        Change
 * ***************************************************************************************************************************
 * Hemalatha.K       1.2        17_DEC_19           Modified                      Getting client consent details from db
 * Hemalatha.K       2.0        25_JUN_2020                                       Removed clearing related changes
 */

@Controller
public class UserProfileController {

   @Autowired
   IUserProfileBusiness userProfileBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/user-profile/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getUserProfile(@RequestBody UserProfilePayloadFromClient userProfilePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	UserProfilePayloadToClient userProfilePayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	ArrayList<UserInfoVO> userInfoList;
	ArrayList<CountryListVO> countriesList;
	ArrayList<YearOfEntryVO> yearOfEntryList;
	ArrayList<PreviousQualVO> previousQualList;
	ArrayList<QualificationVO> qualificationList;
	ArrayList<AppVersionVO> versionDetails;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userProfilePayloadFromClient.getAffiliateId(), userProfilePayloadFromClient.getAccessToken())) {
		resultMap = userProfileBusiness.getUserProfile(userProfilePayloadFromClient);
		userInfoList = (ArrayList<UserInfoVO>) resultMap.get("PC_USER_INFO");
		countriesList = (ArrayList<CountryListVO>) resultMap.get("PC_COUNTRY_LIST");
		yearOfEntryList = (ArrayList<YearOfEntryVO>) resultMap.get("PC_YOE_LIST");
		previousQualList = (ArrayList<PreviousQualVO>) resultMap.get("PC_PRE_QUAL");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		qualificationList = (ArrayList<QualificationVO>) resultMap.get("PC_QUALIFICATION");
		//
		userProfilePayloadToClient = new UserProfilePayloadToClient();
		userProfilePayloadToClient.setVersionDetails(versionDetails);
		userProfilePayloadToClient.setUserInfoList(userInfoList);
		userProfilePayloadToClient.setCountriesList(countriesList);
		userProfilePayloadToClient.setYearOfEntryList(yearOfEntryList);
		userProfilePayloadToClient.setPreviousQualList(previousQualList);
		userProfilePayloadToClient.setQualificationList(qualificationList);
		//
		String guestUserFlag = common.guestFlag(userProfilePayloadFromClient.getUserId());
		userProfilePayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(userProfilePayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   userProfilePayloadFromClient = null;
	   userProfilePayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/save-user-profile/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> saveUserProfileInfo(@RequestBody UserProfilePayloadFromClient userProfilePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	UserProfilePayloadToClient userProfilePayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	String successMsg = null;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userProfilePayloadFromClient.getAffiliateId(), userProfilePayloadFromClient.getAccessToken())) {
		resultMap = userProfileBusiness.saveUserProfileInfo(userProfilePayloadFromClient);
		successMsg = (String) resultMap.get("p_success_msg");
		//
		userProfilePayloadToClient = new UserProfilePayloadToClient();
		userProfilePayloadToClient.setSuccessMsg(successMsg);
		//
		String guestUserFlag = common.guestFlag(userProfilePayloadFromClient.getUserId());
		userProfilePayloadToClient.setGuestUserFlag(guestUserFlag);
		return new ResponseEntity<GenericReturnType>(userProfilePayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/get-user-preference/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getUserPreferenceDetails(@RequestBody UserProfilePayloadFromClient userProfilePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	UserProfilePayloadToClient userProfilePayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<UserFlagVO> userInfoList;
	ArrayList<UserFlagVO> userFlagList;
	ArrayList<PrivacyCAFlagVO> privacyCaFlagList;
	//
	ArrayList<UserClientConsentVO> userClientConsentList;
	CommonUtilities common = new CommonUtilities();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userProfilePayloadFromClient.getAffiliateId(), userProfilePayloadFromClient.getAccessToken())) {
		resultMap = userProfileBusiness.getUserPreference(userProfilePayloadFromClient);
		userInfoList = (ArrayList<UserFlagVO>) resultMap.get("PC_USER_INFO");
		if (userInfoList != null && userInfoList.size() > 0) {
		   for (int i = 0; i < userInfoList.size(); i++) {
			UserFlagVO userFlagVO = (UserFlagVO) userInfoList.get(i);
			if (!common.isBlankOrNull(userFlagVO.getFacebookId())) {
			   userFlagVO.setUserImage(common.getFacebookImagePath(userFlagVO.getFacebookId()));
			} else if (!common.isBlankOrNull(userFlagVO.getUserImage())) {
			   userFlagVO.setUserImage(EmbeddedObject.getInstance().getUploadedImagePath(request, userFlagVO.getUserImage()));
			}
		   }
		}
		//
		userFlagList = (ArrayList<UserFlagVO>) resultMap.get("PC_USER_FLAG");
		privacyCaFlagList = (ArrayList<PrivacyCAFlagVO>) resultMap.get("PC_PRIVACY_CA_FLAG");
		//
		userClientConsentList = (ArrayList<UserClientConsentVO>) resultMap.get("PC_USER_CLIENT_CONSENT_LIST"); // Added for showing client consent details in preference page by Hemalatha.K on 17_DEC_19_REL
		//
		userProfilePayloadToClient = new UserProfilePayloadToClient();
		userProfilePayloadToClient.setUserFlagList(userFlagList);
		userProfilePayloadToClient.setEditUserInfoList(userInfoList);
		userProfilePayloadToClient.setPrivacyCaFlagList(privacyCaFlagList);
		//
		userProfilePayloadToClient.setUserClientConsentList(userClientConsentList); // Added for showing client consent details in preference page by Hemalatha.K on 17_DEC_19_REL
		//
		String guestUserFlag = common.guestFlag(userProfilePayloadFromClient.getUserId());
		userProfilePayloadToClient.setGuestUserFlag(guestUserFlag);
		return new ResponseEntity<GenericReturnType>(userProfilePayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/save-user-preference/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> saveUserPreferenceDetails(@RequestBody UserProfilePayloadFromClient userProfilePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	UserProfilePayloadToClient userProfilePayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	String successMsg = null;
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userProfilePayloadFromClient.getAffiliateId(), userProfilePayloadFromClient.getAccessToken())) {
		resultMap = userProfileBusiness.saveUserPreference(userProfilePayloadFromClient);
		successMsg = (String) resultMap.get("p_success_msg");
		//
		userProfilePayloadToClient = new UserProfilePayloadToClient();
		userProfilePayloadToClient.setSuccessMsg(successMsg);
		//
		String guestUserFlag = common.guestFlag(userProfilePayloadFromClient.getUserId());
		userProfilePayloadToClient.setGuestUserFlag(guestUserFlag);
		return new ResponseEntity<GenericReturnType>(userProfilePayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/school-list-ajax/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getSchoolList(@RequestBody UserProfilePayloadFromClient userProfilePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	UserProfilePayloadToClient userProfilePayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<SchoolInfoVO> schoolList;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userProfilePayloadFromClient.getAffiliateId(), userProfilePayloadFromClient.getAccessToken())) {
		resultMap = userProfileBusiness.getSchoolList(userProfilePayloadFromClient);
		schoolList = (ArrayList<SchoolInfoVO>) resultMap.get("pc_school_list");
		//
		userProfilePayloadToClient = new UserProfilePayloadToClient();
		userProfilePayloadToClient.setSchoolList(schoolList);
		return new ResponseEntity<GenericReturnType>(userProfilePayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/send-email/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> sendEmailToWhatuni(@RequestBody UserProfilePayloadFromClient userProfilePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	UserProfilePayloadToClient userProfilePayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<AppVersionVO> versionDetails;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userProfilePayloadFromClient.getAffiliateId(), userProfilePayloadFromClient.getAccessToken())) {
		resultMap = userProfileBusiness.sendEmailToWhatuni(userProfilePayloadFromClient);
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		//
		userProfilePayloadToClient = new UserProfilePayloadToClient();
		userProfilePayloadToClient.setVersionDetails(versionDetails);
		return new ResponseEntity<GenericReturnType>(userProfilePayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   //
   public IUserProfileBusiness getUserProfileBusiness() {
	return userProfileBusiness;
   }

   public void setUserProfileBusiness(IUserProfileBusiness userProfileBusiness) {
	this.userProfileBusiness = userProfileBusiness;
   }
   //  
}
