package com.wuapp.service.controller.developer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Forward to developer home view
 *
 * @author Sabapathi
 * @since wuapp1.0_20170328 - initial draft
 * @version 1.1
 */
@Controller
public class DeveloperHomeController {

   @RequestMapping(value = { "/developer/home" })
   public String developerHomeController(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
	//
	request.setAttribute("DEVELOPER_PAGE", "HOME");
	//
	/*
	 * response.sendError(410, "410");
	 * String nullCheck = null;
	 * nullCheck.trim();
	 */
	//new CommonUtilities().checkThrowsException();
	//new CommonUtilities().checkNotThrownException();
	return "developer-home-tile";
   }
}
