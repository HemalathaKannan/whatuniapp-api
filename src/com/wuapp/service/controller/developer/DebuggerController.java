package com.wuapp.service.controller.developer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.wuapp.service.pojo.developer.DebugForm;
import com.wuapp.util.developer.Debugger;

@Controller("debuggerController")
public class DebuggerController {

   @RequestMapping(value = "/developer/debug/view-modify-debugger-flags")
   public ModelAndView debuggerController(@ModelAttribute("debugBean") DebugForm form, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
	//
	DebugForm debugForm = (DebugForm) form;
	Debugger flag = Debugger.getInstance();
	//
	if (request.getParameter("FORM_SUBMIT") != null && request.getParameter("FORM_SUBMIT").equals("TRUE")) {
	   flag.setDebugPageTime(Boolean.parseBoolean(debugForm.getDebugPageTime()));
	   //
	   flag.setDebugDbDetails(Boolean.parseBoolean(debugForm.getDebugDbDetails()));
	   //
	   flag.setDebugRequest(Boolean.parseBoolean(debugForm.getDebugRequest()));
	   //
	   flag.setDebugUrl(Boolean.parseBoolean(debugForm.getDebugUrl()));
	   //
	} else {
	   debugForm.setDebugPageTime(Boolean.toString(flag.isDebugPageTime()));
	   //
	   debugForm.setDebugDbDetails(Boolean.toString(flag.isDebugDbDetails()));
	   //
	   debugForm.setDebugRequest(Boolean.toString(flag.isDebugRequest()));
	   //
	   debugForm.setDebugUrl(Boolean.toString(flag.isDebugUrl()));
	   //
	}
	//
	request.setAttribute("DEVELOPER_PAGE", "MODIFY_DEBUG_FLAGS");
	return new ModelAndView("developer-home-tile", "debugForm", debugForm);
	//
   }
}
