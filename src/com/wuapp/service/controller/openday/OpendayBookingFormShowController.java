package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.openday.ClientConsentVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendaysDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.ProviderDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.QualificationDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.StudyModeVO;
import com.wuapp.service.dao.util.valueobject.openday.UserDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.YearOfEntryVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendayShowBookingFormFromClient;
import com.wuapp.service.pojo.json.openday.OpendayShowBookingFormToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY SHOW BOOKING FORM 
 *
 * @author Hema.S
 * @since 06_MAY_2020
 * @version 1.0
 * Class : OpendayBookingFormShowController.java
 * Description : Controller for showing the booking form.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hema S            1.0                                              Initial draft
 */

@Controller
public class OpendayBookingFormShowController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;

   @RequestMapping(value = "/open-day/show-booking-form/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getBookingFormSubmitDetails(@RequestBody OpendayShowBookingFormFromClient opendayShowBookingFormFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendayShowBookingFormToClient opendayShowBookingFormToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities commonUtil = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendayShowBookingFormFromClient.getAffiliateId(), opendayShowBookingFormFromClient.getAccessToken())) {
		//
		resultMap = opendayBusiness.getShowBookingFormList(opendayShowBookingFormFromClient);
		ArrayList<ProviderDetailsVO> providerDetails = (ArrayList<ProviderDetailsVO>) resultMap.get("PC_PROVIDER_DETAILS");
		if (!commonUtil.isBlankOrNull(providerDetails)) {
		   ProviderDetailsVO providerDetailsVO = (ProviderDetailsVO) providerDetails.get(0);
		   if (!commonUtil.isBlankOrNull(providerDetailsVO.getCollegeLogo())) {
			providerDetailsVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, providerDetailsVO.getCollegeLogo()));
			String[] logo = providerDetailsVO.getCollegeLogo().split("/");
			if (commonUtil.checkArrayLength(logo)) {
			   providerDetailsVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + providerDetailsVO.getLogoName());
			   providerDetailsVO.setLogoUrl(logoURL);
			}
		   }
		}
		ArrayList<OpendaysDetailsVO> openDaysDetails = (ArrayList<OpendaysDetailsVO>) resultMap.get("PC_OPEN_DAY_DETAILS");
		ArrayList<UserDetailsVO> userDetails = (ArrayList<UserDetailsVO>) resultMap.get("PC_USER_DETAILS");
		ArrayList<QualificationDetailsVO> qualificationDetails = (ArrayList<QualificationDetailsVO>) resultMap.get("PC_QUALIFICATION_DETAILS");
		ArrayList<StudyModeVO> studyModeDetails = (ArrayList<StudyModeVO>) resultMap.get("PC_STUDY_MODE_DETAILS");
		ArrayList<ClientConsentVO> clientConsentDetails = (ArrayList<ClientConsentVO>) resultMap.get("PC_CLIENT_CONSENT_DETAILS");
		ArrayList<YearOfEntryVO> yearOfEntryDetails = (ArrayList<YearOfEntryVO>) resultMap.get("PC_YOE_LIST");
		//
		opendayShowBookingFormToClient = new OpendayShowBookingFormToClient();
		opendayShowBookingFormToClient.setProviderDetails(providerDetails);
		opendayShowBookingFormToClient.setOpenDaysDetails(openDaysDetails);
		opendayShowBookingFormToClient.setUserDetails(userDetails);
		opendayShowBookingFormToClient.setQualificationDetails(qualificationDetails);
		opendayShowBookingFormToClient.setStudyModeDetails(studyModeDetails);
		opendayShowBookingFormToClient.setClientConsentDetails(clientConsentDetails);
		opendayShowBookingFormToClient.setOdMaxPeopleCount((String) resultMap.get("P_OD_MAX_PEOPLE_COUNT"));
		opendayShowBookingFormToClient.setYearOfEntryDetails(yearOfEntryDetails);
		opendayShowBookingFormToClient.setUserBooked((String) resultMap.get("P_USER_BOOKED"));
		//
		return new ResponseEntity<GenericReturnType>(opendayShowBookingFormToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendayShowBookingFormFromClient = null;
	   opendayShowBookingFormToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
