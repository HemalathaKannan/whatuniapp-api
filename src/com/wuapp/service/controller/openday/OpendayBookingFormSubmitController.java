package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.openday.EventDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.OtherOdPodVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendayBookingFormSubmitFromClient;
import com.wuapp.service.pojo.json.openday.OpendayBookingFormSubmitToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY BOOKING FORM SUBMIT
 *
 * @author Hema.S
 * @since 06_MAY_2020
 * @version 1.0
 * Class : OpendayBookingFormSubmitController.java
 * Description : Controller for submit booking form.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hema S        1.0                                              Initial draft
 */

@Controller
public class OpendayBookingFormSubmitController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;

   @RequestMapping(value = "/open-day/booking-form-submit/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getBookingFormSubmitDetails(@RequestBody OpendayBookingFormSubmitFromClient opendayBookingFormSubmitFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendayBookingFormSubmitToClient opendayBookingFormSubmitToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities commonUtil = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendayBookingFormSubmitFromClient.getAffiliateId(), opendayBookingFormSubmitFromClient.getAccessToken())) {
		//
		opendayBookingFormSubmitFromClient.setUserIp(commonUtil.getUserIp(request));
		opendayBookingFormSubmitFromClient.setUserAgent(GenericValidator.isBlankOrNull(opendayBookingFormSubmitFromClient.getUserAgent()) ? commonUtil.getUserAgent(request) : opendayBookingFormSubmitFromClient.getUserAgent());
		opendayBookingFormSubmitFromClient.setLatitude(commonUtil.round(opendayBookingFormSubmitFromClient.getLatitude(), 2));
		opendayBookingFormSubmitFromClient.setLongitude(commonUtil.round(opendayBookingFormSubmitFromClient.getLongitude(), 2));
		//
		resultMap = opendayBusiness.getBookingFormSubmitList(opendayBookingFormSubmitFromClient);
		opendayBookingFormSubmitToClient = new OpendayBookingFormSubmitToClient();
		opendayBookingFormSubmitToClient.setSessionId((String) resultMap.get("P_SESSION_ID"));
		opendayBookingFormSubmitToClient.setUserId((String) resultMap.get("P_USER_ID"));
		opendayBookingFormSubmitToClient.setUserSent((String) resultMap.get("O_USER_SENT"));
		ArrayList<EventDetailsVO> eventDetails = (ArrayList<EventDetailsVO>) resultMap.get("PC_EVENT_DETAILS");
		ArrayList<OtherOdPodVO> otherUniOpendays = (ArrayList<OtherOdPodVO>) resultMap.get("PC_OTHER_OD_POD");
		if (!commonUtil.isBlankOrNull(otherUniOpendays)) {
		   for (int i = 0; i < otherUniOpendays.size(); i++) {
			OtherOdPodVO otherOdPodVO = (OtherOdPodVO) otherUniOpendays.get(i);
			if (!commonUtil.isBlankOrNull(otherOdPodVO.getLogoPath())) {
			   otherOdPodVO.setLogoPath(EmbeddedObject.getInstance().getUploadedImagePath(request, otherOdPodVO.getLogoPath()));
			}
			if (!commonUtil.isBlankOrNull(otherOdPodVO.getLogoPath())) {
			   String[] logo = otherOdPodVO.getLogoPath().split("/");
			   if (commonUtil.checkArrayLength(logo)) {
				otherOdPodVO.setLogoName(logo[logo.length - 1]);
				String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + otherOdPodVO.getLogoName());
				otherOdPodVO.setLogoUrl(logoURL);
			   }
			}
		   }
		}
		opendayBookingFormSubmitToClient.setEventDetails(eventDetails);
		opendayBookingFormSubmitToClient.setOtherUniOpendays(otherUniOpendays);
		opendayBookingFormSubmitToClient.setFbOdSuccessMsg((String) resultMap.get("P_FB_OD_SUCCESS_MSG"));
		opendayBookingFormSubmitToClient.setTwitterOdSuccessMsg((String) resultMap.get("P_TWITTER_OD_SUCCESS_MSG"));
		opendayBookingFormSubmitToClient.setEmailExistsFlag((String) resultMap.get("P_EMAIL_EXIST_FLAG"));
		//
		return new ResponseEntity<GenericReturnType>(opendayBookingFormSubmitToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendayBookingFormSubmitToClient = null;
	   opendayBookingFormSubmitFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
