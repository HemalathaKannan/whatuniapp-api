package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.openday.SubjectAjaxListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.SubjectAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.openday.SubjectAjaxPayloadToClient;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY SEARCH AJAX PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 * Class : SubjectAjaxController.java
 * Description : Controller for subject ajax.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K        1.0                                              Initial draft
 */
 
@Controller
public class SubjectAjaxController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/subject-ajax-list/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getAjaxSearchList(@RequestBody SubjectAjaxPayloadFromClient subjectAjaxPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	SubjectAjaxPayloadToClient subjectAjaxPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<SubjectAjaxListVO> subjectAjaxList;
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(subjectAjaxPayloadFromClient.getAffiliateId(), subjectAjaxPayloadFromClient.getAccessToken())) {
		resultMap = opendayBusiness.getSubjectAjaxList(subjectAjaxPayloadFromClient);
		subjectAjaxList = (ArrayList<SubjectAjaxListVO>) resultMap.get("PC_SUBJECT_LIST");
		subjectAjaxPayloadToClient = new SubjectAjaxPayloadToClient();
		subjectAjaxPayloadToClient.setSubjectAjaxList(subjectAjaxList);
		//
		return new ResponseEntity<GenericReturnType>(subjectAjaxPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   subjectAjaxPayloadToClient = null;
	   subjectAjaxPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
