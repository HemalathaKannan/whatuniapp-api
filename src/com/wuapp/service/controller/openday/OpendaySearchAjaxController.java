package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchAjaxVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendaySearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchAjaxPayloadToClient;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY SEARCH AJAX PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 * Class : OpendaySearchAjaxController.java
 * Description : Controller for openday search using ajax.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K        1.0                                              Initial draft
 */
 
@Controller
public class OpendaySearchAjaxController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/open-day/search-ajax/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getAjaxSearchList(@RequestBody OpendaySearchAjaxPayloadFromClient opendaySearchAjaxPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendaySearchAjaxPayloadToClient opendaySearchAjaxPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendaySearchAjaxPayloadFromClient.getAffiliateId(), opendaySearchAjaxPayloadFromClient.getAccessToken())) {
		resultMap = opendayBusiness.getOpendaySearchAjaxList(opendaySearchAjaxPayloadFromClient);
		ArrayList<OpendaySearchAjaxVO> opendaySearchAjaxList = (ArrayList<OpendaySearchAjaxVO>) resultMap.get("RETVAL");
		opendaySearchAjaxPayloadToClient = new OpendaySearchAjaxPayloadToClient();
		opendaySearchAjaxPayloadToClient.setOpendaySearchAjaxList(opendaySearchAjaxList);
		//
		return new ResponseEntity<GenericReturnType>(opendaySearchAjaxPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendaySearchAjaxPayloadToClient = null;
	   opendaySearchAjaxPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
