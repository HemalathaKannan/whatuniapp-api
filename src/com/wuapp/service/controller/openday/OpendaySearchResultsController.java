package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.openday.EventListVO;
import com.wuapp.service.dao.util.valueobject.openday.EventTypeListVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendayMonthListVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchLandingVO;
import com.wuapp.service.dao.util.valueobject.openday.RegionListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendaySearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchResultsPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY SEARCH RESULTS PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 * Class : OpendaySearchResultsController.java
 * Description : Controller for getting openday details based on filter applied.
 * Change Log :
 * *************************************************************************************************************
 * Author         Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K        1.0                                              Initial draft
 */
 
@Controller
public class OpendaySearchResultsController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;
   
   @RequestMapping(value = "/open-day/search-results/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getOpendaySearchResults(@RequestBody OpendaySearchResultsPayloadFromClient opendaySearchResultsPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendaySearchResultsPayloadToClient opendaySearchResultsPayloadToClient = null;
	ExceptionJSON exceptionJSON = null;
	Map<String, Object> resultMap = new HashMap<String, Object>();
	Debugger debug = Debugger.getInstance();
	CommonUtilities commonUtil = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendaySearchResultsPayloadFromClient.getAffiliateId(), opendaySearchResultsPayloadFromClient.getAccessToken())) {
		resultMap = opendayBusiness.getOpendaySearchResults(opendaySearchResultsPayloadFromClient);
		String location = (String) resultMap.get("P_LOCATION");
		ArrayList<OpendaySearchLandingVO> viewAllOpendaysList = (ArrayList<OpendaySearchLandingVO>) resultMap.get("PC_OPEN_DAYS_LIST");
		if (!commonUtil.isBlankOrNull(viewAllOpendaysList)) {
		   for (int i = 0; i < viewAllOpendaysList.size(); i++) {
			OpendaySearchLandingVO opendaySearchLandingVO = (OpendaySearchLandingVO) viewAllOpendaysList.get(i);
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getMediaPath())) {
			   opendaySearchLandingVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getMediaPath()));
			}
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getCollegeLogo())) {
			   opendaySearchLandingVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getCollegeLogo()));
			   String[] logo = opendaySearchLandingVO.getCollegeLogo().split("/");
			   if (commonUtil.checkArrayLength(logo)) {
				opendaySearchLandingVO.setLogoName(logo[logo.length - 1]);
				String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + opendaySearchLandingVO.getLogoName());
				opendaySearchLandingVO.setLogoURL(logoURL);
			   }
			}
		   }
		}
		String monthName = (String) resultMap.get("P_MONTH_NAME");
		ArrayList<OpendayMonthListVO> opendayMonthList = (ArrayList<OpendayMonthListVO>) resultMap.get("PC_MONTH_LIST");
		ArrayList<RegionListVO> opendayRegionList = (ArrayList<RegionListVO>) resultMap.get("PC_REGION_LIST");
		String eventTypeName = (String) resultMap.get("P_EVENT_TYPE_NAME");
		String eventCategoryId = (String) resultMap.get("P_EVENT_CATEGORY_ID");
		ArrayList<EventTypeListVO> eventTypeList = (ArrayList<EventTypeListVO>) resultMap.get("PC_EVENT_TYPE_LIST");
		ArrayList<EventListVO> eventList = (ArrayList<EventListVO>) resultMap.get("PC_EVENT_LIST");
		//
		opendaySearchResultsPayloadToClient = new OpendaySearchResultsPayloadToClient();
		opendaySearchResultsPayloadToClient.setLocation(location);
		opendaySearchResultsPayloadToClient.setViewAllOpendaysList(viewAllOpendaysList);
		opendaySearchResultsPayloadToClient.setMonthName(monthName);
		opendaySearchResultsPayloadToClient.setOpendayMonthList(opendayMonthList);
		opendaySearchResultsPayloadToClient.setOpendayRegionList(opendayRegionList);
		opendaySearchResultsPayloadToClient.setEventTypeName(eventTypeName);
		opendaySearchResultsPayloadToClient.setEventCategoryId(eventCategoryId);
		opendaySearchResultsPayloadToClient.setEventTypeList(eventTypeList);
		opendaySearchResultsPayloadToClient.setEventList(eventList);
		//
		return new ResponseEntity<GenericReturnType>(opendaySearchResultsPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendaySearchResultsPayloadFromClient = null;
	   opendaySearchResultsPayloadToClient = null;
	   exceptionJSON = null;
	   debug = null;
	}
   }
}
