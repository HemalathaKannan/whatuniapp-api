package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.openday.EventTypeListVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchLandingVO;
import com.wuapp.service.dao.util.valueobject.openday.RegionListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendaySearchLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchLandingPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY SEARCH LANDING PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 * Class : OpendaySearchLandingController.java
 * Description : Controller for show openday search landing details.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K        1.0                                              Initial draft
 */
 
@Controller
public class OpendaySearchLandingController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;

   @RequestMapping(value = "/open-day/search-landing/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getOpendaySearchLanding(@RequestBody OpendaySearchLandingPayloadFromClient opendaySearchLandingPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendaySearchLandingPayloadToClient opendaySearchLandingPayloadToClient = null;
	ExceptionJSON exceptionJSON = null;
	Map<String, Object> resultMap = new HashMap<String, Object>();
	Debugger debug = Debugger.getInstance();
	CommonUtilities commonUtil = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendaySearchLandingPayloadFromClient.getAffiliateId(), opendaySearchLandingPayloadFromClient.getAccessToken())) {
		opendaySearchLandingPayloadFromClient.setUserIp(commonUtil.getUserIp(request));		
		resultMap = opendayBusiness.getOpendaySearchList(opendaySearchLandingPayloadFromClient);
		ArrayList<OpendaySearchLandingVO> virtualOpendaysList = (ArrayList<OpendaySearchLandingVO>) resultMap.get("PC_VIRTUAL_EVENTS_LIST");
		if (!commonUtil.isBlankOrNull(virtualOpendaysList)) {
		   for (int i = 0; i < virtualOpendaysList.size(); i++) {
			OpendaySearchLandingVO opendaySearchLandingVO = (OpendaySearchLandingVO) virtualOpendaysList.get(i);
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getMediaPath())) {
			   opendaySearchLandingVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getMediaPath()));
			}
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getCollegeLogo())) {
			   opendaySearchLandingVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getCollegeLogo()));
			   String[] logo = opendaySearchLandingVO.getCollegeLogo().split("/");
			   if (commonUtil.checkArrayLength(logo)) {
				opendaySearchLandingVO.setLogoName(logo[logo.length - 1]);
				String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + opendaySearchLandingVO.getLogoName());
				opendaySearchLandingVO.setLogoURL(logoURL);
			   }
			}
		   }
		}
		ArrayList<OpendaySearchLandingVO> upcomingOpendaysList = (ArrayList<OpendaySearchLandingVO>) resultMap.get("PC_UPCOMING_OPENDAYS_LIST");
		if (!commonUtil.isBlankOrNull(upcomingOpendaysList)) {
		   for (int i = 0; i < upcomingOpendaysList.size(); i++) {
			OpendaySearchLandingVO opendaySearchLandingVO = (OpendaySearchLandingVO) upcomingOpendaysList.get(i);
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getMediaPath())) {
			   opendaySearchLandingVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getMediaPath()));
			}
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getCollegeLogo())) {
			   opendaySearchLandingVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getCollegeLogo()));
			   String[] logo = opendaySearchLandingVO.getCollegeLogo().split("/");
			   if (commonUtil.checkArrayLength(logo)) {
				opendaySearchLandingVO.setLogoName(logo[logo.length - 1]);
				String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + opendaySearchLandingVO.getLogoName());
				opendaySearchLandingVO.setLogoURL(logoURL);
			   }
			}
		   }
		}
		ArrayList<OpendaySearchLandingVO> regionOpendaysList = (ArrayList<OpendaySearchLandingVO>) resultMap.get("PC_REGION_OPENDAYS_LIST");
		if (!commonUtil.isBlankOrNull(regionOpendaysList)) {
		   for (int i = 0; i < regionOpendaysList.size(); i++) {
			OpendaySearchLandingVO opendaySearchLandingVO = (OpendaySearchLandingVO) regionOpendaysList.get(i);
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getMediaPath())) {
			   opendaySearchLandingVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getMediaPath()));
			}
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getCollegeLogo())) {
			   opendaySearchLandingVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getCollegeLogo()));
			   String[] logo = opendaySearchLandingVO.getCollegeLogo().split("/");
			   if (commonUtil.checkArrayLength(logo)) {
				opendaySearchLandingVO.setLogoName(logo[logo.length - 1]);
				String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + opendaySearchLandingVO.getLogoName());
				opendaySearchLandingVO.setLogoURL(logoURL);
			   }
			}
		   }
		}
		ArrayList<OpendaySearchLandingVO> eventTypeOpendaysList = (ArrayList<OpendaySearchLandingVO>) resultMap.get("PC_EVENT_TYPE_OPENDAYS_LIST");
		if (!commonUtil.isBlankOrNull(eventTypeOpendaysList)) {
		   for (int i = 0; i < eventTypeOpendaysList.size(); i++) {
			OpendaySearchLandingVO opendaySearchLandingVO = (OpendaySearchLandingVO) eventTypeOpendaysList.get(i);
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getMediaPath())) {
			   opendaySearchLandingVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getMediaPath()));
			}
			if (!commonUtil.isBlankOrNull(opendaySearchLandingVO.getCollegeLogo())) {
			   opendaySearchLandingVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, opendaySearchLandingVO.getCollegeLogo()));
			   String[] logo = opendaySearchLandingVO.getCollegeLogo().split("/");
			   if (commonUtil.checkArrayLength(logo)) {
				opendaySearchLandingVO.setLogoName(logo[logo.length - 1]);
				String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + opendaySearchLandingVO.getLogoName());
				opendaySearchLandingVO.setLogoURL(logoURL);
			   }
			}
		   }
		}
		ArrayList<RegionListVO> regionList = (ArrayList<RegionListVO>) resultMap.get("PC_REGION_LIST");
		String regionName = (String) resultMap.get("P_REGION_NAME");
		ArrayList<EventTypeListVO> eventTypeList = (ArrayList<EventTypeListVO>) resultMap.get("PC_EVENT_TYPE_LIST");
		String eventTypeName = (String) resultMap.get("P_EVENT_TYPE_NAME");
		//
		opendaySearchLandingPayloadToClient = new OpendaySearchLandingPayloadToClient();
		opendaySearchLandingPayloadToClient.setVirtualOpendaysList(virtualOpendaysList);
		opendaySearchLandingPayloadToClient.setUpcomingOpendaysList(upcomingOpendaysList);
		opendaySearchLandingPayloadToClient.setRegionOpendaysList(regionOpendaysList);
		opendaySearchLandingPayloadToClient.setEventTypeOpendaysList(eventTypeOpendaysList);
		opendaySearchLandingPayloadToClient.setRegionList(regionList);
		opendaySearchLandingPayloadToClient.setRegionName(regionName);
		opendaySearchLandingPayloadToClient.setEventTypeList(eventTypeList);
		opendaySearchLandingPayloadToClient.setEventTypeName(eventTypeName);
		//
		return new ResponseEntity<GenericReturnType>(opendaySearchLandingPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendaySearchLandingPayloadFromClient = null;
	   opendaySearchLandingPayloadToClient = null;
	   exceptionJSON = null;
	   debug = null;
	}
   }
}
