package com.wuapp.service.controller.openday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.openday.AdditionalResourcesVO;
import com.wuapp.service.dao.util.valueobject.openday.AddressVO;
import com.wuapp.service.dao.util.valueobject.openday.ContentSectionVO;
import com.wuapp.service.dao.util.valueobject.openday.GallerySectionVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendayListVO;
import com.wuapp.service.dao.util.valueobject.openday.SelectedEventDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.TestimonialSectionVO;
import com.wuapp.service.dao.util.valueobject.openday.UniInfoVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * OPENDAY PROVIDER LANDING PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 * Class : OpendayProviderLandingController.java
 * Description : Controller for getting provider opendays detail.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K        1.0                                              Initial draft
 */
 
@Controller
public class OpendayProviderLandingController {

   @Autowired
   private IOpendayBusiness opendayBusiness = null;
   
   /**
    * To get provider openday details
    * 
    * @author Hemalatha K
    * @param opendayProviderLandingPayloadFromClient, request, response
    * @return ResponseEntity
    */
  
   @RequestMapping(value = "/open-day/provider-landing/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getProviderOpendayDetails(@RequestBody OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendayProviderLandingPayloadToClient opendayProviderLandingPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities commonUtil = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendayProviderLandingPayloadFromClient.getAffiliateId(), opendayProviderLandingPayloadFromClient.getAccessToken())) {		
		resultMap = opendayBusiness.getOpendayProviderDetails(opendayProviderLandingPayloadFromClient);
		ArrayList<UniInfoVO> uniDetails = (ArrayList<UniInfoVO>) resultMap.get("PC_UNIV_TILE_INFO");
		if (!commonUtil.isBlankOrNull(uniDetails)) {
		   UniInfoVO uniInfoVO = (UniInfoVO) uniDetails.get(0);
		   if (!commonUtil.isBlankOrNull(uniInfoVO.getMediaPath())) {
			uniInfoVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, uniInfoVO.getMediaPath()));
		   }
		   if (!commonUtil.isBlankOrNull(uniInfoVO.getCollegeLogo())) {
			uniInfoVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, uniInfoVO.getCollegeLogo()));
			String[] logo = uniInfoVO.getCollegeLogo().split("/");
			if (commonUtil.checkArrayLength(logo)) {
			   uniInfoVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + uniInfoVO.getLogoName());
			   uniInfoVO.setLogoUrl(logoURL);
			}
		   }
		}
		ArrayList<OpendayListVO> opendayList = (ArrayList<OpendayListVO>) resultMap.get("PC_UNI_OPENDAYS_LIST");
		ArrayList<GallerySectionVO> gallerySection = (ArrayList<GallerySectionVO>) resultMap.get("PC_HERO_IMAGE");
		if (!commonUtil.isBlankOrNull(gallerySection)) {
		   for (int i = 0; i < gallerySection.size(); i++) {
			GallerySectionVO gallerySectionVO = (GallerySectionVO) gallerySection.get(i);
			if (!commonUtil.isBlankOrNull(gallerySectionVO.getMediaPath())) {
			   if("IMAGE".equalsIgnoreCase(gallerySectionVO.getMediaType())) {
				gallerySectionVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, gallerySectionVO.getMediaPath()));
			   }
			}
			if (!commonUtil.isBlankOrNull(gallerySectionVO.getThumbnailPath())) {
			   gallerySectionVO.setThumbnailPath(EmbeddedObject.getInstance().getUploadedImagePath(request, gallerySectionVO.getThumbnailPath()));
			}
		   }
		}
		ArrayList<ContentSectionVO> contentSection = (ArrayList<ContentSectionVO>) resultMap.get("PC_CONTENT_SECTION");
		ArrayList<AdditionalResourcesVO> additionalResource = (ArrayList<AdditionalResourcesVO>) resultMap.get("PC_ADDITIONAL_CONTENT");
		if (!commonUtil.isBlankOrNull(additionalResource)) {
		   for (int i = 0; i < additionalResource.size(); i++) {
			AdditionalResourcesVO additionalResourcesVO = (AdditionalResourcesVO) additionalResource.get(i);
			if (!commonUtil.isBlankOrNull(additionalResourcesVO.getPdfPath())) {
			   additionalResourcesVO.setPdfPath(EmbeddedObject.getInstance().getUploadedImagePath(request, additionalResourcesVO.getPdfPath()));
			}
		   }
		}
		ArrayList<AppVersionVO> appversionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		ArrayList<AddressVO> uniAddress = (ArrayList<AddressVO>) resultMap.get("PC_UNI_ADDRESS");
		ArrayList<TestimonialSectionVO> testimonialSection = (ArrayList<TestimonialSectionVO>) resultMap.get("PC_TESTIMONIALS");
		String mapBoxKey = (String) resultMap.get("P_MAP_BOX_KEY");
		ArrayList<SelectedEventDetailsVO> eventDetails = (ArrayList<SelectedEventDetailsVO>) resultMap.get("PC_EVENT_DETAILS");
		//
		opendayProviderLandingPayloadToClient = new OpendayProviderLandingPayloadToClient();
		opendayProviderLandingPayloadToClient.setUniDetails(uniDetails);
		opendayProviderLandingPayloadToClient.setGallerySection(gallerySection);
		opendayProviderLandingPayloadToClient.setContentSection(contentSection);
		opendayProviderLandingPayloadToClient.setOpendayList(opendayList);
		opendayProviderLandingPayloadToClient.setAdditionalResource(additionalResource);
		opendayProviderLandingPayloadToClient.setAppversionDetails(appversionDetails);
		opendayProviderLandingPayloadToClient.setUniAddress(uniAddress);
		opendayProviderLandingPayloadToClient.setTestimonialSection(testimonialSection);
		opendayProviderLandingPayloadToClient.setMapBoxKey(mapBoxKey);
		opendayProviderLandingPayloadToClient.setEventDetails(eventDetails);
		//
		return new ResponseEntity<GenericReturnType>(opendayProviderLandingPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendayProviderLandingPayloadToClient = null;
	   opendayProviderLandingPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
   
   /**
    * To get more openday list
    * 
    * @author Hemalatha K
    * @param opendayProviderLandingPayloadFromClient, request, response
    * @return ResponseEntity
    */
   
   @RequestMapping(value = "/more-open-days/", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   public ResponseEntity<GenericReturnType> getOpendayList(@RequestBody OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	OpendayProviderLandingPayloadToClient opendayProviderLandingPayloadToClient = null;
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendayProviderLandingPayloadFromClient.getAffiliateId(), opendayProviderLandingPayloadFromClient.getAccessToken())) {		
		resultMap = opendayBusiness.getMoreOpendayList(opendayProviderLandingPayloadFromClient);
		ArrayList<OpendayListVO> opendayList = (ArrayList<OpendayListVO>) resultMap.get("PC_UNI_OPENDAYS_LIST");		
		opendayProviderLandingPayloadToClient = new OpendayProviderLandingPayloadToClient();
		opendayProviderLandingPayloadToClient.setOpendayList(opendayList);
		//
		return new ResponseEntity<GenericReturnType>(opendayProviderLandingPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	} finally {
	   opendayProviderLandingPayloadToClient = null;
	   opendayProviderLandingPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
