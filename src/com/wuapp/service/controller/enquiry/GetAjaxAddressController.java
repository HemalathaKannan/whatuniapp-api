package com.wuapp.service.controller.enquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IEnquiryBusiness;
import com.wuapp.service.dao.util.valueobject.enquiry.AjaxUserAddressVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class GetAjaxAddressController {

   @Autowired
   IEnquiryBusiness enquirybusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/get-ajax-address/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> enquiryForm(@RequestBody EnquiryFormPayloadFromClient enquiryFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	EnquiryFormPayloadToClient enquiryFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<AjaxUserAddressVO> ajaxAddressList;
	//String yearOfEntry;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(enquiryFormPayloadFromClient.getAffiliateId(), enquiryFormPayloadFromClient.getAccessToken())) {
		resultMap = enquirybusiness.getAjaxAddressList(enquiryFormPayloadFromClient);
		ajaxAddressList = (ArrayList<AjaxUserAddressVO>) resultMap.get("PC_GET_ADDRESS");
		if (ajaxAddressList != null && ajaxAddressList.size() > 0) {
		   for (int i = 0; i < ajaxAddressList.size(); i++) {
			AjaxUserAddressVO ajaxUserAddressVO = (AjaxUserAddressVO) ajaxAddressList.get(i);
			if (!new CommonUtilities().isBlankOrNull(ajaxUserAddressVO.getAddressDisplay())) {
			   String[] addressArray = ajaxUserAddressVO.getAddressDisplay().split("###");
			   String addressDisplay = "";
			   if (addressArray != null && addressArray.length > 0) {
				for (int lp = 0; lp < addressArray.length; lp++) {
				   addressDisplay += addressArray[lp] + " ";
				}
			   }
			   ajaxUserAddressVO.setAddressDisplay(addressDisplay);
			}
		   }
		}
		//
		enquiryFormPayloadToClient = new EnquiryFormPayloadToClient();
		enquiryFormPayloadToClient.setAjaxAddressList(ajaxAddressList);
		//
		return new ResponseEntity<GenericReturnType>(enquiryFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   enquiryFormPayloadFromClient = null;
	   enquiryFormPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   public IEnquiryBusiness getEnquirybusiness() {
	return enquirybusiness;
   }

   public void setEnquirybusiness(IEnquiryBusiness enquirybusiness) {
	this.enquirybusiness = enquirybusiness;
   }
   //
}
