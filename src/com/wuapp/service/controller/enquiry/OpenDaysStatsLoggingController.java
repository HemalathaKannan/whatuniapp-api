package com.wuapp.service.controller.enquiry;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IEnquiryBusiness;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class OpenDaysStatsLoggingController {

   @Autowired
   IEnquiryBusiness enquirybusiness = null;

   @RequestMapping(value = "/open-days/stats-logging/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> saveOpenDays(@RequestBody EnquiryFormPayloadFromClient enquiryFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	EnquiryFormPayloadToClient enquiryFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	//String yearOfEntry;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(enquiryFormPayloadFromClient.getAffiliateId(), enquiryFormPayloadFromClient.getAccessToken())) {
		enquiryFormPayloadFromClient.setUserIP(new CommonUtilities().getUserIp(request));
		enquiryFormPayloadFromClient.setLatitude(common.round(enquiryFormPayloadFromClient.getLatitude(), 2));
		enquiryFormPayloadFromClient.setLongitude(common.round(enquiryFormPayloadFromClient.getLongitude(), 2));
		resultMap = enquirybusiness.openDaysStatsLog(enquiryFormPayloadFromClient);
		//
		return new ResponseEntity<GenericReturnType>(enquiryFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   enquiryFormPayloadFromClient = null;
	   enquiryFormPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   public IEnquiryBusiness getEnquirybusiness() {
	return enquirybusiness;
   }

   public void setEnquirybusiness(IEnquiryBusiness enquirybusiness) {
	this.enquirybusiness = enquirybusiness;
   }
   //
}
