package com.wuapp.service.controller.enquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IEnquiryBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.enquiry.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.enquiry.UserAddressVO;
import com.wuapp.service.dao.util.valueobject.enquiry.YearListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Class : EnquiryFormController.java 
 * Change Log :
 * ***************************************************************************************************************************
 * Author          version      Modified On     Modification Details                        Change
 * ***************************************************************************************************************************
 * Hemalatha.K       1.1        17_DEC_19           Modified                Added consent flag and text to show consent flag
 */

@Controller
public class EnquiryFormController {

   @Autowired
   IEnquiryBusiness enquirybusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/enquiry/enquiry-form/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> enquiryForm(@RequestBody EnquiryFormPayloadFromClient enquiryFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	EnquiryFormPayloadToClient enquiryFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<InstitutionVO> institutionsList;
	ArrayList<UserAddressVO> userAddressList;
	ArrayList<YearListVO> yearList;
	ArrayList<AppVersionVO> versionDetails;
	//
	String clientConsentFlag;
	String clientConsentText;
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//String yearOfEntry;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(enquiryFormPayloadFromClient.getAffiliateId(), enquiryFormPayloadFromClient.getAccessToken())) {
		resultMap = enquirybusiness.getEnquiryFormList(enquiryFormPayloadFromClient);
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		institutionsList = (ArrayList<InstitutionVO>) resultMap.get("PC_COLLEGE_INFO");
		userAddressList = (ArrayList<UserAddressVO>) resultMap.get("PC_USER_ADDRESS");
		yearList = (ArrayList<YearListVO>) resultMap.get("PC_YEAR_LIST");
		
		// Added to show consent flag in enquiry form by Hemalatha.K on 17_DEC_19_REL
		clientConsentFlag = (String) resultMap.get("P_CLIENT_CONSENT_FLAG");
		clientConsentText = (String) resultMap.get("P_CLIENT_CONSENT_TEXT");
		//
		if (institutionsList != null && institutionsList.size() > 0) {
		   for (int i = 0; i < institutionsList.size(); i++) {
			InstitutionVO institutionVO = (InstitutionVO) institutionsList.get(i);
			if (!common.isBlankOrNull(institutionVO.getLogo())) {
			   institutionVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionVO.getLogo()));
			}
			if (!common.isBlankOrNull(institutionVO.getLogoName())) {
			   String[] logo = institutionVO.getLogoName().split("/");
			   institutionVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + institutionVO.getLogoName());
			   institutionVO.setLogoURL(logoURL);
			}
		   }
		}
		enquiryFormPayloadToClient = new EnquiryFormPayloadToClient();
		enquiryFormPayloadToClient.setVersionDetails(versionDetails);
		enquiryFormPayloadToClient.setInstitutionDetailsList(institutionsList);
		enquiryFormPayloadToClient.setUserAddressList(userAddressList);
		enquiryFormPayloadToClient.setYearList(yearList);
		//		
		enquiryFormPayloadToClient.setClientConsentFlag(clientConsentFlag);
		enquiryFormPayloadToClient.setClientConsentText(clientConsentText);
		//
		String guestUserFlag = common.guestFlag(enquiryFormPayloadFromClient.getUserId());
		enquiryFormPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(enquiryFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   enquiryFormPayloadFromClient = null;
	   enquiryFormPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   public IEnquiryBusiness getEnquirybusiness() {
	return enquirybusiness;
   }

   public void setEnquirybusiness(IEnquiryBusiness enquirybusiness) {
	this.enquirybusiness = enquirybusiness;
   }
   //
}
