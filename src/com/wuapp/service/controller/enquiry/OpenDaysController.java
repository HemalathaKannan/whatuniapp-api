package com.wuapp.service.controller.enquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IEnquiryBusiness;
import com.wuapp.service.dao.util.valueobject.openday.MoreOpendayVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendayVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.openday.OpendayPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * @Modify : 01_Nov_2017 - Added reservePlace method for showing provider opendays by Prabha
 */
@Controller
public class OpenDaysController {

   @Autowired
   IEnquiryBusiness enquirybusiness = null;

   @RequestMapping(value = "/enquiry/save-open-days/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> saveOpenDays(@RequestBody EnquiryFormPayloadFromClient enquiryFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	EnquiryFormPayloadToClient enquiryFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	String returnFlag = null;
	//String yearOfEntry;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(enquiryFormPayloadFromClient.getAffiliateId(), enquiryFormPayloadFromClient.getAccessToken())) {
		resultMap = enquirybusiness.saveOpenDays(enquiryFormPayloadFromClient);
		returnFlag = (String) resultMap.get("P_RETURN_FLAG");
		//
		enquiryFormPayloadToClient = new EnquiryFormPayloadToClient();
		enquiryFormPayloadToClient.setReturnFlag(returnFlag);
		//
		String guestUserFlag = common.guestFlag(enquiryFormPayloadFromClient.getUserId());
		enquiryFormPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(enquiryFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   enquiryFormPayloadFromClient = null;
	   enquiryFormPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   /**
    * @reservePlace method to get the profile opendays info
    * @param opendayPayloadFromClient
    * @param request
    * @param response
    * @return ResponseEntity
    */
   @RequestMapping(value = "/enquiry/reserve-open-days/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> reservePlace(@RequestBody OpendayPayloadFromClient opendayPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	OpendayPayloadToClient opendayPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	ArrayList<OpendayVO> opendayList = null;
	ArrayList<MoreOpendayVO> moreOpendayList = null;
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//String yearOfEntry;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(opendayPayloadFromClient.getAffiliateId(), opendayPayloadFromClient.getAccessToken())) {
		resultMap = enquirybusiness.getOpendaysData(opendayPayloadFromClient);
		if (!resultMap.isEmpty()) {
		   opendayList = (ArrayList<OpendayVO>) resultMap.get("PC_OPENDAYS_INFO");
		   moreOpendayList = (ArrayList<MoreOpendayVO>) resultMap.get("PC_OPENDAYS_LIST");
		}
		if (opendayList != null && opendayList.size() > 0) {
		   OpendayVO opendayVO = (OpendayVO) opendayList.get(0);
		   if (!common.isBlankOrNull(opendayVO.getCollegeLogoPath())) {
			opendayVO.setCollegeLogoPath(EmbeddedObject.getInstance().getUploadedImagePath(request, opendayVO.getCollegeLogoPath()));
			String[] logo = opendayVO.getCollegeLogoPath().split("/");
			opendayVO.setLogoName(logo[logo.length - 1]);
			String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + opendayVO.getLogoName());
			opendayVO.setLogoURL(logoURL);
		   }
		}
		//
		opendayPayloadToClient = new OpendayPayloadToClient();
		opendayPayloadToClient.setOpendayList(opendayList);
		opendayPayloadToClient.setMoreOpendayList(moreOpendayList);
		return new ResponseEntity<GenericReturnType>(opendayPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   opendayPayloadFromClient = null;
	   opendayPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   public IEnquiryBusiness getEnquirybusiness() {
	return enquirybusiness;
   }

   public void setEnquirybusiness(IEnquiryBusiness enquirybusiness) {
	this.enquirybusiness = enquirybusiness;
   }
   //
}
