package com.wuapp.service.controller.enquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IEnquiryBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.enquiry.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.enquiry.OtherCoursesVO;
import com.wuapp.service.dao.util.valueobject.enquiry.OtherInstitutionsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class SubmitEnquiryformcontroller {

   @Autowired
   IEnquiryBusiness enquirybusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/enquiry/submit-enquiry-form/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> submitEnquiryForm(@RequestBody EnquiryFormPayloadFromClient enquiryFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	EnquiryFormPayloadToClient enquiryFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<InstitutionVO> institutionsList;
	ArrayList<OtherInstitutionsVO> otherInstitutionsList;
	ArrayList<OtherCoursesVO> otherCoursesList;
	ArrayList<AppVersionVO> versionDetails;
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//String yearOfEntry;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(enquiryFormPayloadFromClient.getAffiliateId(), enquiryFormPayloadFromClient.getAccessToken())) {
		enquiryFormPayloadFromClient.setUserIP(new CommonUtilities().getUserIp(request));
		enquiryFormPayloadFromClient.setLatitude(common.round(enquiryFormPayloadFromClient.getLatitude(), 2));
		enquiryFormPayloadFromClient.setLongitude(common.round(enquiryFormPayloadFromClient.getLongitude(), 2));
		resultMap = enquirybusiness.submitEnquiryForm(enquiryFormPayloadFromClient);
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		institutionsList = (ArrayList<InstitutionVO>) resultMap.get("PC_COLLEGE_INFO");
		otherInstitutionsList = (ArrayList<OtherInstitutionsVO>) resultMap.get("PC_OTHER_UNI_POD");
		otherCoursesList = (ArrayList<OtherCoursesVO>) resultMap.get("PC_OTHER_COURSE_POD");
		//
		if (institutionsList != null && institutionsList.size() > 0) {
		   for (int i = 0; i < institutionsList.size(); i++) {
			InstitutionVO institutionVO = (InstitutionVO) institutionsList.get(i);
			if (!common.isBlankOrNull(institutionVO.getLogo())) {
			   institutionVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionVO.getLogo()));
			}
			if (!common.isBlankOrNull(institutionVO.getLogoName())) {
			   String[] logo = institutionVO.getLogoName().split("/");
			   institutionVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + institutionVO.getLogoName());
			   institutionVO.setLogoURL(logoURL);
			}
		   }
		}
		//
		if (otherInstitutionsList != null && otherInstitutionsList.size() > 0) {
		   for (int i = 0; i < otherInstitutionsList.size(); i++) {
			OtherInstitutionsVO otherInstitutionsVO = (OtherInstitutionsVO) otherInstitutionsList.get(i);
			if (!common.isBlankOrNull(otherInstitutionsVO.getLogo())) {
			   otherInstitutionsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, otherInstitutionsVO.getLogo()));
			}
			if (!common.isBlankOrNull(otherInstitutionsVO.getLogoName())) {
			   String[] logo = otherInstitutionsVO.getLogoName().split("/");
			   otherInstitutionsVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + otherInstitutionsVO.getLogoName());
			   otherInstitutionsVO.setLogoURL(logoURL);
			}
		   }
		}
		//
		if (otherCoursesList != null && otherCoursesList.size() > 0) {
		   for (int i = 0; i < otherCoursesList.size(); i++) {
			OtherCoursesVO otherCoursesVO = (OtherCoursesVO) otherCoursesList.get(i);
			if (!common.isBlankOrNull(otherCoursesVO.getLogo())) {
			   otherCoursesVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, otherCoursesVO.getLogo()));
			}
			if (!common.isBlankOrNull(otherCoursesVO.getLogoName())) {
			   String[] logo = otherCoursesVO.getLogoName().split("/");
			   otherCoursesVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + otherCoursesVO.getLogoName());
			   otherCoursesVO.setLogoURL(logoURL);
			}
		   }
		}
		//
		enquiryFormPayloadToClient = new EnquiryFormPayloadToClient();
		enquiryFormPayloadToClient.setVersionDetails(versionDetails);
		enquiryFormPayloadToClient.setInstitutionDetailsList(institutionsList);
		enquiryFormPayloadToClient.setOtherInstitutionsList(otherInstitutionsList);
		enquiryFormPayloadToClient.setOtherCoursesList(otherCoursesList);
		//
		String guestUserFlag = common.guestFlag(enquiryFormPayloadFromClient.getUserId());
		enquiryFormPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(enquiryFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   enquiryFormPayloadFromClient = null;
	   enquiryFormPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   public IEnquiryBusiness getEnquirybusiness() {
	return enquirybusiness;
   }

   public void setEnquirybusiness(IEnquiryBusiness enquirybusiness) {
	this.enquirybusiness = enquirybusiness;
   }
   //
}
