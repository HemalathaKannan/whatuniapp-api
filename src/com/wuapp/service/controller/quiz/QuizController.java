package com.wuapp.service.controller.quiz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IHomeBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.quiz.CourseAjaxVO;
import com.wuapp.service.dao.util.valueobject.quiz.JobIndustryAjaxVO;
import com.wuapp.service.dao.util.valueobject.quiz.PreviousQualVO;
import com.wuapp.service.dao.util.valueobject.quiz.QualSubjectAjaxVO;
import com.wuapp.service.dao.util.valueobject.quiz.QuestionDetailsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;
import com.wuapp.service.pojo.json.home.QuizPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

@Controller
public class QuizController {

   @Autowired
   IHomeBusiness homeBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/quiz/quiz-details/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getQuizDetails(@RequestBody QuizPayloadFromClient quizPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	QuizPayloadToClient quizPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	ArrayList<QuestionDetailsVO> questionList;
	ArrayList<PreviousQualVO> previousQualList;
	ArrayList<AppVersionVO> versionDetails;
	String courseCount = "";
	String selectedFilterUrl = "";
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(quizPayloadFromClient.getAffiliateId(), quizPayloadFromClient.getAccessToken())) {
		if(quizPayloadFromClient.getAppVersion() != null || quizPayloadFromClient.getAppVersion() == "") {
		  resultMap = homeBusiness.getQuizDetails(quizPayloadFromClient);
		} else {
		  resultMap = homeBusiness.getDesktopQuizDetails(quizPayloadFromClient);
            }
		questionList = (ArrayList<QuestionDetailsVO>) resultMap.get("pc_question_details");
		previousQualList = (ArrayList<PreviousQualVO>) resultMap.get("pc_prev_qual");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("pc_version_details");
		courseCount = (String) resultMap.get("p_course_count");
		selectedFilterUrl = (String) resultMap.get("p_selected_filter_url");
		//
		quizPayloadToClient = new QuizPayloadToClient();
		quizPayloadToClient.setQuestionDetailsList(questionList);
		quizPayloadToClient.setPreviousQualList(previousQualList);
		quizPayloadToClient.setVersionDetails(versionDetails);
		quizPayloadToClient.setCourseCount(courseCount);
		quizPayloadToClient.setSelectedFilterUrl(selectedFilterUrl);
		//
		String guestUserFlag = common.guestFlag(quizPayloadFromClient.getUserId());
		quizPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(quizPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   quizPayloadToClient = null;
	   quizPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/quiz/course-list-ajax/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseList(@RequestBody QuizPayloadFromClient quizPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	QuizPayloadToClient quizPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<CourseAjaxVO> courseList;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(quizPayloadFromClient.getAffiliateId(), quizPayloadFromClient.getAccessToken())) {
		resultMap = homeBusiness.getCourseList(quizPayloadFromClient);
		courseList = (ArrayList<CourseAjaxVO>) resultMap.get("pc_course_ajax_list");
		//
		quizPayloadToClient = new QuizPayloadToClient();
		quizPayloadToClient.setCourseList(courseList);
		return new ResponseEntity<GenericReturnType>(quizPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   /**
    * Added by Indumathi.S Oct 31 2017 REl
    * 
    * @param quizPayloadFromClient
    * @param request
    * @param response
    * @return
    */
   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/quiz/job-industry-list-ajax/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getJobIndustryList(@RequestBody QuizPayloadFromClient quizPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	QuizPayloadToClient quizPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<JobIndustryAjaxVO> jobIndustryList;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(quizPayloadFromClient.getAffiliateId(), quizPayloadFromClient.getAccessToken())) {
		resultMap = homeBusiness.getJobIndustryList(quizPayloadFromClient);
		jobIndustryList = (ArrayList<JobIndustryAjaxVO>) resultMap.get("pc_job_industry_result");
		//
		quizPayloadToClient = new QuizPayloadToClient();
		quizPayloadToClient.setJobIndustryList(jobIndustryList);
		return new ResponseEntity<GenericReturnType>(quizPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   /**
    * Added by Indumathi.S Oct 31 2017 REl
    * 
    * @param quizPayloadFromClient
    * @param request
    * @param response
    * @return
    */
   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/quiz/qual-subject-ajax/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getQualSubjectList(@RequestBody QuizPayloadFromClient quizPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	QuizPayloadToClient quizPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<QualSubjectAjaxVO> qualSubjectList;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(quizPayloadFromClient.getAffiliateId(), quizPayloadFromClient.getAccessToken())) {
		resultMap = homeBusiness.getQualSubjectList(quizPayloadFromClient);
		qualSubjectList = (ArrayList<QualSubjectAjaxVO>) resultMap.get("pc_results");
		//
		quizPayloadToClient = new QuizPayloadToClient();
		quizPayloadToClient.setQualSubjectList(qualSubjectList);
		return new ResponseEntity<GenericReturnType>(quizPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/quiz/save-user-actions/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> saveUserActions(@RequestBody QuizPayloadFromClient quizPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	QuizPayloadToClient quizPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(quizPayloadFromClient.getAffiliateId(), quizPayloadFromClient.getAccessToken())) {
		resultMap = homeBusiness.saveUserActions(quizPayloadFromClient);
		//
		return new ResponseEntity<GenericReturnType>(quizPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }
}
