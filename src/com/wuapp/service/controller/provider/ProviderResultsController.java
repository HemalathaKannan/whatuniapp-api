package com.wuapp.service.controller.provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IProviderBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.filters.AssesmentTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.PreviousQualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.QualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.ReviewCategoryFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.StudyModeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.SubjectFilterVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.dao.util.valueobject.provider.InstitutionDetailsVO;
import com.wuapp.service.dao.util.valueobject.provider.ProviderResultsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadFromClient;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * SEARCH RESULTS
 *
 * @author Keerthika.P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	         Modified On     	  Modification Details 	                
 * *************************************************************************************************************************
 * Hemalatha.K            2.0            25_JUN_2020          Removed clearing related changes
 */
@Controller
public class ProviderResultsController {

   @Autowired
   IProviderBusiness providerBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/institution/provider-results", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseSearchResults(@RequestBody ProviderResultsPayloadFromClient providerResultsPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	ProviderResultsPayloadToClient providerResultsPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	String selectedValue;
	ArrayList<ProviderResultsVO> searchResults;
	ArrayList<InstitutionDetailsVO> institutionDetails;
	ArrayList<QualificationFilterVO> qualificationFilterDetails;
	ArrayList<SubjectFilterVO> subjectFilterDetails;
	ArrayList<LocationFilterVO> locationFilterDetails;
	ArrayList<LocationTypeFilterVO> locationTypeFilterDetails;
	ArrayList<StudyModeFilterVO> studyModeFilterDetails;
	ArrayList<PreviousQualificationFilterVO> previousQualificationFilterDetails;
	ArrayList<AssesmentTypeFilterVO> assessmentTypeFilterDetails;
	ArrayList<AppVersionVO> versionDetails;
	ArrayList<SubjectVO> orderByName;
	ArrayList<ReviewCategoryFilterVO> reviewCategoryFilterDetails;
	String advertFlag;
	String totalCourseCount;
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(providerResultsPayloadFromClient.getAffiliateId(), providerResultsPayloadFromClient.getAccessToken())) {
		providerResultsPayloadFromClient.setUserIp(common.getUserIp(request));
		providerResultsPayloadFromClient.setUserAgent(GenericValidator.isBlankOrNull(providerResultsPayloadFromClient.getUserAgent()) ? common.getUserAgent(request) : providerResultsPayloadFromClient.getUserAgent());
		providerResultsPayloadFromClient.setLatitude(common.round(providerResultsPayloadFromClient.getLatitude(), 2));
		providerResultsPayloadFromClient.setLongitude(common.round(providerResultsPayloadFromClient.getLongitude(), 2));
		resultMap = providerBusiness.getProviderResults(providerResultsPayloadFromClient);
		selectedValue = providerResultsPayloadFromClient.getOrderBy();
		searchResults = (ArrayList<ProviderResultsVO>) resultMap.get("pc_search_results");
		institutionDetails = (ArrayList<InstitutionDetailsVO>) resultMap.get("pc_college_details");
		if (institutionDetails != null && institutionDetails.size() > 0) {
		   for (int i = 0; i < institutionDetails.size(); i++) {
			InstitutionDetailsVO institutionDetailsVO = (InstitutionDetailsVO) institutionDetails.get(i);
			if (!common.isBlankOrNull(institutionDetailsVO.getMediaPath())) {
			   institutionDetailsVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionDetailsVO.getMediaPath()));
			}
			if (!common.isBlankOrNull(institutionDetailsVO.getLogo())) {
			   institutionDetailsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionDetailsVO.getLogo()));
			}
			if (!common.isBlankOrNull(institutionDetailsVO.getLogoName())) {
			   String[] logo = institutionDetailsVO.getLogoName().split("/");
			   institutionDetailsVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + institutionDetailsVO.getLogoName());
			   institutionDetailsVO.setLogoURL(logoURL);
			}
		   }
		}
		qualificationFilterDetails = (ArrayList<QualificationFilterVO>) resultMap.get("PC_QUAL_FILTERS");
		subjectFilterDetails = (ArrayList<SubjectFilterVO>) resultMap.get("PC_SUBJECT_FILTERS");
		locationFilterDetails = (ArrayList<LocationFilterVO>) resultMap.get("PC_LOCATION_FILTERS");
		locationTypeFilterDetails = (ArrayList<LocationTypeFilterVO>) resultMap.get("PC_LOCATION_TYPES_FILTER");
		studyModeFilterDetails = (ArrayList<StudyModeFilterVO>) resultMap.get("PC_STUDY_MODE_FILTER");
		previousQualificationFilterDetails = (ArrayList<PreviousQualificationFilterVO>) resultMap.get("PC_PREV_QUAL_FILTER");
		assessmentTypeFilterDetails = (ArrayList<AssesmentTypeFilterVO>) resultMap.get("PC_ASSESSMENT_TYPE_FILTER");
		reviewCategoryFilterDetails = (ArrayList<ReviewCategoryFilterVO>) resultMap.get("PC_REVIEW_CATEGORY_FILTER");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		orderByName = getOrderByNames(selectedValue);
		advertFlag = (String) resultMap.get("o_adv_flag");
		totalCourseCount = (String) resultMap.get("p_course_count");
		//
		providerResultsPayloadToClient = new ProviderResultsPayloadToClient();
		providerResultsPayloadToClient.setSearchResults(searchResults);
		providerResultsPayloadToClient.setInstitutionDetails(institutionDetails);
		providerResultsPayloadToClient.setQualificationFilters(qualificationFilterDetails);
		providerResultsPayloadToClient.setSubjectfilters(subjectFilterDetails);
		providerResultsPayloadToClient.setLocationfilters(locationFilterDetails);
		providerResultsPayloadToClient.setLocationTypeFilters(locationTypeFilterDetails);
		providerResultsPayloadToClient.setStudyModeFilters(studyModeFilterDetails);
		providerResultsPayloadToClient.setPreviousQualificationfilters(previousQualificationFilterDetails);
		providerResultsPayloadToClient.setAssessmentTypeFilterDetails(assessmentTypeFilterDetails);
		providerResultsPayloadToClient.setAdvertFlag(advertFlag);
		providerResultsPayloadToClient.setOverAllCourseCount(totalCourseCount);
		providerResultsPayloadToClient.setReviewCategoryFilterDetails(reviewCategoryFilterDetails);
		providerResultsPayloadToClient.setVersionDetails(versionDetails);
		providerResultsPayloadToClient.setOrderByName(orderByName);
		providerResultsPayloadToClient.setUniDefaultBgImgPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.images")));
		//
		String guestUserFlag = common.guestFlag(providerResultsPayloadFromClient.getUserId());
		providerResultsPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(providerResultsPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   providerResultsPayloadToClient = null;
	   providerResultsPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   /**
    * @see This method returns the orderby name list for the search results page
    * @param selectedValue
    * @return orderByNamesList
    */
   public ArrayList<SubjectVO> getOrderByNames(String selectedValue) {
	String key[] = { "MOST_INFO", "BEST_MATCH", "STUDENT_RANKING", "COURSE_COUNT" };
	String displayName[] = { "Most info", "Best match", "Student ranking", "Number of courses" };
	ArrayList<SubjectVO> orderByNamesList = new ArrayList<SubjectVO>();
	for (int lp = 0; lp < key.length; lp++) {
	   SubjectVO subjectVO = new SubjectVO();
	   subjectVO.setOrderByName(key[lp]);
	   subjectVO.setOrderBydisplayName(displayName[lp]);
	   if (selectedValue != null && "selectedValue".equalsIgnoreCase(key[lp])) {
		subjectVO.setOrderBySelected("Y");
	   } else {
		subjectVO.setOrderBySelected("N");
	   }
	   orderByNamesList.add(subjectVO);
	}
	return orderByNamesList;
   }
}
