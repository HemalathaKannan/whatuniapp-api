package com.wuapp.service.controller.interaction;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICommonBusiness;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadToClient;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

@Controller
public class GetClearingFlagController {

   @Autowired
   ICommonBusiness commonBusiness = null;

   @SuppressWarnings({ "unchecked" })
   @RequestMapping(value = "/interaction/get-clearing-flag", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getClearingFlag(@RequestBody GetClearingFlagPayloadFromClient getClearingFlagPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	//GetClearingFlagPayloadToClient getClearingFlagPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(getClearingFlagPayloadFromClient.getAffiliateId(), getClearingFlagPayloadFromClient.getAccessToken())) {
		//getClearingFlagPayloadFromClient.setUserIp(new CommonUtilities().getUserIp(request));
		//resultMap = commonBusiness.getClearingFlag(getClearingFlagPayloadFromClient);
		GetClearingFlagPayloadToClient getClearingFlagPayloadToClient = new GetClearingFlagPayloadToClient();
		//getClearingFlagPayloadToClient.setClearingFlag((String) resultMap.get("P_VALUE"));
		//get clearing flag from static data preloader
		getClearingFlagPayloadToClient.setClearingFlag((String) StaticDataPreLoader.getInstance().getAppClearingFlag());
		//
		return new ResponseEntity<GenericReturnType>(getClearingFlagPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   //getClearingFlagPayloadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
