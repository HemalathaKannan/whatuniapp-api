package com.wuapp.service.controller.interaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICommonBusiness;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.interaction.SaveActionsPayloadFromClient;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * @Class : SaveUserActionsController.java
 * @Description : This class is used to track the user action to DB...
 * @version : 1.0
 * @since : 01_Nov_2017
 * @author : Prabhakaran V.
 * @Modify : 01_Nov_2017 Initial draft
 */
@Controller
public class SaveUserActionsController {

   @Autowired
   ICommonBusiness commonBusiness = null;

   /**
    * @saveUserActions method to save the user action to DB
    * @param saveActionsPayloadFromClient
    * @param request
    * @param response
    * @return ResponseEntity
    */
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/save-user-actions/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> saveUserActions(@RequestBody SaveActionsPayloadFromClient saveActionsPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	SaveActionsPayloadFromClient saveActionPayloadFromClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(saveActionsPayloadFromClient.getAffiliateId(), saveActionsPayloadFromClient.getAccessToken())) {
		commonBusiness.saveUserActions(saveActionsPayloadFromClient);
		//
		return new ResponseEntity<GenericReturnType>(saveActionPayloadFromClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }
}
