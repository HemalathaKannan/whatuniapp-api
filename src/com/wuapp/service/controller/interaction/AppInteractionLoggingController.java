package com.wuapp.service.controller.interaction;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICommonBusiness;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.interaction.InteractionPayloadFromClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;
/**
 * 
 *
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : AppInteractionLoggingController.java
 * Description : controller for stats log. 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 *      		  1.0                   			  Initial draft
 * Venkatesh.G      2.0              12-Oct-2018     Added condition for stats log to prevent double time logging in PR page alone(one in db side(new version) and one in ajax call(old version)). 
 */
@Controller
public class AppInteractionLoggingController {

   @Autowired
   ICommonBusiness commonBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/interaction/db-stats/logging", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> dbStatsLogging(@RequestBody InteractionPayloadFromClient interactionPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(interactionPayloadFromClient.getAffiliateId(), interactionPayloadFromClient.getAccessToken())) {
		interactionPayloadFromClient.setUserIp(new CommonUtilities().getUserIp(request));		
		interactionPayloadFromClient.setLatitude(common.round(interactionPayloadFromClient.getLatitude(), 2));
		interactionPayloadFromClient.setLongitude(common.round(interactionPayloadFromClient.getLongitude(), 2));		
		//Added for the Oct_23_18 rel
		if(!"HOTCOURSES: PROVIDER RESULTS".equalsIgnoreCase(interactionPayloadFromClient.getActivity())) {
		   resultMap = commonBusiness.dbStatsLogging(interactionPayloadFromClient);
		   interactionPayloadFromClient.setLogStatusId((String) resultMap.get("P_LOG_STATUS_ID"));
		} else {
		   interactionPayloadFromClient.setLogStatusId("0");
		}
		//
		return new ResponseEntity<GenericReturnType>(interactionPayloadFromClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   interactionPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
