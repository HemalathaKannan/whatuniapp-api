package com.wuapp.service.controller.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.util.developer.Debugger;

/**
 * Handling Exception in service side
 *
 * @author Bhoovi arashan P
 * @since wuapp1.0_20170315 - initial draft
 * @version 1.1
 */
@Controller
public class ExceptionController {

   @RequestMapping(value = "/exception/")
   public ResponseEntity<GenericReturnType> handleException(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	//
	Debugger debug = Debugger.getInstance();
	ExceptionJSON exceptionJSON = new ExceptionJSON();
	//
	debug.printExceptionDetails(e, request, (this.getClass().toString()));
	//
	exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	exceptionJSON.setException(debug.getExceptionDetails(e));
	//
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
   }
}
