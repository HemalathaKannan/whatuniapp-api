package com.wuapp.service.controller.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.util.developer.Debugger;

/**
 * Handling Error in service side
 *
 * @author Bhoovi arashan P
 * @since wuapp1.0_20170315 - initial draft
 * @version 1.1
 */
@Controller
public class ErrorController {

   Debugger debug = Debugger.getInstance();
   ExceptionJSON exceptionJSON = new ExceptionJSON();

   @RequestMapping(value = "/errors/")
   public ResponseEntity<GenericReturnType> errors(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	exceptionJSON.setException("");
	//exceptionJSON.setException(debug.getExceptionDetails(e)); //TODO here the original exception from the previous URL/request is not caught
	//debug.printExceptionDetails(e, request, (this.getClass().toString()));
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
   }

   @RequestMapping(value = "/errors/400/")
   public ResponseEntity<GenericReturnType> handle400(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	exceptionJSON.setStatus((HttpStatus.BAD_REQUEST).toString());
	exceptionJSON.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
	exceptionJSON.setException("");
	//exceptionJSON.setException(debug.getExceptionDetails(e)); //TODO here the original exception from the previous URL/request is not caught
	//debug.printExceptionDetails(e, request, (this.getClass().toString()));
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.BAD_REQUEST);
   }

   @RequestMapping(value = "/errors/404/")
   public ResponseEntity<GenericReturnType> handle404(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	exceptionJSON.setStatus((HttpStatus.NOT_FOUND).toString());
	exceptionJSON.setMessage(HttpStatus.NOT_FOUND.getReasonPhrase());
	exceptionJSON.setException("");
	//exceptionJSON.setException(debug.getExceptionDetails(e)); //TODO here the original exception from the previous URL/request is not caught
	//debug.printExceptionDetails(e, request, (this.getClass().toString()));
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.NOT_FOUND);
   }

   @RequestMapping(value = "/errors/406/")
   public ResponseEntity<GenericReturnType> handle406(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	exceptionJSON.setStatus((HttpStatus.NOT_ACCEPTABLE).toString());
	exceptionJSON.setMessage(HttpStatus.NOT_ACCEPTABLE.getReasonPhrase());
	exceptionJSON.setException("");
	//exceptionJSON.setException(debug.getExceptionDetails(e)); //TODO here the original exception from the previous URL/request is not caught
	//debug.printExceptionDetails(e, request, (this.getClass().toString()));
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.NOT_ACCEPTABLE);
   }

   @RequestMapping(value = "/errors/415/")
   public ResponseEntity<GenericReturnType> handle515(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	exceptionJSON.setStatus((HttpStatus.UNSUPPORTED_MEDIA_TYPE).toString());
	exceptionJSON.setMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase());
	exceptionJSON.setException("");
	//exceptionJSON.setException(debug.getExceptionDetails(e)); //TODO here the original exception from the previous URL/request is not caught
	//debug.printExceptionDetails(e, request, (this.getClass().toString()));
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
   }

   @RequestMapping(value = "/errors/500/")
   public ResponseEntity<GenericReturnType> handle500(Exception e, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
	exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	exceptionJSON.setException("");
	//exceptionJSON.setException(debug.getExceptionDetails(e)); //TODO here the original exception from the previous URL/request is not caught
	//debug.printExceptionDetails(e, request, (this.getClass().toString()));
	return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
   }
}
