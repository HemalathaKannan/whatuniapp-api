package com.wuapp.service.controller.course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICourseBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.course.SearchFormPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchFormPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * SEARCH LANDING PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class SearchFormController {

   @Autowired
   ICourseBusiness searchBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/search-form/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getSearchFormList(@RequestBody SearchFormPayloadFromClient searchFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//	
	SearchFormPayloadToClient searchFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	ArrayList<SubjectVO> recentSubjectList;
	ArrayList<SubjectVO> nearSubjectList;
	ArrayList<InstitutionVO> recentInstitutions;
	ArrayList<InstitutionVO> institutionsCloseToYou;
	ArrayList<AppVersionVO> versionDetails;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(searchFormPayloadFromClient.getAffiliateId(), searchFormPayloadFromClient.getAccessToken())) {
		searchFormPayloadFromClient.setLatitude(common.round(searchFormPayloadFromClient.getLatitude(), 2));
		searchFormPayloadFromClient.setLongitude(common.round(searchFormPayloadFromClient.getLongitude(), 2));
		resultMap = searchBusiness.getSearchFormList(searchFormPayloadFromClient);
		recentSubjectList = (ArrayList<SubjectVO>) resultMap.get("PC_SEARCH_SUBJECT");
		nearSubjectList = (ArrayList<SubjectVO>) resultMap.get("PC_SUBJECT_LIST");
		recentInstitutions = (ArrayList<InstitutionVO>) resultMap.get("PC_SEARCH_INSTITUTIONS");
		institutionsCloseToYou = (ArrayList<InstitutionVO>) resultMap.get("PC_NEARBY_INSTITUTIONS");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		//
		if (recentSubjectList != null && recentSubjectList.size() > 0) {
		   if (nearSubjectList != null && nearSubjectList.size() > 4) {
			nearSubjectList = new ArrayList<SubjectVO>(nearSubjectList.subList(0, 4));
		   }
		}
		searchFormPayloadToClient = new SearchFormPayloadToClient();
		searchFormPayloadToClient.setRecentSubjects(recentSubjectList);
		searchFormPayloadToClient.setNearSubjects(nearSubjectList);
		searchFormPayloadToClient.setRecentInstitutions(recentInstitutions);
		searchFormPayloadToClient.setInstitutionsCloseToYou(institutionsCloseToYou);
		searchFormPayloadToClient.setVersionDetails(versionDetails);
		String guestUserFlag = common.guestFlag(searchFormPayloadFromClient.getUserId());
		searchFormPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(searchFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   searchFormPayloadToClient = null;
	   searchFormPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
