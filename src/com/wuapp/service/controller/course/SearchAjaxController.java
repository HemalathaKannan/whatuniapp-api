package com.wuapp.service.controller.course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICourseBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.course.SearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchAjaxPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * SEARCH AJAX PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class SearchAjaxController {

   @Autowired
   ICourseBusiness searchBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/ajax-search/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getAjaxSearchList(@RequestBody SearchAjaxPayloadFromClient searchAjaxPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	SearchAjaxPayloadToClient searchAjaxPayloadToClient = null;
	CommonUtilities common = new CommonUtilities();
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<SubjectVO> recentSubjectList;
	ArrayList<SubjectVO> suggestedSubjectsList;
	ArrayList<InstitutionVO> recentInstitutionsList;
	ArrayList<InstitutionVO> suggestedInstitutions;
	ArrayList<AppVersionVO> versionDetails;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(searchAjaxPayloadFromClient.getAffiliateId(), searchAjaxPayloadFromClient.getAccessToken())) {
		searchAjaxPayloadFromClient.setLatitude(common.round(searchAjaxPayloadFromClient.getLatitude(), 2));
		searchAjaxPayloadFromClient.setLongitude(common.round(searchAjaxPayloadFromClient.getLongitude(), 2));
		resultMap = searchBusiness.getSearchAjaxList(searchAjaxPayloadFromClient);
		recentSubjectList = (ArrayList<SubjectVO>) resultMap.get("PC_SEARCH_SUBJECT");
		suggestedSubjectsList = (ArrayList<SubjectVO>) resultMap.get("PC_COURSE_SUGGESTION_LIST");
		recentInstitutionsList = (ArrayList<InstitutionVO>) resultMap.get("PC_SEARCH_INSTITUTIONS");
		suggestedInstitutions = (ArrayList<InstitutionVO>) resultMap.get("PC_INST_SUGGESTION_LIST");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		//
		searchAjaxPayloadToClient = new SearchAjaxPayloadToClient();
		searchAjaxPayloadToClient.setRecentSubjects(recentSubjectList);
		searchAjaxPayloadToClient.setSuggestedSubjects(suggestedSubjectsList);
		searchAjaxPayloadToClient.setRecentInstitutions(recentInstitutionsList);
		searchAjaxPayloadToClient.setSuggestedInstitutions(suggestedInstitutions);
		searchAjaxPayloadToClient.setVersionDetails(versionDetails);
		String guestUserFlag = common.guestFlag(searchAjaxPayloadFromClient.getUserId());
		searchAjaxPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(searchAjaxPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   searchAjaxPayloadToClient = null;
	   searchAjaxPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
