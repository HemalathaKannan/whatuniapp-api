package com.wuapp.service.controller.course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICourseBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.filters.AssesmentTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.PreviousQualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.QualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.ReviewCategoryFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.StudyModeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.SubjectFilterVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.dao.util.valueobject.searchresults.SearchResultsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * SEARCH RESULTS
 *
 * @author Indumathi.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	         Modified On     	  Modification Details 	                
 * *************************************************************************************************************************
 * Hemalatha.K            2.0                25_JUN_2020            Removed clearing related changes
 */
@Controller
public class SearchResultsController {

   @Autowired
   ICourseBusiness searchBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/course/search-results", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseSearchResults(@RequestBody SearchResultsPayloadFromClient searchPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	SearchResultsPayloadToClient searchResultsPayloadToCleint = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<SearchResultsVO> searchResults;
	ArrayList<AppVersionVO> versionDetails;
	ArrayList<QualificationFilterVO> qualificationDetails;
	ArrayList<SubjectFilterVO> subjectDetails;
	ArrayList<LocationFilterVO> locationDetails;
	ArrayList<LocationTypeFilterVO> locationTypeDetails;
	ArrayList<StudyModeFilterVO> studyModeDetails;
	ArrayList<PreviousQualificationFilterVO> previousQualificationDetails;
	ArrayList<AssesmentTypeFilterVO> assessmentTypeFilterDetails;
	ArrayList<ReviewCategoryFilterVO> reviewCategoryFilterDetails;
	String totalUniversitiesCount;
	String selectedValue;
	ArrayList<SubjectVO> orderByNames;
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(searchPayloadFromClient.getAffiliateId(), searchPayloadFromClient.getAccessToken())) {
		resultMap = searchBusiness.getSearchResults(searchPayloadFromClient);
		searchResults = (ArrayList<SearchResultsVO>) resultMap.get("PC_SEARCH_RESULT");
		if (searchResults != null && searchResults.size() > 0) {
		   for (int i = 0; i < searchResults.size(); i++) {
			SearchResultsVO searchResultsVO = (SearchResultsVO) searchResults.get(i);
			if (!common.isBlankOrNull(searchResultsVO.getTileMediaPath())) {
			   searchResultsVO.setTileMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, searchResultsVO.getTileMediaPath()));
			}
			if (!common.isBlankOrNull(searchResultsVO.getLogo())) {
			   searchResultsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, searchResultsVO.getLogo()));
			}
			if (!common.isBlankOrNull(searchResultsVO.getLogoName())) {
			   String[] logo = searchResultsVO.getLogoName().split("/");
			   searchResultsVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + searchResultsVO.getLogoName());
			   searchResultsVO.setLogoURL(logoURL);
			}
		   }
		}
		selectedValue = searchPayloadFromClient.getOrderBy();
		qualificationDetails = (ArrayList<QualificationFilterVO>) resultMap.get("PC_QUAL_FILTERS");
		subjectDetails = (ArrayList<SubjectFilterVO>) resultMap.get("PC_SUBJECT_FILTERS");
		locationDetails = (ArrayList<LocationFilterVO>) resultMap.get("PC_LOCATION_FILTERS");
		locationTypeDetails = (ArrayList<LocationTypeFilterVO>) resultMap.get("PC_LOCATION_TYPES_FILTER");
		studyModeDetails = (ArrayList<StudyModeFilterVO>) resultMap.get("PC_STUDY_MODE_FILTER");
		previousQualificationDetails = (ArrayList<PreviousQualificationFilterVO>) resultMap.get("PC_PREV_QUAL_FILTER");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		assessmentTypeFilterDetails = (ArrayList<AssesmentTypeFilterVO>) resultMap.get("PC_ASSESSMENT_TYPE_FILTER");
		reviewCategoryFilterDetails = (ArrayList<ReviewCategoryFilterVO>) resultMap.get("PC_REVIEW_CATEGORY_FILTER");
		totalUniversitiesCount = (String) resultMap.get("P_TOTAL_COLLEGE_COUNT");
		orderByNames = getOrderByNames(selectedValue);
		//
		searchResultsPayloadToCleint = new SearchResultsPayloadToClient();
		searchResultsPayloadToCleint.setSearchResults(searchResults);
		searchResultsPayloadToCleint.setVersionDetails(versionDetails);
		searchResultsPayloadToCleint.setSubjectTitle((String) resultMap.get("P_SUBJECT_TITLE"));
		searchResultsPayloadToCleint.setSubjectTileMediaPath((String) resultMap.get("P_TILE_MEDIA_PATH"));
		searchResultsPayloadToCleint.setSubjectMediaPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.subjects")));
		searchResultsPayloadToCleint.setSubjectTileMediaId((String) resultMap.get("P_TILE_MEDIA_ID"));
		searchResultsPayloadToCleint.setIconMediaPath((String) resultMap.get("P_ICON_MEDIA_PATH"));
		searchResultsPayloadToCleint.setTotalCourseCount((String) resultMap.get("P_TOTAL_COURSE_COUNT"));
		searchResultsPayloadToCleint.setUniDefaultBgImgPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.images")));
		searchResultsPayloadToCleint.setQualificationFilters(qualificationDetails);
		searchResultsPayloadToCleint.setSubjectfilters(subjectDetails);
		searchResultsPayloadToCleint.setLocationfilters(locationDetails);
		searchResultsPayloadToCleint.setLocationTypeFilters(locationTypeDetails);
		searchResultsPayloadToCleint.setStudyModeFilters(studyModeDetails);
		searchResultsPayloadToCleint.setPreviousQualificationfilters(previousQualificationDetails);
		searchResultsPayloadToCleint.setAssessmentTypeFilterDetails(assessmentTypeFilterDetails);
		searchResultsPayloadToCleint.setReviewCategoryFilterDetails(reviewCategoryFilterDetails);
		searchResultsPayloadToCleint.setTotalUniversityCount(totalUniversitiesCount);
		searchResultsPayloadToCleint.setOrderByName(orderByNames);	
		//
		String guestUserFlag = common.guestFlag(searchPayloadFromClient.getUserId());
		searchResultsPayloadToCleint.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(searchResultsPayloadToCleint, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   searchResultsPayloadToCleint = null;
	   searchPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }

   //
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/course/course-count", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseCount(@RequestBody SearchResultsPayloadFromClient searchPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	SearchResultsPayloadToClient searchResultsPayloadToCleint = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	String courseCount = null;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(searchPayloadFromClient.getAffiliateId(), searchPayloadFromClient.getAccessToken())) {
		resultMap = searchBusiness.getCourseCount(searchPayloadFromClient);
		courseCount = (String) resultMap.get("p_course_count");
		//
		searchResultsPayloadToCleint = new SearchResultsPayloadToClient();
		searchResultsPayloadToCleint.setTotalCourseCount(courseCount);
		//
		return new ResponseEntity<GenericReturnType>(searchResultsPayloadToCleint, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
   }

   /**
    * @see This method returns the orderby name list for the search results page
    * @param selectedValue
    * @return orderByNamesList
    */
   public ArrayList<SubjectVO> getOrderByNames(String selectedValue) {
	String key[] = { "BEST_MATCH", "MOST_INFO", "STUDENT_RANKING", "COURSE_COUNT" };
	String displayName[] = { "Best match", "Most info", "Student ranking", "Number of courses" };
	ArrayList<SubjectVO> orderByNamesList = new ArrayList<SubjectVO>();
	for (int lp = 0; lp < key.length; lp++) {
	   SubjectVO subjectVO = new SubjectVO();
	   subjectVO.setOrderByName(key[lp]);
	   subjectVO.setOrderBydisplayName(displayName[lp]);
	   if (key[lp].equalsIgnoreCase(selectedValue)) {
		subjectVO.setOrderBySelected("Y");
	   } else {
		subjectVO.setOrderBySelected("N");
	   }
	   orderByNamesList.add(subjectVO);
	}
	return orderByNamesList;
   }
}
