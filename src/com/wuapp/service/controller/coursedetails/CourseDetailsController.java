package com.wuapp.service.controller.coursedetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.ICourseBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.ApplicantsDataVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.AssessedCourseVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.OppurtunityEntryReqVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.CourseInfoVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.EnquiryInfoVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.EntryReqVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.FeesDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.KeyStatsInfoVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.ModuleDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.OpportunityDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.PreviousSubjectVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.ReviewDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.UniRankingVO;
import com.wuapp.service.dao.util.valueobject.profile.LoadYearListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.coursedetials.CourseDetailsPayloadFromClient;
import com.wuapp.service.pojo.json.coursedetials.CourseDetailsPayloadToClient;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * COURSE DETAILS
 *
 * @author Indumathi.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : CourseDetailsController.java
 * Description : Controller for getting course details. 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Indumathi.S     1.0                   			Initial draft
 * Sangeeth.S      2.0              5-May-2018     Added clearing related changes,entry requirements and fees pod
 * Sangeeth.s	 3.0			20-Nov-2018	   Hided the previous study pod 
 * Hema.S          4.0              01-Aug-2019    Added whatunigo related changes
 * Hemalatha.K     5.0              25_Jun_2020    Removed clearing related changes
 */
@Controller
public class CourseDetailsController {

   @Autowired
   ICourseBusiness courseBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/course-details", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseDetails(@RequestBody CourseDetailsPayloadFromClient courseDetailsPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	CourseDetailsPayloadToClient courseDetailsPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	String advertiserFlag;
	ArrayList<CourseInfoVO> courseInfo = null;
	ArrayList<EnquiryInfoVO> enquiryInfo = null;
	ArrayList<ModuleDetailsVO> moduleDetails = null;
	ArrayList<ReviewDetailsVO> reviewList = null;
	ArrayList<OpportunityDetailsVO> opportunityDetails = null;
	ArrayList<KeyStatsInfoVO> keyStatsInfo = null;
	ArrayList<UniRankingVO> uniRankingDetails = null;
	ArrayList<AssessedCourseVO> assessedCourse = null;
	ArrayList<CourseInfoVO> otherCoursesPod = null;
	ArrayList<ApplicantsDataVO> applicantsData = null;
	ArrayList<PreviousSubjectVO> previousSubject = null;
	ArrayList<EntryReqVO> entryReqDetails = null;
	ArrayList<LoadYearListVO> loadYearList = null;
	ArrayList<AppVersionVO> versionDetails = null;
	ArrayList<OppurtunityEntryReqVO> oppurtunityEntryReqList = null;
	ArrayList<FeesDetailsVO> feesDetailsList = null;
	String collegeName = null;
	String collegeId = "";
	String universityCourseReviewURL = "";
	String moduleInfo = "";
	String openDayFlag = "";
	String keyStatsScript = "";
	String StatsScript = "";
	String applyNowUrl = "";
	String opportunityId = "";
	String courseId = "";
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(courseDetailsPayloadFromClient.getAffiliateId(), courseDetailsPayloadFromClient.getAccessToken())) {
		courseDetailsPayloadFromClient.setUserLatitude(common.round(courseDetailsPayloadFromClient.getUserLatitude(), 2));
		courseDetailsPayloadFromClient.setUserLongitude(common.round(courseDetailsPayloadFromClient.getUserLongitude(), 2));
		resultMap = courseBusiness.getCourseDetials(courseDetailsPayloadFromClient);
		courseInfo = (ArrayList<CourseInfoVO>) resultMap.get("PC_COURSE_INFO");		
		
		keyStatsInfo = (ArrayList<KeyStatsInfoVO>) resultMap.get("PC_KEY_STATS_INFO");
		if(keyStatsInfo != null &&  keyStatsInfo.size() > 0) {
		   KeyStatsInfoVO keyStatsInfoVO = (KeyStatsInfoVO) keyStatsInfo.get(0);
		   keyStatsScript = "if($('li').hasClass('br_bt')) {var keystats_html = $('#courseDetailsRootCtrl .br_bt').siblings('li').html().replace('On average UK Law grads earn','On average UK "+keyStatsInfoVO.getSubjectName() +" grads earn');$('#courseDetailsRootCtrl .br_bt').siblings('li').html(keystats_html);}";
		}
		
		
		if (courseInfo != null && courseInfo.size() > 0) {
		   for (int i = 0; i < courseInfo.size(); i++) {
			CourseInfoVO courseInfoVO = (CourseInfoVO) courseInfo.get(i);
			if (!common.isBlankOrNull(courseInfoVO.getMediaPath())) {
			   courseInfoVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, courseInfoVO.getMediaPath()));
			}
			if (!common.isBlankOrNull(courseInfoVO.getCollegeLogo())) {
			   courseInfoVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, courseInfoVO.getCollegeLogo()));
			}
			if (!common.isBlankOrNull(courseInfoVO.getLogoName())) {
			   String[] logo = courseInfoVO.getLogoName().split("/");
			   courseInfoVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + courseInfoVO.getLogoName());
			   courseInfoVO.setLogoURL(logoURL);
			}
			
			collegeName = courseInfoVO.getCollegeName();
			collegeId = courseInfoVO.getCollegeId();
			
			StatsScript = "var universityId = "+collegeId+";localStorage.setItem('universityId', universityId);$('#feeDtlsID option:contains(\"International\")').text('Rest of World');";
			
			if (!common.isBlankOrNull(courseInfoVO.getCourseSummary())) {
			   courseInfoVO.setCourseSummary(courseInfoVO.getCourseSummary()+"<script>setTimeout(function(){$('section').each(function(){if($(this).children('h2').html()==='Tuition fees'){if($(this).find('li').length>0) {if($(this).find('small').text()!=''){if($(this).find('small').text().indexOf('per year')!=-1) {$(this).find('small').html('');var span_html = $(this).find('span').html().replace('£','');$(this).find('span').html(span_html);}}}}});"+keyStatsScript+StatsScript+"},1000);</script>");
			}
		   }
		}
		
		//
		otherCoursesPod = (ArrayList<CourseInfoVO>) resultMap.get("PC_OTHER_COURSES_POD");
		if (otherCoursesPod != null && otherCoursesPod.size() > 0) {
		   for (int i = 0; i < otherCoursesPod.size(); i++) {
			CourseInfoVO courseInfoVO = (CourseInfoVO) otherCoursesPod.get(i);
			if (!common.isBlankOrNull(courseInfoVO.getMediaPath())) {
			   courseInfoVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, courseInfoVO.getMediaPath()));
			}
			if (!common.isBlankOrNull(courseInfoVO.getCollegeLogo())) {
			   courseInfoVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, courseInfoVO.getCollegeLogo()));
			}
			if (!common.isBlankOrNull(courseInfoVO.getLogoName())) {
			   String[] logo = courseInfoVO.getLogoName().split("/");
			   courseInfoVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + courseInfoVO.getLogoName());
			   courseInfoVO.setLogoURL(logoURL);
			}
		   }
		}
		moduleInfo = (String) resultMap.get("P_MODULE_INFO");
		enquiryInfo = (ArrayList<EnquiryInfoVO>) resultMap.get("PC_ENQUIRY_INFO");
		moduleDetails = (ArrayList<ModuleDetailsVO>) resultMap.get("PC_MODULE_DETAILS");
		reviewList = (ArrayList<ReviewDetailsVO>) resultMap.get("PC_REVIEW_LIST");
		opportunityDetails = (ArrayList<OpportunityDetailsVO>) resultMap.get("PC_OPPORTUNITY_DETAILS");
		if (opportunityDetails != null && opportunityDetails.size() > 0) {
		   for (int i = 0; i < opportunityDetails.size(); i++) {
			OpportunityDetailsVO opportunityDetailsVO = (OpportunityDetailsVO) opportunityDetails.get(i);
			if (!common.isBlankOrNull(opportunityDetailsVO.getOpportunityId())) {
			   opportunityId = opportunityDetailsVO.getOpportunityId();
			}
		   }
		}
		if (!common.isBlankOrNull(courseDetailsPayloadFromClient.getOpportunityId())) {
		   opportunityId = courseDetailsPayloadFromClient.getOpportunityId();
		}
		if (!common.isBlankOrNull(courseDetailsPayloadFromClient.getCourseId())) {
		   courseId = courseDetailsPayloadFromClient.getCourseId();
		}
		//keyStatsInfo = (ArrayList<KeyStatsInfoVO>) resultMap.get("PC_KEY_STATS_INFO");
		uniRankingDetails = (ArrayList<UniRankingVO>) resultMap.get("PC_UNI_RANKING_FN");
		assessedCourse = (ArrayList<AssessedCourseVO>) resultMap.get("PC_GET_ASSESSED_COURSE");
		applicantsData = (ArrayList<ApplicantsDataVO>) resultMap.get("PC_APPLICANTS_DATA");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		previousSubject = (ArrayList<PreviousSubjectVO>) resultMap.get("PC_PREVIOUS_SUBJECT");   
		entryReqDetails = (ArrayList<EntryReqVO>) resultMap.get("PC_ENTRY_REQ_DETAILS");
		advertiserFlag = (String) resultMap.get("P_ADVERTISER_FLAG");
		loadYearList = (ArrayList<LoadYearListVO>) resultMap.get("PC_LOAD_YEAR_LIST");
		openDayFlag = (String) resultMap.get("P_OD_BUTTON_FLAG");
		oppurtunityEntryReqList = (ArrayList<OppurtunityEntryReqVO>) resultMap.get("PC_OPP_ENTRY_REQ_DETAILS");
		feesDetailsList = getFeesDetails((ArrayList<FeesDetailsVO>) resultMap.get("PC_GET_FEE_DETAILS"));
		universityCourseReviewURL = common.getUniReviewURL(collegeName, collegeId, request);
		applyNowUrl = common.getDomainName(request) +"/degrees/wugo/apply-now.html?pagename=signin&courseId="+courseId+"&opportunityId="+opportunityId;
		//
		courseDetailsPayloadToClient = new CourseDetailsPayloadToClient();
		courseDetailsPayloadToClient.setCourseInfo(courseInfo);
		courseDetailsPayloadToClient.setEnquiryInfo(enquiryInfo);
		courseDetailsPayloadToClient.setModuleDetails(moduleDetails);
		courseDetailsPayloadToClient.setReviewList(reviewList);
		courseDetailsPayloadToClient.setOpportunityDetails(opportunityDetails);
		courseDetailsPayloadToClient.setKeyStatsInfo(keyStatsInfo);
		courseDetailsPayloadToClient.setUniRankingDetails(uniRankingDetails);
		courseDetailsPayloadToClient.setAssessedCourse(assessedCourse);
		courseDetailsPayloadToClient.setOtherCoursesPod(otherCoursesPod);
		courseDetailsPayloadToClient.setApplicantsData(applicantsData);
		courseDetailsPayloadToClient.setVersionDetails(versionDetails);
		courseDetailsPayloadToClient.setPreviousSubject(previousSubject);
		courseDetailsPayloadToClient.setEntryRequirementList(entryReqDetails);
		courseDetailsPayloadToClient.setAdvertiserFlag(advertiserFlag);
		courseDetailsPayloadToClient.setUniversityCourseReviewURL(universityCourseReviewURL);
		courseDetailsPayloadToClient.setModuleInfo(moduleInfo);
		courseDetailsPayloadToClient.setLoadYearList(loadYearList);
		courseDetailsPayloadToClient.setOpenDayFlag(openDayFlag);
		courseDetailsPayloadToClient.setUniDefaultBgImgPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.images")));
		courseDetailsPayloadToClient.setOppurtunityEntryReqList(oppurtunityEntryReqList);
		courseDetailsPayloadToClient.setFeesDetailsList(feesDetailsList);	
		courseDetailsPayloadToClient.setApplyNowUrl(applyNowUrl);
		//
		String guestUserFlag = common.guestFlag(courseDetailsPayloadFromClient.getUserId());
		courseDetailsPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(courseDetailsPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   courseDetailsPayloadToClient = null;
	   courseDetailsPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
   /**
    * @see This method returns the fees tooltip heading and content for the course details page
    * @param feesDetails
    * @return feesDetails
    */
   public ArrayList<FeesDetailsVO> getFeesDetails(ArrayList<FeesDetailsVO> feesDetails) {

	ArrayList<String> feeType = new ArrayList<String>();
	Collections.addAll(feeType,"England", "Scotland", "Wales", "Northern Ireland","Channel Islands");
	for (int i = 0; i < feesDetails.size(); i++) {
	   //
	   String feesDetailsFeesType = feesDetails.get(i).getFeeType(); 
  
	   if (feeType.indexOf(feesDetailsFeesType)<0) {
		if(feesDetailsFeesType.equals("International")) {
		   feesDetails.get(i).setToolTipContent(feesDetails.get(i).getFeeType()+" Students");
		   feesDetails.get(i).setToolTipHeading("<span>For students from</span>" +"<h4>Rest of World</h4>");
		} else {
		   feesDetails.get(i).setToolTipContent(feesDetails.get(i).getFeeType()+" Students");
		   feesDetails.get(i).setToolTipHeading(feesDetails.get(i).getFeeType()+" Students");
		}
	   } else {
		feesDetails.get(i).setToolTipContent("Students from "+feesDetails.get(i).getFeeType());
		feesDetails.get(i).setToolTipHeading("<span>For students from</span>" +"<h4>"+feesDetails.get(i).getFeeType()+"</h4>");
	   }
	}
	return feesDetails;
   }
}
