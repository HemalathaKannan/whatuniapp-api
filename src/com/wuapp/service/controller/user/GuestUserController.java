package com.wuapp.service.controller.user;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IUserBusiness;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.user.GuestUserPayLoadToClient;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * GUEST USER LOGIN
 *
 * @author Hema.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	         Modified On     	  Modification Details 	                
 * *************************************************************************************************************************
 * Hemalatha.K            2.0           25_JUN_2020          Removed clearing related changes and added clearing switch change
 */
 
@Controller
public class GuestUserController {
   
   @Autowired
   IUserBusiness userBusiness = null;
   
   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/guest-user", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> guestUser(@RequestBody UserPayloadFromClient userPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	GuestUserPayLoadToClient guestUserPayLoadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	String guestUserId = null;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userPayloadFromClient.getAffiliateId(), userPayloadFromClient.getAccessToken())) {
		resultMap = userBusiness.getGuestUserId();
		guestUserId = (String)resultMap.get("P_GUEST_USER_ID");
		guestUserPayLoadToClient = new GuestUserPayLoadToClient();
		guestUserPayLoadToClient.setGuestUserId(guestUserId);
		guestUserPayLoadToClient.setGuestUserFlag("Y");
		guestUserPayLoadToClient.setAppClearingBanner((String) resultMap.get("P_CL_INTERSTITIAL_FLAG"));
		guestUserPayLoadToClient.setAppClearingurl((String) resultMap.get("P_CLEARING_URL"));
	   }
	   return new ResponseEntity<GenericReturnType>(guestUserPayLoadToClient, HttpStatus.OK);
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   guestUserPayLoadToClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
