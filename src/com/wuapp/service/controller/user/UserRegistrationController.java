package com.wuapp.service.controller.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IUserBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;
import com.wuapp.service.pojo.json.user.UserPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * USER REGISTRATION PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	         Modified On     	  Modification Details 	                
 * *************************************************************************************************************************
 * Hemalatha.K            2.0                25_JUN_2020            Removed clearing related changes and added clearing switch change
 */

@Controller
public class UserRegistrationController {

   @Autowired
   IUserBusiness userBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/user/registration/", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> userRegistration(@RequestBody UserPayloadFromClient userPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	UserPayloadToClient userPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	String userId = null;
	String responseCode = null;
	ArrayList<AppVersionVO> versionDetails;
	CommonUtilities commonUtilities = new CommonUtilities();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(userPayloadFromClient.getAffiliateId(), userPayloadFromClient.getAccessToken())) {
		userPayloadFromClient.setUserIp(commonUtilities.getUserIp(request));
		userPayloadFromClient.setLatitude(commonUtilities.round(userPayloadFromClient.getLatitude(), 2));
		userPayloadFromClient.setLongitude(commonUtilities.round(userPayloadFromClient.getLongitude(), 2));
		resultMap = userBusiness.userRegistration(userPayloadFromClient);
		userId = (String) resultMap.get("P_USER_ID");
		responseCode = (String) resultMap.get("P_ERROR_MSG");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		//
		userPayloadToClient = new UserPayloadToClient();
		userPayloadToClient.setUserId(userId);
		userPayloadToClient.setResponseCode(responseCode);
		userPayloadToClient.setVersionDetails(versionDetails);
		userPayloadToClient.setAppClearingBanner((String) resultMap.get("P_CL_INTERSTITIAL_FLAG"));
		userPayloadToClient.setAppClearingurl((String) resultMap.get("P_CLEARING_URL"));
		//
		return new ResponseEntity<GenericReturnType>(userPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   userPayloadToClient = null;
	   userPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
