package com.wuapp.service.controller.profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IProfileBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.profile.AccomodationVO;
import com.wuapp.service.dao.util.valueobject.profile.CompetitorDetailVO;
import com.wuapp.service.dao.util.valueobject.profile.EnquiryDetailsVO;
import com.wuapp.service.dao.util.valueobject.profile.KeyStatsInfoVO;
import com.wuapp.service.dao.util.valueobject.profile.LoadYearListVO;
import com.wuapp.service.dao.util.valueobject.profile.OpendayDetailsVO;
import com.wuapp.service.dao.util.valueobject.profile.ProfileVO;
import com.wuapp.service.dao.util.valueobject.profile.ReviewStarRatingVO;
import com.wuapp.service.dao.util.valueobject.profile.StudentReviewVO;
import com.wuapp.service.dao.util.valueobject.profile.UniInfoVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadFromClient;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * PROFILE LANDING
 *
 * @author Indumathi.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * @Modify : 01_Nov_2017 - Added opendayFlag for showing opendays button IP page by Prabha
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K       1.2           06-MAY-2020     Added mapbox key and COVID related article changes 
 * Hemalatha.K       2.0           25-JUN-2020     Removed clearing related changes
 */
@Controller
public class ProfileLandingController {

   @Autowired
   IProfileBusiness profileBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/profile/uni-info", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseSearchResults(@RequestBody ProfileDetailsPayloadFromClient profileDetailsPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	ProfileDetailsPayloadToClient profileDetailsPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<UniInfoVO> universityInfo;
	ArrayList<AppVersionVO> versionDetails;
	ArrayList<ProfileVO> uniProfileContent;
	ArrayList<KeyStatsInfoVO> keyStatsInfo;
	ArrayList<ReviewStarRatingVO> reviewStarRating;
	ArrayList<StudentReviewVO> studentReview;
	ArrayList<OpendayDetailsVO> opendayDetails;
	ArrayList<EnquiryDetailsVO> enquiryDetails;
	ArrayList<AccomodationVO> accomodationDetails;
	ArrayList<CompetitorDetailVO> competitorDetails;
	ArrayList<LoadYearListVO> loadYearList;
	String advertiserFlag;
	String collegeName;
	String collegeId = "";
	String universityCourseReviewURL = "";
	String opendayFlag;
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(profileDetailsPayloadFromClient.getAffiliateId(), profileDetailsPayloadFromClient.getAccessToken())) {
		resultMap = profileBusiness.getProfileDetails(profileDetailsPayloadFromClient);
		universityInfo = (ArrayList<UniInfoVO>) resultMap.get("PC_UNI_INFO");
		if (universityInfo != null && universityInfo.size() > 0) {
		   for (int i = 0; i < universityInfo.size(); i++) {
			UniInfoVO uniInfoVO = (UniInfoVO) universityInfo.get(i);
			collegeId = uniInfoVO.getCollegeId();
			if (!common.isBlankOrNull(uniInfoVO.getTileMediaPath())) {
			   uniInfoVO.setTileMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, uniInfoVO.getTileMediaPath()));
			}
			if (!common.isBlankOrNull(uniInfoVO.getCollegeLogo())) {
			   uniInfoVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, uniInfoVO.getCollegeLogo()));
			}
			if (!common.isBlankOrNull(uniInfoVO.getLogoName())) {
			   String[] logo = uniInfoVO.getLogoName().split("/");
			   uniInfoVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + uniInfoVO.getLogoName());
			   uniInfoVO.setLogoURL(logoURL);
			}
		   }
		}
		uniProfileContent = (ArrayList<ProfileVO>) resultMap.get("PC_UNI_PROFILE_CONTENT");
		if (uniProfileContent != null && uniProfileContent.size() > 0) {
		   for (int i = 0; i < uniProfileContent.size(); i++) {
			ProfileVO profileVO = (ProfileVO) uniProfileContent.get(i);
			if (!common.isBlankOrNull(profileVO.getImagePath())) {
			   profileVO.setImagePath(EmbeddedObject.getInstance().getUploadedImagePath(request, profileVO.getImagePath()));
			}
		   }
		}
		competitorDetails = (ArrayList<CompetitorDetailVO>) resultMap.get("PC_COMPETITOR_DETAIL");
		if (competitorDetails != null && competitorDetails.size() > 0) {
		   for (int i = 0; i < competitorDetails.size(); i++) {
			CompetitorDetailVO competitorDetailVO = (CompetitorDetailVO) competitorDetails.get(i);
			if (!common.isBlankOrNull(competitorDetailVO.getCollegeLogo())) {
			   competitorDetailVO.setCollegeLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, competitorDetailVO.getCollegeLogo()));
			}
			if (!common.isBlankOrNull(competitorDetailVO.getLogoName())) {
			   String[] logo = competitorDetailVO.getLogoName().split("/");
			   competitorDetailVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + competitorDetailVO.getLogoName());
			   competitorDetailVO.setLogoURL(logoURL);
			}
		   }
		}
		keyStatsInfo = (ArrayList<KeyStatsInfoVO>) resultMap.get("PC_KEY_STATS_INFO");
		reviewStarRating = (ArrayList<ReviewStarRatingVO>) resultMap.get("PC_REVIEW_STAR_RATING");
		studentReview = (ArrayList<StudentReviewVO>) resultMap.get("PC_STUDENT_REVIEW");
		opendayDetails = (ArrayList<OpendayDetailsVO>) resultMap.get("PC_OPENDAYS_DETAILS");
		enquiryDetails = (ArrayList<EnquiryDetailsVO>) resultMap.get("PC_ENQUIRY_DETAILS");
		accomodationDetails = (ArrayList<AccomodationVO>) resultMap.get("PC_ACCOMODATION_DET");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		advertiserFlag = (String) resultMap.get("P_ADV_FLAG");
		collegeName = (String) resultMap.get("P_COLLEGE_NAME");
		loadYearList = (ArrayList<LoadYearListVO>) resultMap.get("PC_LOAD_YEAR_LIST");
		opendayFlag = (String) resultMap.get("P_OD_BUTTON_FLAG"); //Added for showing openday button by Prabha
		universityCourseReviewURL = common.getUniReviewURL(collegeName, collegeId, request);
		String mapBoxKey = (String) resultMap.get("P_MAP_BOX_KEY");		
		//
		profileDetailsPayloadToClient = new ProfileDetailsPayloadToClient();
		profileDetailsPayloadToClient.setUniInfo(universityInfo);
		profileDetailsPayloadToClient.setUniProfileContent(uniProfileContent);
		profileDetailsPayloadToClient.setKeyStatsInfo(keyStatsInfo);
		profileDetailsPayloadToClient.setReviewStarRating(reviewStarRating);
		profileDetailsPayloadToClient.setStudentReview(studentReview);
		profileDetailsPayloadToClient.setOpendayDetails(opendayDetails);
		profileDetailsPayloadToClient.setEnquiryDetails(enquiryDetails);
		profileDetailsPayloadToClient.setAccomodationDetails(accomodationDetails);
		profileDetailsPayloadToClient.setCompetitorDetails(competitorDetails);
		profileDetailsPayloadToClient.setVersionDetails(versionDetails);
		profileDetailsPayloadToClient.setAdvertiserFlag(advertiserFlag);
		profileDetailsPayloadToClient.setCollegeName(collegeName);
		profileDetailsPayloadToClient.setOpendayFlag(opendayFlag);
		profileDetailsPayloadToClient.setLoadYearList(loadYearList);
		profileDetailsPayloadToClient.setUniversityCourseReviewURL(universityCourseReviewURL);
		profileDetailsPayloadToClient.setUniDefaultBgImgPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.images")));
		//
		String guestUserFlag = common.guestFlag(profileDetailsPayloadFromClient.getUserId());
		profileDetailsPayloadToClient.setGuestUserFlag(guestUserFlag);
		profileDetailsPayloadToClient.setMapBoxKey(mapBoxKey);
		profileDetailsPayloadToClient.setArticleFlag(WhatuniAppConstants.COVID_19_ARTICLE_FLAG);
		profileDetailsPayloadToClient.setArticleContent(WhatuniAppConstants.COVID_19_PROFILE_CONTENT);
		//
		return new ResponseEntity<GenericReturnType>(profileDetailsPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   profileDetailsPayloadToClient = null;
	   profileDetailsPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
