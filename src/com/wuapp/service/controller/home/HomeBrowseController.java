package com.wuapp.service.controller.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IHomeBusiness;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.AddressVO;
import com.wuapp.service.dao.util.valueobject.home.FeaturedProviderVO;
import com.wuapp.service.dao.util.valueobject.home.FeaturedSlotsVO;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.home.HomePayloadFromClient;
import com.wuapp.service.pojo.json.home.HomePayloadToCleint;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * COURSE DETAILS
 *
 * @author Indumathi.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : CourseDetailsController.java
 * Description : Controller for getting course details. 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Indumathi.S     1.0                   			Initial draft
 * Sangeeth.S      2.0              22-May-2018     Added featured provider pod for the clearing 
 * Hemalatha.K     3.0              04-FEB-2019     Added featured university, course and openday pod
 * Hema.S          4.0              21-APR-2020     Added COVID related article changes 
 * Hemalatha.K     5.0              25-Jun-2020     Removed clearing related changes and added clearing switch flag & url
 */

@Controller
public class HomeBrowseController {

   @Autowired
   IHomeBusiness homeBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/home-browse", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getBrowseResults(@RequestBody HomePayloadFromClient homePayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & varialbes
	//
	HomePayloadToCleint homePayloadToCleint = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<SubjectVO> recentSubjectList;
	ArrayList<SubjectVO> otherSubjectList;
	ArrayList<InstitutionVO> institutionsCloseToYou;
	ArrayList<InstitutionVO> institutionsWithUpCommingOpendays;
	ArrayList<AddressVO> address;
	ArrayList<AppVersionVO> versionDetails;
	ArrayList<FeaturedProviderVO> featuredProviderList;
	//
	ArrayList<FeaturedSlotsVO> featuredUniversity;
	ArrayList<FeaturedSlotsVO> featuredCourse;
	ArrayList<FeaturedSlotsVO> featuredOpenday;
	FeaturedSlotsVO featuredSlotsVO = new FeaturedSlotsVO();
	//
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(homePayloadFromClient.getAffiliateId(), homePayloadFromClient.getAccessToken())) {
		homePayloadFromClient.setLatitude(common.round(homePayloadFromClient.getLatitude(), 2));
		homePayloadFromClient.setLongitude(common.round(homePayloadFromClient.getLongitude(), 2));
		resultMap = homeBusiness.getBrowseResults(homePayloadFromClient);
		recentSubjectList = (ArrayList<SubjectVO>) resultMap.get("PC_SEARCH_CATEGORIES");
		if (recentSubjectList != null && recentSubjectList.size() > 0) {
		   for (int i = 0; i < recentSubjectList.size(); i++) {
			SubjectVO subjectVO = (SubjectVO) recentSubjectList.get(i);
			if (!common.isBlankOrNull(subjectVO.getTileMediaPath())) {
			   subjectVO.setTileMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, subjectVO.getTileMediaPath()));
			}
			subjectVO.setMediaPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.subjects")));
		   }
		}
		otherSubjectList = (ArrayList<SubjectVO>) resultMap.get("PC_CATEGORIES");
		if (otherSubjectList != null && otherSubjectList.size() > 0) {
		   for (int i = 0; i < otherSubjectList.size(); i++) {
			SubjectVO subjectVO = (SubjectVO) otherSubjectList.get(i);
			if (!common.isBlankOrNull(subjectVO.getTileMediaPath())) {
			   subjectVO.setTileMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, subjectVO.getTileMediaPath()));
			}
			subjectVO.setMediaPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.subjects")));
		   }
		}
		institutionsCloseToYou = (ArrayList<InstitutionVO>) resultMap.get("PC_NEARBY_INSTITUTIONS");
		if (institutionsCloseToYou != null && institutionsCloseToYou.size() > 0) {
		   for (int i = 0; i < institutionsCloseToYou.size(); i++) {
			InstitutionVO institutionVO = (InstitutionVO) institutionsCloseToYou.get(i);
			if (!common.isBlankOrNull(institutionVO.getTileMediaPath())) {
			   institutionVO.setTileMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionVO.getTileMediaPath()));
			}
			if (!common.isBlankOrNull(institutionVO.getLogo())) {
			   institutionVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionVO.getLogo()));
			}
			if (!common.isBlankOrNull(institutionVO.getLogoName())) {
			   String[] logo = institutionVO.getLogoName().split("/");
			   institutionVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + institutionVO.getLogoName());
			   institutionVO.setLogoURL(logoURL);
			}
		   }
		}
		institutionsWithUpCommingOpendays = (ArrayList<InstitutionVO>) resultMap.get("PC_OPENDAY_INSTITUTIONS");
		if (institutionsWithUpCommingOpendays != null && institutionsWithUpCommingOpendays.size() > 0) {
		   for (int i = 0; i < institutionsWithUpCommingOpendays.size(); i++) {
			InstitutionVO institutionVO = (InstitutionVO) institutionsWithUpCommingOpendays.get(i);
			if (!common.isBlankOrNull(institutionVO.getTileMediaPath())) {
			   institutionVO.setTileMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionVO.getTileMediaPath()));
			}
			if (!common.isBlankOrNull(institutionVO.getLogo())) {
			   institutionVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, institutionVO.getLogo()));
			}
			if (!common.isBlankOrNull(institutionVO.getLogoName())) {
			   String[] logo = institutionVO.getLogoName().split("/");
			   institutionVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + institutionVO.getLogoName());
			   institutionVO.setLogoURL(logoURL);
			}
		   }
		}
		address = (ArrayList<AddressVO>) resultMap.get("PC_ADDRESS");
		versionDetails = (ArrayList<AppVersionVO>) resultMap.get("PC_VERSION_DETAILS");
		//Modified Id- 2.0 starts
		featuredProviderList = (ArrayList<FeaturedProviderVO>) resultMap.get("PC_CL_FEATURED_PROVIDER");
		if (featuredProviderList != null && featuredProviderList.size() > 0) {
		   for (int i = 0; i < featuredProviderList.size(); i++) {
			FeaturedProviderVO featuredProviderVO = (FeaturedProviderVO) featuredProviderList.get(i);
			if (!common.isBlankOrNull(featuredProviderVO.getMediaPath())) {
			   featuredProviderVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredProviderVO.getMediaPath()));
			}
			if (!common.isBlankOrNull(featuredProviderVO.getLogo())) {
			   featuredProviderVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredProviderVO.getLogo()));
			}
		   }
		}
		//
		featuredUniversity = (ArrayList<FeaturedSlotsVO>) resultMap.get("PC_FEATURED_INSTITUTION");
		if (featuredUniversity != null && featuredUniversity.size() > 0) {
		   featuredSlotsVO = (FeaturedSlotsVO) featuredUniversity.get(0);
		   if (!common.isBlankOrNull(featuredSlotsVO.getMediaPath())) {
			featuredSlotsVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredSlotsVO.getMediaPath()));
		   }
		   if (!common.isBlankOrNull(featuredSlotsVO.getLogo())) {
			featuredSlotsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredSlotsVO.getLogo()));
		   }
		}
		featuredCourse = (ArrayList<FeaturedSlotsVO>) resultMap.get("PC_FEATURED_COURSE");
		if (featuredCourse != null && featuredCourse.size() > 0) {
		   featuredSlotsVO = (FeaturedSlotsVO) featuredCourse.get(0);
		   if (!common.isBlankOrNull(featuredSlotsVO.getMediaPath())) {
			featuredSlotsVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredSlotsVO.getMediaPath()));
		   }
		   if (!common.isBlankOrNull(featuredSlotsVO.getLogo())) {
			featuredSlotsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredSlotsVO.getLogo()));
		   }
		}
		featuredOpenday = (ArrayList<FeaturedSlotsVO>) resultMap.get("PC_FEATURED_OPENDAYS");
		if (featuredOpenday != null && featuredOpenday.size() > 0) {
		   featuredSlotsVO = (FeaturedSlotsVO) featuredOpenday.get(0);
		   if (!common.isBlankOrNull(featuredSlotsVO.getMediaPath())) {
			featuredSlotsVO.setMediaPath(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredSlotsVO.getMediaPath()));
		   }
		   if (!common.isBlankOrNull(featuredSlotsVO.getLogo())) {
			featuredSlotsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, featuredSlotsVO.getLogo()));
		   }
		}
		//
		//Modified Id- 2.0 ends
		//
		homePayloadToCleint = new HomePayloadToCleint();
		homePayloadToCleint.setUserId(homePayloadFromClient.getUserId());
		homePayloadToCleint.setUserName((String) resultMap.get("P_USER_NAME"));
		homePayloadToCleint.setPostcode((String) resultMap.get("P_POST_CODE"));
		homePayloadToCleint.setUniDefaultBgImgPath(EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.images")));
		homePayloadToCleint.setRecentSubjects(recentSubjectList);
		homePayloadToCleint.setOtherSubjects(otherSubjectList);
		homePayloadToCleint.setInstitutionsCloseToYou(institutionsCloseToYou);
		homePayloadToCleint.setInstitutionsWithUpCommingOpendays(institutionsWithUpCommingOpendays);
		homePayloadToCleint.setAddress(address);
		homePayloadToCleint.setVersionDetails(versionDetails);
		homePayloadToCleint.setFeaturedProviderList(featuredProviderList);
		homePayloadToCleint.setCurrentYear((String) resultMap.get("P_CURRENT_YEAR"));
		//
		homePayloadToCleint.setFeaturedUniversity(featuredUniversity);
		homePayloadToCleint.setFeaturedCourse(featuredCourse);
		homePayloadToCleint.setFeaturedOpenDay(featuredOpenday);
		homePayloadToCleint.setArticleFlag(WhatuniAppConstants.COVID_19_ARTICLE_FLAG);
		String articleContent = WhatuniAppConstants.COVID_19_HOME_CONTENT;
		homePayloadToCleint.setArticleContent(articleContent);
		//
		String guestUserFlag = common.guestFlag(homePayloadFromClient.getUserId());
		homePayloadToCleint.setGuestUserFlag(guestUserFlag);
		homePayloadToCleint.setAppClearingBanner((String) resultMap.get("P_CLEARING_SWITCH_FLAG"));
		homePayloadToCleint.setAppClearingurl((String) resultMap.get("P_CLEARING_URL"));
		//
		return new ResponseEntity<GenericReturnType>(homePayloadToCleint, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   homePayloadToCleint = null;
	   homePayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	   resultMap = null;
	}
   }
}
