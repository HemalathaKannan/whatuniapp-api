package com.wuapp.service.controller.home;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * HOME PAGE DETAILS
 *
 * @author Bhoovi arashan P
 * @since wuapp1.0_20170315 - intial draft
 * @version 1.1
 */
@Controller
public class HomeController {

   @RequestMapping(value = { "/home" })
   public String homeController(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
	return "home-tile";
   }
}
