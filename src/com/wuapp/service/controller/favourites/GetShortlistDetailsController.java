package com.wuapp.service.controller.favourites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IFavouritesBusiness;
import com.wuapp.service.dao.util.valueobject.favourites.ShortlistDetailsVO;
import com.wuapp.service.dao.util.valueobject.favourites.SuggestionDetailsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.EmbeddedObject;
import com.wuapp.util.environment.EstablishEnvironment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika.P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class GetShortlistDetailsController {

   @Autowired
   IFavouritesBusiness favouritesBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/shortlist-details", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseDetails(@RequestBody FavouritesFormPayloadFromClient favouritesFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	FavouritesFormPayloadToClient favouritesFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	ArrayList<ShortlistDetailsVO> shortlistDetails = null;
	ArrayList<SuggestionDetailsVO> suggestionDetails = null;
	String shortlistExistFlag = null;
	CommonUtilities common = new CommonUtilities();
	ResourceBundle bundle = EstablishEnvironment.getInstance().getBundle();
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(favouritesFormPayloadFromClient.getAffiliateId(), favouritesFormPayloadFromClient.getAccessToken())) {
		favouritesFormPayloadFromClient.setLatitude(common.round(favouritesFormPayloadFromClient.getLatitude(), 2));
		favouritesFormPayloadFromClient.setLongitude(common.round(favouritesFormPayloadFromClient.getLongitude(), 2));
		resultMap = favouritesBusiness.getShortlistDetials(favouritesFormPayloadFromClient);
		shortlistExistFlag = (String) resultMap.get("p_shortlist_exist_flag");
		shortlistDetails = (ArrayList<ShortlistDetailsVO>) resultMap.get("pc_shortlist_list");
		suggestionDetails = (ArrayList<SuggestionDetailsVO>) resultMap.get("pc_suggestion_list");
		if (shortlistDetails != null && shortlistDetails.size() > 0) {
		   for (int i = 0; i < shortlistDetails.size(); i++) {
			ShortlistDetailsVO shortlistDetailsVO = (ShortlistDetailsVO) shortlistDetails.get(i);
			if (!common.isBlankOrNull(shortlistDetailsVO.getLogo())) {
			   shortlistDetailsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, shortlistDetailsVO.getLogo()));
			}
			if (!common.isBlankOrNull(shortlistDetailsVO.getLogoName())) {
			   String[] logo = shortlistDetailsVO.getLogoName().split("/");
			   shortlistDetailsVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + shortlistDetailsVO.getLogoName());
			   shortlistDetailsVO.setLogoURL(logoURL);
			}
		   }
		}
		//		
		if (suggestionDetails != null && suggestionDetails.size() > 0) {
		   for (int i = 0; i < suggestionDetails.size(); i++) {
			SuggestionDetailsVO suggestionDetailsVO = (SuggestionDetailsVO) suggestionDetails.get(i);
			if (!common.isBlankOrNull(suggestionDetailsVO.getLogo())) {
			   suggestionDetailsVO.setLogo(EmbeddedObject.getInstance().getUploadedImagePath(request, suggestionDetailsVO.getLogo()));
			}
			if (!common.isBlankOrNull(suggestionDetailsVO.getLogoName())) {
			   String[] logo = suggestionDetailsVO.getLogoName().split("/");
			   suggestionDetailsVO.setLogoName(logo[logo.length - 1]);
			   String logoURL = EmbeddedObject.getInstance().getImgPath(request, 0, bundle.getString("cont.folder.img.unis.logos") + suggestionDetailsVO.getLogoName());
			   suggestionDetailsVO.setLogoURL(logoURL);
			}
		   }
		}
		//
		favouritesFormPayloadToClient = new FavouritesFormPayloadToClient();
		favouritesFormPayloadToClient.setShortlistExistFlag(shortlistExistFlag);
		favouritesFormPayloadToClient.setShortlistDetails(shortlistDetails);
		favouritesFormPayloadToClient.setSuggestionDetails(suggestionDetails);
		//	
		String guestUserFlag = common.guestFlag(favouritesFormPayloadFromClient.getUserId());
		favouritesFormPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(favouritesFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   favouritesFormPayloadToClient = null;
	   favouritesFormPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
