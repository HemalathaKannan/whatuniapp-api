package com.wuapp.service.controller.favourites;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wuapp.service.business.IFavouritesBusiness;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import com.wuapp.service.pojo.json.exception.ExceptionJSON;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadToClient;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.WhatuniAppConstants;
import com.wuapp.util.developer.Debugger;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika.P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
@Controller
public class AddRemoveShortlistorFinal5Controller {

   @Autowired
   IFavouritesBusiness favouritesBusiness = null;

   @SuppressWarnings("unchecked")
   @RequestMapping(value = "/add-remove-shortlist-or-final5", method = RequestMethod.POST, produces = { "application/json", "application/xml" }, consumes = { "application/json", "application/xml" })
   public ResponseEntity<GenericReturnType> getCourseDetails(@RequestBody FavouritesFormPayloadFromClient favouritesFormPayloadFromClient, HttpServletRequest request, HttpServletResponse response) {
	//
	// ---(A)--- DECLARE local objects & variables
	//
	FavouritesFormPayloadToClient favouritesFormPayloadToClient = null;
	ExceptionJSON exceptionJSON;
	Debugger debug = Debugger.getInstance();
	Map<String, Object> resultMap = new HashMap<String, Object>();
	CommonUtilities common = new CommonUtilities();
	String returnFlag = null;
	//
	// ---(C)--- Do the core business logic.
	//
	try {
	   //
	   // ---(B)--- To check the access token.
	   //
	   if (StaticDataPreLoader.getInstance().isValidAccessToken(favouritesFormPayloadFromClient.getAffiliateId(), favouritesFormPayloadFromClient.getAccessToken())) {
		favouritesFormPayloadFromClient.setUserIP(common.getUserIp(request));
		favouritesFormPayloadFromClient.setLatitude(common.round(favouritesFormPayloadFromClient.getLatitude(), 2));
		favouritesFormPayloadFromClient.setLongitude(common.round(favouritesFormPayloadFromClient.getLongitude(), 2));
		resultMap = favouritesBusiness.addOrRemoveShortlist(favouritesFormPayloadFromClient);
		returnFlag = (String) resultMap.get("P_RETURN_FLAG");
		//
		favouritesFormPayloadToClient = new FavouritesFormPayloadToClient();
		favouritesFormPayloadToClient.setReturnFlag(returnFlag);
		//	
		String guestUserFlag = common.guestFlag(favouritesFormPayloadFromClient.getUserId());
		favouritesFormPayloadToClient.setGuestUserFlag(guestUserFlag);
		//
		return new ResponseEntity<GenericReturnType>(favouritesFormPayloadToClient, HttpStatus.OK);
	   } else {
		exceptionJSON = new ExceptionJSON();
		exceptionJSON.setStatus(WhatuniAppConstants.ERROR_CODE_401);
		exceptionJSON.setMessage(WhatuniAppConstants.TOKEN_INVALID);
		exceptionJSON.setException(WhatuniAppConstants.AUTHORIZATION_FAILED);
		return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	//
	// ---(D)--- Print the exception traces with URL & CLASS details. 
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	   //
	   exceptionJSON = new ExceptionJSON();
	   exceptionJSON.setStatus((HttpStatus.INTERNAL_SERVER_ERROR).toString());
	   exceptionJSON.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	   exceptionJSON.setException(e.getMessage());
	   //
	   return new ResponseEntity<GenericReturnType>(exceptionJSON, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	//
	//  ---(Y)--- Nullify all unused local objects for GC
	// 
	finally {
	   favouritesFormPayloadToClient = null;
	   favouritesFormPayloadFromClient = null;
	   debug = null;
	   exceptionJSON = null;
	}
   }
}
