package com.wuapp.service.dao;

import java.util.Map;
import com.wuapp.service.pojo.json.course.SearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchFormPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.coursedetials.CourseDetailsPayloadFromClient;

public interface ICourseDAO {

   public Map<String, Object> getSearchResults(SearchResultsPayloadFromClient searchResultsPayloadFromClient);

   public Map<String, Object> getSearchFormList(SearchFormPayloadFromClient searchFormPayloadFromClient);

   public Map<String, Object> getSearchAjaxList(SearchAjaxPayloadFromClient searchAjaxPayloadFromClient);

   public Map<String, Object> getCourseCount(SearchResultsPayloadFromClient searchPayloadFromClient);

   public Map<String, Object> getCourseDetials(CourseDetailsPayloadFromClient courseDetailsPayloadFromClient);
}
