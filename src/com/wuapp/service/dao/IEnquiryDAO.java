package com.wuapp.service.dao;

import java.util.Map;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayPayloadFromClient;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public interface IEnquiryDAO {

   //
   public Map<String, Object> getEnquiryFormList(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient);

   public Map<String, Object> submitEnquiryForm(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient);

   public Map<String, Object> saveOpenDays(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient);

   public Map<String, Object> getAjaxAddressList(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient);

   public Map<String, Object> openDaysStatsLog(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient);

   public Map<String, Object> getOpendaysData(OpendayPayloadFromClient opendayPayloadFromClient);
   //
}
