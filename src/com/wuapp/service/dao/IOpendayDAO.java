package com.wuapp.service.dao;

import java.util.Map;
import com.wuapp.service.pojo.json.openday.OpendayBookingFormSubmitFromClient;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayShowBookingFormFromClient;
import com.wuapp.service.pojo.json.openday.SubjectAjaxPayloadFromClient;

/**
 * @author : Hemalatha K
 * @since : 06_MAY_2020
 * @version : 1.0
 * Class : IOpendayDAO.java
 * Description : This interface is used to getting provider opendays info
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id      Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hema.S              1.1           06_MAY_2020      Added booking form show and submit method
 */

public interface IOpendayDAO {
   
   public Map<String, Object> getOpendayProviderDetails(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient);   
   public Map<String, Object> getMoreOpendayList(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient);
   public Map<String, Object> getBookingFormSubmitList(OpendayBookingFormSubmitFromClient opendayBookingFormSubmitFromClient);
   public Map<String, Object> getShowBookingFormList(OpendayShowBookingFormFromClient opendayShowBookingFormFromClient);
   public Map<String, Object> getSubjectAjaxList(SubjectAjaxPayloadFromClient subjectAjaxPayloadFromClient);
   public Map<String, Object> getOpendaySearchList(OpendaySearchLandingPayloadFromClient opendaySearchLandingPayloadFromClient);
   public Map<String, Object> getOpendaySearchAjaxList(OpendaySearchAjaxPayloadFromClient opendaySearchAjaxPayloadFromClient);
   public Map<String, Object> getOpendaySearchResults(OpendaySearchResultsPayloadFromClient opendaySearchResultsPayloadFromClient);
}
