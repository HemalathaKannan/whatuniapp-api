package com.wuapp.service.dao;

import java.util.HashMap;
import java.util.Map;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.InteractionPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.SaveActionsPayloadFromClient;

/**
 * DAO to manage common data
 *
 * @author Bhoovi arashan P
 * @since wuapp1.0_20170315 - initial draft
 * @version 1.1
 */
public interface ICommonDAO {

   public Map<String, Object> preLoadStaticData(HashMap<String, String> parameters);

   public Map<String, Object> dbStatsLogging(InteractionPayloadFromClient interactionPayloadFromClient);

   public Map<String, Object> saveUserActions(SaveActionsPayloadFromClient saveActionsPayloadFromClient);

   public Map<String, Object> getClearingFlag(GetClearingFlagPayloadFromClient getClearingFlagPayloadFromClient);
}
