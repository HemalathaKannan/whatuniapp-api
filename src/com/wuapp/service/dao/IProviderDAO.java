package com.wuapp.service.dao;

import java.util.Map;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadFromClient;

public interface IProviderDAO {

   public Map<String, Object> getProviderResults(ProviderResultsPayloadFromClient providerResultsPayloadFromClient);
}
