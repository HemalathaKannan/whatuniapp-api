package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IEnquiryDAO;
import com.wuapp.service.dao.sp.enquriy.GetUserAddressSP;
import com.wuapp.service.dao.sp.enquriy.OpenDayStatsLoggingSP;
import com.wuapp.service.dao.sp.enquriy.SaveOpenDaysSP;
import com.wuapp.service.dao.sp.enquriy.ShowEnquiryFormSP;
import com.wuapp.service.dao.sp.enquriy.SubmitEnquiryFormSP;
import com.wuapp.service.dao.sp.openday.ReservePlaceSP;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayPayloadFromClient;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class EnquiryDAOImpl implements IEnquiryDAO {

   public Map<String, Object> getEnquiryFormList(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	ShowEnquiryFormSP enquiryFormSP = new ShowEnquiryFormSP(dataSource);
	Map<String, Object> resultMap = enquiryFormSP.executeProcedure(enquiryFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> submitEnquiryForm(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	SubmitEnquiryFormSP submitEnquiryFormSP = new SubmitEnquiryFormSP(dataSource);
	Map<String, Object> resultMap = submitEnquiryFormSP.executeProcedure(enquiryFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveOpenDays(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	SaveOpenDaysSP saveOpenDays = new SaveOpenDaysSP(dataSource);
	Map<String, Object> resultMap = saveOpenDays.executeProcedure(enquiryFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getAjaxAddressList(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	GetUserAddressSP getUserAddressSP = new GetUserAddressSP(dataSource);
	Map<String, Object> resultMap = getUserAddressSP.executeProcedure(enquiryFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> openDaysStatsLog(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	OpenDayStatsLoggingSP openDayStatsLoggingSP = new OpenDayStatsLoggingSP(dataSource);
	Map<String, Object> resultMap = openDayStatsLoggingSP.executeProcedure(enquiryFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getOpendaysData(OpendayPayloadFromClient opendayPayloadFromClient) {
	ReservePlaceSP reservePlaceSP = new ReservePlaceSP(dataSource);
	Map<String, Object> resultMap = reservePlaceSP.executeProcedure(opendayPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
      * 
      */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
