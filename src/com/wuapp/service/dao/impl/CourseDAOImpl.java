package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.ICourseDAO;
import com.wuapp.service.dao.sp.course.GetCourseCountSP;
import com.wuapp.service.dao.sp.course.SearchAjaxListSP;
import com.wuapp.service.dao.sp.course.SearchFormListSP;
import com.wuapp.service.dao.sp.course.SearchResultsSP;
import com.wuapp.service.dao.sp.coursedetails.GetCourseDetailsSP;
import com.wuapp.service.pojo.json.course.SearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchFormPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.coursedetials.CourseDetailsPayloadFromClient;

public class CourseDAOImpl implements ICourseDAO {

   public Map<String, Object> getSearchResults(SearchResultsPayloadFromClient searchPayloadFromClient) {
	SearchResultsSP searchResultsSP = new SearchResultsSP(dataSource);
	Map<String, Object> resultMap = searchResultsSP.executeProcedure(searchPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getSearchFormList(SearchFormPayloadFromClient searchFormPayloadFromClient) {
	SearchFormListSP searchFormListSP = new SearchFormListSP(dataSource);
	Map<String, Object> resultMap = searchFormListSP.executeProcedure(searchFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getSearchAjaxList(SearchAjaxPayloadFromClient searchAjaxPayloadFromClient) {
	SearchAjaxListSP searchAjaxListSP = new SearchAjaxListSP(dataSource);
	Map<String, Object> resultMap = searchAjaxListSP.executeProcedure(searchAjaxPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getCourseCount(SearchResultsPayloadFromClient searchPayloadFromClient) {
	GetCourseCountSP getCourseCountSP = new GetCourseCountSP(dataSource);
	Map<String, Object> resultMap = getCourseCountSP.executeProcedure(searchPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getCourseDetials(CourseDetailsPayloadFromClient courseDetailsPayloadFromClient) {
	GetCourseDetailsSP getCourseDetailsSP = new GetCourseDetailsSP(dataSource);
	Map<String, Object> resultMap = getCourseDetailsSP.executeProcedure(courseDetailsPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
      * 
      */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
