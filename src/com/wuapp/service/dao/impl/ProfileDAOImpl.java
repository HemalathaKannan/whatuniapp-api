package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IProfileDAO;
import com.wuapp.service.dao.sp.profile.ProfileDetailsSP;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadFromClient;

public class ProfileDAOImpl implements IProfileDAO {

   public Map<String, Object> getProfileDetails(ProfileDetailsPayloadFromClient profileDetailsPayloadFromClient) {
	ProfileDetailsSP profileDetailsSP = new ProfileDetailsSP(dataSource);
	Map<String, Object> resultMap = profileDetailsSP.executeProcedure(profileDetailsPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
