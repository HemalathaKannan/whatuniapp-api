package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IFavouritesDAO;
import com.wuapp.service.dao.sp.favourites.AddOrRemoveShortlistSP;
import com.wuapp.service.dao.sp.favourites.GetShortlistDetailsSP;
import com.wuapp.service.dao.sp.favourites.ReorderChoicesSP;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;

/**
 * FAVOURITES/FINAL 5 PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class FavouritesDAOImpl implements IFavouritesDAO {

   public Map<String, Object> getShortlistDetials(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	GetShortlistDetailsSP getShortlistDetailsSP = new GetShortlistDetailsSP(dataSource);
	Map<String, Object> resultMap = getShortlistDetailsSP.executeProcedure(favouritesFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> reorderChoices(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	ReorderChoicesSP reorderChoicesSP = new ReorderChoicesSP(dataSource);
	Map<String, Object> resultMap = reorderChoicesSP.executeProcedure(favouritesFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> addOrRemoveShortlist(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	AddOrRemoveShortlistSP addOrRemoveShortlistSP = new AddOrRemoveShortlistSP(dataSource);
	Map<String, Object> resultMap = addOrRemoveShortlistSP.executeProcedure(favouritesFormPayloadFromClient);
	return resultMap;
   }

   //
   /*****************************************************************************
   *
   */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
