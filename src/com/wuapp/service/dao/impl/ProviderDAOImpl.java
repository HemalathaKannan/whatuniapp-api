package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IProviderDAO;
import com.wuapp.service.dao.sp.provider.ProviderResultsSP;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadFromClient;

public class ProviderDAOImpl implements IProviderDAO {

   public Map<String, Object> getProviderResults(ProviderResultsPayloadFromClient providerResultsPayloadFromClient) {
	ProviderResultsSP providerResultsSP = new ProviderResultsSP(dataSource);
	Map<String, Object> resultMap = providerResultsSP.executeProcedure(providerResultsPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
