package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IUserDAO;
import com.wuapp.service.dao.sp.user.CheckUserExistedSP;
import com.wuapp.service.dao.sp.user.GuestUserSP;
import com.wuapp.service.dao.sp.user.UserForgotPasswordSP;
import com.wuapp.service.dao.sp.user.UserLoginSP;
import com.wuapp.service.dao.sp.user.UserRegistrationSP;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;

public class UserDAOImpl implements IUserDAO {

   //
   public Map<String, Object> userRegistration(UserPayloadFromClient userPayloadFromClient) {
	UserRegistrationSP userRegistrationSP = new UserRegistrationSP(dataSource);
	Map<String, Object> resultMap = userRegistrationSP.executeProcedure(userPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> userLogin(UserPayloadFromClient userPayloadFromClient) {
	UserLoginSP userLoginSP = new UserLoginSP(dataSource);
	Map<String, Object> resultMap = userLoginSP.executeProcedure(userPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> checkUserExisted(UserPayloadFromClient userPayloadFromClient) {
	CheckUserExistedSP checkUserExistedSP = new CheckUserExistedSP(dataSource);
	Map<String, Object> resultMap = checkUserExistedSP.executeProcedure(userPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> userForgotPassword(UserPayloadFromClient userPayloadFromClient) {
	UserForgotPasswordSP userForgotPasswordSP = new UserForgotPasswordSP(dataSource);
	Map<String, Object> resultMap = userForgotPasswordSP.executeProcedure(userPayloadFromClient);
	return resultMap;
   }
   //
   public Map<String, Object> getGuestUserId() {
	GuestUserSP guestUserSP = new GuestUserSP(dataSource);
	Map<String, Object> resultMap = guestUserSP.executeProcedure();
	return resultMap;
   }
   //
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
   //
}
