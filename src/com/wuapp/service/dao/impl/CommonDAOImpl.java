package com.wuapp.service.dao.impl;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.ICommonDAO;
import com.wuapp.service.dao.sp.common.PreLoadStaticDataSP;
import com.wuapp.service.dao.sp.interaction.AppInteractionLoggingSP;
import com.wuapp.service.dao.sp.interaction.SaveUserActionsSP;
import com.wuapp.service.dao.sp.interaction.getClearingFlagSP;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.InteractionPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.SaveActionsPayloadFromClient;

public class CommonDAOImpl implements ICommonDAO {

   public Map<String, Object> preLoadStaticData(HashMap<String, String> parameters) {
	PreLoadStaticDataSP sp = new PreLoadStaticDataSP(dataSource);
	return sp.executeProcedure(parameters);
   }

   public Map<String, Object> dbStatsLogging(InteractionPayloadFromClient interactionPayloadFromClient) {
	AppInteractionLoggingSP appInteractionLoggingSP = new AppInteractionLoggingSP(dataSource);
	Map<String, Object> resultMap = appInteractionLoggingSP.executeProcedure(interactionPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserActions(SaveActionsPayloadFromClient saveActionsPayloadFromClient) {
	SaveUserActionsSP saveUserActionsSP = new SaveUserActionsSP(dataSource);
	Map<String, Object> resultMap = saveUserActionsSP.executeProcedure(saveActionsPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getClearingFlag(GetClearingFlagPayloadFromClient getClearingFlagPayloadFromClient) {
	getClearingFlagSP getClearingFlagSP = new getClearingFlagSP(dataSource);
	Map<String, Object> resultMap = getClearingFlagSP.executeProcedure(getClearingFlagPayloadFromClient);
	return resultMap;
   }

   /*****************************************************************************
   *
   */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
