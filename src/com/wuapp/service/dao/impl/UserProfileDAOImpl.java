package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IUserProfileDAO;
import com.wuapp.service.dao.sp.userprofile.GetSchoolListSP;
import com.wuapp.service.dao.sp.userprofile.GetUserPreferenceSP;
import com.wuapp.service.dao.sp.userprofile.GetUserProfileSP;
import com.wuapp.service.dao.sp.userprofile.SaveUserPreferenceSP;
import com.wuapp.service.dao.sp.userprofile.SaveUserProfileSP;
import com.wuapp.service.dao.sp.userprofile.SendEmailSP;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;

public class UserProfileDAOImpl implements IUserProfileDAO {

   public Map<String, Object> getUserProfile(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	GetUserProfileSP getUserProfileSP = new GetUserProfileSP(dataSource);
	Map<String, Object> resultMap = getUserProfileSP.executeProcedure(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserProfileInfo(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	SaveUserProfileSP saveUserProfileSP = new SaveUserProfileSP(dataSource);
	Map<String, Object> resultMap = saveUserProfileSP.executeProcedure(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getUserPreference(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	GetUserPreferenceSP getUserPreferenceSP = new GetUserPreferenceSP(dataSource);
	Map<String, Object> resultMap = getUserPreferenceSP.executeProcedure(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserPreference(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	SaveUserPreferenceSP saveUserPreferenceSP = new SaveUserPreferenceSP(dataSource);
	Map<String, Object> resultMap = saveUserPreferenceSP.executeProcedure(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getSchoolList(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	GetSchoolListSP getSchoolListSP = new GetSchoolListSP(dataSource);
	Map<String, Object> resultMap = getSchoolListSP.executeProcedure(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> sendEmailToWhatuni(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	SendEmailSP sendEmailSP = new SendEmailSP(dataSource);
	Map<String, Object> resultMap = sendEmailSP.executeProcedure(userProfilePayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
      * 
      */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
