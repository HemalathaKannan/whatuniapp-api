package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IOpendayDAO;
import com.wuapp.service.dao.sp.openday.GetSubjectAjaxListSP;
import com.wuapp.service.dao.sp.openday.MoreOpendaySP;
import com.wuapp.service.dao.sp.openday.OpendayBookingFormSubmitSP;
import com.wuapp.service.dao.sp.openday.OpendayProviderLandingSP;
import com.wuapp.service.dao.sp.openday.OpendaySearchAjaxSP;
import com.wuapp.service.dao.sp.openday.OpendaySearchLandingSP;
import com.wuapp.service.dao.sp.openday.OpendaySearchResultsSP;
import com.wuapp.service.dao.sp.openday.OpendaysShowBookingFormSP;
import com.wuapp.service.pojo.json.openday.OpendayBookingFormSubmitFromClient;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayShowBookingFormFromClient;
import com.wuapp.service.pojo.json.openday.SubjectAjaxPayloadFromClient;

/**
 * @author : Hemalatha K
 * @since : 06_MAY_2020
 * @version : 1.0
 * Class : OpendayDAOImpl.java
 * Description : This DAO class is used to getting provider opendays info
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On            Modification Details Change
 * *************************************************************************************************************
 * Hema.S            1.1             06_MAY_2020         Added booking form show and submit method
 */

public class OpendayDAOImpl implements IOpendayDAO {   

   @Override
   public Map<String, Object> getOpendayProviderDetails(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient) {
	OpendayProviderLandingSP opendayProviderLandingSP = new OpendayProviderLandingSP(dataSource);
	Map<String, Object> resultMap = opendayProviderLandingSP.executeProcedure(opendayProviderLandingPayloadFromClient);
	return resultMap;
   }
   
   @Override
   public Map<String, Object> getMoreOpendayList(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient) {
	MoreOpendaySP moreOpendaySP = new MoreOpendaySP(dataSource);
	Map<String, Object> resultMap = moreOpendaySP.executeProcedure(opendayProviderLandingPayloadFromClient);
	return resultMap;
   }
   
   @Override
   public Map<String, Object> getBookingFormSubmitList(OpendayBookingFormSubmitFromClient opendayBookingFormSubmitFromClient){
	OpendayBookingFormSubmitSP opendayBookingFormSubmitSP = new OpendayBookingFormSubmitSP(dataSource);
	Map<String, Object> resultMap = opendayBookingFormSubmitSP.executeProcedure(opendayBookingFormSubmitFromClient);
	return resultMap;
   }
   
   @Override
   public Map<String, Object> getShowBookingFormList(OpendayShowBookingFormFromClient opendayShowBookingFormFromClient) {
	OpendaysShowBookingFormSP opendaysShowBookingFormSP = new OpendaysShowBookingFormSP(dataSource);
	Map<String, Object> resultMap = opendaysShowBookingFormSP.executeProcedure(opendayShowBookingFormFromClient);
	return resultMap;
   }
   
   @Override
   public Map<String, Object> getSubjectAjaxList(SubjectAjaxPayloadFromClient subjectAjaxPayloadFromClient) {
	GetSubjectAjaxListSP getSubjectAjaxListSP = new GetSubjectAjaxListSP(dataSource);
	Map<String, Object> resultMap = getSubjectAjaxListSP.executeProcedure(subjectAjaxPayloadFromClient);
	return resultMap;
   } 

   @Override
   public Map<String, Object> getOpendaySearchList(OpendaySearchLandingPayloadFromClient opendaySearchLandingPayloadFromClient) {
	OpendaySearchLandingSP opendaySearchLandingSP = new OpendaySearchLandingSP(dataSource);
	Map<String, Object> resultMap = opendaySearchLandingSP.executeProcedure(opendaySearchLandingPayloadFromClient);
	return resultMap;
   } 

   @Override
   public Map<String, Object> getOpendaySearchAjaxList(OpendaySearchAjaxPayloadFromClient opendaySearchAjaxPayloadFromClient) {
	OpendaySearchAjaxSP opendaySearchAjaxSP = new OpendaySearchAjaxSP(dataSource);
	Map<String, Object> resultMap = opendaySearchAjaxSP.executeProcedure(opendaySearchAjaxPayloadFromClient);
	return resultMap;
   } 

   @Override
   public Map<String, Object> getOpendaySearchResults(OpendaySearchResultsPayloadFromClient opendaySearchResultsPayloadFromClient) {
	OpendaySearchResultsSP opendaySearchResultsSP = new OpendaySearchResultsSP(dataSource);
	Map<String, Object> resultMap = opendaySearchResultsSP.executeProcedure(opendaySearchResultsPayloadFromClient);
	return resultMap;
   }
   
   private DataSource dataSource = null;
   
   public DataSource getDataSource() {
      return dataSource;
   }
   
   public void setDataSource(DataSource dataSource) {
      this.dataSource = dataSource;
   }
}
