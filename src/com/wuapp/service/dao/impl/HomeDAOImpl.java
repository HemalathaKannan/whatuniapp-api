package com.wuapp.service.dao.impl;

import java.util.Map;
import javax.sql.DataSource;
import com.wuapp.service.dao.IHomeDAO;
import com.wuapp.service.dao.sp.home.BrowseResultsSP;
import com.wuapp.service.dao.sp.quiz.CourseListSP;
import com.wuapp.service.dao.sp.quiz.DesktopQuizDetailsSP;
import com.wuapp.service.dao.sp.quiz.JobIndustryListSP;
import com.wuapp.service.dao.sp.quiz.QualSubjectListSP;
import com.wuapp.service.dao.sp.quiz.QuizDetailsSP;
import com.wuapp.service.dao.sp.quiz.SaveUserActionsSP;
import com.wuapp.service.pojo.json.home.HomePayloadFromClient;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;

public class HomeDAOImpl implements IHomeDAO {

   public Map<String, Object> getBrowseResults(HomePayloadFromClient homePayloadFromClient) {
	BrowseResultsSP browseResultsSP = new BrowseResultsSP(dataSource);
	Map<String, Object> resultMap = browseResultsSP.executeProcedure(homePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getQuizDetails(QuizPayloadFromClient quizPayloadFromClient) {
	QuizDetailsSP quizDetailsSP = new QuizDetailsSP(dataSource);
	Map<String, Object> resultMap = quizDetailsSP.executeProcedure(quizPayloadFromClient);
	return resultMap;
   }
   
 //Added for DESKTOP chatbot- Jeyalakshmi 08/Aug/2019
   public Map<String, Object> getDesktopQuizDetails(QuizPayloadFromClient quizPayloadFromClient) {
	DesktopQuizDetailsSP desktopQuizDetailsSP = new DesktopQuizDetailsSP(dataSource);
	Map<String, Object> resultMap = desktopQuizDetailsSP.executeProcedure(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getCourseList(QuizPayloadFromClient quizPayloadFromClient) {
	CourseListSP courseListSP = new CourseListSP(dataSource);
	Map<String, Object> resultMap = courseListSP.executeProcedure(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getJobIndustryList(QuizPayloadFromClient quizPayloadFromClient) {
	JobIndustryListSP jobIndustryListSP = new JobIndustryListSP(dataSource);
	Map<String, Object> resultMap = jobIndustryListSP.executeProcedure(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getQualSubjectList(QuizPayloadFromClient quizPayloadFromClient) {
	QualSubjectListSP qualSubjectListSP = new QualSubjectListSP(dataSource);
	Map<String, Object> resultMap = qualSubjectListSP.executeProcedure(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserActions(QuizPayloadFromClient quizPayloadFromClient) {
	SaveUserActionsSP saveUserActionsSP = new SaveUserActionsSP(dataSource);
	Map<String, Object> resultMap = saveUserActionsSP.executeProcedure(quizPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public DataSource dataSource = null;

   public DataSource getDataSource() {
	return dataSource;
   }

   public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
   }
}
