package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.SubjectAjaxListVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting subject ajax list
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class SubjectAjaxListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SubjectAjaxListVO subjectAjaxListVO = new SubjectAjaxListVO();
	try {
	   subjectAjaxListVO.setCategoryId(resultSet.getString("CATEGORY_ID"));
	   subjectAjaxListVO.setCategoryCode(resultSet.getString("CATEGORY_CODE"));
	   subjectAjaxListVO.setSubjectName(resultSet.getString("SUBJECT_NAME"));
	   subjectAjaxListVO.setQualCode(resultSet.getString("QUAL_CODE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return subjectAjaxListVO;
   }
}
