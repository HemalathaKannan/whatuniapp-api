package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.EventTypeListVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting event type list.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class EventTypeListRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	EventTypeListVO eventTypeListVO = new EventTypeListVO();
	try {
	   eventTypeListVO.setEventCategoryId(resultSet.getString("EVENT_CATEGORY_ID"));
	   eventTypeListVO.setEventCategoryName(resultSet.getString("EVENT_CATEGORY_NAME"));
	   eventTypeListVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return eventTypeListVO;
   }
}
     