package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchAjaxVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting openday search ajax list.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class OpendaySearchAjaxRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OpendaySearchAjaxVO opendaySearchAjaxVO = new OpendaySearchAjaxVO();
	try {
	   opendaySearchAjaxVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	   opendaySearchAjaxVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   opendaySearchAjaxVO.setOpendayMsg(resultSet.getString("OD_MSG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return opendaySearchAjaxVO;
   }
}
