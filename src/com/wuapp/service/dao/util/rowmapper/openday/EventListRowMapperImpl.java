package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.EventListVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting event list.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class EventListRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	EventListVO eventListVO = new EventListVO();
	try {
	   eventListVO.setMonthValue(resultSet.getString("MONTH_VALUE"));
	   eventListVO.setMonthName(resultSet.getString("MONTH_NAME"));
	   eventListVO.setOdExistFlag(resultSet.getString("OD_EXIST_FLAG"));
	   eventListVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return eventListVO;
   }
}
    