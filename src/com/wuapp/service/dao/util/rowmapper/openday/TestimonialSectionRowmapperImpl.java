package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.TestimonialSectionVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting testimonials section details.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class TestimonialSectionRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	TestimonialSectionVO testimonialSectionVO = new TestimonialSectionVO();
	try {
	   testimonialSectionVO.setSectionValue(resultSet.getString("SECTION_VALUE"));
	   testimonialSectionVO.setTitle(resultSet.getString("TITLE"));
	   testimonialSectionVO.setUpdatedDate(resultSet.getString("UPDATED_DATE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return testimonialSectionVO;
   }
}