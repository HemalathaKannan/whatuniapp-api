package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.MoreOpendayVO;
import com.wuapp.util.developer.Debugger;

/**
 * OpendaysListRowmapperImpl for getting provider opendays info for dropdown
 */
public class OpendaysListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	MoreOpendayVO moreOpendayVO = new MoreOpendayVO();
	try {
	   moreOpendayVO.setOpendaysDetail(resultSet.getString("open_days_det"));
	   moreOpendayVO.setEventId(resultSet.getString("event_id"));
	   moreOpendayVO.setSelectedEvent(resultSet.getString("selected_text"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return moreOpendayVO;
   }
}
