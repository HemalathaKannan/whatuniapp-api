package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OpendayMonthListVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting openday month list.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class OpendayMonthListRowMapperImpl implements RowMapper<Object> {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    OpendayMonthListVO opendayMonthListVO = new OpendayMonthListVO();
    try {	 
	 opendayMonthListVO.setMonthValue(resultSet.getString("MONTH_VALUE"));
	 opendayMonthListVO.setMonthName(resultSet.getString("MONTH_NAME"));
	 opendayMonthListVO.setOpendayExistFlag(resultSet.getString("OD_EXIST_FLAG"));
	 opendayMonthListVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
    }
    return opendayMonthListVO;
  }
}
