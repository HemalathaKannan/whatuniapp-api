package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchLandingVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for search landing page.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class OpendaySearchLandingRowMapperImpl implements RowMapper<Object> {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    OpendaySearchLandingVO opendaySearchLandingVO = new OpendaySearchLandingVO();
    try {	 
	 opendaySearchLandingVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	 opendaySearchLandingVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
	 opendaySearchLandingVO.setCollegeLogo(resultSet.getString("LOGO"));
	 opendaySearchLandingVO.setMediaPath(resultSet.getString("MEDIA_PATH"));
	 opendaySearchLandingVO.setEventId(resultSet.getString("EVENT_ID"));
	 opendaySearchLandingVO.setStartDate(resultSet.getString("START_DATE"));
	 opendaySearchLandingVO.setStartTime(resultSet.getString("START_TIME"));
	 opendaySearchLandingVO.setVenue(resultSet.getString("VENUE"));
	 opendaySearchLandingVO.setOpendayCount(resultSet.getString("OPENDAY_COUNT"));
	 opendaySearchLandingVO.setReviewRating(resultSet.getString("REVIEW_RATING"));
	 opendaySearchLandingVO.setReviewRatingDisplay(resultSet.getString("REVIEW_RATING_DISPLAY"));
	 opendaySearchLandingVO.setEventCategoryId(resultSet.getString("EVENT_CATEGORY_ID"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
    }
    return opendaySearchLandingVO;
  }
}
