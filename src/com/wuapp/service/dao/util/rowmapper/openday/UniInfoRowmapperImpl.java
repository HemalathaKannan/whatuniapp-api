package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.UniInfoVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting uni info.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class UniInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UniInfoVO uniInfoVO = new UniInfoVO();
	try {
	   uniInfoVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	   uniInfoVO.setCollegeLogo(resultSet.getString("COLLEGE_LOGO"));
	   uniInfoVO.setMediaPath(resultSet.getString("MEDIA_PATH"));
	   uniInfoVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
	   uniInfoVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   uniInfoVO.setReviewCount(resultSet.getString("REVIEW_COUNT"));
	   uniInfoVO.setOverallRating(resultSet.getString("OVERALL_RATING"));
	   uniInfoVO.setOverallRatingExact(resultSet.getString("OVERALL_RATING_EXACT"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return uniInfoVO;
   }
}
