package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.RegionListVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting region list.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class RegionListRowMapperImpl implements RowMapper<Object> {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
     RegionListVO regionListVO = new RegionListVO();
    try {	 
	 regionListVO.setLocation(resultSet.getString("LOCATION"));
	 regionListVO.setLocationValue(resultSet.getString("LOCATION_VALUE"));
	 regionListVO.setSelectedFlag(resultSet.getString("SELECTED_FLAG"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
    }
    return regionListVO;
  }
}
