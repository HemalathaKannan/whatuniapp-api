package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OtherOdPodVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting other uni info 
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class OtherOdPodRowMapperImpl implements RowMapper<Object> {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    OtherOdPodVO otherOdPodVO = new OtherOdPodVO();
    try {
	otherOdPodVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
      otherOdPodVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
      otherOdPodVO.setCollegeNameDisplay(resultSet.getString("COLLEGE_NAME_DISPLAY"));
      otherOdPodVO.setLogoPath(resultSet.getString("LOGO_PATH"));
	otherOdPodVO.setStartTime(resultSet.getString("START_TIME"));
	otherOdPodVO.setEndTime(resultSet.getString("END_TIME"));
	otherOdPodVO.setVenue(resultSet.getString("VENUE"));
	otherOdPodVO.setQualId(resultSet.getString("QUAL_ID"));
	otherOdPodVO.setQualType(resultSet.getString("QUAL_TYPE"));
	otherOdPodVO.setOpenDate(resultSet.getString("OPENDATE"));
	otherOdPodVO.setOpenDay(resultSet.getString("OPEN_DAY"));
	otherOdPodVO.setOpenMonth(resultSet.getString("OPEN_MONTH"));
	otherOdPodVO.setSuborderItemId(resultSet.getString("SUBORDER_ITEM_ID"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
	}
    return otherOdPodVO;
  }
}
