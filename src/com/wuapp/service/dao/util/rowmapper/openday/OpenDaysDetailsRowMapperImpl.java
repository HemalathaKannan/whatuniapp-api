package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OpendaysDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting openday details.
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class OpenDaysDetailsRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OpendaysDetailsVO opendaysDetailsVO = new OpendaysDetailsVO();
	try {
	   opendaysDetailsVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	   opendaysDetailsVO.setEventId(resultSet.getString("EVENT_ID"));
	   opendaysDetailsVO.setStartDate(resultSet.getString("START_DATE"));
	   opendaysDetailsVO.setStartTime(resultSet.getString("START_TIME"));
	   opendaysDetailsVO.setVenue(resultSet.getString("VENUE"));
	   opendaysDetailsVO.setQualType(resultSet.getString("QUAL_TYPE"));
	   opendaysDetailsVO.setBookingUrl(resultSet.getString("BOOKING_URL"));
	   opendaysDetailsVO.setBookingFormFlag(resultSet.getString("BOOKING_FORM_FLAG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return opendaysDetailsVO;
   }
}
