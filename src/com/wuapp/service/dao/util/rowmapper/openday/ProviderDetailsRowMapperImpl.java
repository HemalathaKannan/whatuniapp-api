package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.ProviderDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting provider details.
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class ProviderDetailsRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ProviderDetailsVO providerDetailsVO = new ProviderDetailsVO();
	try {
	   providerDetailsVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
	   providerDetailsVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   providerDetailsVO.setCollegeLogo(resultSet.getString("COLLEGE_LOGO"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return providerDetailsVO;
   }
}
