package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.SelectedEventDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting selected event detail.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class SelectedEventDetailsRowMapperImpl implements RowMapper<Object> {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    SelectedEventDetailsVO eventDetailsVO = new SelectedEventDetailsVO();
    try {
	eventDetailsVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	eventDetailsVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
	eventDetailsVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	eventDetailsVO.setSuborderItemId(resultSet.getString("SUB_ORDER_ITEM_ID"));
	eventDetailsVO.setNetworkId(resultSet.getString("NETWORK_ID"));
	eventDetailsVO.setStartDate(resultSet.getString("START_DATE"));
	eventDetailsVO.setOpendayDate(resultSet.getString("OPEN_DATE"));
	eventDetailsVO.setOpenDate(resultSet.getString("OPENDATE"));
	eventDetailsVO.setOpenMonthYear(resultSet.getString("OPEN_MONTH_YEAR"));
	eventDetailsVO.setBookingUrl(resultSet.getString("BOOKING_URL"));
	eventDetailsVO.setBookingFormFlag(resultSet.getString("BOOKING_FORM_FLAG"));
	eventDetailsVO.setWebsitePrice(resultSet.getString("WEBSITE_PRICE"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
    }
    return eventDetailsVO;
  }
}
