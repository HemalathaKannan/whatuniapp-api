package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.QualificationDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting user qualification
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class QualificationDetailsRowMapperImpl implements RowMapper<Object>  {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    QualificationDetailsVO qualificationDetailsVO = new QualificationDetailsVO();
    try {
      qualificationDetailsVO.setQualId(resultSet.getString("QUAL_ID"));
      qualificationDetailsVO.setQualDesc(resultSet.getString("QUAL_DESC"));
      qualificationDetailsVO.setQualSelected(resultSet.getString("QUAL_SELECTED"));
      qualificationDetailsVO.setQualCode(resultSet.getString("QUAL_CODE"));    
    } catch (Exception e) {
        Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
	}
    return qualificationDetailsVO;
  }
}
