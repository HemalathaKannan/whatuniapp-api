package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.StudyModeVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting study mode
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class StudyModeDetailsRowMapperImpl implements RowMapper<Object> {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    StudyModeVO studyModeVO = new StudyModeVO();
    try {
      studyModeVO.setStudyModeId(resultSet.getString("STUDY_MODE_ID"));
      studyModeVO.setStudyModeDesc(resultSet.getString("STUDY_MODE_DESC"));
      studyModeVO.setStudyModeSelected(resultSet.getString("STUDY_MODE_SELECTED"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
        throw new SQLException();
      }
    return studyModeVO;
  }
}
