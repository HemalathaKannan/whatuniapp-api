package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.ContentSectionVO;
import com.wuapp.service.dao.util.valueobject.openday.ContentSectionValueVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting openday content section details.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class ContentSectionRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ContentSectionVO contentSectionVO = new ContentSectionVO();
	try {
	   contentSectionVO.setSectionName(resultSet.getString("SECTION_NAME")); 
	   ResultSet contentSectionInnerRS = (ResultSet) resultSet.getObject("SECTION_VALUE");
	   try {
		if (contentSectionInnerRS != null) {
		   ArrayList<ContentSectionValueVO> sectionValueList = new ArrayList<ContentSectionValueVO>();
		   ContentSectionValueVO contentSectionValueVO = null;
		   while (contentSectionInnerRS.next()) {
			contentSectionValueVO = new ContentSectionValueVO();
			contentSectionValueVO.setSectionValue(contentSectionInnerRS.getString("SECTION_VALUE"));
			sectionValueList.add(contentSectionValueVO);
		   }
		   contentSectionVO.setSectionValueList(sectionValueList);
		}
	   } catch (Exception e) {
		e.printStackTrace();
	   } finally {
		try {
		   if (contentSectionInnerRS != null) {
			contentSectionInnerRS.close();
			contentSectionInnerRS = null;
		   }
		} catch (Exception e) {
		   throw new SQLException(e.getMessage());
		}
	   }
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return contentSectionVO;
   }
}
