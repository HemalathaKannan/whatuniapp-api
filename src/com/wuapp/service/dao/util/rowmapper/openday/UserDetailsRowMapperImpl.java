package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.UserDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting user details.
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class UserDetailsRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserDetailsVO userDetailsVO = new UserDetailsVO();
	try {
	   userDetailsVO.setFirstName(resultSet.getString("FIRSTNAME"));
	   userDetailsVO.setLastName(resultSet.getString("LASTNAME"));
	   userDetailsVO.setUserEmail(resultSet.getString("USER_EMAIL"));
	   userDetailsVO.setPassword(resultSet.getString("PASSWORD"));
	   userDetailsVO.setMobileNumber(resultSet.getString("MOBILE_NUMBER"));
	   userDetailsVO.setMarketingEmail(resultSet.getString("MARKETING_EMAIL"));
	   userDetailsVO.setSolusEmail(resultSet.getString("SOLUS_EMAIL"));
	   userDetailsVO.setSurveyFlag(resultSet.getString("SURVEY_FLAG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userDetailsVO;
   }
}
