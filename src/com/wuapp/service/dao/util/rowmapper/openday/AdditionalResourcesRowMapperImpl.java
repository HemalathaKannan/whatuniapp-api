package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.AdditionalResourcesVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting additional resources details.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class AdditionalResourcesRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AdditionalResourcesVO additionalResourceVO = new AdditionalResourcesVO();
	try {
	   additionalResourceVO.setPdfName(resultSet.getString("PDF_NAME"));
	   additionalResourceVO.setPdfPath(resultSet.getString("PDF_PATH"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return additionalResourceVO;
   }
}
