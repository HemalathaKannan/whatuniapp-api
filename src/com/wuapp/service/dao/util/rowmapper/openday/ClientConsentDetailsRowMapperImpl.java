package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.ClientConsentVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting client consent details.
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class ClientConsentDetailsRowMapperImpl implements RowMapper<Object> {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    ClientConsentVO clientConsentVO = new ClientConsentVO();
    try {
      clientConsentVO.setConsentText(resultSet.getString("CONSENT_TEXT"));
      clientConsentVO.setClientMarketingConsentFlag(resultSet.getString("CLIENT_MARKETING_CONSENT_FLAG"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
	}
    return clientConsentVO;
  }
}
