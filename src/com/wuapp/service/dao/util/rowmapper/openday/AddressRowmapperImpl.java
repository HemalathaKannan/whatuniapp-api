package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.AddressVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting uni address details.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class AddressRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AddressVO addressVO = new AddressVO();
	try {
	   addressVO.setLatitude(resultSet.getString("LATITUDE"));
	   addressVO.setLongitude(resultSet.getString("LONGITUDE"));
	   addressVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   addressVO.setAddressLine1(resultSet.getString("ADDRESS_LINE_1"));
	   addressVO.setAddressLine2(resultSet.getString("ADDRESS_LINE_2"));
	   addressVO.setTown(resultSet.getString("TOWN"));
	   addressVO.setPostcode(resultSet.getString("POSTCODE"));
	   addressVO.setCountryState(resultSet.getString("COUNTRY_STATE"));
	   addressVO.setIsInLondon(resultSet.getString("IS_IN_LONDON"));
	   addressVO.setNearestTrainStn(resultSet.getString("NEAREST_TRAIN_STN"));
	   addressVO.setNearestTubeStn(resultSet.getString("NEAREST_TUBE_STN"));
	   addressVO.setDistanceFromTrainStn(resultSet.getString("DISTANCE_FROM_TRAIN_STN"));
	   addressVO.setDistanceFromTubeStn(resultSet.getString("DISTANCE_FROM_TUBE_STN"));
	   addressVO.setCityGuideDisplayFlag(resultSet.getString("CITY_GUIDE_DISPLAY_FLAG"));	   
	   addressVO.setLocation(resultSet.getString("LOCATION"));
	   addressVO.setArticleCategory(resultSet.getString("ARTICLE_CATEGORY"));
	   addressVO.setPostUrl(resultSet.getString("POST_URL"));
	   addressVO.setArticleId(resultSet.getString("ARTICLE_ID"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return addressVO;
   }
}
