package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.GallerySectionVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting uni gallery.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class GallerySectionRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	GallerySectionVO gallerySectionVO = new GallerySectionVO();
	try {
	   gallerySectionVO.setMediaId(resultSet.getString("MEDIA_ID"));
	   gallerySectionVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   gallerySectionVO.setMediaPath(resultSet.getString("MEDIA_PATH"));
	   gallerySectionVO.setMediaType(resultSet.getString("MEDIA_TYPE"));
	   gallerySectionVO.setThumbnailPath(resultSet.getString("THUMBNAIL_PATH"));
	   gallerySectionVO.setContenthubFlag(resultSet.getString("CONTENTHUB_FLAG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return gallerySectionVO;
   }
}
