package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OpendayVO;
import com.wuapp.util.developer.Debugger;

/**
 * OpendaysInfoRowmapperImpl for getting particular openday info
 */
public class OpendaysInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OpendayVO opendayVO = new OpendayVO();
	try {
	   opendayVO.setCollegeId(resultSet.getString("college_id"));
	   opendayVO.setCollegeLogoPath(resultSet.getString("college_logo"));
	   opendayVO.setCollegeDispName(resultSet.getString("college_name_display"));
	   opendayVO.setOpendate(resultSet.getString("open_date"));
	   opendayVO.setStartDate(resultSet.getString("start_date"));
	   opendayVO.setCalendarItemId(resultSet.getString("event_calendar_item_id"));
	   opendayVO.setEventDesc(resultSet.getString("event_description"));
	   opendayVO.setStartTime(resultSet.getString("start_time"));
	   opendayVO.setEndTime(resultSet.getString("end_time"));
	   opendayVO.setStartDate(resultSet.getString("start_date"));
	   opendayVO.setBookingUrl(resultSet.getString("booking_url"));
	   opendayVO.setSelectedopendayVenue(resultSet.getString("venue"));
	   opendayVO.setQualId(resultSet.getString("qual_id"));
	   opendayVO.setQualDesc(resultSet.getString("qual_desc"));
	   opendayVO.setOdMonthYear(resultSet.getString("opendate"));
	   opendayVO.setSubOrderItemId(resultSet.getString("suborder_item_id"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return opendayVO;
   }
}
