package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.EventDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting event details.
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class EventDetailsRowMapperImpl implements RowMapper<Object> {
   
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    EventDetailsVO eventDetailsVO = new EventDetailsVO();
    try {
	eventDetailsVO.setEventId((resultSet.getString("EVENT_ID")));
	eventDetailsVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	eventDetailsVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
	eventDetailsVO.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	eventDetailsVO.setCollegeLogo(resultSet.getString("COLLEGE_LOGO"));
	eventDetailsVO.setStartDate(resultSet.getString("START_DATE"));
	eventDetailsVO.setStartTime(resultSet.getString("START_TIME"));
	eventDetailsVO.setVenue(resultSet.getString("VENUE"));
	eventDetailsVO.setQualType(resultSet.getString("QUAL_TYPE"));
    } catch (Exception e) {
	  Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	  throw new SQLException();
    }
    return eventDetailsVO;
  }
}
