package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.OpendayListVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting uni openday detail.
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

public class OpendayDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OpendayListVO opendayListVO = new OpendayListVO();
	try {
	   opendayListVO.setEventItemId(resultSet.getString("EVENT_ITEM_ID"));
	   opendayListVO.setStartDate(resultSet.getString("START_DATE"));
	   opendayListVO.setStartTime(resultSet.getString("START_TIME"));
	   opendayListVO.setVenue(resultSet.getString("VENUE"));
	   opendayListVO.setQualType(resultSet.getString("QUAL_TYPE"));
	   opendayListVO.setTotalOpendayCount(resultSet.getString("TOTAL_OPEN_DAY_COUNT"));
	   opendayListVO.setMoreOpendayFlag(resultSet.getString("MORE_OPEN_DAYS_FLAG"));
	   opendayListVO.setBookingUrl(resultSet.getString("BOOKING_URL"));
	   opendayListVO.setBookingFormFlag(resultSet.getString("BOOKING_FORM_FLAG"));
	   opendayListVO.setEventCategoryName(resultSet.getString("EVENT_CATEGORY_NAME"));
	   opendayListVO.setEventCategoryId(resultSet.getString("EVENT_CATEGORY_ID"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return opendayListVO;
   }
}
