package com.wuapp.service.dao.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.openday.YearOfEntryVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper for getting YOE details
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

public class YearOfEntryRowMapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	YearOfEntryVO yearOfEntryVO = new YearOfEntryVO();
	try {
	   yearOfEntryVO.setYearOfEntry(resultSet.getString("YEAR_OF_ENTRY"));
	   yearOfEntryVO.setSelectedYear(resultSet.getString("SELECTED_YEAR"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return yearOfEntryVO;
   }
}
