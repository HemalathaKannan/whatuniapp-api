package com.wuapp.service.dao.util.rowmapper.enquiry;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.enquiry.YearListVO;
import com.wuapp.util.developer.Debugger;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class YearListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	YearListVO yearListVO = new YearListVO();
	try {
	   yearListVO.setYearOfEntry(resultSet.getString("YEAR_OF_ENTRY"));
	   yearListVO.setSelectedYear(resultSet.getString("SELECTED_YEAR"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return yearListVO;
   }
}
