package com.wuapp.service.dao.util.rowmapper.enquiry;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.enquiry.InstitutionVO;
import com.wuapp.util.developer.Debugger;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class InstitutionsListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	InstitutionVO institutionVO = new InstitutionVO();
	try {
	   institutionVO.setInstitutionId(resultSet.getString("COLLEGE_ID"));
	   institutionVO.setInstitutionDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   institutionVO.setLogo(resultSet.getString("COLLEGE_LOGO"));
	   institutionVO.setLogoName(resultSet.getString("COLLEGE_LOGO"));
	   institutionVO.setCourseId(resultSet.getString("COURSE_ID"));
	   institutionVO.setCourseTitle(resultSet.getString("COURSE_TITLE"));
	   institutionVO.setShortListFlag(resultSet.getString("SHORTLIST_FLAG"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return institutionVO;
   }
}
