package com.wuapp.service.dao.util.rowmapper.enquiry;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.enquiry.OtherInstitutionsVO;
import com.wuapp.util.developer.Debugger;

public class OtherInstitutionsListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OtherInstitutionsVO otherInstitutionsVO = new OtherInstitutionsVO();
	try {
	   otherInstitutionsVO.setInstitutionId(resultSet.getString("COLLEGE_ID"));
	   otherInstitutionsVO.setInstitutionDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   otherInstitutionsVO.setLogo(resultSet.getString("COLLEGE_LOGO"));
	   otherInstitutionsVO.setLogoName(resultSet.getString("COLLEGE_LOGO"));
	   otherInstitutionsVO.setMatchingPercentage(resultSet.getString("MATCHING_PERCENTAGE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return otherInstitutionsVO;
   }
}
