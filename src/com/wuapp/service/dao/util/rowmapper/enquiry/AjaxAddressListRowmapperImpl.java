package com.wuapp.service.dao.util.rowmapper.enquiry;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.enquiry.AjaxUserAddressVO;
import com.wuapp.util.developer.Debugger;

public class AjaxAddressListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AjaxUserAddressVO ajaxUserAddressVO = new AjaxUserAddressVO();
	try {
	   ajaxUserAddressVO.setAddressCode(resultSet.getString("address_list"));
	   ajaxUserAddressVO.setAddressDisplay(resultSet.getString("address_list"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return ajaxUserAddressVO;
   }
}
