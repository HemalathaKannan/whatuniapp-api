package com.wuapp.service.dao.util.rowmapper.enquiry;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.enquiry.UserAddressVO;
import com.wuapp.util.developer.Debugger;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class UserAddressRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserAddressVO userAddressVO = new UserAddressVO();
	try {
	   userAddressVO.setAddressLineOne(resultSet.getString("ADDRESS_LINE_1"));
	   userAddressVO.setAddressLineTwo(resultSet.getString("ADDRESS_LINE_2"));
	   userAddressVO.setCity(resultSet.getString("CITY"));
	   userAddressVO.setPostCode(resultSet.getString("postcode"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userAddressVO;
   }
}
