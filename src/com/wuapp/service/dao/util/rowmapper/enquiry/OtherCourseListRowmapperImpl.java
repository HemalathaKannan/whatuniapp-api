package com.wuapp.service.dao.util.rowmapper.enquiry;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.enquiry.OtherCoursesVO;
import com.wuapp.util.developer.Debugger;

public class OtherCourseListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OtherCoursesVO otherCoursesVO = new OtherCoursesVO();
	try {
	   otherCoursesVO.setCourseId(resultSet.getString("COURSE_ID"));
	   otherCoursesVO.setCourseTitle(resultSet.getString("COURSE_TITLE"));
	   otherCoursesVO.setInstitutionId(resultSet.getString("COLLEGE_ID"));
	   otherCoursesVO.setInstitutionDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   otherCoursesVO.setLogo(resultSet.getString("COLLEGE_LOGO"));
	   otherCoursesVO.setLogoName(resultSet.getString("COLLEGE_LOGO"));
	   otherCoursesVO.setMatchingPercentage(resultSet.getString("MATCHING_PERCENTAGE"));
	   otherCoursesVO.setMediaPath(resultSet.getString("TILE_MEDIA_PATH"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return otherCoursesVO;
   }
}
