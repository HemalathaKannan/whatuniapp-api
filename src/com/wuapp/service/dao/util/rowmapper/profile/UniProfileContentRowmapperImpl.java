package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.ProfileVO;
import com.wuapp.util.developer.Debugger;

public class UniProfileContentRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ProfileVO profileVO = new ProfileVO();
	try {
	   profileVO.setDescription(resultSet.getString("description"));
	   profileVO.setButtonLabel(resultSet.getString("button_label"));
	   profileVO.setMediaType(resultSet.getString("media_type"));
	   profileVO.setImagePath(resultSet.getString("image_path"));
	   profileVO.setVideoUrl(resultSet.getString("video_url"));
	   profileVO.setProfileId(resultSet.getString("profile_id"));
	   profileVO.setMyhcProfileId(resultSet.getString("myhc_profile_id"));
	   profileVO.setSuborderItemId(resultSet.getString("sub_order_item_id"));
	   profileVO.setTypeName(resultSet.getString("type_name"));
	   profileVO.setSectionDisplayName(resultSet.getString("section_display_name"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return profileVO;
   }
}
