package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.OpendayDetailsVO;
import com.wuapp.util.developer.Debugger;

public class OpendayDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OpendayDetailsVO opendayDetailsVO = new OpendayDetailsVO();
	try {
	   opendayDetailsVO.setOpenDate(resultSet.getString("Open_date"));
	   opendayDetailsVO.setTiming(resultSet.getString("timing"));
	   opendayDetailsVO.setEventId(resultSet.getString("event_id"));
	   opendayDetailsVO.setSubOrderItemId(resultSet.getString("sub_order_item_id"));
	   opendayDetailsVO.setQualType(resultSet.getString("qual_type"));
	   opendayDetailsVO.setOpendaysSavedFlag(resultSet.getString("opendays_saved_flag"));//Added by Indumathi.S Oct 5 2017
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return opendayDetailsVO;
   }
}
