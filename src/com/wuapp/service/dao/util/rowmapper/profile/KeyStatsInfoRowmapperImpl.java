package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.KeyStatsInfoVO;
import com.wuapp.util.developer.Debugger;

public class KeyStatsInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	KeyStatsInfoVO keyStatsInfoVO = new KeyStatsInfoVO();
	try {
	   keyStatsInfoVO.setFullTime(resultSet.getString("full_time"));
	   keyStatsInfoVO.setPartTime(resultSet.getString("part_time"));
	   keyStatsInfoVO.setMature(resultSet.getString("mature"));
	   keyStatsInfoVO.setSchoolLeavers(resultSet.getString("school_leavers"));
	   keyStatsInfoVO.setUkStudents(resultSet.getString("uk_students"));
	   keyStatsInfoVO.setInternational(resultSet.getString("international"));
	   keyStatsInfoVO.setUndergraduate(resultSet.getString("undergraduate"));
	   keyStatsInfoVO.setPostgraduate(resultSet.getString("postgraduate"));
	   keyStatsInfoVO.setNoOfStudents(resultSet.getString("no_of_students"));
	   keyStatsInfoVO.setEmploymentRate(resultSet.getString("employment_rate"));
	   keyStatsInfoVO.setWuscaRanking(resultSet.getString("wusca_ranking"));
	   keyStatsInfoVO.setTotalWuscaRanking(resultSet.getString("total_wusca_ranking"));
	   keyStatsInfoVO.setTimesRanking(resultSet.getString("times_ranking"));
	   keyStatsInfoVO.setTotalTimesRanking(resultSet.getString("total_times_ranking"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return keyStatsInfoVO;
   }
}
