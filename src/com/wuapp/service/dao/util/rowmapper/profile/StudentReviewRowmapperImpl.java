package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.StudentReviewVO;
import com.wuapp.util.developer.Debugger;

public class StudentReviewRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	StudentReviewVO studentReviewVO = new StudentReviewVO();
	try {
	   studentReviewVO.setUserName(resultSet.getString("user_name"));
	   studentReviewVO.setReviewText(resultSet.getString("review_text"));
	   studentReviewVO.setStarRating(resultSet.getString("star_rating"));
	   studentReviewVO.setSections(resultSet.getString("sections"));
	   studentReviewVO.setReviewType(resultSet.getString("review_type"));
	   studentReviewVO.setStarRatingDisplay(resultSet.getString("star_rating_display"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return studentReviewVO;
   }
}
