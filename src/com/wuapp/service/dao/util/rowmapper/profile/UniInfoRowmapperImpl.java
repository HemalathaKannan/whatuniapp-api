package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.UniInfoVO;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.util.developer.Debugger;

public class UniInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UniInfoVO uniInfoVO = new UniInfoVO();
	CommonUtilities common = new CommonUtilities();
	try {
	   uniInfoVO.setCollegeId(resultSet.getString("college_id"));
	   uniInfoVO.setCollegeLogo(resultSet.getString("college_logo"));
	   uniInfoVO.setLogoName(resultSet.getString("college_logo"));
	   uniInfoVO.setCollegeName(resultSet.getString("college_name_display"));
	   uniInfoVO.setTileMediaPath(resultSet.getString("tile_media_path"));
	   uniInfoVO.setOverallRating(resultSet.getString("overall_rating"));
	   uniInfoVO.setReviewRatingDisplay(resultSet.getString("review_rating_display"));
	   uniInfoVO.setReviewCount(resultSet.getString("review_count"));
	   uniInfoVO.setMatchingPercentage(resultSet.getString("matching_percentage"));
	   uniInfoVO.setShortListFlag(resultSet.getString("shortlist_flag"));
	   uniInfoVO.setPullQuotes(resultSet.getString("pull_quotes"));
	   uniInfoVO.setLatitude(resultSet.getString("latitude"));
	   uniInfoVO.setLongitude(resultSet.getString("longitude"));
	   uniInfoVO.setTown(resultSet.getString("town"));
	   uniInfoVO.setPostcode(resultSet.getString("postcode"));
	   uniInfoVO.setNearestTrainStation(resultSet.getString("nearest_train_stn"));
	   uniInfoVO.setNearestTubeStation(resultSet.getString("nearest_tube_stn"));
	   uniInfoVO.setDistanceFromTrainStn(resultSet.getString("distance_from_train_stn"));
	   uniInfoVO.setDistanceFromTubeStn(resultSet.getString("distance_from_tube_stn"));
	   uniInfoVO.setAddressLineOne(resultSet.getString("add_line_1"));
	   uniInfoVO.setAddressLineTwo(resultSet.getString("add_line_2"));
	   uniInfoVO.setCountryState(resultSet.getString("country_state"));
	   uniInfoVO.setIsInLondon(resultSet.getString("is_in_london"));
	   uniInfoVO.setCourseExistFlag(resultSet.getString("course_exist_flag"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return uniInfoVO;
   }
}
