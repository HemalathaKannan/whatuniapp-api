package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.ReviewStarRatingVO;
import com.wuapp.util.developer.Debugger;

public class ReviewStarRatingRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ReviewStarRatingVO reviewStarRatingVO = new ReviewStarRatingVO();
	try {
	   reviewStarRatingVO.setCollegeId(resultSet.getString("college_id"));
	   reviewStarRatingVO.setQuestionTitle(resultSet.getString("question_title"));
	   reviewStarRatingVO.setRating(resultSet.getString("rating"));
	   reviewStarRatingVO.setRoundRating(resultSet.getString("round_rating"));
	   reviewStarRatingVO.setQuestionId(resultSet.getString("question_id"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return reviewStarRatingVO;
   }
}
