package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.AccomodationVO;
import com.wuapp.util.developer.Debugger;

public class AccomodationDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AccomodationVO accomodationVO = new AccomodationVO();
	try {
	   accomodationVO.setAccommodationCost(resultSet.getString("accommodation_cost"));
	   accomodationVO.setLivingCost(resultSet.getString("living_cost"));
	   accomodationVO.setWhatuniCostOfPint(resultSet.getString("whatuni_cost_pint"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return accomodationVO;
   }
}
