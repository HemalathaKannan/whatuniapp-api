package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.CompetitorDetailVO;
import com.wuapp.util.developer.Debugger;

public class CompetitorDetailRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	CompetitorDetailVO competitorDetailVO = new CompetitorDetailVO();
	try {
	   competitorDetailVO.setCollegeId(resultSet.getString("college_id"));
	   competitorDetailVO.setCollegeName(resultSet.getString("college_name_display"));
	   competitorDetailVO.setCollegeLogo(resultSet.getString("college_logo"));
	   competitorDetailVO.setLogoName(resultSet.getString("college_logo"));
	   competitorDetailVO.setMatchingPercentage(resultSet.getString("matching_percentage"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return competitorDetailVO;
   }
}
