package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.LoadYearListVO;
import com.wuapp.util.developer.Debugger;

public class LoadYearListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	LoadYearListVO loadYearListVO = new LoadYearListVO();
	try {
	   loadYearListVO.setLoadYearName(resultSet.getString("load_year_name"));
	   loadYearListVO.setLoadYearData(resultSet.getString("load_year_data"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return loadYearListVO;
   }
}
