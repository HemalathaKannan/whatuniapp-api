package com.wuapp.service.dao.util.rowmapper.profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.profile.EnquiryDetailsVO;
import com.wuapp.util.developer.Debugger;

public class EnquiryDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	EnquiryDetailsVO enquiryDetailsVO = new EnquiryDetailsVO();
	try {
	   enquiryDetailsVO.setOrderItemId(resultSet.getString("order_item_id"));
	   enquiryDetailsVO.setEmailFlag(resultSet.getString("emails_flag"));
	   enquiryDetailsVO.setEmail(resultSet.getString("email"));
	   enquiryDetailsVO.setEmailWebform(resultSet.getString("email_webform"));
	   enquiryDetailsVO.setEmailWebformFlag(resultSet.getString("email_webform_flag"));
	   enquiryDetailsVO.setProspectus(resultSet.getString("prospectus"));
	   enquiryDetailsVO.setProspectusWebform(resultSet.getString("prospectus_webform"));
	   enquiryDetailsVO.setProspectusWebformFlag(resultSet.getString("prospectus_webform_flag"));
	   enquiryDetailsVO.setWebsite(resultSet.getString("website"));
	   enquiryDetailsVO.setWebsiteFlag(resultSet.getString("website_flag"));
	   enquiryDetailsVO.setSuborderItemId(resultSet.getString("suborder_item_id"));
	   enquiryDetailsVO.setNetworkId(resultSet.getString("network_id"));
	   enquiryDetailsVO.setProfileId(resultSet.getString("profile_id"));
	   enquiryDetailsVO.setProfileType(resultSet.getString("profile_type"));
	   enquiryDetailsVO.setHotline(resultSet.getString("hotline"));
	   enquiryDetailsVO.setEmailPrice(resultSet.getString("email_price"));
	   enquiryDetailsVO.setProspectusPrice(resultSet.getString("prospectus_price"));
	   enquiryDetailsVO.setOpenDaysPrice(resultSet.getString("open_days_price"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return enquiryDetailsVO;
   }
}
