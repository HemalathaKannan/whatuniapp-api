package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.PrivacyCAFlagVO;
import com.wuapp.util.developer.Debugger;

public class PrivacyCAFlagRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	PrivacyCAFlagVO privacyCAFlagVO = new PrivacyCAFlagVO();
	try {
	   privacyCAFlagVO.setPrivacyFlag(resultSet.getString("privacy_flag"));
	   privacyCAFlagVO.setCaFlag(resultSet.getString("ca_flag"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return privacyCAFlagVO;
   }
}
