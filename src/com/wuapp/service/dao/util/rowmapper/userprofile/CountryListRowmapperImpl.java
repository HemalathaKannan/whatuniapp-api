package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.CountryListVO;
import com.wuapp.util.developer.Debugger;

public class CountryListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	CountryListVO countryListVO = new CountryListVO();
	try {
	   countryListVO.setCountryId(resultSet.getString("country_id"));
	   countryListVO.setCountryName(resultSet.getString("country_name"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return countryListVO;
   }
}
