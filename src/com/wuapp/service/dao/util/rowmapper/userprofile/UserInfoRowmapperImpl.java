package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.UserInfoVO;
import com.wuapp.util.developer.Debugger;

public class UserInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserInfoVO userInfoVO = new UserInfoVO();
	try {
	   userInfoVO.setForeName(resultSet.getString("forename"));
	   userInfoVO.setSurName(resultSet.getString("surname"));
	   userInfoVO.setEmail(resultSet.getString("email"));
	   userInfoVO.setDateOfBirth(resultSet.getString("date_of_birth"));
	   userInfoVO.setAddressLineOne(resultSet.getString("address_line_1"));
	   userInfoVO.setAddressLineTwo(resultSet.getString("address_line_2"));
	   userInfoVO.setCity(resultSet.getString("city"));
	   userInfoVO.setPostcode(resultSet.getString("postcode_zip_prefix"));
	   userInfoVO.setCountyState(resultSet.getString("county_state"));
	   userInfoVO.setCountryId(resultSet.getString("country_id"));
	   ResultSet userInfoInnerRs = (ResultSet) resultSet.getObject("user_attributes");
	   try {
		if (userInfoInnerRs != null) {
		   while (userInfoInnerRs.next()) {
			userInfoVO.setSchoolId(userInfoInnerRs.getString("school_id"));
			userInfoVO.setSchoolName(userInfoInnerRs.getString("school_name"));
			userInfoVO.setGradeType(userInfoInnerRs.getString("grade_type"));
			userInfoVO.setGradeValue(userInfoInnerRs.getString("grade_value"));
		   }
		}
	   } catch (Exception e) {
		e.printStackTrace();
	   } finally {
		try {
		   if (userInfoInnerRs != null) {
			userInfoInnerRs.close();
			userInfoInnerRs = null;
		   }
		} catch (Exception e) {
		   throw new SQLException(e.getMessage());
		}
	   }
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userInfoVO;
   }
}
