package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.SchoolInfoVO;
import com.wuapp.util.developer.Debugger;

public class SchoolListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SchoolInfoVO schoolInfoVO = new SchoolInfoVO();
	try {
	   schoolInfoVO.setSchoolId(resultSet.getString("school_id"));
	   schoolInfoVO.setSchoolName(resultSet.getString("school_name"));
	   schoolInfoVO.setSchoolDisplayName(resultSet.getString("school_display_name"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return schoolInfoVO;
   }
}
