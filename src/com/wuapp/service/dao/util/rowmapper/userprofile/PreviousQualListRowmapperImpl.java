package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.PreviousQualVO;
import com.wuapp.util.developer.Debugger;

public class PreviousQualListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	PreviousQualVO previousQualVO = new PreviousQualVO();
	try {
	   previousQualVO.setQualificaiton(resultSet.getString("qualification"));
	   previousQualVO.setGradeString(resultSet.getString("grade_str"));
	   previousQualVO.setGradeLevel(resultSet.getString("grade_level"));
	   previousQualVO.setTextKey(resultSet.getString("text_key"));
	   previousQualVO.setPreviousQualSelected(resultSet.getString("prev_qual_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return previousQualVO;
   }
}
