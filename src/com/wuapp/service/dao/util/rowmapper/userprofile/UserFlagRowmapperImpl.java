package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.UserFlagVO;
import com.wuapp.util.developer.Debugger;

public class UserFlagRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserFlagVO userFlagVO = new UserFlagVO();
	try {
	   userFlagVO.setReceiveNoEmailFlag(resultSet.getString("receive_no_email_flag"));
	   userFlagVO.setPermitEmail(resultSet.getString("permit_email"));
	   userFlagVO.setSolusEmail(resultSet.getString("solus_email"));
	   userFlagVO.setMarketingEmail(resultSet.getString("marketing_email"));
	   userFlagVO.setSurveyFlag(resultSet.getString("survey_flag"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userFlagVO;
   }
}
