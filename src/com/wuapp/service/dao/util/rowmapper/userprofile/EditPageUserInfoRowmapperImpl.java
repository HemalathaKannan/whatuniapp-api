package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.UserFlagVO;
import com.wuapp.util.developer.Debugger;

public class EditPageUserInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserFlagVO userFlagVO = new UserFlagVO();
	try {
	   userFlagVO.setFirstName(resultSet.getString("forename"));
	   userFlagVO.setLastName(resultSet.getString("surname"));
	   userFlagVO.setPassword(resultSet.getString("password_match"));
	   userFlagVO.setUserImage(resultSet.getString("profile_image"));
	   userFlagVO.setFacebookId(resultSet.getString("facebook_id"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userFlagVO;
   }
}
