package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.QualificationVO;
import com.wuapp.util.developer.Debugger;

public class QualificationListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	QualificationVO qualificationVO = new QualificationVO();
	try {
	   qualificationVO.setQualificationName(resultSet.getString("qual_name"));
	   qualificationVO.setSelectedQualification(resultSet.getString("selected_qual"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return qualificationVO;
   }
}
