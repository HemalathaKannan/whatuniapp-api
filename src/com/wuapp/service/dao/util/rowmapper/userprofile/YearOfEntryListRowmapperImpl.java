package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.YearOfEntryVO;
import com.wuapp.util.developer.Debugger;

public class YearOfEntryListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	YearOfEntryVO yearOfEntryVO = new YearOfEntryVO();
	try {
	   yearOfEntryVO.setYearOfEntry(resultSet.getString("year_of_entry"));
	   yearOfEntryVO.setSelectedYear(resultSet.getString("selected_year"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return yearOfEntryVO;
   }
}
