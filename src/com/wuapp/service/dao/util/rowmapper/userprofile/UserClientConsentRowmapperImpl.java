package com.wuapp.service.dao.util.rowmapper.userprofile;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.userprofile.UserClientConsentVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper used for showing client consent details in preference page
 *
 * @author Hemalatha.K
 * @since wuapp1.0_ - Initial draft
 * @version 1.1  
 */

public class UserClientConsentRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserClientConsentVO userClientConsentVO = new UserClientConsentVO();
	try {
	   userClientConsentVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	   userClientConsentVO.setCollegeName(resultSet.getString("COLLEGE_NAME"));
	   userClientConsentVO.setUrl(resultSet.getString("URL"));
	   userClientConsentVO.setCreatedDate(resultSet.getString("CREATED_DATE"));	   
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userClientConsentVO;
   }
}
