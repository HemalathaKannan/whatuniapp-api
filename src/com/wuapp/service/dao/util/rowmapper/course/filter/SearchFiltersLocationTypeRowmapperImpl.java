package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.LocationTypeFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersLocationTypeRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	LocationTypeFilterVO locationTypeFilterVO = new LocationTypeFilterVO();
	try {
	   locationTypeFilterVO.setLocationTypeId(resultSet.getString("location_type_id"));
	   locationTypeFilterVO.setLocationTypeName(resultSet.getString("location_type_name"));
	   locationTypeFilterVO.setFlag(resultSet.getString("location_type_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return locationTypeFilterVO;
   }
}
