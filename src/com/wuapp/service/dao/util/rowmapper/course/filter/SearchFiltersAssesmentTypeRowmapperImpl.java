package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.AssesmentTypeFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersAssesmentTypeRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AssesmentTypeFilterVO assesmentTypeFilterVO = new AssesmentTypeFilterVO();
	try {
	   assesmentTypeFilterVO.setAssesmentTypeId(resultSet.getString("Assessment_type_id"));
	   assesmentTypeFilterVO.setAssesmentTypeName(resultSet.getString("Assessment_type_display_name"));
	   assesmentTypeFilterVO.setSelectedText(resultSet.getString("Selected_text"));
	   assesmentTypeFilterVO.setMediaId(resultSet.getString("media_id"));
	   assesmentTypeFilterVO.setMediaPath(resultSet.getString("MEDIA_IMAGE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return assesmentTypeFilterVO;
   }
}
