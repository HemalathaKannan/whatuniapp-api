package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.SubjectFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersSubjectRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SubjectFilterVO subjectFilterVO = new SubjectFilterVO();
	try {
	   subjectFilterVO.setCategoryCode(resultSet.getString("category_code"));
	   subjectFilterVO.setSubjectName(resultSet.getString("subject"));
	   subjectFilterVO.setCourseCount(resultSet.getString("course_cnt"));
	   subjectFilterVO.setSearchCategoryId(resultSet.getString("search_cat_id"));
	   subjectFilterVO.setFlag(resultSet.getString("subject_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return subjectFilterVO;
   }
}
