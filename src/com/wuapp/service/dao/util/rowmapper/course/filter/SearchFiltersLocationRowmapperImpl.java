package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.LocationFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersLocationRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	LocationFilterVO locationFilterVO = new LocationFilterVO();
	try {
	   locationFilterVO.setRegion(resultSet.getString("region_name"));
	   locationFilterVO.setTopRegion(resultSet.getString("top_region_name"));
	   locationFilterVO.setFlag(resultSet.getString("region_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return locationFilterVO;
   }
}
