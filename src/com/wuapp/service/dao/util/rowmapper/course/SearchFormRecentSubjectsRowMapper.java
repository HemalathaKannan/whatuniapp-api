package com.wuapp.service.dao.util.rowmapper.course;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.util.developer.Debugger;

public class SearchFormRecentSubjectsRowMapper implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SubjectVO subjectVO = new SubjectVO();
	try {
	   subjectVO.setCategoryCode(resultSet.getString("category_code"));
	   subjectVO.setDescription(resultSet.getString("description"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return subjectVO;
   }
}
