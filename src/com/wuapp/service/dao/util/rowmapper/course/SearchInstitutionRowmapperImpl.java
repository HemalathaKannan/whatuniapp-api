package com.wuapp.service.dao.util.rowmapper.course;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.util.developer.Debugger;

public class SearchInstitutionRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	InstitutionVO institutionVO = new InstitutionVO();
	try {
	   institutionVO.setId(resultSet.getString("college_id"));
	   institutionVO.setName(resultSet.getString("college_name_display"));
	   institutionVO.setCourseCount(resultSet.getString("course_count"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return institutionVO;
   }
}
