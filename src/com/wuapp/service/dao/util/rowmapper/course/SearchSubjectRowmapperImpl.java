package com.wuapp.service.dao.util.rowmapper.course;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.util.developer.Debugger;

public class SearchSubjectRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SubjectVO subjectVO = new SubjectVO();
	try {
	   subjectVO.setDescription(resultSet.getString("description"));
	   subjectVO.setSearchCode(resultSet.getString("search_code"));
	   subjectVO.setSearchId(resultSet.getString("search_id"));
	   subjectVO.setCourseCount(resultSet.getString("Course_count"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return subjectVO;
   }
}
