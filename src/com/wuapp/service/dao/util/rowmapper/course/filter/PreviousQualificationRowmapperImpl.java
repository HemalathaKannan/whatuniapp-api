package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.PreviousQualificationFilterVO;
import com.wuapp.util.developer.Debugger;

public class PreviousQualificationRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	PreviousQualificationFilterVO previousQualificationFilterVO = new PreviousQualificationFilterVO();
	try {
	   previousQualificationFilterVO.setQualificaiton(resultSet.getString("qualification"));
	   previousQualificationFilterVO.setGradeString(resultSet.getString("grade_str"));
	   previousQualificationFilterVO.setGradeLevel(resultSet.getString("grade_level"));
	   previousQualificationFilterVO.setTextKey(resultSet.getString("text_key"));
	   previousQualificationFilterVO.setPreviousQualSelected(resultSet.getString("prev_qual_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return previousQualificationFilterVO;
   }
}
