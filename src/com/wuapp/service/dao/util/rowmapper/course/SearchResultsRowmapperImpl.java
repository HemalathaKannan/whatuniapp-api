package com.wuapp.service.dao.util.rowmapper.course;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.searchresults.SearchResultsVO;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : SearchResultsRowmapperImpl.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related column 
 */

public class SearchResultsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SearchResultsVO searchResultsVO = new SearchResultsVO();
	try {
	   searchResultsVO.setId(resultSet.getString("COLLEGE_ID"));
	   searchResultsVO.setName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   searchResultsVO.setCourseCount(resultSet.getString("COURSE_COUNT"));
	   searchResultsVO.setLogo(resultSet.getString("COLLEGE_LOGO"));
	   searchResultsVO.setLogoName(resultSet.getString("COLLEGE_LOGO"));
	   searchResultsVO.setReviewRating(resultSet.getString("REVIEW_RATING"));
	   searchResultsVO.setReviewRatingDisplay(resultSet.getString("review_rating_display"));
	   searchResultsVO.setShortlistFlag(resultSet.getString("SHORTLIST_FLAG"));
	   searchResultsVO.setMatchingPercentage(resultSet.getString("MATCHING_PERCENTAGE"));
	   searchResultsVO.setTileMediaPath(resultSet.getString("TILE_MEDIA_PATH"));
	   searchResultsVO.setQualificationMatch(resultSet.getString("QUALIFICATION_MATCH"));
	   searchResultsVO.setSubjectMatch(resultSet.getString("SUBJECT_MATCH"));
	   searchResultsVO.setLocationMatch(resultSet.getString("LOCATION_MATCH"));
	   searchResultsVO.setLocationTypeMatch(resultSet.getString("LOCATION_TYPE_MATCH"));
	   searchResultsVO.setStudyModeMatch(resultSet.getString("STUDY_MODE_MATCH"));
	   searchResultsVO.setPreviousQualificationMatch(resultSet.getString("PREV_QUAL_MATCH"));
	   searchResultsVO.setAssessmentTypeMatch(resultSet.getString("assessment_type_match"));
	   searchResultsVO.setReviewSubjectOneMatch(resultSet.getString("review_subject_one_match"));
	   searchResultsVO.setReviewSubjectTwoMatch(resultSet.getString("review_subject_two_match"));
	   searchResultsVO.setReviewSubjectThreeMatch(resultSet.getString("review_subject_three_match"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return searchResultsVO;
   }
}
