package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.ReviewCategoryFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersReviewCategoryRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ReviewCategoryFilterVO reviewCategoryFilterVO = new ReviewCategoryFilterVO();
	try {
	   reviewCategoryFilterVO.setReviewCategoryId(resultSet.getString("review_category_id"));
	   reviewCategoryFilterVO.setReviewCategoryName(resultSet.getString("review_category_name"));
	   reviewCategoryFilterVO.setTextKey(resultSet.getString("text_key"));
	   reviewCategoryFilterVO.setSelectedText(resultSet.getString("selected_text"));
	   reviewCategoryFilterVO.setMediaId(resultSet.getString("media_id"));
	   reviewCategoryFilterVO.setMediaPath(resultSet.getString("media_image"));
	   reviewCategoryFilterVO.setReviewCategoryDisplayName(resultSet.getString("review_category_display_name"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return reviewCategoryFilterVO;
   }
}
