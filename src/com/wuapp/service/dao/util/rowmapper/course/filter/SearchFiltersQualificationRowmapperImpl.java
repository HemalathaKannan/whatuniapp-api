package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.QualificationFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersQualificationRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	QualificationFilterVO qualificationFilterVO = new QualificationFilterVO();
	try {
	   qualificationFilterVO.setQualificationCode(resultSet.getString("qual_code"));
	   qualificationFilterVO.setQualificaiton(resultSet.getString("qual_name"));
	   qualificationFilterVO.setFlag(resultSet.getString("qual_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return qualificationFilterVO;
   }
}
