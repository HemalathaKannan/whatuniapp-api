package com.wuapp.service.dao.util.rowmapper.course.filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.filters.StudyModeFilterVO;
import com.wuapp.util.developer.Debugger;

public class SearchFiltersStudyModeRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	StudyModeFilterVO studyModeFilterVO = new StudyModeFilterVO();
	try {
	   studyModeFilterVO.setStudyMode(resultSet.getString("study_mode"));
	   studyModeFilterVO.setTextKey(resultSet.getString("text_key"));
	   studyModeFilterVO.setFlag(resultSet.getString("study_mode_selected"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return studyModeFilterVO;
   }
}
