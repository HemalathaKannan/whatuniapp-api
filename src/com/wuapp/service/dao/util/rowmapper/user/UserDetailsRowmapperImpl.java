package com.wuapp.service.dao.util.rowmapper.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.user.UserDetailsVO;
import com.wuapp.util.developer.Debugger;

public class UserDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UserDetailsVO userDetailsVO = new UserDetailsVO();
	try {
	   userDetailsVO.setForename(resultSet.getString("forename"));
	   userDetailsVO.setSurname(resultSet.getString("surname"));
	   userDetailsVO.setUserImage(resultSet.getString("user_image"));
	   userDetailsVO.setFacebookId(resultSet.getString("facebook_id"));
	   userDetailsVO.setSchoolURN(resultSet.getString("school_urn"));
	   userDetailsVO.setSchoolName(resultSet.getString("school_name"));
	   userDetailsVO.setUserSource(resultSet.getString("user_source"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return userDetailsVO;
   }
}
