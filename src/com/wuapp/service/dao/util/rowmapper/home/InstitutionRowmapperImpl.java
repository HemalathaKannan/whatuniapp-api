package com.wuapp.service.dao.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.util.developer.Debugger;

public class InstitutionRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	InstitutionVO institutionVO = new InstitutionVO();
	try {
	   institutionVO.setId(resultSet.getString("COLLEGE_ID"));
	   institutionVO.setName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   institutionVO.setLogo(resultSet.getString("LOGO"));
	   institutionVO.setLogoName(resultSet.getString("LOGO"));
	   institutionVO.setReviewRating(resultSet.getString("REVIEW_RATING"));
	   institutionVO.setReviewRatingDisplay(resultSet.getString("review_rating_display"));
	   institutionVO.setTileMediaPath(resultSet.getString("MEDIA_PATH"));
	   institutionVO.setDistanceFromUserLatLag(resultSet.getString("USER_DIST"));
	   institutionVO.setShortlistFlag(resultSet.getString("SHORTLIST_FLAG"));
	   institutionVO.setNextOpenDay(resultSet.getString("NEXT_OPENDAY"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return institutionVO;
   }
}
