package com.wuapp.service.dao.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.FeaturedProviderVO;

public class FeaturedProviderRowmapperImpl implements RowMapper<Object> {

   @Override
   public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	FeaturedProviderVO featuredProviderVO = new FeaturedProviderVO();
	featuredProviderVO.setCollegeId(rs.getString("COLLEGE_ID"));
	featuredProviderVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
	featuredProviderVO.setLogo(rs.getString("LOGO"));
	featuredProviderVO.setMediaPath(rs.getString("MEDIA_PATH"));
	featuredProviderVO.setReviewRating(rs.getString("REVIEW_RATING"));
	featuredProviderVO.setReviewRatingDisplay(rs.getString("REVIEW_RATING_DISPLAY"));
	featuredProviderVO.setUrl(rs.getString("URL"));
	return featuredProviderVO;
   }
}
