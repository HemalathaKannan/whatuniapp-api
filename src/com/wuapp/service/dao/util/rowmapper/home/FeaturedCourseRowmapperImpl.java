package com.wuapp.service.dao.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.FeaturedSlotsVO;
import com.wuapp.util.developer.Debugger;

/**
 * This row mapper used to get featured course details
 *
 * @author Hemalatha.K
 * @since wuapp1.0_ - Initial draft
 * @version 1.1  
 */

public class FeaturedCourseRowmapperImpl implements RowMapper<Object> {

   @Override
   public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
     FeaturedSlotsVO featuredSlotsVO = new FeaturedSlotsVO();
     try {
	 featuredSlotsVO.setCollegeId(rs.getString("COLLEGE_ID"));
	 featuredSlotsVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
	 featuredSlotsVO.setLogo(rs.getString("LOGO"));
	 featuredSlotsVO.setMediaPath(rs.getString("MEDIA_PATH"));
	 featuredSlotsVO.setReviewRating(rs.getString("REVIEW_RATING"));
	 featuredSlotsVO.setReviewRatingDisplay(rs.getString("REVIEW_RATING_DISPLAY"));
	 featuredSlotsVO.setCourseId(rs.getString("COURSE_ID"));
	 featuredSlotsVO.setCourseName(rs.getString("COURSE_NAME"));
     } catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
     }
     return featuredSlotsVO;
   }
}
