package com.wuapp.service.dao.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.util.developer.Debugger;

public class SubjectRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SubjectVO subjectVO = new SubjectVO();
	try {
	   subjectVO.setSearchCode(resultSet.getString("category_code"));
	   subjectVO.setSearchId(resultSet.getString("search_cat_id"));
	   subjectVO.setSubjectName(resultSet.getString("subject"));
	   subjectVO.setTileMediaPath(resultSet.getString("media_path"));
	   subjectVO.setMediaId(resultSet.getString("media_id"));
	   subjectVO.setCourseCount(resultSet.getString("course_count"));
	   subjectVO.setIconMediaPath(resultSet.getString("ICON_MEDIA_PATH"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return subjectVO;
   }
}
