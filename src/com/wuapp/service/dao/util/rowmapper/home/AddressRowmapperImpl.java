package com.wuapp.service.dao.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.home.AddressVO;
import com.wuapp.util.developer.Debugger;

public class AddressRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AddressVO addressVO = new AddressVO();
	try {
	   addressVO.setAddressLine1(resultSet.getString("ADDRESS_LINE_1"));
	   addressVO.setAddressLine2(resultSet.getString("ADDRESS_LINE_2"));
	   addressVO.setCity(resultSet.getString("CITY"));
	   addressVO.setPostcode(resultSet.getString("POSTCODE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return addressVO;
   }
}
