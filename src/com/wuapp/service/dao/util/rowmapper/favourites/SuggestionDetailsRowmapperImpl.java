package com.wuapp.service.dao.util.rowmapper.favourites;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.favourites.SuggestionDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class SuggestionDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	SuggestionDetailsVO suggestionDetailsVO = new SuggestionDetailsVO();
	try {
	   suggestionDetailsVO.setInstitutionId(resultSet.getString("college_id"));
	   suggestionDetailsVO.setInstitutionName(resultSet.getString("college_name"));
	   suggestionDetailsVO.setLogo(resultSet.getString("college_logo"));
	   suggestionDetailsVO.setLogoName(resultSet.getString("college_logo"));
	   suggestionDetailsVO.setCourseId(resultSet.getString("course_id"));
	   suggestionDetailsVO.setCourseTitle(resultSet.getString("course_title"));
	   suggestionDetailsVO.setMatchingPercentage(resultSet.getString("matching_percentage"));
	   suggestionDetailsVO.setShortlistFlag(resultSet.getString("shortlist_flag"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return suggestionDetailsVO;
   }
}
