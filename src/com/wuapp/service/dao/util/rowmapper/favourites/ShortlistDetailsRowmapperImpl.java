package com.wuapp.service.dao.util.rowmapper.favourites;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.favourites.ShortlistDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class ShortlistDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ShortlistDetailsVO shortlistDetailsVO = new ShortlistDetailsVO();
	try {
	   shortlistDetailsVO.setBasketContentId(resultSet.getString("basket_content_id"));
	   shortlistDetailsVO.setInstitutionId(resultSet.getString("college_id"));
	   shortlistDetailsVO.setInstitutionName(resultSet.getString("college_name"));
	   shortlistDetailsVO.setLogo(resultSet.getString("college_logo"));
	   shortlistDetailsVO.setLogoName(resultSet.getString("college_logo"));
	   shortlistDetailsVO.setCourseId(resultSet.getString("course_id"));
	   shortlistDetailsVO.setCourseTitle(resultSet.getString("course_title"));
	   shortlistDetailsVO.setBasketId(resultSet.getString("basket_id"));
	   shortlistDetailsVO.setPosition(resultSet.getString("position"));
	   shortlistDetailsVO.setFinalChoiceId(resultSet.getString("final_choice_id"));
	   shortlistDetailsVO.setMatchingPercentage(resultSet.getString("matching_percentage"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return shortlistDetailsVO;
   }
}
