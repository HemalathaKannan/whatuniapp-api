package com.wuapp.service.dao.util.rowmapper.provider;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.provider.ProviderResultsVO;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : InstitutionSearchResultsRowmapperImpl.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related column 
 */

public class InstitutionSearchResultsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ProviderResultsVO providerResultsVO = new ProviderResultsVO();
	try {
	   providerResultsVO.setCourseId(resultSet.getString("COURSE_ID"));
	   providerResultsVO.setDescription(resultSet.getString("COURSE_DESC"));
	   providerResultsVO.setStudymode(resultSet.getString("AVAILABLE_STUDY_MODES"));
	   providerResultsVO.setFaculty(resultSet.getString("DEPARTMENT_OR_FACULTY"));
	   providerResultsVO.setTotalCourseCount(resultSet.getString("TOTAL_CNT"));
	   providerResultsVO.setCourseTitle(resultSet.getString("COURSE_TITLE"));
	   providerResultsVO.setInstitutionName(resultSet.getString("COLLEGE_NAME"));
	   providerResultsVO.setInstitutionDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   providerResultsVO.setInstitutionId(resultSet.getString("COLLEGE_ID"));
	   providerResultsVO.setStudyLevel(resultSet.getString("study_level"));
	   providerResultsVO.setEntryLevel(resultSet.getString("entry_level"));
	   providerResultsVO.setSuborderItemId(resultSet.getString("SUBORDER_ITEM_ID"));
	   providerResultsVO.setCourseRank(resultSet.getString("COURSE_RANK"));
	   providerResultsVO.setQualificationMatch(resultSet.getString("qualification_match"));
	   providerResultsVO.setSubjectMatch(resultSet.getString("subject_match"));
	   providerResultsVO.setLocationMatch(resultSet.getString("location_match"));
	   providerResultsVO.setLocationTypeMatch(resultSet.getString("location_type_match"));
	   providerResultsVO.setStudyModeMatch(resultSet.getString("study_mode_match"));
	   providerResultsVO.setPreviousQualMatch(resultSet.getString("prev_qual_match"));
	   providerResultsVO.setAssessmentTypeMatch(resultSet.getString("assessment_type_match"));
	   providerResultsVO.setReviewSubjectOneMatch(resultSet.getString("review_subject_one_match"));
	   providerResultsVO.setReviewSubjectTwoMatch(resultSet.getString("review_subject_two_match"));
	   providerResultsVO.setReviewSubjectThreeMatch(resultSet.getString("review_subject_three_match"));
	   providerResultsVO.setMatchingPercentage(resultSet.getString("matching_percentage"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return providerResultsVO;
   }
}
