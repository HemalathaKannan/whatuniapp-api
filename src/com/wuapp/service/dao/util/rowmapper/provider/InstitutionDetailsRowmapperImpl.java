package com.wuapp.service.dao.util.rowmapper.provider;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.provider.InstitutionDetailsVO;
import com.wuapp.util.developer.Debugger;

public class InstitutionDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	InstitutionDetailsVO institutionDetailsVO = new InstitutionDetailsVO();
	try {
	   institutionDetailsVO.setOverview(resultSet.getString("OVERVIEW"));
	   institutionDetailsVO.setName(resultSet.getString("COLLEGE_NAME"));
	   institutionDetailsVO.setDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
	   institutionDetailsVO.setOverallRating(resultSet.getString("OVERALL_RATING"));
	   institutionDetailsVO.setReviewRatingDisplay(resultSet.getString("review_rating_display"));
	   institutionDetailsVO.setId(resultSet.getString("HC_COLLEGE_ID"));
	   institutionDetailsVO.setReviewCount(resultSet.getString("REVIEW_COUNT"));
	   institutionDetailsVO.setMediaPath(resultSet.getString("media_path"));
	   institutionDetailsVO.setShortListFlag(resultSet.getString("shortlist_flag"));
	   institutionDetailsVO.setLogo(resultSet.getString("logo"));
	   institutionDetailsVO.setLogoName(resultSet.getString("logo"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return institutionDetailsVO;
   }
}
