package com.wuapp.service.dao.util.rowmapper.quiz;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.quiz.JobIndustryAjaxVO;
import com.wuapp.util.developer.Debugger;

public class JobIndustryAjaxListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	JobIndustryAjaxVO jobIndustryAjaxVO = new JobIndustryAjaxVO();
	try {
	   jobIndustryAjaxVO.setJobOrIndustryFlag(resultSet.getString("job_industry_flag"));
	   jobIndustryAjaxVO.setJobOrIndustryId(resultSet.getString("job_industry_id"));
	   jobIndustryAjaxVO.setJobOrIndustryName(resultSet.getString("job_industry_name"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return jobIndustryAjaxVO;
   }
}
