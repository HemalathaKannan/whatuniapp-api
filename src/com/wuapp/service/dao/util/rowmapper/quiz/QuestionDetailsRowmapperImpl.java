package com.wuapp.service.dao.util.rowmapper.quiz;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.quiz.AnswerOptionVO;
import com.wuapp.service.dao.util.valueobject.quiz.QuestionDetailsVO;
import com.wuapp.util.developer.Debugger;

public class QuestionDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	QuestionDetailsVO questionVO = new QuestionDetailsVO();
	try {
	   questionVO.setQuestionId(resultSet.getString("question_id"));
	   questionVO.setQuestionName(resultSet.getString("question_name"));
	   ResultSet questionTextInnerRs = (ResultSet) resultSet.getObject("question_text");
	   try {
		if (questionTextInnerRs != null) {
		   ArrayList<QuestionDetailsVO> questionList = new ArrayList<QuestionDetailsVO>();
		   QuestionDetailsVO questionInnerVO = null;
		   while (questionTextInnerRs.next()) {
			questionInnerVO = new QuestionDetailsVO();
			questionInnerVO.setQuestionText(questionTextInnerRs.getString("question_text"));
			questionList.add(questionInnerVO);
		   }
		   questionVO.setQuestionList(questionList);
		}
	   } catch (Exception e) {
		e.printStackTrace();
	   } finally {
		try {
		   if (questionTextInnerRs != null) {
			questionTextInnerRs.close();
			questionTextInnerRs = null;
		   }
		} catch (Exception e) {
		   throw new SQLException(e.getMessage());
		}
	   }
	   questionVO.setAnswerType(resultSet.getString("answer_type"));
	   questionVO.setAnswerDisplayStyle(resultSet.getString("answer_display_style"));
	   questionVO.setSkipToNext(resultSet.getString("skip_to_next"));
	   questionVO.setSkipExit(resultSet.getString("skip_exit"));
	   questionVO.setProgressPercent(resultSet.getString("progress_percent"));
	   questionVO.setCtaButtonText(resultSet.getString("cta_button_text"));
	   questionVO.setFilterName(resultSet.getString("filter_name"));
	   questionVO.setTimeDeplay(resultSet.getString("time_delay"));
	   ResultSet answerOptionsInnerRs = (ResultSet) resultSet.getObject("answer_options");
	   try {
		if (answerOptionsInnerRs != null) {
		   ArrayList<AnswerOptionVO> answerOptionList = new ArrayList<AnswerOptionVO>();
		   AnswerOptionVO answerOptionVO = null;
		   while (answerOptionsInnerRs.next()) {
			answerOptionVO = new AnswerOptionVO();
			answerOptionVO.setOptionText(answerOptionsInnerRs.getString("option_text"));
			answerOptionVO.setOptionValue(answerOptionsInnerRs.getString("option_value"));
			answerOptionVO.setNextQuestionId(answerOptionsInnerRs.getString("next_question_id"));
			answerOptionVO.setCommentAgainstAnswer(answerOptionsInnerRs.getString("comment_against_answer"));
			answerOptionList.add(answerOptionVO);
		   }
		   questionVO.setAnswerOptionsList(answerOptionList);
		}
	   } catch (Exception e) {
		e.printStackTrace();
	   } finally {
		try {
		   if (answerOptionsInnerRs != null) {
			answerOptionsInnerRs.close();
			answerOptionsInnerRs = null;
		   }
		} catch (Exception e) {
		   throw new SQLException(e.getMessage());
		}
	   }
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return questionVO;
   }
}
