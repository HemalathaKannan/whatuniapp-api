package com.wuapp.service.dao.util.rowmapper.quiz;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.quiz.CourseAjaxVO;
import com.wuapp.util.developer.Debugger;

public class CourseAjaxListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	CourseAjaxVO courseAjaxVO = new CourseAjaxVO();
	try {
	   courseAjaxVO.setDescription(resultSet.getString("description"));
	   courseAjaxVO.setSearchId(resultSet.getString("search_id"));
	   courseAjaxVO.setSearchCode(resultSet.getString("search_code"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return courseAjaxVO;
   }
}
