package com.wuapp.service.dao.util.rowmapper.quiz;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.quiz.PreviousQualVO;
import com.wuapp.util.developer.Debugger;

public class PreviousQualRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	PreviousQualVO previousQualVO = new PreviousQualVO();
	try {
	   previousQualVO.setQualification(resultSet.getString("QUALIFICATION"));
	   previousQualVO.setGradeStr(resultSet.getString("GRADE_STR"));
	   previousQualVO.setGradeLevel(resultSet.getString("GRADE_LEVEL"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return previousQualVO;
   }
}
