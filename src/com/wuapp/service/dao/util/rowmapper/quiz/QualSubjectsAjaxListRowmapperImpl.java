package com.wuapp.service.dao.util.rowmapper.quiz;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.quiz.QualSubjectAjaxVO;
import com.wuapp.util.developer.Debugger;

public class QualSubjectsAjaxListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	QualSubjectAjaxVO qualSubjectAjaxVO = new QualSubjectAjaxVO();
	try {
	   qualSubjectAjaxVO.setSearchId(resultSet.getString("qual_subject_id"));
	   qualSubjectAjaxVO.setDescription(resultSet.getString("qual_subject_desc"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return qualSubjectAjaxVO;
   }
}
