package com.wuapp.service.dao.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.common.AccessTokenVO;

public class AccessTokenRowMapperImpl implements RowMapper {

   public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	AccessTokenVO accessTokenVO = new AccessTokenVO();
	accessTokenVO.setAffiliatedId(rs.getString("affiliate_id"));
	accessTokenVO.setAccessToken(rs.getString("access_token"));
	return accessTokenVO;
   }
}
