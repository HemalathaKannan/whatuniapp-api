package com.wuapp.service.dao.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.util.developer.Debugger;

public class AppVersionRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AppVersionVO appVersionVO = new AppVersionVO();
	try {
	   appVersionVO.setVersionName(resultSet.getString("VERSION_NAME"));
	   appVersionVO.setStatus(resultSet.getString("STATUS"));
	   appVersionVO.setMessage(resultSet.getString("MESSAGE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw e;
	}
	return appVersionVO;
   }
}
