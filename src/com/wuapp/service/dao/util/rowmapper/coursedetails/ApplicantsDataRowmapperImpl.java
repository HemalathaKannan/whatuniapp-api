package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.ApplicantsDataVO;
import com.wuapp.util.developer.Debugger;

public class ApplicantsDataRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ApplicantsDataVO applicationDataVO = new ApplicantsDataVO();
	try {
	   applicationDataVO.setTotalApplicants(resultSet.getString("TOTAL_APPLICANTS"));
	   applicationDataVO.setSuccessfulApplicants(resultSet.getString("SUCCESSFUL_APPLICANTS"));
	   applicationDataVO.setApplicantsVal(resultSet.getString("APPLICANTS_VAL"));
	   applicationDataVO.setOffersVal(resultSet.getString("OFFERS_VAL"));
	   applicationDataVO.setApplicantSuccessRate(resultSet.getString("APPLICANT_SUCCESSRATE"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return applicationDataVO;
   }
}
