package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.AssessedCourseVO;
import com.wuapp.util.developer.Debugger;

public class GetAssessedCourseRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	AssessedCourseVO assessedCourseVO = new AssessedCourseVO();
	try {
	   assessedCourseVO.setExam(resultSet.getString("EXAM"));
	   assessedCourseVO.setCourseWork(resultSet.getString("COURSE_WORK"));
	   assessedCourseVO.setPraticalWork(resultSet.getString("PRACTICAL_WORK"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return assessedCourseVO;
   }
}
