package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.OppurtunityEntryReqVO;
import com.wuapp.util.developer.Debugger;

/**
 * Clearing entry requirement detail row mapper for the course details page.
 *
 * @author Sangeeth.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class OppEntryReqDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OppurtunityEntryReqVO oppurtunityEntryReqVO = new OppurtunityEntryReqVO();
	try {
	   oppurtunityEntryReqVO.setOpportunityId(resultSet.getString("opportunity_id"));
	   oppurtunityEntryReqVO.setEntryDesc(resultSet.getString("entry_desc"));
	   oppurtunityEntryReqVO.setEntryPoints(resultSet.getString("entry_points"));
	   oppurtunityEntryReqVO.setAdditionalInfo(resultSet.getString("additional_info"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return oppurtunityEntryReqVO;
   }
}
