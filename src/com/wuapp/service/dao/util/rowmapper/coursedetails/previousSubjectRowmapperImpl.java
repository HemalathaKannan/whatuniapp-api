package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.PreviousSubjectVO;
import com.wuapp.util.developer.Debugger;

public class previousSubjectRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	PreviousSubjectVO previousSubjectVO = new PreviousSubjectVO();
	try {
	   previousSubjectVO.setSubjectName(resultSet.getString("subject_name"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return previousSubjectVO;
   }
}
