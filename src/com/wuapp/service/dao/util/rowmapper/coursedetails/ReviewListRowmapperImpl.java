package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.ReviewDetailsVO;
import com.wuapp.util.developer.Debugger;

public class ReviewListRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ReviewDetailsVO reviewDetailsVO = new ReviewDetailsVO();
	try {
	   reviewDetailsVO.setReviewId(resultSet.getString("REVIEW_ID"));
	   reviewDetailsVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
	   reviewDetailsVO.setOverallRating(resultSet.getString("OVERALL_RATING"));
	   reviewDetailsVO.setReviewRatingDisplay(resultSet.getString("review_rating_display"));
	   reviewDetailsVO.setOverallRatingComments(resultSet.getString("OVERALL_RATING_COMMENTS"));
	   reviewDetailsVO.setReviewedDate(resultSet.getString("REVIEWED_DATE"));
	   reviewDetailsVO.setReviewerName(resultSet.getString("REVIEWER_NAME"));
	   reviewDetailsVO.setDisplayOrder(resultSet.getString("DISPLAY_ORDER"));
	   reviewDetailsVO.setReviewCount(resultSet.getString("REVIEW_CNT"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return reviewDetailsVO;
   }
}
