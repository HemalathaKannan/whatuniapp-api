package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.EnquiryInfoVO;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : EnquiryInfoRowmapperImpl.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related column 
 */

public class EnquiryInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	EnquiryInfoVO enquiryInfoVO = new EnquiryInfoVO();
	try {
	   enquiryInfoVO.setOrderItemId(resultSet.getString("order_item_id"));
	   enquiryInfoVO.setWebsiteFlag(resultSet.getString("website_flag"));
	   enquiryInfoVO.setEmailFlag(resultSet.getString("email_flag"));
	   enquiryInfoVO.setWebformFlag(resultSet.getString("webform_flag"));
	   enquiryInfoVO.setProspectusFlag(resultSet.getString("prospectus_flag"));
	   enquiryInfoVO.setProspectusWebformFlag(resultSet.getString("prospectus_webform_flag"));
	   enquiryInfoVO.setEmail(resultSet.getString("email"));
	   enquiryInfoVO.setEmailWebform(resultSet.getString("email_webform"));
	   enquiryInfoVO.setProspectus(resultSet.getString("prospectus"));
	   enquiryInfoVO.setProspectusWebform(resultSet.getString("prospectus_webform"));
	   enquiryInfoVO.setWebsite(resultSet.getString("website"));
	   enquiryInfoVO.setSuborderItemId(resultSet.getString("suborder_item_id"));
	   enquiryInfoVO.setMyhcProfileId(resultSet.getString("myhc_profile_id"));
	   enquiryInfoVO.setProfileType(resultSet.getString("profile_type"));
	   enquiryInfoVO.setNetworkId(resultSet.getString("network_id"));
	   enquiryInfoVO.setHotline(resultSet.getString("hotline")); 
	   enquiryInfoVO.setEmailPrice(resultSet.getString("email_price"));
	   enquiryInfoVO.setProspectusPrice(resultSet.getString("prospectus_price"));
	   enquiryInfoVO.setOpenDaysPrice(resultSet.getString("open_days_price"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return enquiryInfoVO;
   }
}
