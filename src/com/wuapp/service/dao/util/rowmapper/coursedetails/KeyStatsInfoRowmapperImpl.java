package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.KeyStatsInfoVO;
import com.wuapp.util.developer.Debugger;

public class KeyStatsInfoRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	KeyStatsInfoVO keyStatsInfoVO = new KeyStatsInfoVO();
	try {
	   keyStatsInfoVO.setSubjectName(resultSet.getString("SUBJECT_NAME"));
	   keyStatsInfoVO.setRatio(resultSet.getString("RATIO"));
	   keyStatsInfoVO.setDropOutRate(resultSet.getString("DROP_OUT_RATE"));
	   keyStatsInfoVO.setGraduatesPercent(resultSet.getString("GRADUATES_PERCENT"));
	   keyStatsInfoVO.setSalary6Months(resultSet.getString("SALARY_6_MONTHS"));
	   keyStatsInfoVO.setSalary40Months(resultSet.getString("SALARY_40_MONTHS"));
	   keyStatsInfoVO.setSalary6MonthsAtUni(resultSet.getString("SALARY_6_MONTHS_AT_UNI"));
	   keyStatsInfoVO.setSalary6MonthsPer(resultSet.getString("SALARY_6_MONTHS_PER"));
	   keyStatsInfoVO.setSalary40MonthsPer(resultSet.getString("SALARY_40_MONTHS_PER"));
	   keyStatsInfoVO.setSalary6MonthsAtUniPer(resultSet.getString("SALARY_6_MONTHS_AT_UNI_PER"));
	   keyStatsInfoVO.setContinuingCourse(resultSet.getString("CONTINUING_COURSE"));
	   keyStatsInfoVO.setCompletedCourse(resultSet.getString("COMPLETED_COURSE"));
	   keyStatsInfoVO.setCourseContinuation(resultSet.getString("COURSE_CONTINUATION"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return keyStatsInfoVO;
   }
}
