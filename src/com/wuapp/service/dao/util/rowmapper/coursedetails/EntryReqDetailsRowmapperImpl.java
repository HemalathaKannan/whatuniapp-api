package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.EntryReqVO;
import com.wuapp.util.developer.Debugger;

public class EntryReqDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	EntryReqVO entryReqVO = new EntryReqVO();
	try {
	   entryReqVO.setEntryLevel(resultSet.getString("entry_level"));
	   entryReqVO.setPoints(resultSet.getString("points"));
	   entryReqVO.setDescription(resultSet.getString("description"));
	   entryReqVO.setUcasFlag(resultSet.getString("ucas_flag"));
	   entryReqVO.setUcasTariff(resultSet.getString("ucas_tariff"));
	   entryReqVO.setUcasDescription(resultSet.getString("ucas_desc"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return entryReqVO;
   }
}
