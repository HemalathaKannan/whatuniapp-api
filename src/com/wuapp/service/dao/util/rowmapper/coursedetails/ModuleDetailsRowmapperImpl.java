package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.ModuleDetailsVO;
import com.wuapp.util.developer.Debugger;

public class ModuleDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	ModuleDetailsVO moduleDetailsVO = new ModuleDetailsVO();
	try {
	   moduleDetailsVO.setModuleGroupId(resultSet.getString("module_group_id"));
	   moduleDetailsVO.setCourseId(resultSet.getString("course_id"));
	   moduleDetailsVO.setStageLableId(resultSet.getString("stage_label_id"));
	   moduleDetailsVO.setStageLableName(resultSet.getString("stage_label_name"));
	   moduleDetailsVO.setModule(resultSet.getString("modules"));
	   moduleDetailsVO.setModuleGroup(resultSet.getString("module_group"));
	   ResultSet moduleDetailsInnerRs = (ResultSet) resultSet.getObject("module_details");
	   try {
		if (moduleDetailsInnerRs != null) {
		   ArrayList<ModuleDetailsVO> moduleDetailList = new ArrayList<ModuleDetailsVO>();
		   ModuleDetailsVO moduleDetailsInnerVO = null;
		   while (moduleDetailsInnerRs.next()) {
			moduleDetailsInnerVO = new ModuleDetailsVO();
			moduleDetailsInnerVO.setModuleId(moduleDetailsInnerRs.getString("module_id"));
			moduleDetailsInnerVO.setModuleTitle(moduleDetailsInnerRs.getString("module_title"));
			moduleDetailsInnerVO.setCredits(moduleDetailsInnerRs.getString("credits"));
			moduleDetailsInnerVO.setModuleTypeName(moduleDetailsInnerRs.getString("module_type_name"));
			moduleDetailsInnerVO.setModuleDescription(moduleDetailsInnerRs.getString("module_description"));
			moduleDetailsInnerVO.setModuleUrl(moduleDetailsInnerRs.getString("module_url"));
			moduleDetailsInnerVO.setShortDescription(moduleDetailsInnerRs.getString("short_desc"));
			moduleDetailList.add(moduleDetailsInnerVO);
		   }
		   moduleDetailsVO.setModuleDetaiList(moduleDetailList);
		}
	   } catch (Exception e) {
		e.printStackTrace();
	   } finally {
		try {
		   if (moduleDetailsInnerRs != null) {
			moduleDetailsInnerRs.close();
			moduleDetailsInnerRs = null;
		   }
		} catch (Exception e) {
		   throw new SQLException(e.getMessage());
		}
	   }
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return moduleDetailsVO;
   }
}
