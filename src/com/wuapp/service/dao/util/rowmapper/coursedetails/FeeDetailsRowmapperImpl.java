package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.FeesDetailsVO;
import com.wuapp.util.developer.Debugger;

/**
 * Fee detail row mapper for the course details page.
 *
 * @author Sangeeth.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class FeeDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	FeesDetailsVO feesDetailsVO = new FeesDetailsVO();
	try {
	   feesDetailsVO.setFeeType(resultSet.getString("fee_type"));
	   feesDetailsVO.setFee(resultSet.getString("fee"));
	   feesDetailsVO.setDuration(resultSet.getString("duration"));
	   feesDetailsVO.setCurrency(resultSet.getString("currency"));
	   feesDetailsVO.setFeeDesc(resultSet.getString("fee_desc"));
	   feesDetailsVO.setState(resultSet.getString("state"));
	   feesDetailsVO.setUrl(resultSet.getString("url"));
	   feesDetailsVO.setSuborderItemId(resultSet.getString("suborder_item_id"));
	   feesDetailsVO.setNetworkID(resultSet.getString("network_id"));
	   feesDetailsVO.setSequenceNO(resultSet.getString("seq_no"));
	   feesDetailsVO.setMatchedRegion(resultSet.getString("matched_region"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return feesDetailsVO;
   }
}
