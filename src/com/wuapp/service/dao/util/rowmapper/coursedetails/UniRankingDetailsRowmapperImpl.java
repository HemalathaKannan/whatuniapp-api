package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.UniRankingVO;
import com.wuapp.util.developer.Debugger;

public class UniRankingDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	UniRankingVO uniRankingVO = new UniRankingVO();
	try {
	   uniRankingVO.setStudentRanking(resultSet.getString("STUDENT_RANKING"));
	   uniRankingVO.setTotalStudentRanking(resultSet.getString("TOTAL_STUDENT_RANKING"));
	   uniRankingVO.setTimesRanking(resultSet.getString("TIMES_RANKING"));
	   ResultSet timesSubjectRankingRS = (ResultSet) resultSet.getObject("TIMES_SUBJECT_RANKING");
	   try {
		if (timesSubjectRankingRS != null) {
		   ArrayList<UniRankingVO> subjectRankingList = new ArrayList<UniRankingVO>();
		   UniRankingVO uniRankVO = null;
		   while (timesSubjectRankingRS.next()) {
			uniRankVO = new UniRankingVO();
			uniRankVO.setTrSubjectName(timesSubjectRankingRS.getString("tr_subject_name"));
			uniRankVO.setCurrentRanking(timesSubjectRankingRS.getString("current_ranking"));
			subjectRankingList.add(uniRankVO);
		   }
		   uniRankingVO.setTimesSubjectRankingList(subjectRankingList);
		}
	   } catch (Exception e) {
		e.printStackTrace();
	   } finally {
		try {
		   if (timesSubjectRankingRS != null) {
			timesSubjectRankingRS.close();
			timesSubjectRankingRS = null;
		   }
		} catch (Exception e) {
		   throw new SQLException(e.getMessage());
		}
	   }
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return uniRankingVO;
   }
}
