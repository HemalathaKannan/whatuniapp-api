package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.CourseInfoVO;
import com.wuapp.util.developer.Debugger;

public class OtherCoursesRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	CourseInfoVO courseInfoVO = new CourseInfoVO();
	try {
	   courseInfoVO.setCourseId(resultSet.getString("course_id"));
	   courseInfoVO.setCourseTitle(resultSet.getString("course_title"));
	   courseInfoVO.setCollegeId(resultSet.getString("college_id"));
	   courseInfoVO.setCollegeNameDisplay(resultSet.getString("college_name_display"));
	   courseInfoVO.setCollegeLogo(resultSet.getString("college_logo"));
	   courseInfoVO.setLogoName(resultSet.getString("college_logo"));
	   courseInfoVO.setMatchingPercentage(resultSet.getString("matching_percentage"));
	   courseInfoVO.setMediaPath(resultSet.getString("tile_media_path"));
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return courseInfoVO;
   }
}
