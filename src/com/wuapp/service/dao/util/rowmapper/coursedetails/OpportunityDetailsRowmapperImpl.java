package com.wuapp.service.dao.util.rowmapper.coursedetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wuapp.service.dao.util.valueobject.coursedetails.OpportunityDetailsVO;
import com.wuapp.util.developer.Debugger;

public class OpportunityDetailsRowmapperImpl implements RowMapper<Object> {

   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	OpportunityDetailsVO opportunityDetailsVO = new OpportunityDetailsVO();
	try {
	   opportunityDetailsVO.setOpportunityId(resultSet.getString("opportunity_id"));
	   opportunityDetailsVO.setOpportunity(resultSet.getString("opportunity"));
	   opportunityDetailsVO.setStudyModeDesc(resultSet.getString("study_mode_desc"));
	   opportunityDetailsVO.setDurationDesc(resultSet.getString("duration_desc"));
	   opportunityDetailsVO.setStartDate(resultSet.getString("start_date"));
	   opportunityDetailsVO.setDomPerYearCost(resultSet.getString("dom_per_year_cost"));
	   opportunityDetailsVO.setDomCrsCost(resultSet.getString("dom_crs_cost"));
	   opportunityDetailsVO.setRestUkCourseCost(resultSet.getString("rest_uk_course_cost"));
	   opportunityDetailsVO.setRestUkOtherCost(resultSet.getString("rest_uk_other_cost"));
	   opportunityDetailsVO.setUkFeeDesc(resultSet.getString("uk_fee_desc"));
	   opportunityDetailsVO.setFeeDescription(resultSet.getString("fee_description"));
	   opportunityDetailsVO.setIntlPerYearCost(resultSet.getString("intl_per_year_cost"));
	   opportunityDetailsVO.setEuCostPerYear(resultSet.getString("eu_cost_per_year"));
	   opportunityDetailsVO.setSelectedOpportunity(resultSet.getString("selected_opportunity")); //Added by Indumathi.S Oct 5 2017
	} catch (Exception e) {
	   Debugger.getInstance().printExceptionDetails(e, (this.getClass().toString()));
	   throw new SQLException();
	}
	return opportunityDetailsVO;
   }
}
