package com.wuapp.service.dao.util.name;

public interface ProcedureNames {

   //
   // Preload some application wise static data
   //
   public static final String GET_STATIC_DATA_SP = "APP_UTIL_PKG.GET_STATIC_DATA_PRC";
   //
   // Home 
   //
   public static final String HOME_BROWSE_SP = "APP_HOME_PKG.HOME_BROWSE_PRC";
   //
   // Course 
   //
   public static final String COURSE_SEARCH_RESULTS_SP = "APP_SEARCH_PKG.GET_COURSE_SEARCH_PRC";
   public static final String SEARCH_LANDING_SP = "APP_SEARCH_FORM_PKG.GET_FORM_DATA_PRC"; // need - /search-form/
   public static final String SEARCH_AJAX_SP = "APP_SEARCH_FORM_PKG.GET_AJAX_PAGE_DATA_PRC"; //need - /ajax-search/
   public static final String GET_COURSE_COUNT_SP = "APP_SEARCH_PKG.GET_COURSE_COUNT_PRC";
   //
   // User Registration 
   //
   public static final String USER_REGISTRATION_SP = "APP_REGISTERATION_PKG.APP_USER_REGISTERATION_PRC";
   public static final String USER_LOGIN_SP = "APP_REGISTERATION_PKG.APP_USER_SIGNIN_PRC";
   public static final String CHECK_USER_EXISTED_SP = "APP_REGISTERATION_PKG.APP_FACEBOOK_LOGIN_PRC";
   public static final String USER_FORGOT_PASSWORD_SP = "APP_REGISTERATION_PKG.FORGOTTEN_PASSWORD_PRC";
   //
   // Institution
   // 
   public static final String PROVIDER_RESULTS_SP = "APP_SEARCH_PKG.get_provider_result_page_prc";
   //
   //Profile
   //
   public static final String UNI_PROFILE_SP = "APP_PROFILE_PKG.UNI_PROFILE_PRC";
   //
   //Course details
   //
   public static final String GET_COURSE_DETAILS_PAGE_PRC = "APP_COURSE_DETAILS_PKG.GET_COURSE_DETAILS_PAGE_PRC";
   //
   // Interaction Db logging
   //
   public static final String STATS_LOG_SP = "APP_STATS_LOG_PKG.STATS_LOG_PRC";
   //
   //Enquiry page
   //
   public static final String ENQUIRY_FORM_SP = "APP_ENQUIRY_PKG.GET_ENQUIRY_FORM_PRC";
   public static final String SUBMIT_ENQUIRY_FORM_SP = "APP_ENQUIRY_PKG.SUBMIT_ENQUIRY_FORM_PRC";
   public static final String SAVE_OPEN_DAYS_SP = "APP_ENQUIRY_PKG.SAVE_OPENDAYS_PRC";
   public static final String GET_AJAX_ADDRESS_SP = "APP_ENQUIRY_PKG.GET_ADDRESS_AJAX_PRC";
   public static final String OPEN_DAYS_STATS_LOGGING_SP = "APP_STATS_LOG_PKG.STATS_LOG_PRC";
   //
   //User profile page
   //
   public static final String GET_USER_PROFILE_SP = "APP_USER_PROFILE_PKG.GET_USER_PROFILE_PRC";
   public static final String SAVE_USER_PROFILE_SP = "APP_USER_PROFILE_PKG.STORE_USER_PROF_DETAILS";
   public static final String GET_USER_PREFERENCE_SP = "APP_USER_PROFILE_PKG.GET_USER_EMAIL_FLAGS_PRC";
   public static final String SAVE_USER_PREFERENCE_SP = "APP_USER_PROFILE_PKG.SAVE_USER_EMAIL_FLAGS_PRC";
   public static final String GET_SCHOOL_LIST_SP = "APP_UTIL_PKG.GET_SCHOOL_AUTOCOMPLETE_PRC";
   public static final String SEND_EMAIL_SP = "APP_USER_PROFILE_PKG.SEND_EMAIL_PRC";
   //
   //Quiz page
   //
   public static final String GET_QUIZ_DATA_SP = "APP_HOME_PKG.GET_QUIZ_PAGE_PRC";
   public static final String GET_QUIZ_COURSE_AJAX_SP = "APP_HOME_PKG.GET_QUIZ_COURSE_AJAX_PRC";
   public static final String GET_JOB_INDUSTRY_AJAX_SP = "APP_HOME_PKG.GET_JOB_INDUSTRY_LIST_PRC";
   public static final String SAVE_USER_ACTIONS_SP = "APP_HOME_PKG.LOG_TRACK_USER_ACTION";
   public static final String GET_QUAL_SUBJECT_AJAX_SP = "APP_HOME_PKG.GET_QUAL_SUBJECTS_PRC";
   public static final String GET_CHATBOT_DETAILS_SP = "WU_CHATBOT_WRAPPER_PKG.GET_CHATBOT_DETAILS_PRC"; 
	   
   //
   //Favourites page
   //
   public static final String GET_SHORTLIST_LIST_SP = "APP_FINAL_FIVE_PKG.GET_SHORTLIST_LIST_PRC";
   public static final String REORDER_CHOICE_SP = "APP_FINAL_FIVE_PKG.REORDER_CHOICE_PRC";
   public static final String ADD_REMOVE_SHORTLIST_SP = "APP_FINAL_FIVE_PKG.ADD_REMOVE_ACTION_PRC";
   //
   //Reserve a place
   //
   public static final String GET_PROVIDER_OPENDAYS_SP = "APP_PROFILE_PKG.GET_PROVIDER_OPENDAYS_FN";
   //
   //
   //
   public static final String GET_CLEARING_FLAG_SP = "APP_UTIL_PKG.GET_SYS_VAR_VALUES_PRC";
   //
   public static final String GET_GUEST_USER_ID_SP = "HOT_HEAPP.APP_REGISTERATION_PKG.GET_GUEST_USER_ID_PRC";
   //
   //  Openday Landing Page
   //
   public static final String OPENDAY_PROVIDER_LANDING_SP = "APP_OPENDAY_LANDING_PKG.GET_LANDING_PAGE_DETAILS_PRC";
   public static final String GET_MORE_OPENDAY_LIST_SP = "APP_OPENDAY_LANDING_PKG.GET_UNI_OPENDAYS_LIST_PRC";
   public static final String OPENDAYS_BOOKING_FORM_SUBMIT = "APP_OPENDAY_LANDING_PKG.OPENDAYS_FORM_SUBMIT_PRC"; 
   public static final String OPENDAYS_SHOW_BOOKING_FORM = "APP_OPENDAY_LANDING_PKG.GET_OPENDAY_FORM_DETAIL_PRC";
   public static final String GET_SUBJECT_AJAX_LIST_SP = "APP_OPENDAY_LANDING_PKG.GET_SUBJECT_AJAX_LIST_PRC";
   //
   public static final String OPENDAY_SEARCH_LANDING_SP = "APP_OPENDAYS_PKG.GET_OPENDAYS_HOMEPAGE_PRC";
   public static final String GET_OPENDAY_SEARCH_AJAX_FN = "APP_OPENDAYS_PKG.GET_AJAX_OPENDAYS_FN";
   public static final String OPENDAY_SEARCH_RESULTS_SP = "APP_OPENDAYS_PKG.GET_MORE_OPENDAYS_PRC";
}
