package com.wuapp.service.dao.util.valueobject.provider;

public class InstitutionDetailsVO {

   private String overview;
   private String name;
   private String displayName;
   private String overallRating;
   private String id;
   private String reviewCount;
   private String mediaPath;
   private String logo;
   private String logoName;
   private String logoURL;
   private String reviewRatingDisplay;
   private String shortListFlag;

   //
   public String getOverview() {
	return overview;
   }

   public void setOverview(String overview) {
	this.overview = overview;
   }

   public String getName() {
	return name;
   }

   public void setName(String name) {
	this.name = name;
   }

   public String getDisplayName() {
	return displayName;
   }

   public void setDisplayName(String displayName) {
	this.displayName = displayName;
   }

   public String getOverallRating() {
	return overallRating;
   }

   public void setOverallRating(String overallRating) {
	this.overallRating = overallRating;
   }

   public String getId() {
	return id;
   }

   public void setId(String id) {
	this.id = id;
   }

   public String getReviewCount() {
	return reviewCount;
   }

   public void setReviewCount(String reviewCount) {
	this.reviewCount = reviewCount;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getLogoName() {
	return logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getReviewRatingDisplay() {
	return reviewRatingDisplay;
   }

   public void setReviewRatingDisplay(String reviewRatingDisplay) {
	this.reviewRatingDisplay = reviewRatingDisplay;
   }

   public String getShortListFlag() {
	return shortListFlag;
   }

   public void setShortListFlag(String shortListFlag) {
	this.shortListFlag = shortListFlag;
   }
}
