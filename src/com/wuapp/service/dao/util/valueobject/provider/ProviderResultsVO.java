package com.wuapp.service.dao.util.valueobject.provider;

public class ProviderResultsVO {

   //
   private String courseId;
   private String description;
   private String totalCourseCount;
   private String studymode;
   private String faculty;
   private String courseTitle;
   private String institutionName;
   private String institutionDisplayName;
   private String institutionId;
   private String suborderItemId;
   private String courseRank;
   private String studyLevel;
   private String entryLevel;
   private String matchingPercentage;
   private String qualificationMatch;
   private String subjectMatch;
   private String locationMatch;
   private String locationTypeMatch;
   private String studyModeMatch;
   private String previousQualMatch;
   private String assessmentTypeMatch;
   private String reviewSubjectOneMatch;
   private String reviewSubjectTwoMatch;
   private String reviewSubjectThreeMatch;
   private String applyNowBadge;

   
   public String getApplyNowBadge() {
      return applyNowBadge;
   }

   
   public void setApplyNowBadge(String applyNowBadge) {
      this.applyNowBadge = applyNowBadge;
   }

   //
   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getDescription() {
	return description;
   }

   public void setDescription(String description) {
	this.description = description;
   }

   public String getTotalCourseCount() {
	return totalCourseCount;
   }

   public void setTotalCourseCount(String totalCourseCount) {
	this.totalCourseCount = totalCourseCount;
   }

   public String getStudymode() {
	return studymode;
   }

   public void setStudymode(String studymode) {
	this.studymode = studymode;
   }

   public String getFaculty() {
	return faculty;
   }

   public void setFaculty(String faculty) {
	this.faculty = faculty;
   }

   public String getCourseTitle() {
	return courseTitle;
   }

   public void setCourseTitle(String courseTitle) {
	this.courseTitle = courseTitle;
   }

   public String getInstitutionName() {
	return institutionName;
   }

   public void setInstitutionName(String institutionName) {
	this.institutionName = institutionName;
   }

   public String getInstitutionDisplayName() {
	return institutionDisplayName;
   }

   public void setInstitutionDisplayName(String institutionDisplayName) {
	this.institutionDisplayName = institutionDisplayName;
   }

   public String getInstitutionId() {
	return institutionId;
   }

   public void setInstitutionId(String institutionId) {
	this.institutionId = institutionId;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getCourseRank() {
	return courseRank;
   }

   public void setCourseRank(String courseRank) {
	this.courseRank = courseRank;
   }

   public String getStudyLevel() {
	return studyLevel;
   }

   public void setStudyLevel(String studyLevel) {
	this.studyLevel = studyLevel;
   }

   public String getEntryLevel() {
	return entryLevel;
   }

   public void setEntryLevel(String entryLevel) {
	this.entryLevel = entryLevel;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getQualificationMatch() {
	return qualificationMatch;
   }

   public void setQualificationMatch(String qualificationMatch) {
	this.qualificationMatch = qualificationMatch;
   }

   public String getSubjectMatch() {
	return subjectMatch;
   }

   public void setSubjectMatch(String subjectMatch) {
	this.subjectMatch = subjectMatch;
   }

   public String getLocationMatch() {
	return locationMatch;
   }

   public void setLocationMatch(String locationMatch) {
	this.locationMatch = locationMatch;
   }

   public String getStudyModeMatch() {
	return studyModeMatch;
   }

   public void setStudyModeMatch(String studyModeMatch) {
	this.studyModeMatch = studyModeMatch;
   }

   public String getPreviousQualMatch() {
	return previousQualMatch;
   }

   public void setPreviousQualMatch(String previousQualMatch) {
	this.previousQualMatch = previousQualMatch;
   }

   public String getAssessmentTypeMatch() {
	return assessmentTypeMatch;
   }

   public void setAssessmentTypeMatch(String assessmentTypeMatch) {
	this.assessmentTypeMatch = assessmentTypeMatch;
   }

   public String getReviewSubjectOneMatch() {
	return reviewSubjectOneMatch;
   }

   public void setReviewSubjectOneMatch(String reviewSubjectOneMatch) {
	this.reviewSubjectOneMatch = reviewSubjectOneMatch;
   }

   public String getReviewSubjectTwoMatch() {
	return reviewSubjectTwoMatch;
   }

   public void setReviewSubjectTwoMatch(String reviewSubjectTwoMatch) {
	this.reviewSubjectTwoMatch = reviewSubjectTwoMatch;
   }

   public String getReviewSubjectThreeMatch() {
	return reviewSubjectThreeMatch;
   }

   public void setReviewSubjectThreeMatch(String reviewSubjectThreeMatch) {
	this.reviewSubjectThreeMatch = reviewSubjectThreeMatch;
   }

   public String getLocationTypeMatch() {
	return locationTypeMatch;
   }

   public void setLocationTypeMatch(String locationTypeMatch) {
	this.locationTypeMatch = locationTypeMatch;
   }
}
