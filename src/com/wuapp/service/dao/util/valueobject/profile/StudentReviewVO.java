package com.wuapp.service.dao.util.valueobject.profile;

public class StudentReviewVO {

   private String userName;
   private String reviewText;
   private String starRating;
   private String sections;
   private String reviewType;
   private String starRatingDisplay;

   public String getUserName() {
	return userName;
   }

   public void setUserName(String userName) {
	this.userName = userName;
   }

   public String getReviewText() {
	return reviewText;
   }

   public void setReviewText(String reviewText) {
	this.reviewText = reviewText;
   }

   public String getStarRating() {
	return starRating;
   }

   public void setStarRating(String starRating) {
	this.starRating = starRating;
   }

   public String getSections() {
	return sections;
   }

   public void setSections(String sections) {
	this.sections = sections;
   }

   public String getReviewType() {
	return reviewType;
   }

   public void setReviewType(String reviewType) {
	this.reviewType = reviewType;
   }

   public String getStarRatingDisplay() {
	return starRatingDisplay;
   }

   public void setStarRatingDisplay(String starRatingDisplay) {
	this.starRatingDisplay = starRatingDisplay;
   }
}
