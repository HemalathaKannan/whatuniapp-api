package com.wuapp.service.dao.util.valueobject.profile;

public class KeyStatsInfoVO {

   private String fullTime;
   private String partTime;
   private String mature;
   private String schoolLeavers;
   private String ukStudents;
   private String international;
   private String undergraduate;
   private String postgraduate;
   private String noOfStudents;
   private String employmentRate;
   private String wuscaRanking;
   private String totalWuscaRanking;
   private String timesRanking;
   private String totalTimesRanking;

   public String getFullTime() {
	return fullTime;
   }

   public void setFullTime(String fullTime) {
	this.fullTime = fullTime;
   }

   public String getPartTime() {
	return partTime;
   }

   public void setPartTime(String partTime) {
	this.partTime = partTime;
   }

   public String getMature() {
	return mature;
   }

   public void setMature(String mature) {
	this.mature = mature;
   }

   public String getSchoolLeavers() {
	return schoolLeavers;
   }

   public void setSchoolLeavers(String schoolLeavers) {
	this.schoolLeavers = schoolLeavers;
   }

   public String getUkStudents() {
	return ukStudents;
   }

   public void setUkStudents(String ukStudents) {
	this.ukStudents = ukStudents;
   }

   public String getInternational() {
	return international;
   }

   public void setInternational(String international) {
	this.international = international;
   }

   public String getUndergraduate() {
	return undergraduate;
   }

   public void setUndergraduate(String undergraduate) {
	this.undergraduate = undergraduate;
   }

   public String getPostgraduate() {
	return postgraduate;
   }

   public void setPostgraduate(String postgraduate) {
	this.postgraduate = postgraduate;
   }

   public String getNoOfStudents() {
	return noOfStudents;
   }

   public void setNoOfStudents(String noOfStudents) {
	this.noOfStudents = noOfStudents;
   }

   public String getEmploymentRate() {
	return employmentRate;
   }

   public void setEmploymentRate(String employmentRate) {
	this.employmentRate = employmentRate;
   }

   public String getWuscaRanking() {
	return wuscaRanking;
   }

   public void setWuscaRanking(String wuscaRanking) {
	this.wuscaRanking = wuscaRanking;
   }

   public String getTotalWuscaRanking() {
	return totalWuscaRanking;
   }

   public void setTotalWuscaRanking(String totalWuscaRanking) {
	this.totalWuscaRanking = totalWuscaRanking;
   }

   public String getTimesRanking() {
	return timesRanking;
   }

   public void setTimesRanking(String timesRanking) {
	this.timesRanking = timesRanking;
   }

   public String getTotalTimesRanking() {
	return totalTimesRanking;
   }

   public void setTotalTimesRanking(String totalTimesRanking) {
	this.totalTimesRanking = totalTimesRanking;
   }
}
