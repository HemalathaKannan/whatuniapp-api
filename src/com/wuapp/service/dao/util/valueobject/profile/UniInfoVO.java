package com.wuapp.service.dao.util.valueobject.profile;

public class UniInfoVO {

   private String collegeId;
   private String collegeLogo;
   private String collegeName;
   private String tileMediaPath;
   private String overallRating;
   private String reviewCount;
   private String matchingPercentage;
   private String shortListFlag;
   private String pullQuotes;
   private String latitude;
   private String longitude;
   private String town;
   private String postcode;
   private String nearestTrainStation;
   private String nearestTubeStation;
   private String distanceFromTrainStn;
   private String distanceFromTubeStn;
   private String addressLineOne;
   private String addressLineTwo;
   private String countryState;
   private String isInLondon;
   private String courseExistFlag;
   private String logoName;
   private String logoURL;
   private String reviewRatingDisplay;

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getCollegeLogo() {
	return collegeLogo;
   }

   public void setCollegeLogo(String collegeLogo) {
	this.collegeLogo = collegeLogo;
   }

   public String getCollegeName() {
	return collegeName;
   }

   public void setCollegeName(String collegeName) {
	this.collegeName = collegeName;
   }

   public String getTileMediaPath() {
	return tileMediaPath;
   }

   public void setTileMediaPath(String tileMediaPath) {
	this.tileMediaPath = tileMediaPath;
   }

   public String getOverallRating() {
	return overallRating;
   }

   public void setOverallRating(String overallRating) {
	this.overallRating = overallRating;
   }

   public String getReviewCount() {
	return reviewCount;
   }

   public void setReviewCount(String reviewCount) {
	this.reviewCount = reviewCount;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getShortListFlag() {
	return shortListFlag;
   }

   public void setShortListFlag(String shortListFlag) {
	this.shortListFlag = shortListFlag;
   }

   public String getPullQuotes() {
	return pullQuotes;
   }

   public void setPullQuotes(String pullQuotes) {
	this.pullQuotes = pullQuotes;
   }

   public String getLatitude() {
	return latitude;
   }

   public void setLatitude(String latitude) {
	this.latitude = latitude;
   }

   public String getLongitude() {
	return longitude;
   }

   public void setLongitude(String longitude) {
	this.longitude = longitude;
   }

   public String getTown() {
	return town;
   }

   public void setTown(String town) {
	this.town = town;
   }

   public String getPostcode() {
	return postcode;
   }

   public void setPostcode(String postcode) {
	this.postcode = postcode;
   }

   public String getNearestTrainStation() {
	return nearestTrainStation;
   }

   public void setNearestTrainStation(String nearestTrainStation) {
	this.nearestTrainStation = nearestTrainStation;
   }

   public String getNearestTubeStation() {
	return nearestTubeStation;
   }

   public void setNearestTubeStation(String nearestTubeStation) {
	this.nearestTubeStation = nearestTubeStation;
   }

   public String getDistanceFromTrainStn() {
	return distanceFromTrainStn;
   }

   public void setDistanceFromTrainStn(String distanceFromTrainStn) {
	this.distanceFromTrainStn = distanceFromTrainStn;
   }

   public String getDistanceFromTubeStn() {
	return distanceFromTubeStn;
   }

   public void setDistanceFromTubeStn(String distanceFromTubeStn) {
	this.distanceFromTubeStn = distanceFromTubeStn;
   }

   public String getAddressLineOne() {
	return addressLineOne;
   }

   public void setAddressLineOne(String addressLineOne) {
	this.addressLineOne = addressLineOne;
   }

   public String getAddressLineTwo() {
	return addressLineTwo;
   }

   public void setAddressLineTwo(String addressLineTwo) {
	this.addressLineTwo = addressLineTwo;
   }

   public String getCountryState() {
	return countryState;
   }

   public void setCountryState(String countryState) {
	this.countryState = countryState;
   }

   public String getIsInLondon() {
	return isInLondon;
   }

   public void setIsInLondon(String isInLondon) {
	this.isInLondon = isInLondon;
   }

   public String getCourseExistFlag() {
	return courseExistFlag;
   }

   public void setCourseExistFlag(String courseExistFlag) {
	this.courseExistFlag = courseExistFlag;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }

   public String getReviewRatingDisplay() {
	return reviewRatingDisplay;
   }

   public void setReviewRatingDisplay(String reviewRatingDisplay) {
	this.reviewRatingDisplay = reviewRatingDisplay;
   }
}
