package com.wuapp.service.dao.util.valueobject.profile;

public class CompetitorDetailVO {

   private String collegeLogo;
   private String collegeId;
   private String collegeName;
   private String matchingPercentage;
   private String logoName;
   private String logoURL;

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }

   public String getCollegeLogo() {
	return collegeLogo;
   }

   public void setCollegeLogo(String collegeLogo) {
	this.collegeLogo = collegeLogo;
   }

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getCollegeName() {
	return collegeName;
   }

   public void setCollegeName(String collegeName) {
	this.collegeName = collegeName;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }
}
