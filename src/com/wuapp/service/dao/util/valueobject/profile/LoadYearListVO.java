package com.wuapp.service.dao.util.valueobject.profile;

public class LoadYearListVO {

   private String loadYearName;
   private String loadYearData;

   public String getLoadYearName() {
	return loadYearName;
   }

   public void setLoadYearName(String loadYearName) {
	this.loadYearName = loadYearName;
   }

   public String getLoadYearData() {
	return loadYearData;
   }

   public void setLoadYearData(String loadYearData) {
	this.loadYearData = loadYearData;
   }
}
