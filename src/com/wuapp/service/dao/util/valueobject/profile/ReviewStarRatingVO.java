package com.wuapp.service.dao.util.valueobject.profile;

public class ReviewStarRatingVO {

   private String collegeId;
   private String questionTitle;
   private String rating;
   private String questionId;
   private String roundRating;

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getQuestionTitle() {
	return questionTitle;
   }

   public void setQuestionTitle(String questionTitle) {
	this.questionTitle = questionTitle;
   }

   public String getRating() {
	return rating;
   }

   public void setRating(String rating) {
	this.rating = rating;
   }

   public String getQuestionId() {
	return questionId;
   }

   public void setQuestionId(String questionId) {
	this.questionId = questionId;
   }

   public String getRoundRating() {
	return roundRating;
   }

   public void setRoundRating(String roundRating) {
	this.roundRating = roundRating;
   }
}
