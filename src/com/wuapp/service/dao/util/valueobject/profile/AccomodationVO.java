package com.wuapp.service.dao.util.valueobject.profile;

public class AccomodationVO {

   private String accommodationCost;
   private String livingCost;
   private String whatuniCostOfPint;

   public String getAccommodationCost() {
	return accommodationCost;
   }

   public void setAccommodationCost(String accommodationCost) {
	this.accommodationCost = accommodationCost;
   }

   public String getLivingCost() {
	return livingCost;
   }

   public void setLivingCost(String livingCost) {
	this.livingCost = livingCost;
   }

   public String getWhatuniCostOfPint() {
	return whatuniCostOfPint;
   }

   public void setWhatuniCostOfPint(String whatuniCostOfPint) {
	this.whatuniCostOfPint = whatuniCostOfPint;
   }
}
