package com.wuapp.service.dao.util.valueobject.profile;

public class ProfileVO {

   private String description;
   private String buttonLabel;
   private String mediaType;
   private String imagePath;
   private String profileId;
   private String myhcProfileId;
   private String suborderItemId;
   private String typeName;
   private String videoUrl;
   private String sectionDisplayName;

   
   public String getSectionDisplayName() {
      return sectionDisplayName;
   }

   
   public void setSectionDisplayName(String sectionDisplayName) {
      this.sectionDisplayName = sectionDisplayName;
   }

   public String getDescription() {
	return description;
   }

   public void setDescription(String description) {
	this.description = description;
   }

   public String getButtonLabel() {
	return buttonLabel;
   }

   public void setButtonLabel(String buttonLabel) {
	this.buttonLabel = buttonLabel;
   }

   public String getImagePath() {
	return imagePath;
   }

   public void setImagePath(String imagePath) {
	this.imagePath = imagePath;
   }

   public String getProfileId() {
	return profileId;
   }

   public void setProfileId(String profileId) {
	this.profileId = profileId;
   }

   public String getMyhcProfileId() {
	return myhcProfileId;
   }

   public void setMyhcProfileId(String myhcProfileId) {
	this.myhcProfileId = myhcProfileId;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getTypeName() {
	return typeName;
   }

   public void setTypeName(String typeName) {
	this.typeName = typeName;
   }

   public String getMediaType() {
	return mediaType;
   }

   public void setMediaType(String mediaType) {
	this.mediaType = mediaType;
   }

   public String getVideoUrl() {
	return videoUrl;
   }

   public void setVideoUrl(String videoUrl) {
	this.videoUrl = videoUrl;
   }
}
