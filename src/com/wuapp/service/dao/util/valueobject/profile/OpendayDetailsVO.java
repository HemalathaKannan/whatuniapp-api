package com.wuapp.service.dao.util.valueobject.profile;

public class OpendayDetailsVO {

   private String openDate;
   private String timing;
   private String eventId;
   private String subOrderItemId;
   private String qualType;
   private String opendaysSavedFlag;

   public String getOpendaysSavedFlag() {
	return opendaysSavedFlag;
   }

   public void setOpendaysSavedFlag(String opendaysSavedFlag) {
	this.opendaysSavedFlag = opendaysSavedFlag;
   }

   public String getQualType() {
	return qualType;
   }

   public void setQualType(String qualType) {
	this.qualType = qualType;
   }

   public String getOpenDate() {
	return openDate;
   }

   public void setOpenDate(String openDate) {
	this.openDate = openDate;
   }

   public String getTiming() {
	return timing;
   }

   public void setTiming(String timing) {
	this.timing = timing;
   }

   public String getEventId() {
	return eventId;
   }

   public void setEventId(String eventId) {
	this.eventId = eventId;
   }

   public String getSubOrderItemId() {
	return subOrderItemId;
   }

   public void setSubOrderItemId(String subOrderItemId) {
	this.subOrderItemId = subOrderItemId;
   }
}
