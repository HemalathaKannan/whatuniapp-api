package com.wuapp.service.dao.util.valueobject.profile;

public class EnquiryDetailsVO {

   private String orderItemId;
   private String email;
   private String emailFlag;
   private String emailWebform;
   private String emailWebformFlag;
   private String prospectus;
   private String prospectusWebformFlag;
   private String website;
   private String suborderItemId;
   private String prospectusWebform;
   private String websiteFlag;
   private String networkId;
   private String profileId;
   private String profileType;
   private String hotline;
   private String emailPrice;
   private String prospectusPrice;
   private String openDaysPrice; 

   
   public String getEmailPrice() {
      return emailPrice;
   }

   
   public void setEmailPrice(String emailPrice) {
      this.emailPrice = emailPrice;
   }

   
   public String getProspectusPrice() {
      return prospectusPrice;
   }

   
   public void setProspectusPrice(String prospectusPrice) {
      this.prospectusPrice = prospectusPrice;
   }

   
   public String getOpenDaysPrice() {
      return openDaysPrice;
   }

   
   public void setOpenDaysPrice(String openDaysPrice) {
      this.openDaysPrice = openDaysPrice;
   }

   public String getProfileId() {
	return profileId;
   }

   public void setProfileId(String profileId) {
	this.profileId = profileId;
   }

   public String getProfileType() {
	return profileType;
   }

   public void setProfileType(String profileType) {
	this.profileType = profileType;
   }

   public String getHotline() {
	return hotline;
   }

   public void setHotline(String hotline) {
	this.hotline = hotline;
   }

   public String getOrderItemId() {
	return orderItemId;
   }

   public void setOrderItemId(String orderItemId) {
	this.orderItemId = orderItemId;
   }

   public String getEmail() {
	return email;
   }

   public void setEmail(String email) {
	this.email = email;
   }

   public String getEmailFlag() {
	return emailFlag;
   }

   public void setEmailFlag(String emailFlag) {
	this.emailFlag = emailFlag;
   }

   public String getEmailWebform() {
	return emailWebform;
   }

   public void setEmailWebform(String emailWebform) {
	this.emailWebform = emailWebform;
   }

   public String getEmailWebformFlag() {
	return emailWebformFlag;
   }

   public void setEmailWebformFlag(String emailWebformFlag) {
	this.emailWebformFlag = emailWebformFlag;
   }

   public String getProspectus() {
	return prospectus;
   }

   public void setProspectus(String prospectus) {
	this.prospectus = prospectus;
   }

   public String getProspectusWebformFlag() {
	return prospectusWebformFlag;
   }

   public void setProspectusWebformFlag(String prospectusWebformFlag) {
	this.prospectusWebformFlag = prospectusWebformFlag;
   }

   public String getWebsite() {
	return website;
   }

   public void setWebsite(String website) {
	this.website = website;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getProspectusWebform() {
	return prospectusWebform;
   }

   public void setProspectusWebform(String prospectusWebform) {
	this.prospectusWebform = prospectusWebform;
   }

   public String getWebsiteFlag() {
	return websiteFlag;
   }

   public void setWebsiteFlag(String websiteFlag) {
	this.websiteFlag = websiteFlag;
   }

   public String getNetworkId() {
	return networkId;
   }

   public void setNetworkId(String networkId) {
	this.networkId = networkId;
   }
}
