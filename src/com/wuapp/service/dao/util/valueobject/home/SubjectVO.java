package com.wuapp.service.dao.util.valueobject.home;

public class SubjectVO {

   private String categoryCode;
   private String description;
   private String searchCode;
   private String searchId;
   private String subjectName;
   private String tileMediaPath;
   private String mediaId;
   private String mediaPath;
   private String courseCount;
   private String iconMediaPath;
   private String orderByName;
   private String orderBydisplayName;
   private String orderBySelected;

   //
   public String getCategoryCode() {
	return categoryCode;
   }

   public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
   }

   public String getSubjectName() {
	return subjectName;
   }

   public void setSubjectName(String subjectName) {
	this.subjectName = subjectName;
   }

   public String getTileMediaPath() {
	return tileMediaPath;
   }

   public void setTileMediaPath(String tileMediaPath) {
	this.tileMediaPath = tileMediaPath;
   }

   public String getIconMediaPath() {
	return iconMediaPath;
   }

   public void setIconMediaPath(String iconMediaPath) {
	this.iconMediaPath = iconMediaPath;
   }

   public String getDescription() {
	return description;
   }

   public void setDescription(String description) {
	this.description = description;
   }

   public String getSearchCode() {
	return searchCode;
   }

   public void setSearchCode(String searchCode) {
	this.searchCode = searchCode;
   }

   public String getSearchId() {
	return searchId;
   }

   public void setSearchId(String searchId) {
	this.searchId = searchId;
   }

   public String getCourseCount() {
	return courseCount;
   }

   public void setCourseCount(String courseCount) {
	this.courseCount = courseCount;
   }

   public String getMediaId() {
	return mediaId;
   }

   public void setMediaId(String mediaId) {
	this.mediaId = mediaId;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }

   public String getOrderByName() {
	return orderByName;
   }

   public void setOrderByName(String orderByName) {
	this.orderByName = orderByName;
   }

   public String getOrderBydisplayName() {
	return orderBydisplayName;
   }

   public void setOrderBydisplayName(String orderBydisplayName) {
	this.orderBydisplayName = orderBydisplayName;
   }

   public String getOrderBySelected() {
	return orderBySelected;
   }

   public void setOrderBySelected(String orderBySelected) {
	this.orderBySelected = orderBySelected;
   }
}
