package com.wuapp.service.dao.util.valueobject.home;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to getting featured slots details
 *
 * @author Hemalatha.K
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */

@Getter @Setter
public class FeaturedSlotsVO {   
   
   private String collegeId = null;
   private String collegeNameDisplay = null;
   private String logo = null;
   private String mediaPath = null;
   private String nextOpenday = null;
   private String reviewRating = null;
   private String reviewRatingDisplay = null;
   private String courseId = null;
   private String courseName = null;
}
