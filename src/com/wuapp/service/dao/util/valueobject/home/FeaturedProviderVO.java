package com.wuapp.service.dao.util.valueobject.home;

public class FeaturedProviderVO {

   String collegeId = null;
   String collegeNameDisplay = null;
   String logo = null;
   String mediaPath = null;
   String reviewRating = null;
   String reviewRatingDisplay = null;
   String url = null;

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getCollegeNameDisplay() {
	return collegeNameDisplay;
   }

   public void setCollegeNameDisplay(String collegeNameDisplay) {
	this.collegeNameDisplay = collegeNameDisplay;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }

   public String getReviewRating() {
	return reviewRating;
   }

   public void setReviewRating(String reviewRating) {
	this.reviewRating = reviewRating;
   }

   public String getReviewRatingDisplay() {
	return reviewRatingDisplay;
   }

   public void setReviewRatingDisplay(String reviewRatingDisplay) {
	this.reviewRatingDisplay = reviewRatingDisplay;
   }

   public String getUrl() {
	return url;
   }

   public void setUrl(String url) {
	this.url = url;
   }
}
