package com.wuapp.service.dao.util.valueobject.home;

public class InstitutionVO {

   //
   private String id;
   private String name;
   private String logo;
   private String tileMediaPath;
   private String reviewRating;
   private String distanceFromUserLatLag;
   private String shortlistFlag;
   private String nextOpenDay;
   private String courseCount;
   private String reviewRatingDisplay;
   private String logoName;
   private String logoURL;

   //
   public String getId() {
	return id;
   }

   public void setId(String id) {
	this.id = id;
   }

   public String getName() {
	return name;
   }

   public void setName(String name) {
	this.name = name;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getTileMediaPath() {
	return tileMediaPath;
   }

   public void setTileMediaPath(String tileMediaPath) {
	this.tileMediaPath = tileMediaPath;
   }

   public String getReviewRating() {
	return reviewRating;
   }

   public void setReviewRating(String reviewRating) {
	this.reviewRating = reviewRating;
   }

   public String getDistanceFromUserLatLag() {
	return distanceFromUserLatLag;
   }

   public void setDistanceFromUserLatLag(String distanceFromUserLatLag) {
	this.distanceFromUserLatLag = distanceFromUserLatLag;
   }

   public String getShortlistFlag() {
	return shortlistFlag;
   }

   public void setShortlistFlag(String shortlistFlag) {
	this.shortlistFlag = shortlistFlag;
   }

   public String getNextOpenDay() {
	return nextOpenDay;
   }

   public void setNextOpenDay(String nextOpenDay) {
	this.nextOpenDay = nextOpenDay;
   }

   public void setCourseCount(String courseCount) {
	this.courseCount = courseCount;
   }

   public String getCourseCount() {
	return courseCount;
   }

   public String getReviewRatingDisplay() {
	return reviewRatingDisplay;
   }

   public void setReviewRatingDisplay(String reviewRatingDisplay) {
	this.reviewRatingDisplay = reviewRatingDisplay;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }
}
