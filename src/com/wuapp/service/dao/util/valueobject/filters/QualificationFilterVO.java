package com.wuapp.service.dao.util.valueobject.filters;

public class QualificationFilterVO {

   //
   private String qualificationCode;
   private String qualificaiton;
   private String flag;

   //   
   public String getQualificationCode() {
	return qualificationCode;
   }

   public void setQualificationCode(String qualificationCode) {
	this.qualificationCode = qualificationCode;
   }

   public String getQualificaiton() {
	return qualificaiton;
   }

   public void setQualificaiton(String qualificaiton) {
	this.qualificaiton = qualificaiton;
   }

   public String getFlag() {
	return flag;
   }

   public void setFlag(String flag) {
	this.flag = flag;
   }
}
