package com.wuapp.service.dao.util.valueobject.filters;

public class LocationFilterVO {

   //
   private String region;
   private String topRegion;
   private String flag;

   //
   public String getRegion() {
	return region;
   }

   public void setRegion(String region) {
	this.region = region;
   }

   public String getTopRegion() {
	return topRegion;
   }

   public void setTopRegion(String topRegion) {
	this.topRegion = topRegion;
   }

   public String getFlag() {
	return flag;
   }

   public void setFlag(String flag) {
	this.flag = flag;
   }
}
