package com.wuapp.service.dao.util.valueobject.filters;

public class StudyModeFilterVO {

   //
   private String studyMode;
   private String textKey;
   private String flag;

   //
   public String getStudyMode() {
	return studyMode;
   }

   public void setStudyMode(String studyMode) {
	this.studyMode = studyMode;
   }

   public String getTextKey() {
	return textKey;
   }

   public void setTextKey(String textKey) {
	this.textKey = textKey;
   }

   public String getFlag() {
	return flag;
   }

   public void setFlag(String flag) {
	this.flag = flag;
   }
}
