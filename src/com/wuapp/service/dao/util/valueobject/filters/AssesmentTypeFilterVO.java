package com.wuapp.service.dao.util.valueobject.filters;

public class AssesmentTypeFilterVO {

   private String assesmentTypeId;
   private String assesmentTypeName;
   private String selectedText;
   private String mediaId;
   private String mediaPath;

   //
   public String getAssesmentTypeId() {
	return assesmentTypeId;
   }

   public void setAssesmentTypeId(String assesmentTypeId) {
	this.assesmentTypeId = assesmentTypeId;
   }

   public String getAssesmentTypeName() {
	return assesmentTypeName;
   }

   public void setAssesmentTypeName(String assesmentTypeName) {
	this.assesmentTypeName = assesmentTypeName;
   }

   public String getSelectedText() {
	return selectedText;
   }

   public void setSelectedText(String selectedText) {
	this.selectedText = selectedText;
   }

   public String getMediaId() {
	return mediaId;
   }

   public void setMediaId(String mediaId) {
	this.mediaId = mediaId;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }
}
