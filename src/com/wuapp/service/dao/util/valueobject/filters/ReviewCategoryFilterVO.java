package com.wuapp.service.dao.util.valueobject.filters;

public class ReviewCategoryFilterVO {

   private String reviewCategoryId;
   private String reviewCategoryName;
   private String textKey;
   private String selectedText;
   private String mediaId;
   private String mediaPath;
   private String reviewCategoryDisplayName;

   
   public String getReviewCategoryDisplayName() {
      return reviewCategoryDisplayName;
   }

   
   public void setReviewCategoryDisplayName(String reviewCategoryDisplayName) {
      this.reviewCategoryDisplayName = reviewCategoryDisplayName;
   }

   //
   public String getReviewCategoryId() {
	return reviewCategoryId;
   }

   public void setReviewCategoryId(String reviewCategoryId) {
	this.reviewCategoryId = reviewCategoryId;
   }

   public String getReviewCategoryName() {
	return reviewCategoryName;
   }

   public void setReviewCategoryName(String reviewCategoryName) {
	this.reviewCategoryName = reviewCategoryName;
   }

   public String getTextKey() {
	return textKey;
   }

   public void setTextKey(String textKey) {
	this.textKey = textKey;
   }

   public String getSelectedText() {
	return selectedText;
   }

   public void setSelectedText(String selectedText) {
	this.selectedText = selectedText;
   }

   public String getMediaId() {
	return mediaId;
   }

   public void setMediaId(String mediaId) {
	this.mediaId = mediaId;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }
}
