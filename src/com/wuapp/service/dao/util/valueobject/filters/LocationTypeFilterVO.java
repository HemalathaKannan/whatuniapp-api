package com.wuapp.service.dao.util.valueobject.filters;

public class LocationTypeFilterVO {

   //
   private String locationTypeId;
   private String locationTypeName;
   private String flag;

   //
   public String getLocationTypeId() {
	return locationTypeId;
   }

   public void setLocationTypeId(String locationTypeId) {
	this.locationTypeId = locationTypeId;
   }

   public String getLocationTypeName() {
	return locationTypeName;
   }

   public void setLocationTypeName(String locationTypeName) {
	this.locationTypeName = locationTypeName;
   }

   public String getFlag() {
	return flag;
   }

   public void setFlag(String flag) {
	this.flag = flag;
   }
}
