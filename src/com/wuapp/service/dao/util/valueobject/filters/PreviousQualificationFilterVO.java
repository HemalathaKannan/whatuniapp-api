package com.wuapp.service.dao.util.valueobject.filters;

public class PreviousQualificationFilterVO {

   //
   private String qualificaiton;
   private String gradeString;
   private String gradeLevel;
   private String textKey;
   private String previousQualSelected;

   //
   public String getQualificaiton() {
	return qualificaiton;
   }

   public void setQualificaiton(String qualificaiton) {
	this.qualificaiton = qualificaiton;
   }

   public String getGradeString() {
	return gradeString;
   }

   public void setGradeString(String gradeString) {
	this.gradeString = gradeString;
   }

   public String getGradeLevel() {
	return gradeLevel;
   }

   public void setGradeLevel(String gradeLevel) {
	this.gradeLevel = gradeLevel;
   }

   public String getTextKey() {
	return textKey;
   }

   public void setTextKey(String textKey) {
	this.textKey = textKey;
   }

   public String getPreviousQualSelected() {
	return previousQualSelected;
   }

   public void setPreviousQualSelected(String previousQualSelected) {
	this.previousQualSelected = previousQualSelected;
   }
}
