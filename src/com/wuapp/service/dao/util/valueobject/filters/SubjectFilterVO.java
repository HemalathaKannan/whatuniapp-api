package com.wuapp.service.dao.util.valueobject.filters;

public class SubjectFilterVO {

   //
   private String categoryCode;
   private String subjectName;
   private String courseCount;
   private String searchCategoryId;
   private String flag;

   //   
   public String getCategoryCode() {
	return categoryCode;
   }

   public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
   }

   public String getSubjectName() {
	return subjectName;
   }

   public void setSubjectName(String subjectName) {
	this.subjectName = subjectName;
   }

   public String getCourseCount() {
	return courseCount;
   }

   public void setCourseCount(String courseCount) {
	this.courseCount = courseCount;
   }

   public String getSearchCategoryId() {
	return searchCategoryId;
   }

   public void setSearchCategoryId(String searchCategoryId) {
	this.searchCategoryId = searchCategoryId;
   }

   public String getFlag() {
	return flag;
   }

   public void setFlag(String flag) {
	this.flag = flag;
   }
}
