package com.wuapp.service.dao.util.valueobject.common;

public class AccessTokenVO {

   private String affiliatedId = null;
   private String accessToken = null;

   public String getAffiliatedId() {
	return affiliatedId;
   }

   public void setAffiliatedId(String affiliatedId) {
	this.affiliatedId = affiliatedId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }
}
