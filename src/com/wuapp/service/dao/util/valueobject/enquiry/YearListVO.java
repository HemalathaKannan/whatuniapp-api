package com.wuapp.service.dao.util.valueobject.enquiry;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class YearListVO {

   //
   private String yearOfEntry;
   private String selectedYear;

   //
   public String getYearOfEntry() {
	return yearOfEntry;
   }

   public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
   }

   public String getSelectedYear() {
	return selectedYear;
   }

   public void setSelectedYear(String selectedYear) {
	this.selectedYear = selectedYear;
   }
}
