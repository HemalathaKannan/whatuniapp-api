package com.wuapp.service.dao.util.valueobject.enquiry;

public class OtherInstitutionsVO {

   //
   private String institutionId;
   private String institutionDisplayName;
   private String logo;
   private String logoName;
   private String logoURL;
   private String matchingPercentage;

   //   
   public String getInstitutionId() {
	return institutionId;
   }

   public void setInstitutionId(String institutionId) {
	this.institutionId = institutionId;
   }

   public String getInstitutionDisplayName() {
	return institutionDisplayName;
   }

   public void setInstitutionDisplayName(String institutionDisplayName) {
	this.institutionDisplayName = institutionDisplayName;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }
}
