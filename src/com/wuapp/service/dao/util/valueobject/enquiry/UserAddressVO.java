package com.wuapp.service.dao.util.valueobject.enquiry;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class UserAddressVO {

   //
   private String addressLineOne;
   private String addressLineTwo;
   private String postCode;
   private String city;

   //   
   public String getAddressLineOne() {
	return addressLineOne;
   }

   public void setAddressLineOne(String addressLineOne) {
	this.addressLineOne = addressLineOne;
   }

   public String getAddressLineTwo() {
	return addressLineTwo;
   }

   public void setAddressLineTwo(String addressLineTwo) {
	this.addressLineTwo = addressLineTwo;
   }

   public String getPostCode() {
	return postCode;
   }

   public void setPostCode(String postCode) {
	this.postCode = postCode;
   }

   public String getCity() {
	return city;
   }

   public void setCity(String city) {
	this.city = city;
   }
}
