package com.wuapp.service.dao.util.valueobject.enquiry;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class InstitutionVO {

   //
   private String institutionId;
   private String institutionDisplayName;
   private String logo;
   private String logoName;
   private String logoURL;
   private String courseId;
   private String courseTitle;
   private String shortListFlag;

   //      
   public String getInstitutionId() {
	return institutionId;
   }

   public String getShortListFlag() {
	return shortListFlag;
   }

   public void setShortListFlag(String shortListFlag) {
	this.shortListFlag = shortListFlag;
   }

   public void setInstitutionId(String institutionId) {
	this.institutionId = institutionId;
   }

   public String getInstitutionDisplayName() {
	return institutionDisplayName;
   }

   public void setInstitutionDisplayName(String institutionDisplayName) {
	this.institutionDisplayName = institutionDisplayName;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getCourseTitle() {
	return courseTitle;
   }

   public void setCourseTitle(String courseTitle) {
	this.courseTitle = courseTitle;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }
}
