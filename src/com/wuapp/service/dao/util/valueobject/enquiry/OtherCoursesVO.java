package com.wuapp.service.dao.util.valueobject.enquiry;

public class OtherCoursesVO {

   //
   private String courseId;
   private String courseTitle;
   private String institutionId;
   private String institutionDisplayName;
   private String logo;
   private String logoName;
   private String logoURL;
   private String matchingPercentage;
   private String mediaPath;

   //   
   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getCourseTitle() {
	return courseTitle;
   }

   public void setCourseTitle(String courseTitle) {
	this.courseTitle = courseTitle;
   }

   public String getInstitutionId() {
	return institutionId;
   }

   public void setInstitutionId(String institutionId) {
	this.institutionId = institutionId;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }

   public String getInstitutionDisplayName() {
	return institutionDisplayName;
   }

   public void setInstitutionDisplayName(String institutionDisplayName) {
	this.institutionDisplayName = institutionDisplayName;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }
}
