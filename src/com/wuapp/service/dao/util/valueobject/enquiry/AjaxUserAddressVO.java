package com.wuapp.service.dao.util.valueobject.enquiry;

public class AjaxUserAddressVO {

   //
   private String addressCode;
   private String addressDisplay;

   //
   public String getAddressCode() {
	return addressCode;
   }

   public void setAddressCode(String addressCode) {
	this.addressCode = addressCode;
   }

   public String getAddressDisplay() {
	return addressDisplay;
   }

   public void setAddressDisplay(String addressDisplay) {
	this.addressDisplay = addressDisplay;
   }
}
