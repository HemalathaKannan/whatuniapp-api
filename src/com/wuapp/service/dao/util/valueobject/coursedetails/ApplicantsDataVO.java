package com.wuapp.service.dao.util.valueobject.coursedetails;

public class ApplicantsDataVO {

   private String totalApplicants;
   private String successfulApplicants;
   private String applicantsVal;
   private String offersVal;
   private String applicantSuccessRate;

   public String getTotalApplicants() {
	return totalApplicants;
   }

   public void setTotalApplicants(String totalApplicants) {
	this.totalApplicants = totalApplicants;
   }

   public String getSuccessfulApplicants() {
	return successfulApplicants;
   }

   public void setSuccessfulApplicants(String successfulApplicants) {
	this.successfulApplicants = successfulApplicants;
   }

   public String getApplicantsVal() {
	return applicantsVal;
   }

   public void setApplicantsVal(String applicantsVal) {
	this.applicantsVal = applicantsVal;
   }

   public String getOffersVal() {
	return offersVal;
   }

   public void setOffersVal(String offersVal) {
	this.offersVal = offersVal;
   }

   public String getApplicantSuccessRate() {
	return applicantSuccessRate;
   }

   public void setApplicantSuccessRate(String applicantSuccessRate) {
	this.applicantSuccessRate = applicantSuccessRate;
   }
}
