package com.wuapp.service.dao.util.valueobject.coursedetails;

public class CourseInfoVO {

   private String courseId;
   private String courseTitle;
   private String courseSummary;
   private String ucasCode;
   private String collegeId;
   private String collegeName;
   private String collegeNameDisplay;
   private String mediaPath;
   private String matchingPercentage;
   private String collegeLogo;
   private String logoName;
   private String logoURL;
   private String shortListFlag;

   public String getShortListFlag() {
	return shortListFlag;
   }

   public void setShortListFlag(String shortListFlag) {
	this.shortListFlag = shortListFlag;
   }

   public String getCollegeLogo() {
	return collegeLogo;
   }

   public void setCollegeLogo(String collegeLogo) {
	this.collegeLogo = collegeLogo;
   }

   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getCourseTitle() {
	return courseTitle;
   }

   public void setCourseTitle(String courseTitle) {
	this.courseTitle = courseTitle;
   }

   public String getCourseSummary() {
	return courseSummary;
   }

   public void setCourseSummary(String courseSummary) {
	this.courseSummary = courseSummary;
   }

   public String getUcasCode() {
	return ucasCode;
   }

   public void setUcasCode(String ucasCode) {
	this.ucasCode = ucasCode;
   }

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getCollegeName() {
	return collegeName;
   }

   public void setCollegeName(String collegeName) {
	this.collegeName = collegeName;
   }

   public String getCollegeNameDisplay() {
	return collegeNameDisplay;
   }

   public void setCollegeNameDisplay(String collegeNameDisplay) {
	this.collegeNameDisplay = collegeNameDisplay;
   }

   public String getMediaPath() {
	return mediaPath;
   }

   public void setMediaPath(String mediaPath) {
	this.mediaPath = mediaPath;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }
}
