package com.wuapp.service.dao.util.valueobject.coursedetails;

public class AssessedCourseVO {

   private String exam;
   private String courseWork;
   private String praticalWork;

   public String getExam() {
	return exam;
   }

   public void setExam(String exam) {
	this.exam = exam;
   }

   public String getCourseWork() {
	return courseWork;
   }

   public void setCourseWork(String courseWork) {
	this.courseWork = courseWork;
   }

   public String getPraticalWork() {
	return praticalWork;
   }

   public void setPraticalWork(String praticalWork) {
	this.praticalWork = praticalWork;
   }
}
