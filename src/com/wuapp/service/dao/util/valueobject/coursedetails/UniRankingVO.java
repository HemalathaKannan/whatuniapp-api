package com.wuapp.service.dao.util.valueobject.coursedetails;

import java.util.ArrayList;

public class UniRankingVO {

   private String studentRanking;
   private String totalStudentRanking;
   private String timesRanking;
   private String trSubjectName;
   private String currentRanking;
   private ArrayList<UniRankingVO> timesSubjectRankingList;

   public ArrayList<UniRankingVO> getTimesSubjectRankingList() {
	return timesSubjectRankingList;
   }

   public void setTimesSubjectRankingList(ArrayList<UniRankingVO> timesSubjectRankingList) {
	this.timesSubjectRankingList = timesSubjectRankingList;
   }

   public String getStudentRanking() {
	return studentRanking;
   }

   public void setStudentRanking(String studentRanking) {
	this.studentRanking = studentRanking;
   }

   public String getTotalStudentRanking() {
	return totalStudentRanking;
   }

   public void setTotalStudentRanking(String totalStudentRanking) {
	this.totalStudentRanking = totalStudentRanking;
   }

   public String getTimesRanking() {
	return timesRanking;
   }

   public void setTimesRanking(String timesRanking) {
	this.timesRanking = timesRanking;
   }

   public String getTrSubjectName() {
	return trSubjectName;
   }

   public void setTrSubjectName(String trSubjectName) {
	this.trSubjectName = trSubjectName;
   }

   public String getCurrentRanking() {
	return currentRanking;
   }

   public void setCurrentRanking(String currentRanking) {
	this.currentRanking = currentRanking;
   }
}
