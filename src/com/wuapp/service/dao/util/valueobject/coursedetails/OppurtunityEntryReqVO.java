package com.wuapp.service.dao.util.valueobject.coursedetails;

/**
 * ClearingEntryReqVO value object for the course details page.
 *
 * @author Sangeeth.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class OppurtunityEntryReqVO {

   private String opportunityId;
   private String entryDesc;
   private String entryPoints;
   private String additionalInfo;

   public String getOpportunityId() {
	return opportunityId;
   }

   public void setOpportunityId(String opportunityId) {
	this.opportunityId = opportunityId;
   }

   public String getEntryDesc() {
	return entryDesc;
   }

   public void setEntryDesc(String entryDesc) {
	this.entryDesc = entryDesc;
   }

   public String getEntryPoints() {
	return entryPoints;
   }

   public void setEntryPoints(String entryPoints) {
	this.entryPoints = entryPoints;
   }

   public String getAdditionalInfo() {
	return additionalInfo;
   }

   public void setAdditionalInfo(String additionalInfo) {
	this.additionalInfo = additionalInfo;
   }
}
