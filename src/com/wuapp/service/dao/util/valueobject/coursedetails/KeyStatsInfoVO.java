package com.wuapp.service.dao.util.valueobject.coursedetails;

public class KeyStatsInfoVO {

   private String subjectName;
   private String ratio;
   private String dropOutRate;
   private String graduatesPercent;
   private String salary6Months;
   private String salary40Months;
   private String salary6MonthsAtUni;
   private String salary6MonthsPer;
   private String salary40MonthsPer;
   private String salary6MonthsAtUniPer;
   private String continuingCourse;
   private String completedCourse;
   private String courseContinuation;

   public String getSubjectName() {
	return subjectName;
   }

   public void setSubjectName(String subjectName) {
	this.subjectName = subjectName;
   }

   public String getRatio() {
	return ratio;
   }

   public void setRatio(String ratio) {
	this.ratio = ratio;
   }

   public String getDropOutRate() {
	return dropOutRate;
   }

   public void setDropOutRate(String dropOutRate) {
	this.dropOutRate = dropOutRate;
   }

   public String getGraduatesPercent() {
	return graduatesPercent;
   }

   public void setGraduatesPercent(String graduatesPercent) {
	this.graduatesPercent = graduatesPercent;
   }

   public String getSalary6Months() {
	return salary6Months;
   }

   public void setSalary6Months(String salary6Months) {
	this.salary6Months = salary6Months;
   }

   public String getSalary40Months() {
	return salary40Months;
   }

   public void setSalary40Months(String salary40Months) {
	this.salary40Months = salary40Months;
   }

   public String getSalary6MonthsAtUni() {
	return salary6MonthsAtUni;
   }

   public void setSalary6MonthsAtUni(String salary6MonthsAtUni) {
	this.salary6MonthsAtUni = salary6MonthsAtUni;
   }

   public String getSalary6MonthsPer() {
	return salary6MonthsPer;
   }

   public void setSalary6MonthsPer(String salary6MonthsPer) {
	this.salary6MonthsPer = salary6MonthsPer;
   }

   public String getSalary40MonthsPer() {
	return salary40MonthsPer;
   }

   public void setSalary40MonthsPer(String salary40MonthsPer) {
	this.salary40MonthsPer = salary40MonthsPer;
   }

   public String getSalary6MonthsAtUniPer() {
	return salary6MonthsAtUniPer;
   }

   public void setSalary6MonthsAtUniPer(String salary6MonthsAtUniPer) {
	this.salary6MonthsAtUniPer = salary6MonthsAtUniPer;
   }

   public String getContinuingCourse() {
	return continuingCourse;
   }

   public void setContinuingCourse(String continuingCourse) {
	this.continuingCourse = continuingCourse;
   }

   public String getCompletedCourse() {
	return completedCourse;
   }

   public void setCompletedCourse(String completedCourse) {
	this.completedCourse = completedCourse;
   }

   public String getCourseContinuation() {
	return courseContinuation;
   }

   public void setCourseContinuation(String courseContinuation) {
	this.courseContinuation = courseContinuation;
   }
}
