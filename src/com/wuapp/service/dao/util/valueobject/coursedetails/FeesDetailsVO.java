package com.wuapp.service.dao.util.valueobject.coursedetails;

/**
 * FeesDetailsVO value object for the course details page.
 *
 * @author Sangeeth.S
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class FeesDetailsVO {

   private String feeType;
   private String fee;
   private String duration;
   private String currency;
   private String feeDesc;
   private String state;
   private String url;
   private String suborderItemId;
   private String networkID;
   private String sequenceNO;
   private String toolTipHeading;
   private String toolTipContent;
   private String matchedRegion;
   
   
   
   
   public String getMatchedRegion() {
      return matchedRegion;
   }


   
   public void setMatchedRegion(String matchedRegion) {
      this.matchedRegion = matchedRegion;
   }


   public String getToolTipHeading() {
      return toolTipHeading;
   }

   
   public void setToolTipHeading(String toolTipHeading) {
      this.toolTipHeading = toolTipHeading;
   }

   
   public String getToolTipContent() {
      return toolTipContent;
   }

   
   public void setToolTipContent(String toolTipContent) {
      this.toolTipContent = toolTipContent;
   }

   public String getFeeType() {
	return feeType;
   }

   public void setFeeType(String feeType) {
	this.feeType = feeType;
   }

   public String getFee() {
	return fee;
   }

   public void setFee(String fee) {
	this.fee = fee;
   }

   public String getDuration() {
	return duration;
   }

   public void setDuration(String duration) {
	this.duration = duration;
   }

   public String getCurrency() {
	return currency;
   }

   public void setCurrency(String currency) {
	this.currency = currency;
   }

   public String getFeeDesc() {
	return feeDesc;
   }

   public void setFeeDesc(String feeDesc) {
	this.feeDesc = feeDesc;
   }

   public String getState() {
	return state;
   }

   public void setState(String state) {
	this.state = state;
   }

   public String getUrl() {
	return url;
   }

   public void setUrl(String url) {
	this.url = url;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getNetworkID() {
	return networkID;
   }

   public void setNetworkID(String networkID) {
	this.networkID = networkID;
   }

   public String getSequenceNO() {
	return sequenceNO;
   }

   public void setSequenceNO(String sequenceNO) {
	this.sequenceNO = sequenceNO;
   }
}
