package com.wuapp.service.dao.util.valueobject.coursedetails;

public class OpportunityDetailsVO {

   private String opportunityId;
   private String opportunity;
   private String studyModeDesc;
   private String durationDesc;
   private String startDate;
   private String domPerYearCost;
   private String domCrsCost;
   private String restUkCourseCost;
   private String restUkOtherCost;
   private String ukFeeDesc;
   private String feeDescription;
   private String intlPerYearCost;
   private String euCostPerYear;
   private String selectedOpportunity;

   public String getSelectedOpportunity() {
	return selectedOpportunity;
   }

   public void setSelectedOpportunity(String selectedOpportunity) {
	this.selectedOpportunity = selectedOpportunity;
   }

   public String getIntlPerYearCost() {
	return intlPerYearCost;
   }

   public void setIntlPerYearCost(String intlPerYearCost) {
	this.intlPerYearCost = intlPerYearCost;
   }

   public String getEuCostPerYear() {
	return euCostPerYear;
   }

   public void setEuCostPerYear(String euCostPerYear) {
	this.euCostPerYear = euCostPerYear;
   }

   public String getOpportunityId() {
	return opportunityId;
   }

   public void setOpportunityId(String opportunityId) {
	this.opportunityId = opportunityId;
   }

   public String getOpportunity() {
	return opportunity;
   }

   public void setOpportunity(String opportunity) {
	this.opportunity = opportunity;
   }

   public String getStudyModeDesc() {
	return studyModeDesc;
   }

   public void setStudyModeDesc(String studyModeDesc) {
	this.studyModeDesc = studyModeDesc;
   }

   public String getDurationDesc() {
	return durationDesc;
   }

   public void setDurationDesc(String durationDesc) {
	this.durationDesc = durationDesc;
   }

   public String getStartDate() {
	return startDate;
   }

   public void setStartDate(String startDate) {
	this.startDate = startDate;
   }

   public String getDomPerYearCost() {
	return domPerYearCost;
   }

   public void setDomPerYearCost(String domPerYearCost) {
	this.domPerYearCost = domPerYearCost;
   }

   public String getDomCrsCost() {
	return domCrsCost;
   }

   public void setDomCrsCost(String domCrsCost) {
	this.domCrsCost = domCrsCost;
   }

   public String getRestUkCourseCost() {
	return restUkCourseCost;
   }

   public void setRestUkCourseCost(String restUkCourseCost) {
	this.restUkCourseCost = restUkCourseCost;
   }

   public String getRestUkOtherCost() {
	return restUkOtherCost;
   }

   public void setRestUkOtherCost(String restUkOtherCost) {
	this.restUkOtherCost = restUkOtherCost;
   }

   public String getUkFeeDesc() {
	return ukFeeDesc;
   }

   public void setUkFeeDesc(String ukFeeDesc) {
	this.ukFeeDesc = ukFeeDesc;
   }

   public String getFeeDescription() {
	return feeDescription;
   }

   public void setFeeDescription(String feeDescription) {
	this.feeDescription = feeDescription;
   }
}
