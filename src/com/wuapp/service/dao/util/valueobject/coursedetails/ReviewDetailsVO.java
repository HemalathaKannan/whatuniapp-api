package com.wuapp.service.dao.util.valueobject.coursedetails;

public class ReviewDetailsVO {

   private String reviewId;
   private String reviewerName;
   private String collegeId;
   private String overallRating;
   private String overallRatingComments;
   private String reviewedDate;
   private String displayOrder;
   private String reviewCount;
   private String reviewRatingDisplay;

   public String getReviewCount() {
	return reviewCount;
   }

   public void setReviewCount(String reviewCount) {
	this.reviewCount = reviewCount;
   }

   public String getReviewId() {
	return reviewId;
   }

   public void setReviewId(String reviewId) {
	this.reviewId = reviewId;
   }

   public String getReviewerName() {
	return reviewerName;
   }

   public void setReviewerName(String reviewerName) {
	this.reviewerName = reviewerName;
   }

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getOverallRating() {
	return overallRating;
   }

   public void setOverallRating(String overallRating) {
	this.overallRating = overallRating;
   }

   public String getOverallRatingComments() {
	return overallRatingComments;
   }

   public void setOverallRatingComments(String overallRatingComments) {
	this.overallRatingComments = overallRatingComments;
   }

   public String getReviewedDate() {
	return reviewedDate;
   }

   public void setReviewedDate(String reviewedDate) {
	this.reviewedDate = reviewedDate;
   }

   public String getDisplayOrder() {
	return displayOrder;
   }

   public void setDisplayOrder(String displayOrder) {
	this.displayOrder = displayOrder;
   }

   public String getReviewRatingDisplay() {
	return reviewRatingDisplay;
   }

   public void setReviewRatingDisplay(String reviewRatingDisplay) {
	this.reviewRatingDisplay = reviewRatingDisplay;
   }
}
