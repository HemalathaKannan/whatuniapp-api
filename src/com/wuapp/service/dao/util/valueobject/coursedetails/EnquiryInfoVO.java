package com.wuapp.service.dao.util.valueobject.coursedetails;

public class EnquiryInfoVO {

   private String orderItemId;
   private String websiteFlag;
   private String emailFlag;
   private String webformFlag;
   private String prospectusFlag;
   private String prospectusWebformFlag;
   private String email;
   private String emailWebform;
   private String prospectus;
   private String prospectusWebform;
   private String website;
   private String suborderItemId;
   private String myhcProfileId;
   private String profileType;
   private String networkId;
   private String hotline;
   private String applyNowButton;
   private String emailPrice;
   private String prospectusPrice;
   private String openDaysPrice;
   
   public String getProspectusPrice() {
      return prospectusPrice;
   }

   public void setProspectusPrice(String prospectusPrice) {
      this.prospectusPrice = prospectusPrice;
   }

   public String getOpenDaysPrice() {
      return openDaysPrice;
   }
 
   public void setOpenDaysPrice(String openDaysPrice) {
      this.openDaysPrice = openDaysPrice;
   }


   public String getEmailPrice() {
      return emailPrice;
   }

   public void setEmailPrice(String emailPrice) {
      this.emailPrice = emailPrice;
   }

   public String getApplyNowButton() {
      return applyNowButton;
   }

   
   public void setApplyNowButton(String applyNowButton) {
      this.applyNowButton = applyNowButton;
   }

   public String getHotline() {
	return hotline;
   }

   public void setHotline(String hotline) {
	this.hotline = hotline;
   }

   public String getOrderItemId() {
	return orderItemId;
   }

   public void setOrderItemId(String orderItemId) {
	this.orderItemId = orderItemId;
   }

   public String getWebsiteFlag() {
	return websiteFlag;
   }

   public void setWebsiteFlag(String websiteFlag) {
	this.websiteFlag = websiteFlag;
   }

   public String getEmailFlag() {
	return emailFlag;
   }

   public void setEmailFlag(String emailFlag) {
	this.emailFlag = emailFlag;
   }

   public String getWebformFlag() {
	return webformFlag;
   }

   public void setWebformFlag(String webformFlag) {
	this.webformFlag = webformFlag;
   }

   public String getProspectusFlag() {
	return prospectusFlag;
   }

   public void setProspectusFlag(String prospectusFlag) {
	this.prospectusFlag = prospectusFlag;
   }

   public String getProspectusWebformFlag() {
	return prospectusWebformFlag;
   }

   public void setProspectusWebformFlag(String prospectusWebformFlag) {
	this.prospectusWebformFlag = prospectusWebformFlag;
   }

   public String getEmail() {
	return email;
   }

   public void setEmail(String email) {
	this.email = email;
   }

   public String getEmailWebform() {
	return emailWebform;
   }

   public void setEmailWebform(String emailWebform) {
	this.emailWebform = emailWebform;
   }

   public String getProspectus() {
	return prospectus;
   }

   public void setProspectus(String prospectus) {
	this.prospectus = prospectus;
   }

   public String getProspectusWebform() {
	return prospectusWebform;
   }

   public void setProspectusWebform(String prospectusWebform) {
	this.prospectusWebform = prospectusWebform;
   }

   public String getWebsite() {
	return website;
   }

   public void setWebsite(String website) {
	this.website = website;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getMyhcProfileId() {
	return myhcProfileId;
   }

   public void setMyhcProfileId(String myhcProfileId) {
	this.myhcProfileId = myhcProfileId;
   }

   public String getProfileType() {
	return profileType;
   }

   public void setProfileType(String profileType) {
	this.profileType = profileType;
   }

   public String getNetworkId() {
	return networkId;
   }

   public void setNetworkId(String networkId) {
	this.networkId = networkId;
   }
}
