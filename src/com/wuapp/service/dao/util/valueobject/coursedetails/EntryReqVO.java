package com.wuapp.service.dao.util.valueobject.coursedetails;

public class EntryReqVO {

   private String entryLevel;
   private String points;
   private String description;
   private String ucasFlag;
   private String ucasTariff;
   private String ucasDescription;

   public String getEntryLevel() {
	return entryLevel;
   }

   public void setEntryLevel(String entryLevel) {
	this.entryLevel = entryLevel;
   }

   public String getPoints() {
	return points;
   }

   public void setPoints(String points) {
	this.points = points;
   }

   public String getDescription() {
	return description;
   }

   public void setDescription(String description) {
	this.description = description;
   }

   public String getUcasFlag() {
	return ucasFlag;
   }

   public void setUcasFlag(String ucasFlag) {
	this.ucasFlag = ucasFlag;
   }

   public String getUcasTariff() {
	return ucasTariff;
   }

   public void setUcasTariff(String ucasTariff) {
	this.ucasTariff = ucasTariff;
   }

   public String getUcasDescription() {
	return ucasDescription;
   }

   public void setUcasDescription(String ucasDescription) {
	this.ucasDescription = ucasDescription;
   }
}
