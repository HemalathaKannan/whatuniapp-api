package com.wuapp.service.dao.util.valueobject.coursedetails;

public class PreviousSubjectVO {

   private String subjectName;

   public String getSubjectName() {
	return subjectName;
   }

   public void setSubjectName(String subjectName) {
	this.subjectName = subjectName;
   }
}
