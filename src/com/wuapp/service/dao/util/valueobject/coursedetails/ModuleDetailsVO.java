package com.wuapp.service.dao.util.valueobject.coursedetails;

import java.util.ArrayList;

public class ModuleDetailsVO {

   private String moduleGroupId;
   private String courseId;
   private String stageLableId;
   private String stageLableName;
   private String module;
   private String moduleId;
   private String moduleGroup;
   private String moduleTitle;
   private String credits;
   private String moduleTypeName;
   private String moduleDescription;
   private String moduleUrl;
   private String shortDescription;
   private ArrayList<ModuleDetailsVO> moduleDetaiList;

   public ArrayList<ModuleDetailsVO> getModuleDetaiList() {
	return moduleDetaiList;
   }

   public void setModuleDetaiList(ArrayList<ModuleDetailsVO> moduleDetaiList) {
	this.moduleDetaiList = moduleDetaiList;
   }

   public String getModuleId() {
	return moduleId;
   }

   public void setModuleId(String moduleId) {
	this.moduleId = moduleId;
   }

   public String getModuleTitle() {
	return moduleTitle;
   }

   public void setModuleTitle(String moduleTitle) {
	this.moduleTitle = moduleTitle;
   }

   public String getCredits() {
	return credits;
   }

   public void setCredits(String credits) {
	this.credits = credits;
   }

   public String getModuleTypeName() {
	return moduleTypeName;
   }

   public void setModuleTypeName(String moduleTypeName) {
	this.moduleTypeName = moduleTypeName;
   }

   public String getModuleDescription() {
	return moduleDescription;
   }

   public void setModuleDescription(String moduleDescription) {
	this.moduleDescription = moduleDescription;
   }

   public String getModuleUrl() {
	return moduleUrl;
   }

   public void setModuleUrl(String moduleUrl) {
	this.moduleUrl = moduleUrl;
   }

   public String getShortDescription() {
	return shortDescription;
   }

   public void setShortDescription(String shortDescription) {
	this.shortDescription = shortDescription;
   }

   public String getModuleGroupId() {
	return moduleGroupId;
   }

   public void setModuleGroupId(String moduleGroupId) {
	this.moduleGroupId = moduleGroupId;
   }

   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getStageLableId() {
	return stageLableId;
   }

   public void setStageLableId(String stageLableId) {
	this.stageLableId = stageLableId;
   }

   public String getStageLableName() {
	return stageLableName;
   }

   public void setStageLableName(String stageLableName) {
	this.stageLableName = stageLableName;
   }

   public String getModule() {
	return module;
   }

   public void setModule(String module) {
	this.module = module;
   }

   public String getModuleGroup() {
	return moduleGroup;
   }

   public void setModuleGroup(String moduleGroup) {
	this.moduleGroup = moduleGroup;
   }
}
