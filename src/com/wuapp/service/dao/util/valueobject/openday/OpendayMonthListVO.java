package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get openday month list
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayMonthListVO {
  private String monthValue = null;
  private String monthName = null;
  private String opendayExistFlag = null;
  private String selectedFlag = null;
}
