package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get event type list
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class EventTypeListVO {
  
  private String eventCategoryId = null;
  private String eventCategoryName = null;
  private String selectedFlag = null;
}
