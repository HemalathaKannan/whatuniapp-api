package com.wuapp.service.dao.util.valueobject.openday;

public class OpendayVO {

   private String collegeId = null;
   private String collegeName = null;
   private String collegeDispName = null;
   private String collegeLogoPath = null;
   private String logoName = null;
   private String logoURL = null;
   private String selectedopendayVenue = null;
   private String selectedEventId = null;
   private String selectedEventDate = null;
   private String selectedEventTime = null;
   private String eventHeading = null;
   private String eventDesc = null;
   private String calendarItemId = null;
   private String opendate = null;
   private String startDate = null;
   private String endDate = null;
   private String startTime = null;
   private String endTime = null;
   private String bookingUrl = null;
   private String websitePrice = null;
   private String networkId = null;
   private String odMonthYear = null;
   private String qualId = null;
   private String qualDesc = null;
   private String subOrderItemId = null;

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getCollegeName() {
	return collegeName;
   }

   public void setCollegeName(String collegeName) {
	this.collegeName = collegeName;
   }

   public String getCollegeDispName() {
	return collegeDispName;
   }

   public void setCollegeDispName(String collegeDispName) {
	this.collegeDispName = collegeDispName;
   }

   public String getCollegeLogoPath() {
	return collegeLogoPath;
   }

   public void setCollegeLogoPath(String collegeLogoPath) {
	this.collegeLogoPath = collegeLogoPath;
   }

   public String getSelectedopendayVenue() {
	return selectedopendayVenue;
   }

   public void setSelectedopendayVenue(String selectedopendayVenue) {
	this.selectedopendayVenue = selectedopendayVenue;
   }

   public String getOpendate() {
	return opendate;
   }

   public void setOpendate(String opendate) {
	this.opendate = opendate;
   }

   public String getSelectedEventId() {
	return selectedEventId;
   }

   public void setSelectedEventId(String selectedEventId) {
	this.selectedEventId = selectedEventId;
   }

   public String getSelectedEventDate() {
	return selectedEventDate;
   }

   public void setSelectedEventDate(String selectedEventDate) {
	this.selectedEventDate = selectedEventDate;
   }

   public String getSelectedEventTime() {
	return selectedEventTime;
   }

   public void setSelectedEventTime(String selectedEventTime) {
	this.selectedEventTime = selectedEventTime;
   }

   public String getEventHeading() {
	return eventHeading;
   }

   public void setEventHeading(String eventHeading) {
	this.eventHeading = eventHeading;
   }

   public String getEventDesc() {
	return eventDesc;
   }

   public void setEventDesc(String eventDesc) {
	this.eventDesc = eventDesc;
   }

   public String getCalendarItemId() {
	return calendarItemId;
   }

   public void setCalendarItemId(String subOrderItemId) {
	this.calendarItemId = subOrderItemId;
   }

   public String getStartDate() {
	return startDate;
   }

   public void setStartDate(String startDate) {
	this.startDate = startDate;
   }

   public String getEndDate() {
	return endDate;
   }

   public void setEndDate(String endDate) {
	this.endDate = endDate;
   }

   public String getStartTime() {
	return startTime;
   }

   public void setStartTime(String startTime) {
	this.startTime = startTime;
   }

   public String getEndTime() {
	return endTime;
   }

   public void setEndTime(String endTime) {
	this.endTime = endTime;
   }

   public String getBookingUrl() {
	return bookingUrl;
   }

   public void setBookingUrl(String bookingUrl) {
	this.bookingUrl = bookingUrl;
   }

   public String getWebsitePrice() {
	return websitePrice;
   }

   public void setWebsitePrice(String websitePrice) {
	this.websitePrice = websitePrice;
   }

   public String getNetworkId() {
	return networkId;
   }

   public void setNetworkId(String networkId) {
	this.networkId = networkId;
   }

   public String getOdMonthYear() {
	return odMonthYear;
   }

   public void setOdMonthYear(String odMonthYear) {
	this.odMonthYear = odMonthYear;
   }

   public String getQualId() {
	return qualId;
   }

   public void setQualId(String qualId) {
	this.qualId = qualId;
   }

   public String getQualDesc() {
	return qualDesc;
   }

   public void setQualDesc(String qualDesc) {
	this.qualDesc = qualDesc;
   }

   public String getSubOrderItemId() {
	return subOrderItemId;
   }

   public void setSubOrderItemId(String subOrderItemId) {
	this.subOrderItemId = subOrderItemId;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }
}
