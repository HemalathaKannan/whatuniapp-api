package com.wuapp.service.dao.util.valueobject.openday;

import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get content section details
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class ContentSectionVO {
   
   private String sectionName = null;
   ArrayList<ContentSectionValueVO> sectionValueList = null;
}
