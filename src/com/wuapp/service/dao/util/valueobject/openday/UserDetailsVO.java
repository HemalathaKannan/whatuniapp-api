package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get user details
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class UserDetailsVO {

   private String firstName;
   private String lastName;
   private String userEmail;
   private String password;
   private String mobileNumber;
   private String marketingEmail;
   private String solusEmail;
   private String surveyFlag;
}
