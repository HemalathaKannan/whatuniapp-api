package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get study mode list
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class StudyModeVO {
   
  private String studyModeId = null;
  private String studyModeDesc = null;
  private String studyModeSelected = null;
}
