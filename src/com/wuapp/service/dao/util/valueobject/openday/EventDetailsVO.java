package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get uni event details
 *
 * @author Hema s
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class EventDetailsVO {
  
  private String eventId = null;
  private String collegeId = null;
  private String collegeName = null;
  private String collegeDisplayName = null;
  private String collegeLogo = null;
  private String startDate = null;
  private String startTime = null;
  private String venue = null;
  private String qualType = null;
}
