package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get other uni details
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OtherOdPodVO {
   
  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String logoPath = null;
  private String startTime = null;
  private String endTime = null;
  private String venue = null;
  private String qualId = null;
  private String qualType = null;
  private String openDate = null;
  private String openMonth = null;
  private String openDay = null;
  private String logoName = null;
  private String logoUrl = null;
  private String suborderItemId = null;
}
