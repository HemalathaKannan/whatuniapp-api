package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get event details
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class EventListVO {
  
  private String monthValue = null;
  private String monthName = null;
  private String odExistFlag = null;
  private String selectedFlag = null;
}
