package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get selected event details
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class SelectedEventDetailsVO {
  
  private String collegeId = null;
  private String collegeName = null;
  private String collegeDisplayName = null;
  private String suborderItemId = null;
  private String networkId = null;
  private String startDate = null;
  private String opendayDate = null;
  private String openDate = null;
  private String openMonthYear = null;
  private String bookingUrl = null;
  private String bookingFormFlag = null;
  private String websitePrice = null;
}
