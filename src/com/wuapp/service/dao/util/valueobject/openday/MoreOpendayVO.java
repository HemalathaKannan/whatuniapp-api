package com.wuapp.service.dao.util.valueobject.openday;

public class MoreOpendayVO {

   private String opendaysDetail = null;
   private String eventId = null;
   private String eventVenue = null;
   private String eventDate = null;
   private String eventTime = null;
   private String selectedEvent = null;

   public String getOpendaysDetail() {
	return opendaysDetail;
   }

   public void setOpendaysDetail(String opendaysDetail) {
	this.opendaysDetail = opendaysDetail;
   }

   public String getEventId() {
	return eventId;
   }

   public void setEventId(String eventId) {
	this.eventId = eventId;
   }

   public String getEventVenue() {
	return eventVenue;
   }

   public void setEventVenue(String eventVenue) {
	this.eventVenue = eventVenue;
   }

   public String getEventDate() {
	return eventDate;
   }

   public void setEventDate(String eventDate) {
	this.eventDate = eventDate;
   }

   public String getEventTime() {
	return eventTime;
   }

   public void setEventTime(String eventTime) {
	this.eventTime = eventTime;
   }

   public String getSelectedEvent() {
	return selectedEvent;
   }

   public void setSelectedEvent(String selectedEvent) {
	this.selectedEvent = selectedEvent;
   }
}
