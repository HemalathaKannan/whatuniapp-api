package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get search ajax list
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchAjaxVO {

   private String collegeId;
   private String collegeDisplayName;
   private String opendayMsg;
}
