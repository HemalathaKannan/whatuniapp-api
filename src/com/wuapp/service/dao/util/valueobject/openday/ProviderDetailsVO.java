package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get provider details
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class ProviderDetailsVO {
   
  private String collegeName = null;
  private String collegeDisplayName = null;
  private String collegeLogo = null;
  private String logoName = null;
  private String logoUrl = null;
}
