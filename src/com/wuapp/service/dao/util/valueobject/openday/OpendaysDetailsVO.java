package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get openday details
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaysDetailsVO {

   private String collegeId = null;
   private String eventId = null;
   private String startDate = null;
   private String startTime = null;
   private String venue = null;
   private String qualType = null;
   private String bookingUrl = null;
   private String bookingFormFlag = null;
}
