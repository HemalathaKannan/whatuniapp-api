package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get openday details
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayListVO {
   
   private String eventItemId = null;
   private String startDate = null;
   private String startTime = null;
   private String venue = null;
   private String qualType = null;
   private String totalOpendayCount = null;   
   private String moreOpendayFlag = null;
   private String bookingUrl = null;
   private String bookingFormFlag = null;
   private String eventCategoryName = null;
   private String eventCategoryId = null;
}
