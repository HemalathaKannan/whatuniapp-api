package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get university address
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class AddressVO {
   
   private String collegeDisplayName = null;
   private String addressLine1 = null;
   private String addressLine2 = null;
   private String town = null;
   private String countryState = null;
   private String postcode = null;
   private String isInLondon = null;
   private String latitude = null;
   private String longitude = null;
   private String nearestTrainStn = null;  
   private String nearestTubeStn = null;  
   private String distanceFromTrainStn = null; 
   private String distanceFromTubeStn = null;
   private String cityGuideDisplayFlag = null;  
   private String location = null;
   private String articleCategory = null;
   private String postUrl = null;
   private String articleId = null;
}
