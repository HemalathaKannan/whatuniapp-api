package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get year of entry detail
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class YearOfEntryVO {

   private String yearOfEntry = null;
   private String selectedYear = null;
}
