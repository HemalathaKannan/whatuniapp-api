package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get testimonial section details
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class TestimonialSectionVO {
   
   private String sectionValue = null;
   private String title = null;
   private String updatedDate = null;   
}
