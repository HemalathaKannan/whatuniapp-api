package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get client consent details 
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class ClientConsentVO {

   private String consentText = null;
   private String clientMarketingConsentFlag = null;
}
