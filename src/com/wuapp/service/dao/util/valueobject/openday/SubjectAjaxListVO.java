package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get subject ajax list
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class SubjectAjaxListVO {

   private String categoryId;
   private String categoryCode;
   private String subjectName;
   private String qualCode;
}