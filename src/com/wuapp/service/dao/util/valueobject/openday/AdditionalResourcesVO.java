package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get addition resources 
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class AdditionalResourcesVO {
   
   private String pdfName = null;
   private String pdfPath = null;
}
