package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get qualification details
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class QualificationDetailsVO {
   
  private String qualId = null;
  private String qualDesc = null;
  private String qualSelected = null;
  private String qualCode = null;
}
