package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get uni gallery detail
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class GallerySectionVO {
   
   private String mediaId = null;
   private String collegeDisplayName = null;
   private String mediaPath = null;
   private String mediaType = null;
   private String thumbnailPath = null;
   private String contenthubFlag = null;
}
