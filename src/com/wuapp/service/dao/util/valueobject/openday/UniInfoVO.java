package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get uni info
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class UniInfoVO {
   
   private String collegeId = null;
   private String collegeLogo = null;
   private String logoName = null;
   private String logoUrl = null;
   private String mediaPath = null;
   private String collegeName = null;
   private String collegeDisplayName = null;
   private String reviewCount = null;
   private String overallRating = null;
   private String overallRatingExact = null;
}
