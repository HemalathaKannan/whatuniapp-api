package com.wuapp.service.dao.util.valueobject.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * This VO used to get openday search landing page detail
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchLandingVO {

   private String collegeId = null;
   private String collegeName = null;
   private String collegeLogo = null;
   private String logoName = null;
   private String logoURL = null;
   private String mediaPath = null;
   private String eventId = null;
   private String startDate = null;
   private String startTime = null;
   private String venue = null;
   private String opendayCount = null;
   private String reviewRating = null;
   private String reviewRatingDisplay = null;
   private String eventCategoryId = null;
}
