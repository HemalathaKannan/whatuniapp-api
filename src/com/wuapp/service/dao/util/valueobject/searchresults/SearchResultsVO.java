package com.wuapp.service.dao.util.valueobject.searchresults;

public class SearchResultsVO {

   //
   private String id;
   private String name;
   private String courseCount;
   private String logo;
   private String reviewRating;
   private String shortlistFlag;
   private String matchingPercentage;
   private String tileMediaPath;
   private String qualificationMatch;
   private String subjectMatch;
   private String locationMatch;
   private String locationTypeMatch;
   private String studyModeMatch;
   private String previousQualificationMatch;
   private String assessmentTypeMatch;
   private String reviewRatingDisplay;
   private String logoName;
   private String logoURL;
   private String reviewSubjectOneMatch;
   private String reviewSubjectTwoMatch;
   private String reviewSubjectThreeMatch;
   private String applyNowFlag;

   
   public String getApplyNowFlag() {
      return applyNowFlag;
   }

   
   public void setApplyNowFlag(String applyNowFlag) {
      this.applyNowFlag = applyNowFlag;
   }

   //
   public String getId() {
	return id;
   }

   public void setId(String id) {
	this.id = id;
   }

   public String getName() {
	return name;
   }

   public void setName(String name) {
	this.name = name;
   }

   public String getCourseCount() {
	return courseCount;
   }

   public void setCourseCount(String courseCount) {
	this.courseCount = courseCount;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getReviewRating() {
	return reviewRating;
   }

   public void setReviewRating(String reviewRating) {
	this.reviewRating = reviewRating;
   }

   public String getShortlistFlag() {
	return shortlistFlag;
   }

   public void setShortlistFlag(String shortlistFlag) {
	this.shortlistFlag = shortlistFlag;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getTileMediaPath() {
	return tileMediaPath;
   }

   public void setTileMediaPath(String tileMediaPath) {
	this.tileMediaPath = tileMediaPath;
   }

   public String getQualificationMatch() {
	return qualificationMatch;
   }

   public void setQualificationMatch(String qualificationMatch) {
	this.qualificationMatch = qualificationMatch;
   }

   public String getSubjectMatch() {
	return subjectMatch;
   }

   public void setSubjectMatch(String subjectMatch) {
	this.subjectMatch = subjectMatch;
   }

   public String getLocationMatch() {
	return locationMatch;
   }

   public void setLocationMatch(String locationMatch) {
	this.locationMatch = locationMatch;
   }

   public String getLocationTypeMatch() {
	return locationTypeMatch;
   }

   public void setLocationTypeMatch(String locationTypeMatch) {
	this.locationTypeMatch = locationTypeMatch;
   }

   public String getStudyModeMatch() {
	return studyModeMatch;
   }

   public void setStudyModeMatch(String studyModeMatch) {
	this.studyModeMatch = studyModeMatch;
   }

   public String getPreviousQualificationMatch() {
	return previousQualificationMatch;
   }

   public void setPreviousQualificationMatch(String previousQualificationMatch) {
	this.previousQualificationMatch = previousQualificationMatch;
   }

   public String getReviewRatingDisplay() {
	return reviewRatingDisplay;
   }

   public void setReviewRatingDisplay(String reviewRatingDisplay) {
	this.reviewRatingDisplay = reviewRatingDisplay;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }

   public String getAssessmentTypeMatch() {
	return assessmentTypeMatch;
   }

   public void setAssessmentTypeMatch(String assessmentTypeMatch) {
	this.assessmentTypeMatch = assessmentTypeMatch;
   }

   public String getReviewSubjectOneMatch() {
	return reviewSubjectOneMatch;
   }

   public void setReviewSubjectOneMatch(String reviewSubjectOneMatch) {
	this.reviewSubjectOneMatch = reviewSubjectOneMatch;
   }

   public String getReviewSubjectTwoMatch() {
	return reviewSubjectTwoMatch;
   }

   public void setReviewSubjectTwoMatch(String reviewSubjectTwoMatch) {
	this.reviewSubjectTwoMatch = reviewSubjectTwoMatch;
   }

   public String getReviewSubjectThreeMatch() {
	return reviewSubjectThreeMatch;
   }

   public void setReviewSubjectThreeMatch(String reviewSubjectThreeMatch) {
	this.reviewSubjectThreeMatch = reviewSubjectThreeMatch;
   }
}
