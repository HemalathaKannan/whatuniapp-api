package com.wuapp.service.dao.util.valueobject.user;

public class UserDetailsVO {

   private String forename;
   private String surname;
   private String userImage;
   private String facebookId;
   private String schoolURN;
   private String schoolName;
   private String userSource;

   public String getSchoolURN() {
	return schoolURN;
   }

   public void setSchoolURN(String schoolURN) {
	this.schoolURN = schoolURN;
   }

   public String getSchoolName() {
	return schoolName;
   }

   public void setSchoolName(String schoolName) {
	this.schoolName = schoolName;
   }

   public String getUserSource() {
	return userSource;
   }

   public void setUserSource(String userSource) {
	this.userSource = userSource;
   }

   public String getForename() {
	return forename;
   }

   public void setForename(String forename) {
	this.forename = forename;
   }

   public String getSurname() {
	return surname;
   }

   public void setSurname(String surname) {
	this.surname = surname;
   }

   public String getUserImage() {
	return userImage;
   }

   public void setUserImage(String userImage) {
	this.userImage = userImage;
   }

   public String getFacebookId() {
	return facebookId;
   }

   public void setFacebookId(String facebookId) {
	this.facebookId = facebookId;
   }
}
