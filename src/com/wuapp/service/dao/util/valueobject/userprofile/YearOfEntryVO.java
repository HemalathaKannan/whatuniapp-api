package com.wuapp.service.dao.util.valueobject.userprofile;

public class YearOfEntryVO {

   private String yearOfEntry;
   private String selectedYear;

   public String getYearOfEntry() {
	return yearOfEntry;
   }

   public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
   }

   public String getSelectedYear() {
	return selectedYear;
   }

   public void setSelectedYear(String selectedYear) {
	this.selectedYear = selectedYear;
   }
}
