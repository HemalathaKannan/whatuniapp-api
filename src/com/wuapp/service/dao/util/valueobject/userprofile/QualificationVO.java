package com.wuapp.service.dao.util.valueobject.userprofile;

public class QualificationVO {

   private String qualificationName;
   private String selectedQualification;

   public String getQualificationName() {
	return qualificationName;
   }

   public void setQualificationName(String qualificationName) {
	this.qualificationName = qualificationName;
   }

   public String getSelectedQualification() {
	return selectedQualification;
   }

   public void setSelectedQualification(String selectedQualification) {
	this.selectedQualification = selectedQualification;
   }
}
