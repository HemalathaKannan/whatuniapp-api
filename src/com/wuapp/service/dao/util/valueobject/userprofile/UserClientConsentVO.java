package com.wuapp.service.dao.util.valueobject.userprofile;

/**
 * This class used for showing client consent details in preference page
 *
 * @author Hemalatha.K
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */

public class UserClientConsentVO {

   private String collegeId;
   private String collegeName;
   private String url;
   private String createdDate;
   
   public String getCollegeId() {
      return collegeId;
   }
   
   public void setCollegeId(String collegeId) {
      this.collegeId = collegeId;
   }
   
   public String getCollegeName() {
      return collegeName;
   }
   
   public void setCollegeName(String collegeName) {
      this.collegeName = collegeName;
   }
   
   public String getUrl() {
      return url;
   }
   
   public void setUrl(String url) {
      this.url = url;
   }
   
   public String getCreatedDate() {
      return createdDate;
   }
   
   public void setCreatedDate(String createdDate) {
      this.createdDate = createdDate;
   }   
}
