package com.wuapp.service.dao.util.valueobject.userprofile;

public class PrivacyCAFlagVO {

   private String privacyFlag;
   private String caFlag;

   public String getPrivacyFlag() {
	return privacyFlag;
   }

   public void setPrivacyFlag(String privacyFlag) {
	this.privacyFlag = privacyFlag;
   }

   public String getCaFlag() {
	return caFlag;
   }

   public void setCaFlag(String caFlag) {
	this.caFlag = caFlag;
   }
}
