package com.wuapp.service.dao.util.valueobject.userprofile;

public class SchoolInfoVO {

   private String schoolId;
   private String schoolName;
   private String schoolDisplayName;

   public String getSchoolId() {
	return schoolId;
   }

   public void setSchoolId(String schoolId) {
	this.schoolId = schoolId;
   }

   public String getSchoolName() {
	return schoolName;
   }

   public void setSchoolName(String schoolName) {
	this.schoolName = schoolName;
   }

   public String getSchoolDisplayName() {
	return schoolDisplayName;
   }

   public void setSchoolDisplayName(String schoolDisplayName) {
	this.schoolDisplayName = schoolDisplayName;
   }
}
