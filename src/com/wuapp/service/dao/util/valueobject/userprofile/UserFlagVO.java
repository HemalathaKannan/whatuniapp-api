package com.wuapp.service.dao.util.valueobject.userprofile;

public class UserFlagVO {

   private String receiveNoEmailFlag;
   private String permitEmail;
   private String solusEmail;
   private String marketingEmail;
   private String surveyFlag;
   private String firstName;
   private String lastName;
   private String userImage;
   private String password;
   private String facebookId;

   public String getFacebookId() {
	return facebookId;
   }

   public void setFacebookId(String facebookId) {
	this.facebookId = facebookId;
   }

   public String getFirstName() {
	return firstName;
   }

   public void setFirstName(String firstName) {
	this.firstName = firstName;
   }

   public String getLastName() {
	return lastName;
   }

   public void setLastName(String lastName) {
	this.lastName = lastName;
   }

   public String getUserImage() {
	return userImage;
   }

   public void setUserImage(String userImage) {
	this.userImage = userImage;
   }

   public String getPassword() {
	return password;
   }

   public void setPassword(String password) {
	this.password = password;
   }

   public String getReceiveNoEmailFlag() {
	return receiveNoEmailFlag;
   }

   public void setReceiveNoEmailFlag(String receiveNoEmailFlag) {
	this.receiveNoEmailFlag = receiveNoEmailFlag;
   }

   public String getPermitEmail() {
	return permitEmail;
   }

   public void setPermitEmail(String permitEmail) {
	this.permitEmail = permitEmail;
   }

   public String getSolusEmail() {
	return solusEmail;
   }

   public void setSolusEmail(String solusEmail) {
	this.solusEmail = solusEmail;
   }

   public String getMarketingEmail() {
	return marketingEmail;
   }

   public void setMarketingEmail(String marketingEmail) {
	this.marketingEmail = marketingEmail;
   }

   public String getSurveyFlag() {
	return surveyFlag;
   }

   public void setSurveyFlag(String surveyFlag) {
	this.surveyFlag = surveyFlag;
   }
}
