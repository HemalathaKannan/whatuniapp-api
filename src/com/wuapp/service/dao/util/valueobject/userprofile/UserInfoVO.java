package com.wuapp.service.dao.util.valueobject.userprofile;

/**
 * PROVIDER RESULTS
 *
 * @author Keerthika.P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class UserInfoVO {

   //
   private String foreName;
   private String surName;
   private String email;
   private String dateOfBirth;
   private String addressLineOne;
   private String addressLineTwo;
   private String city;
   private String postcode;
   private String countryId;
   private String yearOfEntry;
   private String studyLevel;
   private String schoolId;
   private String schoolName;
   private String grade;
   private String gradeType;
   private String gradeValue;
   private String countyState;

   //    
   public String getForeName() {
	return foreName;
   }

   public String getGradeValue() {
	return gradeValue;
   }

   public void setGradeValue(String gradeValue) {
	this.gradeValue = gradeValue;
   }

   public String getSchoolId() {
	return schoolId;
   }

   public void setSchoolId(String schoolId) {
	this.schoolId = schoolId;
   }

   public String getCountyState() {
	return countyState;
   }

   public void setCountyState(String countyState) {
	this.countyState = countyState;
   }

   public void setForeName(String foreName) {
	this.foreName = foreName;
   }

   public String getSurName() {
	return surName;
   }

   public void setSurName(String surName) {
	this.surName = surName;
   }

   public String getEmail() {
	return email;
   }

   public void setEmail(String email) {
	this.email = email;
   }

   public String getDateOfBirth() {
	return dateOfBirth;
   }

   public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
   }

   public String getAddressLineOne() {
	return addressLineOne;
   }

   public void setAddressLineOne(String addressLineOne) {
	this.addressLineOne = addressLineOne;
   }

   public String getAddressLineTwo() {
	return addressLineTwo;
   }

   public void setAddressLineTwo(String addressLineTwo) {
	this.addressLineTwo = addressLineTwo;
   }

   public String getCity() {
	return city;
   }

   public void setCity(String city) {
	this.city = city;
   }

   public String getPostcode() {
	return postcode;
   }

   public void setPostcode(String postcode) {
	this.postcode = postcode;
   }

   public String getCountryId() {
	return countryId;
   }

   public void setCountryId(String countryId) {
	this.countryId = countryId;
   }

   public String getYearOfEntry() {
	return yearOfEntry;
   }

   public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
   }

   public String getStudyLevel() {
	return studyLevel;
   }

   public void setStudyLevel(String studyLevel) {
	this.studyLevel = studyLevel;
   }

   public String getSchoolName() {
	return schoolName;
   }

   public void setSchoolName(String schoolName) {
	this.schoolName = schoolName;
   }

   public String getGrade() {
	return grade;
   }

   public void setGrade(String grade) {
	this.grade = grade;
   }

   public String getGradeType() {
	return gradeType;
   }

   public void setGradeType(String gradeType) {
	this.gradeType = gradeType;
   }
}
