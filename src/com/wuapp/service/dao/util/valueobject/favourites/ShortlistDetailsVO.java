package com.wuapp.service.dao.util.valueobject.favourites;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class ShortlistDetailsVO {

   //
   private String institutionId;
   private String institutionName;
   private String logo;
   private String logoName;
   private String logoURL;
   private String courseId;
   private String courseTitle;
   private String basketId;
   private String position;
   private String finalChoiceId;
   private String matchingPercentage;
   private String basketContentId;

   //
   public String getInstitutionId() {
	return institutionId;
   }

   public void setInstitutionId(String institutionId) {
	this.institutionId = institutionId;
   }

   public String getInstitutionName() {
	return institutionName;
   }

   public void setInstitutionName(String institutionName) {
	this.institutionName = institutionName;
   }

   public String getLogo() {
	return logo;
   }

   public void setLogo(String logo) {
	this.logo = logo;
   }

   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getCourseTitle() {
	return courseTitle;
   }

   public void setCourseTitle(String courseTitle) {
	this.courseTitle = courseTitle;
   }

   public String getBasketId() {
	return basketId;
   }

   public void setBasketId(String basketId) {
	this.basketId = basketId;
   }

   public String getPosition() {
	return position;
   }

   public void setPosition(String position) {
	this.position = position;
   }

   public String getFinalChoiceId() {
	return finalChoiceId;
   }

   public void setFinalChoiceId(String finalChoiceId) {
	this.finalChoiceId = finalChoiceId;
   }

   public String getMatchingPercentage() {
	return matchingPercentage;
   }

   public void setMatchingPercentage(String matchingPercentage) {
	this.matchingPercentage = matchingPercentage;
   }

   public String getLogoName() {
	return logoName;
   }

   public void setLogoName(String logoName) {
	this.logoName = logoName;
   }

   public String getLogoURL() {
	return logoURL;
   }

   public void setLogoURL(String logoURL) {
	this.logoURL = logoURL;
   }

   public String getBasketContentId() {
	return basketContentId;
   }

   public void setBasketContentId(String basketContentId) {
	this.basketContentId = basketContentId;
   }
}
