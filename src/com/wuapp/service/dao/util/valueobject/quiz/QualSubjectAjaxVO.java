package com.wuapp.service.dao.util.valueobject.quiz;

public class QualSubjectAjaxVO {

   private String searchId;
   private String description;

   public String getSearchId() {
	return searchId;
   }

   public void setSearchId(String searchId) {
	this.searchId = searchId;
   }

   public String getDescription() {
	return description;
   }

   public void setDescription(String description) {
	this.description = description;
   }
}
