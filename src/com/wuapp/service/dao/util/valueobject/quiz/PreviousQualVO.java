package com.wuapp.service.dao.util.valueobject.quiz;

public class PreviousQualVO {

   private String qualification;
   private String gradeStr;
   private String gradeLevel;
   private String textKey;
   private String qualId;
   private String noOfsubjects;

   public String getQualId() {
      return qualId;
   }
   
   public void setQualId(String qualId) {
      this.qualId = qualId;
   }
   
   public String getNoOfsubjects() {
      return noOfsubjects;
   }


   public void setNoOfsubjects(String noOfsubjects) {
      this.noOfsubjects = noOfsubjects;
   }

   public String getQualification() {
	return qualification;
   }

   public void setQualification(String qualification) {
	this.qualification = qualification;
   }

   public String getGradeStr() {
	return gradeStr;
   }

   public void setGradeStr(String gradeStr) {
	this.gradeStr = gradeStr;
   }

   public String getGradeLevel() {
	return gradeLevel;
   }

   public void setGradeLevel(String gradeLevel) {
	this.gradeLevel = gradeLevel;
   }

   public String getTextKey() {
	return textKey;
   }

   public void setTextKey(String textKey) {
	this.textKey = textKey;
   }
}
