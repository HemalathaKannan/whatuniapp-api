package com.wuapp.service.dao;

import java.util.Map;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadFromClient;

public interface IProfileDAO {

   public Map<String, Object> getProfileDetails(ProfileDetailsPayloadFromClient profileDetailsPayloadFromClient);
}
