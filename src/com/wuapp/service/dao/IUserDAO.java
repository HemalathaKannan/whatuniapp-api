package com.wuapp.service.dao;

import java.util.Map;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;

public interface IUserDAO {

   public Map<String, Object> userRegistration(UserPayloadFromClient userPayloadFromClient);

   public Map<String, Object> userLogin(UserPayloadFromClient userPayloadFromClient);

   public Map<String, Object> checkUserExisted(UserPayloadFromClient userPayloadFromClient);

   public Map<String, Object> userForgotPassword(UserPayloadFromClient userPayloadFromClient);
   
   public Map<String, Object> getGuestUserId();
}
