package com.wuapp.service.dao.sp.userprofile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class SaveUserProfileSP extends StoredProcedure {

   private String SAVE_USER_PROFILE_SP = ProcedureNames.SAVE_USER_PROFILE_SP;

   public SaveUserProfileSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SAVE_USER_PROFILE_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_forename", Types.VARCHAR));
	declareParameter(new SqlParameter("p_surname", Types.VARCHAR));
	declareParameter(new SqlParameter("p_email", Types.VARCHAR));
	declareParameter(new SqlParameter("p_password", Types.VARCHAR));
	declareParameter(new SqlParameter("p_dob", Types.VARCHAR));
	declareParameter(new SqlParameter("p_nationality", Types.VARCHAR));
	declareParameter(new SqlParameter("p_address_line_1", Types.VARCHAR));
	declareParameter(new SqlParameter("p_address_line_2", Types.VARCHAR));
	declareParameter(new SqlParameter("p_city", Types.VARCHAR));
	declareParameter(new SqlParameter("p_county_state", Types.VARCHAR));
	declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
	declareParameter(new SqlParameter("p_yoe", Types.VARCHAR));
	declareParameter(new SqlParameter("p_study_level", Types.VARCHAR));
	declareParameter(new SqlParameter("p_school", Types.VARCHAR));
	declareParameter(new SqlParameter("p_non_uk_school", Types.VARCHAR));
	declareParameter(new SqlParameter("p_pre_qual_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_pre_qual_grade", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("p_success_msg", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", userProfilePayloadFromClient.getUserId());
	inMap.put("p_forename", userProfilePayloadFromClient.getFirstName());
	inMap.put("p_surname", userProfilePayloadFromClient.getLastName());
	inMap.put("p_email", userProfilePayloadFromClient.getEmail());
	inMap.put("p_password", userProfilePayloadFromClient.getPassword());
	inMap.put("p_dob", userProfilePayloadFromClient.getDateOfBirth());
	inMap.put("p_nationality", userProfilePayloadFromClient.getNationality());
	inMap.put("p_address_line_1", userProfilePayloadFromClient.getAddressLineOne());
	inMap.put("p_address_line_2", userProfilePayloadFromClient.getAddressLineTwo());
	inMap.put("p_city", userProfilePayloadFromClient.getCity());
	inMap.put("p_county_state", userProfilePayloadFromClient.getCountyState());
	inMap.put("p_postcode", userProfilePayloadFromClient.getPostCode());
	inMap.put("p_yoe", userProfilePayloadFromClient.getYearOfEntry());
	inMap.put("p_study_level", userProfilePayloadFromClient.getStudyLevel());
	inMap.put("p_school", userProfilePayloadFromClient.getSchool());
	inMap.put("p_non_uk_school", userProfilePayloadFromClient.getNonUkSchool());
	inMap.put("p_pre_qual_type", userProfilePayloadFromClient.getPreQualType());
	inMap.put("p_pre_qual_grade", userProfilePayloadFromClient.getPreQualGrade());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SAVE_USER_PROFILE_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
