package com.wuapp.service.dao.sp.userprofile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.userprofile.EditPageUserInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.PrivacyCAFlagRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.UserClientConsentRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.UserFlagRowmapperImpl;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log :
 * ***************************************************************************************************************************
 * Author          version      Modified On     Modification Details                        Change
 * ***************************************************************************************************************************
 * Hemalatha.K       1.1        17_DEC_19           Modified                       Getting client consent details
 * Hemalatha.K       2.0        25-JUN-2020                                        Removed clearing related param 
 */

public class GetUserPreferenceSP extends StoredProcedure {

   private String GET_USER_PREFERENCE_SP = ProcedureNames.GET_USER_PREFERENCE_SP;

   public GetUserPreferenceSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_USER_PREFERENCE_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_password", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("PC_USER_INFO", OracleTypes.CURSOR, new EditPageUserInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_USER_FLAG", OracleTypes.CURSOR, new UserFlagRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_PRIVACY_CA_FLAG", OracleTypes.CURSOR, new PrivacyCAFlagRowmapperImpl()));
	//
	declareParameter(new SqlOutParameter("PC_USER_CLIENT_CONSENT_LIST", OracleTypes.CURSOR, new UserClientConsentRowmapperImpl()));  // Added for client lead consent by Hemalatha.K on 17_DEC_19_REL
	//
	compile();
   }

   public Map<String, Object> executeProcedure(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", userProfilePayloadFromClient.getUserId());
	inMap.put("p_password", userProfilePayloadFromClient.getPassword());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_USER_PREFERENCE_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
