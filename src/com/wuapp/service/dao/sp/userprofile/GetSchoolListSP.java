package com.wuapp.service.dao.sp.userprofile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.userprofile.SchoolListRowmapperImpl;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class GetSchoolListSP extends StoredProcedure {

   private String GET_SCHOOL_LIST_SP = ProcedureNames.GET_SCHOOL_LIST_SP;

   public GetSchoolListSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_SCHOOL_LIST_SP);
	declareParameter(new SqlParameter("p_text", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("pc_school_list", OracleTypes.CURSOR, new SchoolListRowmapperImpl()));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_text", userProfilePayloadFromClient.getSchool());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_SCHOOL_LIST_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
