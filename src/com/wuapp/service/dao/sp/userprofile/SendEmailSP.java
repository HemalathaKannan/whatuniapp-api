package com.wuapp.service.dao.sp.userprofile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class SendEmailSP extends StoredProcedure {

   private String SEND_EMAIL_SP = ProcedureNames.SEND_EMAIL_SP;

   public SendEmailSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SEND_EMAIL_SP);
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_text", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlParameter("P_EMAIL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_FIRST_NAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LAST_NAME", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", userProfilePayloadFromClient.getUserId());
	inMap.put("p_text", userProfilePayloadFromClient.getMessage());
	inMap.put("P_APP_VERSION", userProfilePayloadFromClient.getAppVersion());
	inMap.put("P_EMAIL", userProfilePayloadFromClient.getEmail());
	inMap.put("P_FIRST_NAME", userProfilePayloadFromClient.getFirstName());
	inMap.put("P_LAST_NAME", userProfilePayloadFromClient.getLastName());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SEND_EMAIL_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
