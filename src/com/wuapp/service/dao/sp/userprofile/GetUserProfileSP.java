package com.wuapp.service.dao.sp.userprofile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.CountryListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.PreviousQualListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.QualificationListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.UserInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.userprofile.YearOfEntryListRowmapperImpl;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class GetUserProfileSP extends StoredProcedure {

   private String GET_USER_PROFILE_SP = ProcedureNames.GET_USER_PROFILE_SP;

   public GetUserProfileSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_USER_PROFILE_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("PC_USER_INFO", OracleTypes.CURSOR, new UserInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_COUNTRY_LIST", OracleTypes.CURSOR, new CountryListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_YOE_LIST", OracleTypes.CURSOR, new YearOfEntryListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_QUALIFICATION", OracleTypes.CURSOR, new QualificationListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_PRE_QUAL", OracleTypes.CURSOR, new PreviousQualListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", userProfilePayloadFromClient.getUserId());
	inMap.put("P_APP_VERSION", userProfilePayloadFromClient.getAppVersion());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_USER_PROFILE_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
