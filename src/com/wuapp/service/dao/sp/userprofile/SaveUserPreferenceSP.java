package com.wuapp.service.dao.sp.userprofile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class SaveUserPreferenceSP extends StoredProcedure {

   private String SAVE_USER_PREFERENCE_SP = ProcedureNames.SAVE_USER_PREFERENCE_SP;

   public SaveUserPreferenceSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SAVE_USER_PREFERENCE_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_receive_no_email_flag", Types.VARCHAR));
	declareParameter(new SqlParameter("p_permit_email", Types.VARCHAR));
	declareParameter(new SqlParameter("p_solus_email", Types.VARCHAR));
	declareParameter(new SqlParameter("p_marketing_email", Types.VARCHAR));
	declareParameter(new SqlParameter("p_survey_email", Types.VARCHAR));
	declareParameter(new SqlParameter("p_privacy_flag", Types.VARCHAR));
	declareParameter(new SqlParameter("p_ca_track_flag", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("p_success_msg", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", userProfilePayloadFromClient.getUserId());
	inMap.put("p_receive_no_email_flag", userProfilePayloadFromClient.getReceiveNoEmailFlag());
	inMap.put("p_permit_email", userProfilePayloadFromClient.getPermitEmail());
	inMap.put("p_solus_email", userProfilePayloadFromClient.getSolusEmail());
	inMap.put("p_marketing_email", userProfilePayloadFromClient.getMarketingEmail());
	inMap.put("p_survey_email", userProfilePayloadFromClient.getSurveyEmail());
	inMap.put("p_privacy_flag", userProfilePayloadFromClient.getPrivacyFlag());
	inMap.put("p_ca_track_flag", userProfilePayloadFromClient.getCaTrackFalg());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SAVE_USER_PREFERENCE_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
