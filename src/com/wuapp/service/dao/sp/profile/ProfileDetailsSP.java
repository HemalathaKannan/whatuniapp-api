package com.wuapp.service.dao.sp.profile;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.AccomodationDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.CompetitorDetailRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.EnquiryDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.KeyStatsInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.LoadYearListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.OpendayDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.ReviewStarRatingRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.StudentReviewRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.UniInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.UniProfileContentRowmapperImpl;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * @Class : ProfileDetailsSP.java
 * @Description : This class is used to get the profile details...
 * @version : 1.0
 * @since : 01_Nov_2017
 * @author :
 * @Modify : 01_Nov_2017 - Added P_OD_BUTTON_FLAG param for showing openday button in IP page by Prabha
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param 
 */
public class ProfileDetailsSP extends StoredProcedure {

   private String UNI_PROFILE_SP = ProcedureNames.UNI_PROFILE_SP;

   public ProfileDetailsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(UNI_PROFILE_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_CODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_KEYWORD_SEARCH", Types.VARCHAR));
	declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ORDER_BY", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCATION_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION_FLAG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL_GRADE", Types.VARCHAR));
	declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_one", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_two", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_three", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("PC_UNI_INFO", OracleTypes.CURSOR, new UniInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_UNI_PROFILE_CONTENT", OracleTypes.CURSOR, new UniProfileContentRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_KEY_STATS_INFO", OracleTypes.CURSOR, new KeyStatsInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_REVIEW_STAR_RATING", OracleTypes.CURSOR, new ReviewStarRatingRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_STUDENT_REVIEW", OracleTypes.CURSOR, new StudentReviewRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OPENDAYS_DETAILS", OracleTypes.CURSOR, new OpendayDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ENQUIRY_DETAILS", OracleTypes.CURSOR, new EnquiryDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ACCOMODATION_DET", OracleTypes.CURSOR, new AccomodationDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_COMPETITOR_DETAIL", OracleTypes.CURSOR, new CompetitorDetailRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_ADV_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_COLLEGE_NAME", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_OD_BUTTON_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_LOAD_YEAR_LIST", OracleTypes.CURSOR, new LoadYearListRowmapperImpl()));
	declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR));
	//Added mapbox key by Hemalatha K on 06_MAY_2020_REL
	declareParameter(new SqlOutParameter("P_MAP_BOX_KEY", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(ProfileDetailsPayloadFromClient profileDetailsPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", profileDetailsPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", profileDetailsPayloadFromClient.getCollegeId());
	inMap.put("P_USER_IP", profileDetailsPayloadFromClient.getUserIp());
	inMap.put("P_USER_AGENT", profileDetailsPayloadFromClient.getUserAgent());
	inMap.put("P_APP_VERSION", profileDetailsPayloadFromClient.getAppVersion());
	inMap.put("P_SEARCH_CAT_CODE", profileDetailsPayloadFromClient.getSearchCategoryCode());
	inMap.put("P_SEARCH_CAT_ID", profileDetailsPayloadFromClient.getSearchCategoryId());
	inMap.put("P_KEYWORD_SEARCH", profileDetailsPayloadFromClient.getKeywordSearch());
	inMap.put("P_QUALIFICATION", profileDetailsPayloadFromClient.getQualification());
	inMap.put("P_ORDER_BY", profileDetailsPayloadFromClient.getOrderBy());
	inMap.put("P_LOCATION_TYPE", profileDetailsPayloadFromClient.getLocationType());
	inMap.put("P_REGION", profileDetailsPayloadFromClient.getRegion());
	inMap.put("P_REGION_FLAG", profileDetailsPayloadFromClient.getRegionFlag());
	inMap.put("P_STUDY_MODE", profileDetailsPayloadFromClient.getStudyMode());
	inMap.put("P_PREV_QUAL", profileDetailsPayloadFromClient.getPreviousQual());
	inMap.put("P_PREV_QUAL_GRADE", profileDetailsPayloadFromClient.getPreviousQualGrade());
	inMap.put("p_assessment_type", profileDetailsPayloadFromClient.getAssessmentType());
	inMap.put("p_review_subject_one", profileDetailsPayloadFromClient.getReviewSubjectOne());
	inMap.put("p_review_subject_two", profileDetailsPayloadFromClient.getReviewSubjectTwo());
	inMap.put("p_review_subject_three", profileDetailsPayloadFromClient.getReviewSubjectThree());
	inMap.put("p_jacs_code", profileDetailsPayloadFromClient.getJacsCode());
	//			
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (UNI_PROFILE_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
