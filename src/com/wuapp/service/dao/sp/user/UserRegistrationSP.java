package com.wuapp.service.dao.sp.user;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.0
 * Class : UserRegistrationSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K        1.1           31-Mar-2020     Added lat and long in param
 * Hemalatha.K        2.0           25-JUN-2020     Removed clearing related param and added out param for clearing switch 
 */

public class UserRegistrationSP extends StoredProcedure {

   private static final String USER_REGISTRATION_SP = ProcedureNames.USER_REGISTRATION_SP;

   //
   public UserRegistrationSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(USER_REGISTRATION_SP);
	// 
	declareParameter(new SqlParameter("P_FIRSTNAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LASTNAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_EMAIL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PASSWORD", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_AGE_RANGE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCALE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_FRIEND_LIST", Types.VARCHAR));
	declareParameter(new SqlParameter("P_FB_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_ERROR_MSG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_CL_INTERSTITIAL_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_CLEARING_URL", Types.VARCHAR));
	//	
	compile();
   }

   public Map<String, Object> executeProcedure(UserPayloadFromClient userPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_FIRSTNAME", userPayloadFromClient.getFirstName());
	inMap.put("P_LASTNAME", userPayloadFromClient.getLastName());
	inMap.put("P_EMAIL", userPayloadFromClient.getEmail());
	inMap.put("P_PASSWORD", userPayloadFromClient.getPassword());
	inMap.put("P_USER_IP", userPayloadFromClient.getUserIp());
	inMap.put("P_USER_AGENT", userPayloadFromClient.getUserAgent());
	inMap.put("P_AGE_RANGE", userPayloadFromClient.getAgeRange());
	inMap.put("P_LOCALE", userPayloadFromClient.getLocale());
	inMap.put("P_USER_FRIEND_LIST", userPayloadFromClient.getUserFriendList());
	inMap.put("P_FB_USER_ID", userPayloadFromClient.getFbUserId());
	inMap.put("P_APP_VERSION", userPayloadFromClient.getAppVersion());
	inMap.put("P_LATITUDE", userPayloadFromClient.getLatitude());
	inMap.put("P_LONGITUDE", userPayloadFromClient.getLongitude());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (USER_REGISTRATION_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
