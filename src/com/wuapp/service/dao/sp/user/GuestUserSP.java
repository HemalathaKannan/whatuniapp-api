package com.wuapp.service.dao.sp.user;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : GuestUserSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param and added out param for clearing switch 
 */

public class GuestUserSP extends StoredProcedure {
   
   private static final String GET_GUEST_USER_ID_SP = ProcedureNames.GET_GUEST_USER_ID_SP;
   
   //
   public GuestUserSP(DataSource ds) {
	
	//
	setDataSource(ds);
	setSql(GET_GUEST_USER_ID_SP);
	//
	declareParameter(new SqlOutParameter("P_GUEST_USER_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_CL_INTERSTITIAL_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_CLEARING_URL", Types.VARCHAR));
	//	
	compile();
   }
   
   public Map<String, Object> executeProcedure() {
     //
	HashMap<String, String> inMap = new HashMap<String, String>();
      //
	Map<String, Object> outMap = execute(inMap);
	//
	return outMap;
   }
}
