package com.wuapp.service.dao.sp.user;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class CheckUserExistedSP extends StoredProcedure {

   private static final String CHECK_USER_EXISTED_SP = ProcedureNames.CHECK_USER_EXISTED_SP;

   //
   public CheckUserExistedSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(CHECK_USER_EXISTED_SP);
	//   	
	declareParameter(new SqlParameter("P_FIRSTNAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LASTNAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_EMAIL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PASSWORD", Types.VARCHAR));
	declareParameter(new SqlParameter("P_AGE_RANGE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCALE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_FRIEND_LIST", Types.VARCHAR));
	declareParameter(new SqlParameter("P_FB_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_USER_PASSWORD", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_ERROR_MSG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_USER_EXIST_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	//	
	compile();
   }

   public Map<String, Object> executeProcedure(UserPayloadFromClient userPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_FIRSTNAME", userPayloadFromClient.getFirstName());
	inMap.put("P_LASTNAME", userPayloadFromClient.getLastName());
	inMap.put("P_EMAIL", userPayloadFromClient.getEmail());
	inMap.put("P_PASSWORD", userPayloadFromClient.getPassword());
	inMap.put("P_AGE_RANGE", userPayloadFromClient.getAgeRange());
	inMap.put("P_LOCALE", userPayloadFromClient.getLocale());
	inMap.put("P_USER_FRIEND_LIST", userPayloadFromClient.getUserFriendList());
	inMap.put("P_FB_USER_ID", userPayloadFromClient.getFbUserId());
	inMap.put("P_APP_VERSION", userPayloadFromClient.getAppVersion());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (CHECK_USER_EXISTED_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
