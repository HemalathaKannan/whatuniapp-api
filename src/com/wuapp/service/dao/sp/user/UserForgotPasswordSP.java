package com.wuapp.service.dao.sp.user;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class UserForgotPasswordSP extends StoredProcedure {

   private static final String USER_FORGOT_PASSWORD_SP = ProcedureNames.USER_FORGOT_PASSWORD_SP;

   //
   public UserForgotPasswordSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(USER_FORGOT_PASSWORD_SP);
	//   	
	declareParameter(new SqlParameter("P_EMAIL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("P_RETURN_CODE", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	//	
	compile();
   }

   public Map<String, Object> executeProcedure(UserPayloadFromClient userPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_EMAIL", userPayloadFromClient.getEmail());
	inMap.put("P_APP_VERSION", userPayloadFromClient.getAppVersion());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (USER_FORGOT_PASSWORD_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
