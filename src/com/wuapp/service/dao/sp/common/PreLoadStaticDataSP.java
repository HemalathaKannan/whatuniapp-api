package com.wuapp.service.dao.sp.common;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AccessTokenRowMapperImpl;
import com.wuapp.util.developer.Debugger;

/**
 * Get sys_var of environment (FO/LIVE/TEST/DEV)
 *
 * @author Sabapathi
 * @since whatuniapp1.0_20170315 - initial draft
 * @version 1.1
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	         Modified On     	  Modification Details 	                
 * *************************************************************************************************************************
 * Hemalatha.K            2.0                25-JUN-2020          Removed clearing related param 
 */

public class PreLoadStaticDataSP extends StoredProcedure {

   private String SP_NAME = ProcedureNames.GET_STATIC_DATA_SP;

   public PreLoadStaticDataSP(DataSource dataSource) {
	setDataSource(dataSource);
	setSql(SP_NAME);
	declareParameter(new SqlParameter("P_NAME", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_ACCESS_TOKEN", OracleTypes.CURSOR, new AccessTokenRowMapperImpl()));
	declareParameter(new SqlOutParameter("P_VALUE", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(HashMap<String, String> parameters) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	Map<String, Object> outMap = null;
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_NAME", parameters.get("NAME"));
	//
	outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SP_NAME), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
