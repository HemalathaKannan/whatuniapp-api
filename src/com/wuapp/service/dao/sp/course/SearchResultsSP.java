package com.wuapp.service.dao.sp.course;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.SearchResultsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.PreviousQualificationRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersAssesmentTypeRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersLocationRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersLocationTypeRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersQualificationRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersReviewCategoryRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersStudyModeRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersSubjectRowmapperImpl;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : SearchResultsSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param 
 */

public class SearchResultsSP extends StoredProcedure {

   private String SEARCH_RESULTS_SP = ProcedureNames.COURSE_SEARCH_RESULTS_SP;

   public SearchResultsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SEARCH_RESULTS_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_CODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_FROM_NO", Types.NUMERIC));
	declareParameter(new SqlParameter("P_TO_NO", Types.NUMERIC));
	declareParameter(new SqlParameter("P_KEYWORD_SEARCH", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_URL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ORDER_BY", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LAT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCATION_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION_FLAG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL_GRADE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ASSESSMENT_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_one", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_two", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_three", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("PC_SEARCH_RESULT", OracleTypes.CURSOR, new SearchResultsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_SUBJECT_TITLE", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_TILE_MEDIA_PATH", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_TILE_MEDIA_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_ICON_MEDIA_PATH", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_TOTAL_COURSE_COUNT", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_QUAL_FILTERS", OracleTypes.CURSOR, new SearchFiltersQualificationRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_SUBJECT_FILTERS", OracleTypes.CURSOR, new SearchFiltersSubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_LOCATION_FILTERS", OracleTypes.CURSOR, new SearchFiltersLocationRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_LOCATION_TYPES_FILTER", OracleTypes.CURSOR, new SearchFiltersLocationTypeRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_STUDY_MODE_FILTER", OracleTypes.CURSOR, new SearchFiltersStudyModeRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_PREV_QUAL_FILTER", OracleTypes.CURSOR, new PreviousQualificationRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ASSESSMENT_TYPE_FILTER", OracleTypes.CURSOR, new SearchFiltersAssesmentTypeRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_REVIEW_CATEGORY_FILTER", OracleTypes.CURSOR, new SearchFiltersReviewCategoryRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_TOTAL_COLLEGE_COUNT", Types.VARCHAR));
	//
	declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(SearchResultsPayloadFromClient searchResultsPayFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", searchResultsPayFromClient.getUserId());
	inMap.put("P_SEARCH_CAT_CODE", searchResultsPayFromClient.getCategoryCode());
	inMap.put("P_SEARCH_CAT_ID", searchResultsPayFromClient.getCategoryId());
	inMap.put("P_QUALIFICATION", searchResultsPayFromClient.getQualification());
	inMap.put("P_FROM_NO", searchResultsPayFromClient.getFromPageNo());
	inMap.put("P_TO_NO", searchResultsPayFromClient.getToPageNo());
	inMap.put("P_KEYWORD_SEARCH", searchResultsPayFromClient.getKeywordSearch());
	inMap.put("P_APP_VERSION", searchResultsPayFromClient.getAppVersion());
	inMap.put("P_USER_AGENT", searchResultsPayFromClient.getUserAgent());
	inMap.put("P_SEARCH_URL", searchResultsPayFromClient.getSearchUrl());
	inMap.put("P_ORDER_BY", searchResultsPayFromClient.getOrderBy());
	inMap.put("P_LAT", searchResultsPayFromClient.getLatitude());
	inMap.put("P_LONG", searchResultsPayFromClient.getLongtitude());
	inMap.put("P_LOCATION_TYPE", searchResultsPayFromClient.getLocationType());
	inMap.put("P_REGION", searchResultsPayFromClient.getRegion());
	inMap.put("P_REGION_FLAG", searchResultsPayFromClient.getRegionFlag());
	inMap.put("P_STUDY_MODE", searchResultsPayFromClient.getStudyMode());
	inMap.put("P_PREV_QUAL", searchResultsPayFromClient.getPreviousQualification());
	inMap.put("P_PREV_QUAL_GRADE", searchResultsPayFromClient.getPreviousQualificationGrade());
	inMap.put("P_ASSESSMENT_TYPE", searchResultsPayFromClient.getAssessmentType());
	inMap.put("p_review_subject_one", searchResultsPayFromClient.getReviewSubjectOne());
	inMap.put("p_review_subject_two", searchResultsPayFromClient.getReviewSubjectTwo());
	inMap.put("p_review_subject_three", searchResultsPayFromClient.getReviewSubjectThree());
	inMap.put("p_jacs_code", searchResultsPayFromClient.getJacsCode());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SEARCH_RESULTS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
