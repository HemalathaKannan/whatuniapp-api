package com.wuapp.service.dao.sp.course;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : GetCourseCountSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param
 */

public class GetCourseCountSP extends StoredProcedure {

   private String GET_COURSE_COUNT_SP = ProcedureNames.GET_COURSE_COUNT_SP;

   public GetCourseCountSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_COURSE_COUNT_SP);
	//	
	declareParameter(new SqlParameter("p_institution_id", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_CODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_KEYWORD_SEARCH", Types.VARCHAR));
	declareParameter(new SqlParameter("p_prev_qual", Types.VARCHAR));
	declareParameter(new SqlParameter("p_prev_qual_grade", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("p_course_count", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(SearchResultsPayloadFromClient searchResultsPayFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_institution_id", searchResultsPayFromClient.getInstitutionId());
	inMap.put("P_SEARCH_CAT_CODE", searchResultsPayFromClient.getCategoryCode());
	inMap.put("P_SEARCH_CAT_ID", searchResultsPayFromClient.getCategoryId());
	inMap.put("P_QUALIFICATION", searchResultsPayFromClient.getQualification());
	inMap.put("P_KEYWORD_SEARCH", searchResultsPayFromClient.getKeywordSearch());
	inMap.put("p_prev_qual", searchResultsPayFromClient.getPreviousQualification());
	inMap.put("p_prev_qual_grade", searchResultsPayFromClient.getPreviousQualificationGrade());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_COURSE_COUNT_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
