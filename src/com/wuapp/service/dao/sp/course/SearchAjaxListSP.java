package com.wuapp.service.dao.sp.course;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.SearchInstitutionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.SearchSubjectRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.SubjectRowmapperImpl;
import com.wuapp.service.pojo.json.course.SearchAjaxPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : SearchAjaxListSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param
 */

public class SearchAjaxListSP extends StoredProcedure {

   private String SEARCH_AJAX_SP = ProcedureNames.SEARCH_AJAX_SP;

   public SearchAjaxListSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SEARCH_AJAX_SP);
	//
	declareParameter(new SqlParameter("P_LAT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_KEYWORD_TEXT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_QUALIFICATION_CODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("PC_SEARCH_SUBJECT", OracleTypes.CURSOR, new SubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_COURSE_SUGGESTION_LIST", OracleTypes.CURSOR, new SearchSubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_SEARCH_INSTITUTIONS", OracleTypes.CURSOR, new SearchInstitutionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_INST_SUGGESTION_LIST", OracleTypes.CURSOR, new SearchInstitutionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	//	
	compile();
   }

   public Map<String, Object> executeProcedure(SearchAjaxPayloadFromClient searchAjaxPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_LAT", searchAjaxPayloadFromClient.getLatitude());
	inMap.put("P_LONG", searchAjaxPayloadFromClient.getLongitude());
	inMap.put("P_USER_ID", searchAjaxPayloadFromClient.getUserId());
	inMap.put("P_KEYWORD_TEXT", searchAjaxPayloadFromClient.getSearchKeyword());
	inMap.put("P_QUALIFICATION_CODE", searchAjaxPayloadFromClient.getQualificationCode());
	inMap.put("P_APP_VERSION", searchAjaxPayloadFromClient.getAppVersion());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SEARCH_AJAX_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
