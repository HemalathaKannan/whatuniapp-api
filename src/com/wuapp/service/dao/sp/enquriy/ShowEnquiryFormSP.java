package com.wuapp.service.dao.sp.enquriy;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.enquiry.InstitutionsListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.enquiry.UserAddressRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.enquiry.YearListRowmapperImpl;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log :
 * ***************************************************************************************************************************
 * Author          version      Modified On     Modification Details                        Change
 * ***************************************************************************************************************************
 * Hemalatha.K       1.1        17_DEC_19           Modified               Getting consent flag and text for client lead consent
 */
 
public class ShowEnquiryFormSP extends StoredProcedure {

   private String ENQUIRY_FORM_SP = ProcedureNames.ENQUIRY_FORM_SP;

   //
   public ShowEnquiryFormSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(ENQUIRY_FORM_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ENQUIRY_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_COLLEGE_INFO", OracleTypes.CURSOR, new InstitutionsListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_USER_ADDRESS", OracleTypes.CURSOR, new UserAddressRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_YEAR_LIST", OracleTypes.CURSOR, new YearListRowmapperImpl()));
	// 
	declareParameter(new SqlOutParameter("P_CLIENT_CONSENT_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_CLIENT_CONSENT_TEXT", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", enquiryFormPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", enquiryFormPayloadFromClient.getInstitutionId());
	inMap.put("P_COURSE_ID", enquiryFormPayloadFromClient.getCourseId());
	inMap.put("P_ENQUIRY_TYPE", enquiryFormPayloadFromClient.getEnquiryType());
	inMap.put("P_APP_VERSION", enquiryFormPayloadFromClient.getAppVersion());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (ENQUIRY_FORM_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
