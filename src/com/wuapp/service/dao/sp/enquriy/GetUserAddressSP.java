package com.wuapp.service.dao.sp.enquriy;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.enquiry.AjaxAddressListRowmapperImpl;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * @see
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class GetUserAddressSP extends StoredProcedure {

   //
   private String GET_AJAX_ADDRESS_SP = ProcedureNames.GET_AJAX_ADDRESS_SP;

   //
   public GetUserAddressSP(DataSource ds) {
	setDataSource(ds);
	setSql(GET_AJAX_ADDRESS_SP);
	//
	declareParameter(new SqlParameter("P_POSTCODE", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("PC_GET_ADDRESS", OracleTypes.CURSOR, new AjaxAddressListRowmapperImpl()));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//	
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_POSTCODE", enquiryFormPayloadFromClient.getPostCode());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_AJAX_ADDRESS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
