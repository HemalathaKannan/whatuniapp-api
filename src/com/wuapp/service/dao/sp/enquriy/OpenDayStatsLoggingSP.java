package com.wuapp.service.dao.sp.enquriy;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;
/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : OpenDayStatsLoggingSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On        Modification Details Change
 * *************************************************************************************************************
 * 			                  		      	Initial draft
 * Sangeeth.S      1.0              09-oct-2018        Added study mode in param 
 * Hemalatha K     1.1              31_MAR_2020       Added lat and long in param
 */
public class OpenDayStatsLoggingSP extends StoredProcedure {

   private String OPEN_DAYS_STATS_LOGGING_SP = ProcedureNames.OPEN_DAYS_STATS_LOGGING_SP;

   public OpenDayStatsLoggingSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(OPEN_DAYS_STATS_LOGGING_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_JS_LOG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SUBORDER_ITEM_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PROFILE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_TYPE_NAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ACTIVITY", Types.VARCHAR));
	declareParameter(new SqlParameter("P_EXTRA_TEXT", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_LOG_STATUS_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));//Added in param for the oct_23_18 rel
	declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", enquiryFormPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", enquiryFormPayloadFromClient.getInstitutionId());
	inMap.put("P_COURSE_ID", enquiryFormPayloadFromClient.getCourseId());
	inMap.put("P_USER_IP", enquiryFormPayloadFromClient.getUserIP());
	inMap.put("P_USER_AGENT", enquiryFormPayloadFromClient.getUserAgent());
	inMap.put("P_JS_LOG", enquiryFormPayloadFromClient.getJsLog());
	inMap.put("P_SUBORDER_ITEM_ID", enquiryFormPayloadFromClient.getSuborderItemId());
	inMap.put("P_PROFILE_ID", enquiryFormPayloadFromClient.getProfileId());
	inMap.put("P_TYPE_NAME", enquiryFormPayloadFromClient.getTypeName());
	inMap.put("P_ACTIVITY", enquiryFormPayloadFromClient.getActivity());
	inMap.put("P_EXTRA_TEXT", enquiryFormPayloadFromClient.getExtraText());
	inMap.put("P_STUDY_MODE", enquiryFormPayloadFromClient.getStudyMode());
	inMap.put("P_LATITUDE", enquiryFormPayloadFromClient.getLatitude());
	inMap.put("P_LONGITUDE", enquiryFormPayloadFromClient.getLongitude());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (OPEN_DAYS_STATS_LOGGING_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
