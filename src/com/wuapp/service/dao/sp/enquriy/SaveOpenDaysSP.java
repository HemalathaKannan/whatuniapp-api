package com.wuapp.service.dao.sp.enquriy;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class SaveOpenDaysSP extends StoredProcedure {

   private String SAVE_OPEN_DAYS_SP = ProcedureNames.SAVE_OPEN_DAYS_SP;

   public SaveOpenDaysSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SAVE_OPEN_DAYS_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_OPEN_DATE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_EVENT_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_RETURN_FLAG", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", enquiryFormPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", enquiryFormPayloadFromClient.getInstitutionId());
	inMap.put("P_OPEN_DATE", enquiryFormPayloadFromClient.getOpenDate());
	inMap.put("P_EVENT_ID", enquiryFormPayloadFromClient.getEventId());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SAVE_OPEN_DAYS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
