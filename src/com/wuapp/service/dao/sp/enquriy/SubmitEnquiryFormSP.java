package com.wuapp.service.dao.sp.enquriy;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.enquiry.InstitutionsListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.enquiry.OtherCourseListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.enquiry.OtherInstitutionsListRowmapperImpl;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log :
 * ***************************************************************************************************************************
 * Author          version      Modified On     Modification Details                        Change
 * ***************************************************************************************************************************
 * Hemalatha.K       1.1        17_DEC_19           Modified                  Added parameter for client lead consent
 * Hema.S            1.2        31_MAR_20           Modified                  Added lat and long in paramaters
 */
 
public class SubmitEnquiryFormSP extends StoredProcedure {

   //
   private String SUBMIT_ENQUIRY_FORM_SP = ProcedureNames.SUBMIT_ENQUIRY_FORM_SP;

   //
   public SubmitEnquiryFormSP(DataSource ds) {
	setDataSource(ds);
	setSql(SUBMIT_ENQUIRY_FORM_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_YEAR_OF_ENTRY", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ENQUIRY_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_MESSAGE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ADDRESS_LINE_ONE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ADDRESS_LINE_TWO", Types.VARCHAR));
	declareParameter(new SqlParameter("P_CITY", Types.VARCHAR));
	//declareParameter(new SqlParameter("P_COUNTY_STATE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_POSTCODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SUBORDER_ITEM_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_CODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SEARCH_CAT_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_KEYWORD_SEARCH", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCATION_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION_FLAG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL_GRADE", Types.VARCHAR));
	declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_one", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_two", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_three", Types.VARCHAR));
	//
	//declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_COLLEGE_INFO", OracleTypes.CURSOR, new InstitutionsListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OTHER_UNI_POD", OracleTypes.CURSOR, new OtherInstitutionsListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OTHER_COURSE_POD", OracleTypes.CURSOR, new OtherCourseListRowmapperImpl()));
	// Added for client lead consent by Hemalatha.K on 17_DEC_19_REL
	declareParameter(new SqlParameter("P_CLIENT_CONSENT_FLAG", Types.VARCHAR));
	//
	declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", enquiryFormPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", enquiryFormPayloadFromClient.getInstitutionId());
	inMap.put("P_COURSE_ID", enquiryFormPayloadFromClient.getCourseId());
	inMap.put("P_YEAR_OF_ENTRY", enquiryFormPayloadFromClient.getYearOfEntry());
	inMap.put("P_USER_IP", enquiryFormPayloadFromClient.getUserIP());
	inMap.put("P_USER_AGENT", enquiryFormPayloadFromClient.getUserAgent());
	inMap.put("P_ENQUIRY_TYPE", enquiryFormPayloadFromClient.getEnquiryType());
	inMap.put("P_MESSAGE", enquiryFormPayloadFromClient.getMessage());
	inMap.put("P_ADDRESS_LINE_ONE", enquiryFormPayloadFromClient.getAddressLineOne());
	inMap.put("P_ADDRESS_LINE_TWO", enquiryFormPayloadFromClient.getAddressLineTwo());
	inMap.put("P_CITY", enquiryFormPayloadFromClient.getCity());
	//inMap.put("P_COUNTY_STATE", enquiryFormPayloadFromClient.getCountyState());
	inMap.put("P_POSTCODE", enquiryFormPayloadFromClient.getPostCode());
	inMap.put("P_SUBORDER_ITEM_ID", enquiryFormPayloadFromClient.getSuborderItemId());
	inMap.put("P_SEARCH_CAT_CODE", enquiryFormPayloadFromClient.getCategoryCode());
	inMap.put("P_SEARCH_CAT_ID", enquiryFormPayloadFromClient.getCategoryId());
	inMap.put("P_QUALIFICATION", enquiryFormPayloadFromClient.getQualification());
	inMap.put("P_KEYWORD_SEARCH", enquiryFormPayloadFromClient.getSearchKeyword());
	inMap.put("P_LOCATION_TYPE", enquiryFormPayloadFromClient.getLocationType());
	inMap.put("P_REGION", enquiryFormPayloadFromClient.getRegion());
	inMap.put("P_REGION_FLAG", enquiryFormPayloadFromClient.getRegionFlag());
	inMap.put("P_STUDY_MODE", enquiryFormPayloadFromClient.getStudyMode());
	inMap.put("P_PREV_QUAL", enquiryFormPayloadFromClient.getPreviousQualification());
	inMap.put("P_PREV_QUAL_GRADE", enquiryFormPayloadFromClient.getPreviousQualificationGrade());
	inMap.put("p_assessment_type", enquiryFormPayloadFromClient.getAssessmentType());
	inMap.put("p_review_subject_one", enquiryFormPayloadFromClient.getReviewSubjectOne());
	inMap.put("p_review_subject_two", enquiryFormPayloadFromClient.getReviewSubjectTwo());
	inMap.put("p_review_subject_three", enquiryFormPayloadFromClient.getReviewSubjectThree());
	//
	inMap.put("P_CLIENT_CONSENT_FLAG", enquiryFormPayloadFromClient.getClientConsentFlag()); 
	//
	inMap.put("P_LATITUDE", enquiryFormPayloadFromClient.getLatitude());
	inMap.put("P_LONGITUDE", enquiryFormPayloadFromClient.getLongitude());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SUBMIT_ENQUIRY_FORM_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
