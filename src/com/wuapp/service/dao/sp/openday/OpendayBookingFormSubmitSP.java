package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.EventDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OtherOdPodRowMapperImpl;
import com.wuapp.service.pojo.json.openday.OpendayBookingFormSubmitFromClient;
import com.wuapp.util.developer.Debugger;
import oracle.jdbc.OracleTypes;

/**
 * Class : OpendayBookingFormSubmitSP.java
 * @Description : This class is used to submit the opendays booking form
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hema S
 */

public class OpendayBookingFormSubmitSP extends StoredProcedure {
   
  private String OPENDAYS_BOOKING_FORM_SUBMIT_SP = ProcedureNames.OPENDAYS_BOOKING_FORM_SUBMIT;
   
  public OpendayBookingFormSubmitSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(OPENDAYS_BOOKING_FORM_SUBMIT_SP);
    declareParameter(new SqlParameter("P_EVENT_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_FORENAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SURNAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_EMAIL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PASSWORD", Types.VARCHAR));
    declareParameter(new SqlParameter("P_MOBILE_NUMBER", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));   
    declareParameter(new SqlParameter("P_PEOPLE_ATTEND_COUNT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_STUDY_LEVEL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_INTERESTED_SUBJECT_STR", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COURSE_START_MONTH", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COURSE_START_YEAR", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SPECIAL_REQ_FLAG", Types.VARCHAR)); 
    declareParameter(new SqlParameter("P_SPECIAL_REQ_MESSAGE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_MARKETING_EMAIL_FLAG", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SOLUS_EMAIL_FLAG", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SURVEY_EMAIL_FLAG", Types.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_CONSENT_FLAG", Types.VARCHAR));
    declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
    declareParameter(new SqlInOutParameter("P_SESSION_ID", Types.VARCHAR));
    declareParameter(new SqlInOutParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_EVENT_DETAILS", OracleTypes.CURSOR, new EventDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_OTHER_OD_POD", OracleTypes.CURSOR, new OtherOdPodRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_FB_OD_SUCCESS_MSG", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_TWITTER_OD_SUCCESS_MSG", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_EMAIL_EXIST_FLAG", Types.VARCHAR));
    compile();
  }
  
  public Map<String, Object> executeProcedure(OpendayBookingFormSubmitFromClient opendayBookingFormSubmitFromClient) {
    Long startExecuteTime = new Long(System.currentTimeMillis());
    //
    HashMap<String, String> inMap = new HashMap<String, String>();
    inMap.put("P_EVENT_ID", opendayBookingFormSubmitFromClient.getEventId());
    inMap.put("P_COLLEGE_ID", opendayBookingFormSubmitFromClient.getCollegeId());
    inMap.put("P_FORENAME", opendayBookingFormSubmitFromClient.getForeName());
    inMap.put("P_SURNAME", opendayBookingFormSubmitFromClient.getSurName());
    inMap.put("P_USER_EMAIL", opendayBookingFormSubmitFromClient.getUserEmail());
    inMap.put("P_PASSWORD", opendayBookingFormSubmitFromClient.getPassword());
    inMap.put("P_MOBILE_NUMBER", opendayBookingFormSubmitFromClient.getMobileNumber());
    inMap.put("P_USER_AGENT", opendayBookingFormSubmitFromClient.getUserAgent());
    inMap.put("P_USER_IP", opendayBookingFormSubmitFromClient.getUserIp());
    inMap.put("P_PEOPLE_ATTEND_COUNT", opendayBookingFormSubmitFromClient.getPeopleAttendCount());
    inMap.put("P_STUDY_LEVEL", opendayBookingFormSubmitFromClient.getStudyingLevel());
    inMap.put("P_STUDY_MODE", opendayBookingFormSubmitFromClient.getStudyMode());
    inMap.put("P_INTERESTED_SUBJECT_STR", opendayBookingFormSubmitFromClient.getInterestedSubjectArray());
    inMap.put("P_COURSE_START_MONTH", opendayBookingFormSubmitFromClient.getCourseStartMonth());
    inMap.put("P_COURSE_START_YEAR", opendayBookingFormSubmitFromClient.getCourseStartYear());
    inMap.put("P_SPECIAL_REQ_FLAG", opendayBookingFormSubmitFromClient.getSpecialRegFlag());
    inMap.put("P_SPECIAL_REQ_MESSAGE", opendayBookingFormSubmitFromClient.getSpecialRegMessage());
    inMap.put("P_MARKETING_EMAIL_FLAG", opendayBookingFormSubmitFromClient.getMarketingEmailFlag());
    inMap.put("P_SOLUS_EMAIL_FLAG", opendayBookingFormSubmitFromClient.getSolusEmailFlag());
    inMap.put("P_SURVEY_EMAIL_FLAG", opendayBookingFormSubmitFromClient.getSurveyEmailFlag());
    inMap.put("P_CLIENT_CONSENT_FLAG", opendayBookingFormSubmitFromClient.getClientConsentFlag());
    inMap.put("P_LATITUDE", opendayBookingFormSubmitFromClient.getLatitude());
    inMap.put("P_LONGITUDE", opendayBookingFormSubmitFromClient.getLongitude());
    inMap.put("P_SESSION_ID", opendayBookingFormSubmitFromClient.getSessionId());
    inMap.put("P_USER_ID", opendayBookingFormSubmitFromClient.getUserId());
    Map<String, Object> outMap = execute(inMap);
    //
    if (Debugger.getInstance().isDebugDbDetails()) {
      Debugger.getInstance().printDbDetails((this.getClass().toString()), (OPENDAYS_BOOKING_FORM_SUBMIT_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
    }
    return outMap;
  }
}
