package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.ClientConsentDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OpenDaysDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.ProviderDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.QualificationDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.StudyModeDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.UserDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.YearOfEntryRowMapperImpl;
import com.wuapp.service.pojo.json.openday.OpendayShowBookingFormFromClient;
import com.wuapp.util.developer.Debugger;
import oracle.jdbc.OracleTypes;

/**
 * Class : OpendaysShowBookingFormSP.java
 * @Description : This class is used to show the opendays booking form
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hema S
 */

public class OpendaysShowBookingFormSP extends StoredProcedure {
  
  private String OPENDAYS_SHOW_BOOKING_FORM_SP = ProcedureNames.OPENDAYS_SHOW_BOOKING_FORM;
  
  public OpendaysShowBookingFormSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(OPENDAYS_SHOW_BOOKING_FORM_SP);
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_EVENT_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUBORDER_ITEM_ID", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_PROVIDER_DETAILS", OracleTypes.CURSOR, new ProviderDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_OPEN_DAY_DETAILS", OracleTypes.CURSOR, new OpenDaysDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_DETAILS", OracleTypes.CURSOR, new UserDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_QUALIFICATION_DETAILS", OracleTypes.CURSOR, new QualificationDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_STUDY_MODE_DETAILS", OracleTypes.CURSOR, new StudyModeDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_CLIENT_CONSENT_DETAILS", OracleTypes.CURSOR, new ClientConsentDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_OD_MAX_PEOPLE_COUNT", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_YOE_LIST", OracleTypes.CURSOR, new YearOfEntryRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_USER_BOOKED", Types.VARCHAR));
    compile();
  }
  
  public Map<String, Object> executeProcedure(OpendayShowBookingFormFromClient opendayShowBookingFormFromClient) {
    Long startExecuteTime = new Long(System.currentTimeMillis());
    //
    HashMap<String, String> inMap = new HashMap<String, String>();
    inMap.put("P_USER_ID", opendayShowBookingFormFromClient.getUserId());
    inMap.put("P_COLLEGE_ID", opendayShowBookingFormFromClient.getCollegeId());
    inMap.put("P_EVENT_ID", opendayShowBookingFormFromClient.getEventId());
    inMap.put("P_SUBORDER_ITEM_ID", opendayShowBookingFormFromClient.getSuborderItemId());
    Map<String, Object> outMap = execute(inMap);
    //
    if (Debugger.getInstance().isDebugDbDetails()) {
      Debugger.getInstance().printDbDetails((this.getClass().toString()), (OPENDAYS_SHOW_BOOKING_FORM_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
    }
    return outMap;
  }
}
