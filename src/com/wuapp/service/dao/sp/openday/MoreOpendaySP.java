package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.OpendayDetailsRowmapperImpl;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadFromClient;
import com.wuapp.util.developer.Debugger;
import oracle.jdbc.OracleTypes;

/**
 * Class : MoreOpendaySP.java
 * @Description : This class is used to get openday list
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hemalatha K
 */

public class MoreOpendaySP extends StoredProcedure {
  
  private String GET_MORE_OPENDAY_LIST_SP = ProcedureNames.GET_MORE_OPENDAY_LIST_SP;
   
  public MoreOpendaySP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(GET_MORE_OPENDAY_LIST_SP);
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_DISPLAY_CNT", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_UNI_OPENDAYS_LIST", OracleTypes.CURSOR, new OpendayDetailsRowmapperImpl()));    
    declareParameter(new SqlParameter("P_BOOKING_FORM_FLAG", Types.VARCHAR));
    compile();
  }
  
  public Map<String, Object> executeProcedure(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_COLLEGE_ID", opendayProviderLandingPayloadFromClient.getCollegeId());
	inMap.put("P_DISPLAY_CNT", opendayProviderLandingPayloadFromClient.getTotalOpendayCount());
	inMap.put("P_BOOKING_FORM_FLAG", opendayProviderLandingPayloadFromClient.getBookingFormFlag());
	inMap.put("P_SESSION_ID", opendayProviderLandingPayloadFromClient.getSessionId());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_MORE_OPENDAY_LIST_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	return outMap;
  }
}
