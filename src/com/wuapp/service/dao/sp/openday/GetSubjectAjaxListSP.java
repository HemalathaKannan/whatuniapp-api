package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.SubjectAjaxListRowmapperImpl;
import com.wuapp.service.pojo.json.openday.SubjectAjaxPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * Class : GetSubjectAjaxListSP.java
 * @Description : This class for get subject ajax list.
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hemalatha K
 */

public class GetSubjectAjaxListSP extends StoredProcedure {

   private String GET_SUBJECT_AJAX_LIST_SP = ProcedureNames.GET_SUBJECT_AJAX_LIST_SP;
   
   public GetSubjectAjaxListSP(DataSource ds) {
	setDataSource(ds);
	setSql(GET_SUBJECT_AJAX_LIST_SP);
	declareParameter(new SqlParameter("P_KEYWORD", Types.VARCHAR));
	declareParameter(new SqlParameter("P_QUAL_CODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_CATEGORY_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_SUBJECT_LIST", OracleTypes.CURSOR, new SubjectAjaxListRowmapperImpl()));
	compile();
   }

   public Map<String, Object> executeProcedure(SubjectAjaxPayloadFromClient subjectAjaxPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_KEYWORD", subjectAjaxPayloadFromClient.getKeyword());
	inMap.put("P_QUAL_CODE", subjectAjaxPayloadFromClient.getQualCode());
	inMap.put("P_CATEGORY_ID", subjectAjaxPayloadFromClient.getCategoryId());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_SUBJECT_AJAX_LIST_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	return outMap;
   }
}
