package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.OpendaysInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OpendaysListRowmapperImpl;
import com.wuapp.service.pojo.json.openday.OpendayPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * @Class : ReservePlaceSP.java
 * @Description : This class is used to get the provider opendays info ...
 * @version : 1.0
 * @since : 01_Nov_2017
 * @author : Prabhakaran V.
 */
public class ReservePlaceSP extends StoredProcedure {

   private String GET_PROVIDER_OPENDAYS_SP = ProcedureNames.GET_PROVIDER_OPENDAYS_SP;

   //
   public ReservePlaceSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_PROVIDER_OPENDAYS_SP);
	//
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_EVENT_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_OPEN_DAY_DATE", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("PC_OPENDAYS_INFO", OracleTypes.CURSOR, new OpendaysInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OPENDAYS_LIST", OracleTypes.CURSOR, new OpendaysListRowmapperImpl()));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(OpendayPayloadFromClient opendayPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_COLLEGE_ID", opendayPayloadFromClient.getCollegeId());
	inMap.put("P_EVENT_ID", opendayPayloadFromClient.getEventId());
	inMap.put("P_OPEN_DAY_DATE", opendayPayloadFromClient.getOpendayDate());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_PROVIDER_OPENDAYS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
