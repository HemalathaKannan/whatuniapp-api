package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.EventListRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.EventTypeListRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OpendayMonthListRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OpendaySearchLandingRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.RegionListRowMapperImpl;
import com.wuapp.service.pojo.json.openday.OpendaySearchResultsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * Class : OpendaySearchResultsSP.java
 * @Description : This class is used to get openday search results
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hemalatha K
 */

public class OpendaySearchResultsSP extends StoredProcedure {

   private String OPENDAY_SEARCH_RESULTS_SP = ProcedureNames.OPENDAY_SEARCH_RESULTS_SP;

   public OpendaySearchResultsSP(DataSource dataSource) {
	setDataSource(dataSource);
	setSql(OPENDAY_SEARCH_RESULTS_SP);
	declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_MONTH_VALUE", Types.VARCHAR));
	declareParameter(new SqlInOutParameter("P_LOCATION", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_OPEN_DAYS_LIST", OracleTypes.CURSOR, new OpendaySearchLandingRowMapperImpl()));
	declareParameter(new SqlInOutParameter("P_MONTH_NAME", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_MONTH_LIST", OracleTypes.CURSOR, new OpendayMonthListRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_REGION_LIST", OracleTypes.CURSOR, new RegionListRowMapperImpl()));
	declareParameter(new SqlInOutParameter("P_EVENT_TYPE_NAME", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_EVENT_CATEGORY_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_EVENT_TYPE_LIST", OracleTypes.CURSOR, new EventTypeListRowMapperImpl()));
	declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_EVENT_LIST", OracleTypes.CURSOR, new EventListRowMapperImpl()));
	declareParameter(new SqlParameter("P_FROM_NO", Types.VARCHAR));
	declareParameter(new SqlParameter("P_TO_NO", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(OpendaySearchResultsPayloadFromClient opendaySearchResultsPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//	
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_PAGE_NAME", opendaySearchResultsPayloadFromClient.getPageName());
	inMap.put("P_MONTH_VALUE", opendaySearchResultsPayloadFromClient.getMonthValue());
	inMap.put("P_LOCATION", opendaySearchResultsPayloadFromClient.getLocation());
	inMap.put("P_MONTH_NAME", opendaySearchResultsPayloadFromClient.getMonthName());
	inMap.put("P_EVENT_TYPE_NAME", opendaySearchResultsPayloadFromClient.getEventTypeName());
	inMap.put("P_EVENT_CATEGORY_ID", opendaySearchResultsPayloadFromClient.getEventCategoryId());
	inMap.put("P_SESSION_ID", opendaySearchResultsPayloadFromClient.getSessionId());
	inMap.put("P_FROM_NO", opendaySearchResultsPayloadFromClient.getFromPageNo());
	inMap.put("P_TO_NO", opendaySearchResultsPayloadFromClient.getToPageNo());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (OPENDAY_SEARCH_RESULTS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	return outMap;
   }
}
