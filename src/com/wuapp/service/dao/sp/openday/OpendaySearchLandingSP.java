package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.EventTypeListRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OpendaySearchLandingRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.RegionListRowMapperImpl;
import com.wuapp.service.pojo.json.openday.OpendaySearchLandingPayloadFromClient;
import com.wuapp.util.developer.Debugger;
import oracle.jdbc.OracleTypes;

/**
 * Class : OpendaySearchLandingSP.java
 * @Description : This class for getting openday search results
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hemalatha K
 */

public class OpendaySearchLandingSP extends StoredProcedure {
   
   private String OPENDAY_SEARCH_LANDING_SP = ProcedureNames.OPENDAY_SEARCH_LANDING_SP;
   
   public OpendaySearchLandingSP(DataSource dataSource) {
	setDataSource(dataSource);
	setSql(OPENDAY_SEARCH_LANDING_SP);
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCATION_MODE", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_VIRTUAL_EVENTS_LIST", OracleTypes.CURSOR, new OpendaySearchLandingRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_UPCOMING_OPENDAYS_LIST", OracleTypes.CURSOR, new OpendaySearchLandingRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_REGION_OPENDAYS_LIST", OracleTypes.CURSOR, new OpendaySearchLandingRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_EVENT_TYPE_OPENDAYS_LIST", OracleTypes.CURSOR, new OpendaySearchLandingRowMapperImpl()));
	declareParameter(new SqlOutParameter("PC_REGION_LIST", OracleTypes.CURSOR, new RegionListRowMapperImpl()));
	declareParameter(new SqlInOutParameter("P_REGION_NAME", OracleTypes.VARCHAR));
	declareParameter(new SqlOutParameter("PC_EVENT_TYPE_LIST", OracleTypes.CURSOR, new EventTypeListRowMapperImpl()));
	declareParameter(new SqlInOutParameter("P_EVENT_TYPE_NAME", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_EVENT_CATEGORY_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	compile();
   }
   
   public Map<String, Object> executeProcedure(OpendaySearchLandingPayloadFromClient opendaySearchLandingPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", opendaySearchLandingPayloadFromClient.getUserId());
	inMap.put("P_LOCATION_MODE", opendaySearchLandingPayloadFromClient.getLocationMode());
	inMap.put("P_REGION_NAME", opendaySearchLandingPayloadFromClient.getRegionName());
	inMap.put("P_EVENT_TYPE_NAME", opendaySearchLandingPayloadFromClient.getEventTypeName());
	inMap.put("P_EVENT_CATEGORY_ID", opendaySearchLandingPayloadFromClient.getEventCategoryId());
	inMap.put("P_SESSION_ID", opendaySearchLandingPayloadFromClient.getSessionId());
	inMap.put("P_USER_IP", opendaySearchLandingPayloadFromClient.getUserIp());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (OPENDAY_SEARCH_LANDING_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	return outMap;
  }
}
