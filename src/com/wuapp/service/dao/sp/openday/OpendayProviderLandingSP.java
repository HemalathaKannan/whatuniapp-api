package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.AdditionalResourcesRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.AddressRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.ContentSectionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.GallerySectionRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.UniInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.OpendayDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.SelectedEventDetailsRowMapperImpl;
import com.wuapp.service.dao.util.rowmapper.openday.TestimonialSectionRowmapperImpl;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadFromClient;
import com.wuapp.util.developer.Debugger;
import oracle.jdbc.OracleTypes;

/**
 * Class : OpendayProviderLandingSP.java
 * @Description : This class is used to get the provider opendays info
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hemalatha K
 */

public class OpendayProviderLandingSP extends StoredProcedure {
  
  private String OPENDAY_PROVIDER_LANDING_SP = ProcedureNames.OPENDAY_PROVIDER_LANDING_SP;
   
  public OpendayProviderLandingSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(OPENDAY_PROVIDER_LANDING_SP);
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_EVENT_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_UNIV_TILE_INFO", OracleTypes.CURSOR, new UniInfoRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_UNI_OPENDAYS_LIST", OracleTypes.CURSOR, new OpendayDetailsRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_HERO_IMAGE", OracleTypes.CURSOR, new GallerySectionRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_CONTENT_SECTION", OracleTypes.CURSOR, new ContentSectionRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_UNI_ADDRESS", OracleTypes.CURSOR, new AddressRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_TESTIMONIALS", OracleTypes.CURSOR, new TestimonialSectionRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_ADDITIONAL_CONTENT", OracleTypes.CURSOR, new AdditionalResourcesRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
    declareParameter(new SqlOutParameter("P_MAP_BOX_KEY", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_EVENT_DETAILS", OracleTypes.CURSOR, new SelectedEventDetailsRowMapperImpl()));
    compile();
  }
  
  public Map<String, Object> executeProcedure(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", opendayProviderLandingPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", opendayProviderLandingPayloadFromClient.getCollegeId());
	inMap.put("P_EVENT_ID", opendayProviderLandingPayloadFromClient.getEventId());
	inMap.put("P_APP_VERSION", opendayProviderLandingPayloadFromClient.getAppVersion());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (OPENDAY_PROVIDER_LANDING_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	return outMap;
  }
}
