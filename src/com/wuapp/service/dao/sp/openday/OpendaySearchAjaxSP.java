package com.wuapp.service.dao.sp.openday;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.openday.OpendaySearchAjaxRowMapperImpl;
import com.wuapp.service.pojo.json.openday.OpendaySearchAjaxPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * Class : OpendaySearchAjaxSP.java
 * @Description : This class is used to get search ajax list
 * @version : 1.0
 * @since : 06_MAY_2020
 * @author : Hemalatha K
 */

public class OpendaySearchAjaxSP extends StoredProcedure {
   
   private String GET_OPENDAY_SEARCH_AJAX_FN = ProcedureNames.GET_OPENDAY_SEARCH_AJAX_FN;
   
   public OpendaySearchAjaxSP(DataSource dataSource) {
	setDataSource(dataSource);
	setFunction(true);
	setSql(GET_OPENDAY_SEARCH_AJAX_FN);
	declareParameter(new SqlOutParameter("RETVAL", OracleTypes.CURSOR, new OpendaySearchAjaxRowMapperImpl()));
	declareParameter(new SqlParameter("P_KEYWORD_TEXT", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(OpendaySearchAjaxPayloadFromClient opendaySearchAjaxPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//	
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_KEYWORD_TEXT", opendaySearchAjaxPayloadFromClient.getKeywordText());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_OPENDAY_SEARCH_AJAX_FN), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	return outMap;
   }
}
