package com.wuapp.service.dao.sp.interaction;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.interaction.InteractionPayloadFromClient;
import com.wuapp.util.developer.Debugger;
/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : AppInteractionLoggingSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * 			                  			Initial draft
 * Sangeeth.S      1.0              09-oct-2018     Added study mode in param 
 * Hema.S          1.1              31-Mar-2020     Added lat and long in param
 */
public class AppInteractionLoggingSP extends StoredProcedure {

   private String STATS_LOG_SP = ProcedureNames.STATS_LOG_SP;

   public AppInteractionLoggingSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(STATS_LOG_SP);
	//	
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_JS_LOG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_SUBORDER_ITEM_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PROFILE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_TYPE_NAME", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ACTIVITY", Types.VARCHAR));
	declareParameter(new SqlParameter("P_EXTRA_TEXT", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("P_LOG_STATUS_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));//Added in param for the oct_23_18 rel
	//
	declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(InteractionPayloadFromClient interactionPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", interactionPayloadFromClient.getUserId());
	inMap.put("P_COLLEGE_ID", interactionPayloadFromClient.getCollegeId());
	inMap.put("P_COURSE_ID", interactionPayloadFromClient.getCourseId());
	inMap.put("P_USER_IP", interactionPayloadFromClient.getUserIp());
	inMap.put("P_USER_AGENT", interactionPayloadFromClient.getUserAgent());
	inMap.put("P_JS_LOG", interactionPayloadFromClient.getJsLog());
	inMap.put("P_SUBORDER_ITEM_ID", interactionPayloadFromClient.getSuborderItemId());
	inMap.put("P_PROFILE_ID", interactionPayloadFromClient.getProfileId());
	inMap.put("P_TYPE_NAME", interactionPayloadFromClient.getTypeName());
	inMap.put("P_ACTIVITY", interactionPayloadFromClient.getActivity());
	inMap.put("P_EXTRA_TEXT", interactionPayloadFromClient.getExtraText());
	inMap.put("P_STUDY_MODE", interactionPayloadFromClient.getStudyMode());
	inMap.put("P_LATITUDE", interactionPayloadFromClient.getLatitude());
	inMap.put("P_LONGITUDE", interactionPayloadFromClient.getLongitude());
	//	
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (STATS_LOG_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
