package com.wuapp.service.dao.sp.interaction;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.interaction.SaveActionsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class SaveUserActionsSP extends StoredProcedure {

   private String SAVE_USER_ACTIONS_SP = ProcedureNames.SAVE_USER_ACTIONS_SP;

   public SaveUserActionsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SAVE_USER_ACTIONS_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_date", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_status", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_url", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_key_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_details", Types.VARCHAR));
	declareParameter(new SqlParameter("p_track_session_id", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(SaveActionsPayloadFromClient saveActionsPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", saveActionsPayloadFromClient.getUserId());
	inMap.put("p_action_type", saveActionsPayloadFromClient.getActionType());
	inMap.put("p_action_date", saveActionsPayloadFromClient.getActionDate());
	inMap.put("p_action_status", saveActionsPayloadFromClient.getActionStatus());
	inMap.put("p_action_url", saveActionsPayloadFromClient.getActionUrl());
	inMap.put("p_action_key_id", saveActionsPayloadFromClient.getActionKeyId());
	inMap.put("p_action_details", saveActionsPayloadFromClient.getActionDetails());
	inMap.put("p_track_session_id", saveActionsPayloadFromClient.getTrackSessionId());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SAVE_USER_ACTIONS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
