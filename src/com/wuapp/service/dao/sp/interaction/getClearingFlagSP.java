package com.wuapp.service.dao.sp.interaction;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class getClearingFlagSP extends StoredProcedure {

   private String GET_CLEARING_FLAG_SP = ProcedureNames.GET_CLEARING_FLAG_SP;

   public getClearingFlagSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_CLEARING_FLAG_SP);
	//
	declareParameter(new SqlParameter("P_NAME", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("P_VALUE", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(GetClearingFlagPayloadFromClient getClearingFlagPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_NAME", getClearingFlagPayloadFromClient.getFlagName());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_CLEARING_FLAG_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
