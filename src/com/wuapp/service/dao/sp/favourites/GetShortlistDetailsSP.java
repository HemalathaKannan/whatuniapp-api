package com.wuapp.service.dao.sp.favourites;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.favourites.ShortlistDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.favourites.SuggestionDetailsRowmapperImpl;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class GetShortlistDetailsSP extends StoredProcedure {

   private String GET_SHORTLIST_LIST_SP = ProcedureNames.GET_SHORTLIST_LIST_SP;

   //
   public GetShortlistDetailsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_SHORTLIST_LIST_SP);
	//	
	declareParameter(new SqlParameter("p_subject", Types.VARCHAR));
	declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_lat", Types.VARCHAR));
	declareParameter(new SqlParameter("p_long", Types.VARCHAR));
	declareParameter(new SqlParameter("p_location_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_region", Types.VARCHAR));
	declareParameter(new SqlParameter("p_region_flag", Types.VARCHAR));
	declareParameter(new SqlParameter("p_study_mode", Types.VARCHAR));
	declareParameter(new SqlParameter("p_prev_qual", Types.VARCHAR));
	declareParameter(new SqlParameter("p_prev_qual_grade", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_one", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_two", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_three", Types.VARCHAR));
	declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("p_shortlist_exist_flag", Types.VARCHAR));
	declareParameter(new SqlOutParameter("pc_shortlist_list", OracleTypes.CURSOR, new ShortlistDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("pc_suggestion_list", OracleTypes.CURSOR, new SuggestionDetailsRowmapperImpl()));
	//
	declareParameter(new SqlParameter("p_jacs_subject", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_subject", favouritesFormPayloadFromClient.getSubject());
	inMap.put("p_qualification", favouritesFormPayloadFromClient.getQualification());
	inMap.put("p_user_id", favouritesFormPayloadFromClient.getUserId());
	inMap.put("p_lat", favouritesFormPayloadFromClient.getLatitude());
	inMap.put("p_long", favouritesFormPayloadFromClient.getLongitude());
	inMap.put("p_location_type", favouritesFormPayloadFromClient.getLocationType());
	inMap.put("p_region", favouritesFormPayloadFromClient.getRegion());
	inMap.put("p_region_flag", favouritesFormPayloadFromClient.getRegionFlag());
	inMap.put("p_study_mode", favouritesFormPayloadFromClient.getStudyMode());
	inMap.put("p_prev_qual", favouritesFormPayloadFromClient.getPreviousQualification());
	inMap.put("p_prev_qual_grade", favouritesFormPayloadFromClient.getPreviousQualificationGrade());
	inMap.put("p_review_subject_one", favouritesFormPayloadFromClient.getReviewSubjectOne());
	inMap.put("p_review_subject_two", favouritesFormPayloadFromClient.getReviewSubjectTwo());
	inMap.put("p_review_subject_three", favouritesFormPayloadFromClient.getReviewSubjectThree());
	inMap.put("p_assessment_type", favouritesFormPayloadFromClient.getAssessmentType());
	inMap.put("p_jacs_subject", favouritesFormPayloadFromClient.getJacsCode());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_SHORTLIST_LIST_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
