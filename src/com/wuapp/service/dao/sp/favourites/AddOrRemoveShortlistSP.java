package com.wuapp.service.dao.sp.favourites;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 * Change Log :
 * ***************************************************************************************************************************
 * Author          version      Modified On     Modification Details                        Change
 * ***************************************************************************************************************************
 * Hema.S            1.1        31_MAR_20           Modified                  Added lat and long in paramaters
 */
public class AddOrRemoveShortlistSP extends StoredProcedure {

   private String ADD_REMOVE_SHORTLIST_SP = ProcedureNames.ADD_REMOVE_SHORTLIST_SP;

   //
   public AddOrRemoveShortlistSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(ADD_REMOVE_SHORTLIST_SP);
	//
	declareParameter(new SqlParameter("P_COLLEGE_IDS", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COURSE_IDS", Types.VARCHAR));
	declareParameter(new SqlParameter("P_BASKET_CONTENT_IDS", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REMOVE_FROM", Types.VARCHAR));
	declareParameter(new SqlParameter("P_ADD_TO", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("P_RETURN_FLAG", Types.VARCHAR));
	//
	declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_COLLEGE_IDS", favouritesFormPayloadFromClient.getInstitutionIds());
	inMap.put("P_COURSE_IDS", favouritesFormPayloadFromClient.getCourseIds());
	inMap.put("P_BASKET_CONTENT_IDS", favouritesFormPayloadFromClient.getBasketContentIds());
	inMap.put("P_USER_ID", favouritesFormPayloadFromClient.getUserId());
	inMap.put("P_REMOVE_FROM", favouritesFormPayloadFromClient.getRemoveFrom());
	inMap.put("P_ADD_TO", favouritesFormPayloadFromClient.getAddTo());
	inMap.put("P_USER_IP", favouritesFormPayloadFromClient.getUserIP());
	inMap.put("P_USER_AGENT", favouritesFormPayloadFromClient.getUserAgent());
	inMap.put("P_LATITUDE", favouritesFormPayloadFromClient.getLatitude());
	inMap.put("P_LONGITUDE", favouritesFormPayloadFromClient.getLongitude());
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (ADD_REMOVE_SHORTLIST_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
