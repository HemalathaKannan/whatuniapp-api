package com.wuapp.service.dao.sp.favourites;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * FAVOURITES PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class ReorderChoicesSP extends StoredProcedure {

   private String REORDER_CHOICE_SP = ProcedureNames.REORDER_CHOICE_SP;

   //
   public ReorderChoicesSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(REORDER_CHOICE_SP);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_FINAL_CHOICE_IDS", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("P_RETURN_FLAG", Types.VARCHAR));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", favouritesFormPayloadFromClient.getUserId());
	inMap.put("P_FINAL_CHOICE_IDS", favouritesFormPayloadFromClient.getFinalChoiceIds());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (REORDER_CHOICE_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
