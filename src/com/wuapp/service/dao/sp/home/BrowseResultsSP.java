package com.wuapp.service.dao.sp.home;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.AddressRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.FeaturedCourseRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.FeaturedOpendayRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.FeaturedProviderRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.FeaturedUniversityRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.InstitutionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.home.SubjectRowmapperImpl;
import com.wuapp.service.pojo.json.home.HomePayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : BrowseResultsSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param and added out param for clearing switch 
 */

public class BrowseResultsSP extends StoredProcedure {

   private String SP_NAME = ProcedureNames.HOME_BROWSE_SP;

   public BrowseResultsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SP_NAME);
	//
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LAT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCATION_MODE", Types.VARCHAR));
	//
	declareParameter(new SqlOutParameter("P_USER_NAME", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_POST_CODE", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_SEARCH_CATEGORIES", OracleTypes.CURSOR, new SubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_CATEGORIES", OracleTypes.CURSOR, new SubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_NEARBY_INSTITUTIONS", OracleTypes.CURSOR, new InstitutionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OPENDAY_INSTITUTIONS", OracleTypes.CURSOR, new InstitutionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ADDRESS", OracleTypes.CURSOR, new AddressRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_CL_FEATURED_PROVIDER", OracleTypes.CURSOR, new FeaturedProviderRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_CURRENT_YEAR", Types.VARCHAR));
	//
	// Added for getting featured university, course and openday details by Hemalatha.K on 04_FEB_2019_REL
	declareParameter(new SqlOutParameter("PC_FEATURED_INSTITUTION", OracleTypes.CURSOR, new FeaturedUniversityRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_FEATURED_COURSE", OracleTypes.CURSOR, new FeaturedCourseRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_FEATURED_OPENDAYS", OracleTypes.CURSOR, new FeaturedOpendayRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_CLEARING_SWITCH_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("P_CLEARING_URL", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(HomePayloadFromClient homePayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_USER_ID", homePayloadFromClient.getUserId());
	inMap.put("P_LAT", homePayloadFromClient.getLatitude());
	inMap.put("P_LONG", homePayloadFromClient.getLongitude());
	inMap.put("P_APP_VERSION", homePayloadFromClient.getAppVersion());
	inMap.put("P_LOCATION_MODE", homePayloadFromClient.getLocationMode());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SP_NAME), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
