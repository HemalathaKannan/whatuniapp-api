package com.wuapp.service.dao.sp.quiz;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.quiz.QualSubjectsAjaxListRowmapperImpl;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class QualSubjectListSP extends StoredProcedure {

   private String GET_QUAL_SUBJECT_AJAX_SP = ProcedureNames.GET_QUAL_SUBJECT_AJAX_SP;

   public QualSubjectListSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_QUAL_SUBJECT_AJAX_SP);
	//
	declareParameter(new SqlParameter("p_qual_type_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_subject_text", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("pc_results", OracleTypes.CURSOR, new QualSubjectsAjaxListRowmapperImpl()));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(QuizPayloadFromClient quizPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_qual_type_id", quizPayloadFromClient.getQualTypeId());
	inMap.put("p_subject_text", quizPayloadFromClient.getSubjectText());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_QUAL_SUBJECT_AJAX_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
