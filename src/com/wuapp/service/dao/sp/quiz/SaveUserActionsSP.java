package com.wuapp.service.dao.sp.quiz;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class SaveUserActionsSP extends StoredProcedure {

   private String SAVE_USER_ACTIONS_SP = ProcedureNames.SAVE_USER_ACTIONS_SP;

   public SaveUserActionsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(SAVE_USER_ACTIONS_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_date", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_status", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_url", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_key_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_action_details", Types.VARCHAR));
	declareParameter(new SqlParameter("p_track_session_id", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(QuizPayloadFromClient quizPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", quizPayloadFromClient.getUserId());
	inMap.put("p_action_type", quizPayloadFromClient.getActionType());
	inMap.put("p_action_date", quizPayloadFromClient.getActionDate());
	inMap.put("p_action_status", quizPayloadFromClient.getActionStatus());
	inMap.put("p_action_url", quizPayloadFromClient.getActionUrl());
	inMap.put("p_action_key_id", quizPayloadFromClient.getActionKeyId());
	inMap.put("p_action_details", quizPayloadFromClient.getActionDetails());
	inMap.put("p_track_session_id", quizPayloadFromClient.getTrackSessionId());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (SAVE_USER_ACTIONS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
