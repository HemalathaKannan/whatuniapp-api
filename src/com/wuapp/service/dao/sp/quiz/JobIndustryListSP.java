package com.wuapp.service.dao.sp.quiz;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.quiz.JobIndustryAjaxListRowmapperImpl;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class JobIndustryListSP extends StoredProcedure {

   private String GET_JOB_INDUSTRY_AJAX_SP = ProcedureNames.GET_JOB_INDUSTRY_AJAX_SP;

   public JobIndustryListSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_JOB_INDUSTRY_AJAX_SP);
	//
	declareParameter(new SqlParameter("p_job_industry_text", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("pc_job_industry_result", OracleTypes.CURSOR, new JobIndustryAjaxListRowmapperImpl()));
	//
	compile();
   }

   public Map<String, Object> executeProcedure(QuizPayloadFromClient quizPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_job_industry_text", quizPayloadFromClient.getJobOrIndustry());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_JOB_INDUSTRY_AJAX_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
