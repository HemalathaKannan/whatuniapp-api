package com.wuapp.service.dao.sp.quiz;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.quiz.PreviousQualRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.quiz.QuestionDetailsRowmapperImpl;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;
import com.wuapp.util.developer.Debugger;

public class QuizDetailsSP extends StoredProcedure {

   private String GET_QUIZ_DATA_SP = ProcedureNames.GET_QUIZ_DATA_SP;

   public QuizDetailsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_QUIZ_DATA_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_app_version", Types.VARCHAR));
	declareParameter(new SqlParameter("p_question_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_answer", Types.VARCHAR));
	declareParameter(new SqlParameter("p_category_code", Types.VARCHAR));
	declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
	declareParameter(new SqlParameter("p_prev_qual", Types.VARCHAR));
	declareParameter(new SqlParameter("p_prev_qual_grade", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("p_course_count", Types.VARCHAR));
	declareParameter(new SqlOutParameter("pc_question_details", OracleTypes.CURSOR, new QuestionDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("pc_prev_qual", OracleTypes.CURSOR, new PreviousQualRowmapperImpl()));
	declareParameter(new SqlOutParameter("pc_version_details", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	//
	declareParameter(new SqlParameter("p_input_qual_type_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subject1_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subject2_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subject3_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subject4_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subject5_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subject6_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subj1_tariff_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subj2_tariff_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subj3_tariff_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subj4_tariff_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subj5_tariff_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_input_subj6_tariff_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR));
	declareParameter(new SqlInOutParameter("p_selected_filter_url", Types.VARCHAR));
	compile();
   }

   public Map<String, Object> executeProcedure(QuizPayloadFromClient quizPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", quizPayloadFromClient.getUserId());
	inMap.put("p_app_version", quizPayloadFromClient.getAppVersion());
	inMap.put("p_question_id", quizPayloadFromClient.getQuestionId());
	inMap.put("p_answer", quizPayloadFromClient.getAnswer());
	inMap.put("p_category_code", quizPayloadFromClient.getCategoryCode());
	inMap.put("p_qualification", quizPayloadFromClient.getQualificationCode());
	inMap.put("p_prev_qual", quizPayloadFromClient.getPreviousQual());
	inMap.put("p_prev_qual_grade", quizPayloadFromClient.getPreviousQualGrades());
	//
	inMap.put("p_input_qual_type_id", quizPayloadFromClient.getQualTypeId());
	inMap.put("p_input_subject1_id", quizPayloadFromClient.getSubject1_id());
	inMap.put("p_input_subject2_id", quizPayloadFromClient.getSubject2_id());
	inMap.put("p_input_subject3_id", quizPayloadFromClient.getSubject3_id());
	inMap.put("p_input_subject4_id", quizPayloadFromClient.getSubject4_id());
	inMap.put("p_input_subject5_id", quizPayloadFromClient.getSubject5_id());
	inMap.put("p_input_subject6_id", quizPayloadFromClient.getSubject6_id());
	inMap.put("p_input_subj1_tariff_points", quizPayloadFromClient.getSubj1_tariff_points());
	inMap.put("p_input_subj2_tariff_points", quizPayloadFromClient.getSubj2_tariff_points());
	inMap.put("p_input_subj3_tariff_points", quizPayloadFromClient.getSubj3_tariff_points());
	inMap.put("p_input_subj4_tariff_points", quizPayloadFromClient.getSubj4_tariff_points());
	inMap.put("p_input_subj5_tariff_points", quizPayloadFromClient.getSubj5_tariff_points());
	inMap.put("p_input_subj6_tariff_points", quizPayloadFromClient.getSubj6_tariff_points());
	inMap.put("p_jacs_code", quizPayloadFromClient.getJacsCode());
	inMap.put("p_selected_filter_url", quizPayloadFromClient.getSelectedFilterUrl());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_QUIZ_DATA_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
