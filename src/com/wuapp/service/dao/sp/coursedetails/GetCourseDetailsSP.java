package com.wuapp.service.dao.sp.coursedetails;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.ApplicantsDataRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.CourseInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.EnquiryInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.EntryReqDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.FeeDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.GetAssessedCourseRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.KeyStatsInfoRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.ModuleDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.OppEntryReqDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.OpportunityDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.OtherCoursesRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.ReviewListRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.UniRankingDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.coursedetails.previousSubjectRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.profile.LoadYearListRowmapperImpl;
import com.wuapp.service.pojo.json.coursedetials.CourseDetailsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : GetCourseDetailsSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * Hemalatha.K      2.0            25-JUN-2020     Removed clearing related param 
 */

public class GetCourseDetailsSP extends StoredProcedure {

   private String GET_COURSE_DETAILS_PAGE_PRC = ProcedureNames.GET_COURSE_DETAILS_PAGE_PRC;

   public GetCourseDetailsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(GET_COURSE_DETAILS_PAGE_PRC);
	//
	declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_OPPORTUNITY_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
	declareParameter(new SqlParameter("P_TRACK_SESSION_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("p_subject", Types.VARCHAR));
	declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION", Types.VARCHAR));
	declareParameter(new SqlParameter("P_REGION_FLAG", Types.VARCHAR));
	declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL", Types.VARCHAR));
	declareParameter(new SqlParameter("P_PREV_QUAL_GRADE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LOCATION_TYPE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_APP_VERSION", Types.VARCHAR));
	declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_keyword_search", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_one", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_two", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_three", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("P_MODULE_INFO", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_COURSE_INFO", OracleTypes.CURSOR, new CourseInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ENQUIRY_INFO", OracleTypes.CURSOR, new EnquiryInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_MODULE_DETAILS", OracleTypes.CURSOR, new ModuleDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_REVIEW_LIST", OracleTypes.CURSOR, new ReviewListRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OPPORTUNITY_DETAILS", OracleTypes.CURSOR, new OpportunityDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_KEY_STATS_INFO", OracleTypes.CURSOR, new KeyStatsInfoRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_UNI_RANKING_FN", OracleTypes.CURSOR, new UniRankingDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_GET_ASSESSED_COURSE", OracleTypes.CURSOR, new GetAssessedCourseRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_OTHER_COURSES_POD", OracleTypes.CURSOR, new OtherCoursesRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_APPLICANTS_DATA", OracleTypes.CURSOR, new ApplicantsDataRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_PREVIOUS_SUBJECT", OracleTypes.CURSOR, new previousSubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ENTRY_REQ_DETAILS", OracleTypes.CURSOR, new EntryReqDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_ADVERTISER_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_LOAD_YEAR_LIST", OracleTypes.CURSOR, new LoadYearListRowmapperImpl()));
	declareParameter(new SqlOutParameter("P_OD_BUTTON_FLAG", Types.VARCHAR));
	declareParameter(new SqlOutParameter("PC_OPP_ENTRY_REQ_DETAILS", OracleTypes.CURSOR, new OppEntryReqDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_GET_FEE_DETAILS", OracleTypes.CURSOR, new FeeDetailsRowmapperImpl()));
	
	declareParameter(new SqlParameter("p_user_lat", Types.VARCHAR)); 
	declareParameter(new SqlParameter("p_user_long", Types.VARCHAR)); 
	//
	compile();
   }

   public Map<String, Object> executeProcedure(CourseDetailsPayloadFromClient courseDetailsPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("P_COURSE_ID", courseDetailsPayloadFromClient.getCourseId());
	inMap.put("P_COLLEGE_ID", courseDetailsPayloadFromClient.getCollegeId());
	inMap.put("P_OPPORTUNITY_ID", courseDetailsPayloadFromClient.getOpportunityId());
	inMap.put("P_USER_ID", courseDetailsPayloadFromClient.getUserId());
	inMap.put("P_USER_AGENT", courseDetailsPayloadFromClient.getUserAgent());
	inMap.put("P_TRACK_SESSION_ID", courseDetailsPayloadFromClient.getTrackSessionId());
	inMap.put("p_subject", courseDetailsPayloadFromClient.getSubject());
	inMap.put("p_qualification", courseDetailsPayloadFromClient.getQualification());
	inMap.put("P_REGION", courseDetailsPayloadFromClient.getRegion());
	inMap.put("P_REGION_FLAG", courseDetailsPayloadFromClient.getRegionFlag());
	inMap.put("P_STUDY_MODE", courseDetailsPayloadFromClient.getStudyMode());
	inMap.put("P_PREV_QUAL", courseDetailsPayloadFromClient.getPreviousQual());
	inMap.put("P_PREV_QUAL_GRADE", courseDetailsPayloadFromClient.getPreviousQualGrade());
	inMap.put("P_LOCATION_TYPE", courseDetailsPayloadFromClient.getLocationType());
	inMap.put("P_APP_VERSION", courseDetailsPayloadFromClient.getAppVersion());
	inMap.put("p_assessment_type", courseDetailsPayloadFromClient.getAssessmentType());
	inMap.put("p_keyword_search", courseDetailsPayloadFromClient.getSearchKeyword());
	inMap.put("p_review_subject_one", courseDetailsPayloadFromClient.getReviewSubjectOne());
	inMap.put("p_review_subject_two", courseDetailsPayloadFromClient.getReviewSubjectTwo());
	inMap.put("p_review_subject_three", courseDetailsPayloadFromClient.getReviewSubjectThree());
	inMap.put("p_user_lat", courseDetailsPayloadFromClient.getUserLatitude());
	inMap.put("p_user_long", courseDetailsPayloadFromClient.getUserLongitude());	
	
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (GET_COURSE_DETAILS_PAGE_PRC), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
