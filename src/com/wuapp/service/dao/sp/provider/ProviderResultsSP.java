package com.wuapp.service.dao.sp.provider;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.wuapp.service.dao.util.name.ProcedureNames;
import com.wuapp.service.dao.util.rowmapper.common.AppVersionRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.PreviousQualificationRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersAssesmentTypeRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersLocationRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersLocationTypeRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersQualificationRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersReviewCategoryRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersStudyModeRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.course.filter.SearchFiltersSubjectRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.provider.InstitutionDetailsRowmapperImpl;
import com.wuapp.service.dao.util.rowmapper.provider.InstitutionSearchResultsRowmapperImpl;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadFromClient;
import com.wuapp.util.developer.Debugger;

/**
 * 
 * @author 
 * @since wuapp1.0_ - Initial draft
 * @version 1.1 
 * Class : ProviderResultsSP.java 
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id   Modified On     Modification Details Change
 * *************************************************************************************************************
 * 			                  			Initial draft
 * Hema.S          1.1              31-Mar-2020     Added lat and long in param
 * Hemalatha.K     2.0              25-JUN-2020     Removed clearing related param 
 */

public class ProviderResultsSP extends StoredProcedure {

   private String PROVIDER_RESULTS_SP = ProcedureNames.PROVIDER_RESULTS_SP;

   public ProviderResultsSP(DataSource ds) {
	//
	setDataSource(ds);
	setSql(PROVIDER_RESULTS_SP);
	//
	declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_phrase_search", Types.VARCHAR));
	declareParameter(new SqlParameter("p_college_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
	declareParameter(new SqlParameter("p_town_city", Types.VARCHAR));
	declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
	declareParameter(new SqlParameter("p_study_mode", Types.VARCHAR));
	declareParameter(new SqlParameter("p_location_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_region", Types.VARCHAR));
	declareParameter(new SqlParameter("p_region_flag", Types.VARCHAR));
	declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_search_how", Types.VARCHAR));
	declareParameter(new SqlParameter("p_search_category", Types.VARCHAR));
	declareParameter(new SqlParameter("p_search_cat_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
	declareParameter(new SqlParameter("p_from_no", Types.VARCHAR));
	declareParameter(new SqlParameter("p_to_no", Types.VARCHAR));
	declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
	declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
	declareParameter(new SqlParameter("p_univ_loc_type_name_str", Types.VARCHAR));
	declareParameter(new SqlParameter("p_search_url", Types.VARCHAR));
	declareParameter(new SqlParameter("p_track_session_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
	declareParameter(new SqlParameter("p_app_version", Types.VARCHAR));
	declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_one", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_two", Types.VARCHAR));
	declareParameter(new SqlParameter("p_review_subject_three", Types.VARCHAR));
	//	
	declareParameter(new SqlOutParameter("pc_search_results", OracleTypes.CURSOR, new InstitutionSearchResultsRowmapperImpl()));
	declareParameter(new SqlOutParameter("pc_college_details", OracleTypes.CURSOR, new InstitutionDetailsRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_QUAL_FILTERS", OracleTypes.CURSOR, new SearchFiltersQualificationRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_SUBJECT_FILTERS", OracleTypes.CURSOR, new SearchFiltersSubjectRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_LOCATION_FILTERS", OracleTypes.CURSOR, new SearchFiltersLocationRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_LOCATION_TYPES_FILTER", OracleTypes.CURSOR, new SearchFiltersLocationTypeRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_STUDY_MODE_FILTER", OracleTypes.CURSOR, new SearchFiltersStudyModeRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_PREV_QUAL_FILTER", OracleTypes.CURSOR, new PreviousQualificationRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_VERSION_DETAILS", OracleTypes.CURSOR, new AppVersionRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_ASSESSMENT_TYPE_FILTER", OracleTypes.CURSOR, new SearchFiltersAssesmentTypeRowmapperImpl()));
	declareParameter(new SqlOutParameter("PC_REVIEW_CATEGORY_FILTER", OracleTypes.CURSOR, new SearchFiltersReviewCategoryRowmapperImpl()));
	declareParameter(new SqlOutParameter("o_adv_flag", Types.VARCHAR));
	declareParameter(new SqlOutParameter("p_course_count", Types.VARCHAR));
	//
	declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR));
	declareParameter(new SqlParameter("P_USER_IP", Types.VARCHAR)); //Added for clearing
	//
	declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));
	declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));
	
	//
	compile();
   }

   public Map<String, Object> executeProcedure(ProviderResultsPayloadFromClient providerResultsPayloadFromClient) {
	Long startExecuteTime = new Long(System.currentTimeMillis());
	//
	//
	HashMap<String, String> inMap = new HashMap<String, String>();
	inMap.put("p_user_id", providerResultsPayloadFromClient.getUserId());
	inMap.put("p_phrase_search", providerResultsPayloadFromClient.getPhraseSearch());
	inMap.put("p_college_id", providerResultsPayloadFromClient.getInstitutionId());
	inMap.put("p_qualification", providerResultsPayloadFromClient.getQualification());
	inMap.put("p_town_city", providerResultsPayloadFromClient.getCity());
	inMap.put("p_postcode", providerResultsPayloadFromClient.getPostCode());
	inMap.put("p_study_mode", providerResultsPayloadFromClient.getStudyMode());
	inMap.put("p_location_type", providerResultsPayloadFromClient.getLocationType());
	inMap.put("p_region", providerResultsPayloadFromClient.getRegion());
	inMap.put("p_region_flag", providerResultsPayloadFromClient.getRegionFlag());
	inMap.put("p_affiliate_id", providerResultsPayloadFromClient.getAffiliateId());
	inMap.put("p_search_how", providerResultsPayloadFromClient.getSearchHow());
	inMap.put("p_search_category", providerResultsPayloadFromClient.getSearchCategory());
	inMap.put("p_search_cat_id", providerResultsPayloadFromClient.getSearchCategoryId());
	inMap.put("p_user_agent", providerResultsPayloadFromClient.getUserAgent());
	inMap.put("p_from_no", providerResultsPayloadFromClient.getFromPage());
	inMap.put("p_to_no", providerResultsPayloadFromClient.getToPage());
	inMap.put("p_entry_level", providerResultsPayloadFromClient.getEntryLevel());
	inMap.put("p_entry_points", providerResultsPayloadFromClient.getEntryPoints());
	inMap.put("p_univ_loc_type_name_str", providerResultsPayloadFromClient.getUniversityLocationType());
	inMap.put("p_search_url", providerResultsPayloadFromClient.getSearchUrl());
	inMap.put("p_track_session_id", providerResultsPayloadFromClient.getTrackSessionId());
	inMap.put("p_network_id", providerResultsPayloadFromClient.getNetworkId());
	inMap.put("p_app_version", providerResultsPayloadFromClient.getAppVersion());
	inMap.put("p_assessment_type", providerResultsPayloadFromClient.getAssessmentType());
	inMap.put("p_review_subject_one", providerResultsPayloadFromClient.getReviewSubjectOne());
	inMap.put("p_review_subject_two", providerResultsPayloadFromClient.getReviewSubjectTwo());
	inMap.put("p_review_subject_three", providerResultsPayloadFromClient.getReviewSubjectThree());
	inMap.put("p_jacs_code", providerResultsPayloadFromClient.getJacsCode());
	inMap.put("P_USER_IP", providerResultsPayloadFromClient.getUserIp());
	inMap.put("P_LATITUDE", providerResultsPayloadFromClient.getLatitude());
	inMap.put("P_LONGITUDE", providerResultsPayloadFromClient.getLongitude());
	//
	Map<String, Object> outMap = execute(inMap);
	//
	if (Debugger.getInstance().isDebugDbDetails()) {
	   Debugger.getInstance().printDbDetails((this.getClass().toString()), (PROVIDER_RESULTS_SP), (inMap), (new Long((new Long(System.currentTimeMillis())) - startExecuteTime)));
	}
	//
	return outMap;
   }
}
