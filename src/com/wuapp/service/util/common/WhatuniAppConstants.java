package com.wuapp.service.util.common;

public interface WhatuniAppConstants {

   public final String ERROR_CODE_401 = "401";
   public final String TOKEN_INVALID = "Token is invalid";
   public final String AUTHORIZATION_FAILED = "Authorization failed";
   public final String GUEST_USER_ID = "10001";
   //Added for COVID related changes by Hemalatha K on 21_APR_2020_REL
   public final String COVID_19_ARTICLE_FLAG = "Y";
   public final String COVID_19_HOME_CONTENT = "<style>\r\n" + 
   	".covid_infosect {background:#00B26D !important}\r\n" + 
   	".covid_infosect .covid_infotxt{margin-top:5px}\r\n" + 
   	".covid_infosect .covid_infotxt h4{display:inline-block;margin-right:8px;margin-bottom:0}\r\n" + 
   	".covid_infosect .covid_infotxt p, .covid_infosect .covid_infotxt a{font-size:14px;display:inline-block}\r\n" + 
   	".covid_infosect .covid_infotxt a{width:98px}\r\n" + 
   	"</style>\r\n" + 
   	"<div class=\"covid_infotxt\">\r\n" + 
   	"    <h4>Coronavirus (COVID-19)</h4>\r\n" + 
   	"    <p><a href=\"#\" title=\"Latest info\" articlelink=\"https://www.whatuni.com/advice/coronavirus/\">Latest info <img src=\"img/icons/rgt_arrow.png\" alt=\"Right arrow\"></a>\r\n" + 
   	"    </p>          \r\n" + 
   	"</div>";
   public final String COVID_19_PROFILE_CONTENT = "<style>\r\n" + 
   	".covid_upt .covid_txt {color: #00B26D;}\r\n" + 
   	".covid_upt .covid_link {color: #02BBFD;font: 14px/20px \"Lato-Regular\",Arial;text-decoration: underline;}\r\n" + 
   	".covid_upt .covid_link .linfo {padding-right: 5px;color: #02BBFD}\r\n" + 
   	".covid_upt .covid_link img {vertical-align: middle;display: inline-block;width: 16px;}\r\n" + 
   	"</style>\r\n" + 
   	"<span class=\"covid_txt\">University course intakes may be affected by Coronavirus (COVID-19).</span>\r\n" + 
   	"<a href=\"#\" class=\"covid_link\" articlelink=\"https://www.whatuni.com/advice/coronavirus/\"><span class=\"linfo\">Latest info</span><img src=\"img/covid_blu_arw.svg\"></a>";	   
}
