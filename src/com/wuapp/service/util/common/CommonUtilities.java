package com.wuapp.service.util.common;

import java.util.Map;
import java.util.ArrayList;

import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.validator.GenericValidator;
import com.wuapp.util.environment.EstablishEnvironment;

public class CommonUtilities {

   public String getUserIp(HttpServletRequest request) {
	String clientIp = request.getHeader("CLIENTIP");
	if (clientIp == null || clientIp.trim().length() == 0) {
	   clientIp = request.getRemoteAddr();
	}
	return clientIp;
   }

   /**
    * Util to check whether given values is null or not
    *
    * @author Sabapathi
    * @since FL5.0_20170328 - initial draft
    * @param String
    * @return boolean
    */
   public boolean isBlankOrNull(String value) {
	GenericValidator validator = new GenericValidator();
	if (validator.isBlankOrNull(value) || "null".equals(value.trim())) {
	   return true;
	} else {
	   return false;
	}
   }

   /**
    * To replace space by hypen
    * 
    * @author Indumathi.S
    * @param inString
    * @return String
    */
   public String replaceSpaceByHypen(String inString) {
	if (inString != null && inString.trim().length() > 0) {
	   inString = inString.replaceAll("     ", "-"); //five spaces
	   inString = inString.replaceAll("    ", "-"); //four spaces
	   inString = inString.replaceAll("   ", "-"); //three spaces
	   inString = inString.replaceAll("  ", "-"); //two spaces
	   inString = inString.replaceAll(" ", "-"); //one space    
	   inString = inString.replaceAll("-----", "-"); //five hyphen
	   inString = inString.replaceAll("----", "-"); //four hyphen
	   inString = inString.replaceAll("---", "-"); //three hyphen
	   inString = inString.replaceAll("--", "-"); //two hyphen      
	}
	return inString;
   }

   /**
    * To replace special characters
    * 
    * @author Indumathi.S
    * @param inString
    * @return String
    */
   public String replaceSpecialCharacter(String inString) {
	String outString = inString;
	if (outString != null) {
	   char data[] = outString.toCharArray();
	   for (int i = 0; i < data.length; i++) {
		int character = data[i];
		if (!((character >= 48 && character <= 57) || (character >= 65 && character <= 90) || (character >= 97 && character <= 122) || character == 45)) {
		   data[i] = ' ';
		}
	   }
	   outString = new String(data);
	}
	return outString;
   }

   /**
    * To replace special characters
    * 
    * @author Indumathi.S
    * @param collegeName
    * @param collegeId
    * @param request
    * @return String
    */
   public String getUniReviewURL(String collegeName, String collegeId, HttpServletRequest request) {
	String reviewUrl = getDomainName(request);
	if (!isBlankOrNull(collegeName) && !isBlankOrNull(collegeId)) {
	   StringBuffer tmpUrl = new StringBuffer();
	   tmpUrl.append("/university-course-reviews/");
	   tmpUrl.append(replaceSpaceByHypen(replaceSpecialCharacter(collegeName)));
	   tmpUrl.append("/");
	   tmpUrl.append(collegeId);
	   tmpUrl.append("/");
	   reviewUrl += tmpUrl.toString().toLowerCase();
	}
	return reviewUrl;
   }

   /**
    * To get domain name
    * 
    * @author Indumathi.S
    * @param request
    * @return String
    */
   public String getDomainName(HttpServletRequest request) {
	String domainName = request.getServerName();
	if (domainName.indexOf("mdev.whatuni.com") > -1) {
	   domainName = EstablishEnvironment.getInstance().getBundle().getString("domain.mdev");
	} else if (domainName.indexOf("mtest.whatuni.com") > -1) {
	   domainName = EstablishEnvironment.getInstance().getBundle().getString("domain.mtest");
	} else if (domainName.indexOf("www.whatuni.com") > -1) {
	   domainName = EstablishEnvironment.getInstance().getBundle().getString("domain.live");
	}
	return domainName;
   }

   /**
    * Util to get User Agent
    *
    * @author Sabapathi
    * @since FL5.0_20170328 - initial draft
    * @param request
    * @return String
    */
   public String getUserAgent(HttpServletRequest request) {
	return request.getHeader("user-agent");
   }

   /**
    * To get facebook user image
    * 
    * @author Indumathi.S
    * @param request
    * @return String
    */
   public String getFacebookImagePath(String facebookId) {
	String imagePath = "";
	if (!isBlankOrNull(facebookId)) {
	   StringBuffer tmpUrl = new StringBuffer();
	   tmpUrl.append("https://graph.facebook.com");
	   tmpUrl.append("/");
	   tmpUrl.append(facebookId);
	   tmpUrl.append("/");
	   tmpUrl.append("picture?type=normal");
	   imagePath += tmpUrl.toString().toLowerCase();
	}
	return imagePath;
   }
   
   /**
    * To get guest user flag
    * 
    * @author Hema.S
    * @param request
    * @return String
    */
   
  public String guestFlag(String fromClientUserId){
     String guestUserFlag = "N";
     if((WhatuniAppConstants.GUEST_USER_ID).equals(fromClientUserId)){
       guestUserFlag = "Y";
     }else{
	  guestUserFlag = "N";
	}
    return guestUserFlag;
  }
  
  /**
   * To get round off value
   * 
   * @author Hemalatha K
   * @param value, numberOfDigitsAfterDecimalPoint
   * @return String
   */
    
  public String round(String value, int numberOfDigitsAfterDecimalPoint) {
     String outputValue = null;
     if(!isBlankOrNull(value)) {
	 try {
         BigDecimal bigDecimal = new BigDecimal(value);
         bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint, BigDecimal.ROUND_HALF_UP);
         outputValue = bigDecimal.toString();
	 } catch (Exception e) {
	   outputValue = null;
	}
     }
     return outputValue;
  }
 
  /**
   * @see This method to check arraylist is blankOrNull
   * @author Hemalatha K
   * @param list
   * @return boolean
   */
  
  public boolean isBlankOrNull(ArrayList list){
    if(list != null && list.size() > 0){
      return false;
    }else{
      return true;
    }
  }
  
  /**
   * @see This method to check Map is blankOrNull
   * @author Hemalatha K
   * @param map
   * @return boolean
   */
  
  public boolean isBlankOrNull(Map map){
    if(map != null && !map.isEmpty()){
      return false;
    }else{
      return true;
    }
  }  
  
  /**
   * @see This method to check length of array value
   * @author Hemalatha K
   * @param arrayValue
   * @return boolean
   */
  
  public boolean checkArrayLength(String[] arrayValue){
    if(arrayValue.length <= 0) {
	 return false;
    }
    return true;
  }  
}
