package com.wuapp.service.util.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Common > Date Utilities
 * 
 * @author Mohamed Syed
 * @version 1.0
 * @since eduk301_20150128 - initial draft
 * -----------------------------------------------------------------------------
 * Character Description Example
 * -----------------------------------------------------------------------------
 * G Era designator AD
 * y Year in four digits 2001
 * Y Year in four digits 2001
 * M Month in year July or 07
 * d Day in month 10
 * h Hour in A.M./P.M.(1~12) 12
 * H Hour in day (0~23) 22
 * m Minute in hour 30
 * s Second in minute 55
 * S Millisecond 234
 * E Day in week Tuesday
 * D Day in year 360
 * F Day of week in month 2(second Wed. in July)
 * w Week in year 40
 * W Week in month 1
 * a A.M./P.M. marker PM
 * k Hour in day (1~24) 24
 * K Hour in A.M./P.M. (0~11) 10
 * z Time zone Eastern Standard Time
 * ' Escape for text Delimiter
 * " Single quote `
 * ------------------------------------------------------------------------------
 */
public class DateUtilities {

   public String getFullDate() {
	Date date = new Date();
	SimpleDateFormat dateFormatter = new SimpleDateFormat("E yyyy/MMM/dd 'at' hh:mm:ss a zzz");
	String formattedDate = dateFormatter.format(date);
	//
	date = null;
	dateFormatter = null;
	return formattedDate;
   }
}
