package com.wuapp.service.pojo.developer;

public class DebugForm {

   private String debugPageTime = "false";
   private String debugDbDetails = "false";
   private String debugRequest = "false";
   private String debugUrl = "false";

   public String getDebugPageTime() {
	return debugPageTime;
   }

   public void setDebugPageTime(String debugPageTime) {
	this.debugPageTime = debugPageTime;
   }

   public String getDebugDbDetails() {
	return debugDbDetails;
   }

   public void setDebugDbDetails(String debugDbDetails) {
	this.debugDbDetails = debugDbDetails;
   }

   public String getDebugRequest() {
	return debugRequest;
   }

   public void setDebugRequest(String debugRequest) {
	this.debugRequest = debugRequest;
   }

   public String getDebugUrl() {
	return debugUrl;
   }

   public void setDebugUrl(String debugUrl) {
	this.debugUrl = debugUrl;
   }
}
