package com.wuapp.service.pojo.json.profile;

import lombok.Data;

@Data
public class ProfileDetailsPayloadFromClient {

   private String userId;
   private String collegeId;
   private String userIp;
   private String userAgent;
   private String appVersion;
   private String affiliateId;
   private String accessToken;
   private String searchCategoryCode;
   private String searchCategoryId;
   private String keywordSearch;
   private String orderBy;
   private String locationType;
   private String region;
   private String regionFlag;
   private String studyMode;
   private String previousQual;
   private String previousQualGrade;
   private String qualification;
   private String assessmentType;
   private String reviewSubjectOne;
   private String reviewSubjectTwo;
   private String reviewSubjectThree;
   private String jacsCode;
}
