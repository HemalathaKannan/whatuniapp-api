package com.wuapp.service.pojo.json.profile;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.profile.AccomodationVO;
import com.wuapp.service.dao.util.valueobject.profile.CompetitorDetailVO;
import com.wuapp.service.dao.util.valueobject.profile.EnquiryDetailsVO;
import com.wuapp.service.dao.util.valueobject.profile.KeyStatsInfoVO;
import com.wuapp.service.dao.util.valueobject.profile.LoadYearListVO;
import com.wuapp.service.dao.util.valueobject.profile.OpendayDetailsVO;
import com.wuapp.service.dao.util.valueobject.profile.ProfileVO;
import com.wuapp.service.dao.util.valueobject.profile.ReviewStarRatingVO;
import com.wuapp.service.dao.util.valueobject.profile.StudentReviewVO;
import com.wuapp.service.dao.util.valueobject.profile.UniInfoVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProfileDetailsPayloadToClient implements GenericReturnType {

   private String advertiserFlag;
   private String collegeName;
   private String universityCourseReviewURL;
   private ArrayList<UniInfoVO> uniInfo = null;
   private ArrayList<ProfileVO> uniProfileContent = null;
   private ArrayList<KeyStatsInfoVO> keyStatsInfo = null;
   private ArrayList<ReviewStarRatingVO> reviewStarRating = null;
   private ArrayList<StudentReviewVO> studentReview = null;
   private ArrayList<OpendayDetailsVO> opendayDetails = null;
   private ArrayList<EnquiryDetailsVO> enquiryDetails = null;
   private ArrayList<AccomodationVO> accomodationDetails = null;
   private ArrayList<CompetitorDetailVO> competitorDetails = null;
   private ArrayList<AppVersionVO> versionDetails = null;
   private String opendayFlag = null;
   private ArrayList<LoadYearListVO> loadYearList = null;
   private String uniDefaultBgImgPath;
   private String clearingResultFlag = null;
   private String guestUserFlag;
   //Added for mapbox key and COVID related article changes by Hemalatha K on 06_MAY_2020_REL
   private String mapBoxKey = null;   
   private String articleFlag = null;
   private String articleContent = null;
}
