package com.wuapp.service.pojo.json.userprofile;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class UserProfilePayloadFromClient implements GenericReturnType {

   private String userId;
   private String firstName;
   private String lastName;
   private String email;
   private String password;
   private String dateOfBirth;
   private String nationality;
   private String studyLevel;
   private String startDate;
   private String address;
   private String schoolOrCollegeName;
   private String predictedGrades;
   private String affiliateId;
   private String accessToken;
   private String appVersion;
   private String addressLineOne;
   private String addressLineTwo;
   private String city;
   private String countyState;
   private String postCode;
   private String school;
   private String nonUkSchool;
   private String preQualType;
   private String preQualGrade;
   private String receiveNoEmailFlag;
   private String permitEmail;
   private String solusEmail;
   private String marketingEmail;
   private String surveyEmail;
   private String yearOfEntry;
   private String privacyFlag;
   private String caTrackFalg;
   private String message;

   public String getMessage() {
	return message;
   }

   public void setMessage(String message) {
	this.message = message;
   }

   public String getPrivacyFlag() {
	return privacyFlag;
   }

   public void setPrivacyFlag(String privacyFlag) {
	this.privacyFlag = privacyFlag;
   }

   public String getCaTrackFalg() {
	return caTrackFalg;
   }

   public void setCaTrackFalg(String caTrackFalg) {
	this.caTrackFalg = caTrackFalg;
   }

   public String getCountyState() {
	return countyState;
   }

   public void setCountyState(String countyState) {
	this.countyState = countyState;
   }

   public String getYearOfEntry() {
	return yearOfEntry;
   }

   public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
   }

   public String getReceiveNoEmailFlag() {
	return receiveNoEmailFlag;
   }

   public void setReceiveNoEmailFlag(String receiveNoEmailFlag) {
	this.receiveNoEmailFlag = receiveNoEmailFlag;
   }

   public String getPermitEmail() {
	return permitEmail;
   }

   public void setPermitEmail(String permitEmail) {
	this.permitEmail = permitEmail;
   }

   public String getSolusEmail() {
	return solusEmail;
   }

   public void setSolusEmail(String solusEmail) {
	this.solusEmail = solusEmail;
   }

   public String getMarketingEmail() {
	return marketingEmail;
   }

   public void setMarketingEmail(String marketingEmail) {
	this.marketingEmail = marketingEmail;
   }

   public String getSurveyEmail() {
	return surveyEmail;
   }

   public void setSurveyEmail(String surveyEmail) {
	this.surveyEmail = surveyEmail;
   }

   public String getSchool() {
	return school;
   }

   public void setSchool(String school) {
	this.school = school;
   }

   public String getNonUkSchool() {
	return nonUkSchool;
   }

   public void setNonUkSchool(String nonUkSchool) {
	this.nonUkSchool = nonUkSchool;
   }

   public String getPreQualType() {
	return preQualType;
   }

   public void setPreQualType(String preQualType) {
	this.preQualType = preQualType;
   }

   public String getPreQualGrade() {
	return preQualGrade;
   }

   public void setPreQualGrade(String preQualGrade) {
	this.preQualGrade = preQualGrade;
   }

   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getFirstName() {
	return firstName;
   }

   public void setFirstName(String firstName) {
	this.firstName = firstName;
   }

   public String getLastName() {
	return lastName;
   }

   public void setLastName(String lastName) {
	this.lastName = lastName;
   }

   public String getEmail() {
	return email;
   }

   public void setEmail(String email) {
	this.email = email;
   }

   public String getPassword() {
	return password;
   }

   public void setPassword(String password) {
	this.password = password;
   }

   public String getDateOfBirth() {
	return dateOfBirth;
   }

   public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
   }

   public String getNationality() {
	return nationality;
   }

   public void setNationality(String nationality) {
	this.nationality = nationality;
   }

   public String getStudyLevel() {
	return studyLevel;
   }

   public void setStudyLevel(String studyLevel) {
	this.studyLevel = studyLevel;
   }

   public String getStartDate() {
	return startDate;
   }

   public void setStartDate(String startDate) {
	this.startDate = startDate;
   }

   public String getAddress() {
	return address;
   }

   public void setAddress(String address) {
	this.address = address;
   }

   public String getSchoolOrCollegeName() {
	return schoolOrCollegeName;
   }

   public void setSchoolOrCollegeName(String schoolOrCollegeName) {
	this.schoolOrCollegeName = schoolOrCollegeName;
   }

   public String getPredictedGrades() {
	return predictedGrades;
   }

   public void setPredictedGrades(String predictedGrades) {
	this.predictedGrades = predictedGrades;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }

   public String getAddressLineOne() {
	return addressLineOne;
   }

   public void setAddressLineOne(String addressLineOne) {
	this.addressLineOne = addressLineOne;
   }

   public String getAddressLineTwo() {
	return addressLineTwo;
   }

   public void setAddressLineTwo(String addressLineTwo) {
	this.addressLineTwo = addressLineTwo;
   }

   public String getCity() {
	return city;
   }

   public void setCity(String city) {
	this.city = city;
   }

   public String getPostCode() {
	return postCode;
   }

   public void setPostCode(String postCode) {
	this.postCode = postCode;
   }
}
