package com.wuapp.service.pojo.json.userprofile;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.userprofile.CountryListVO;
import com.wuapp.service.dao.util.valueobject.userprofile.PreviousQualVO;
import com.wuapp.service.dao.util.valueobject.userprofile.PrivacyCAFlagVO;
import com.wuapp.service.dao.util.valueobject.userprofile.QualificationVO;
import com.wuapp.service.dao.util.valueobject.userprofile.SchoolInfoVO;
import com.wuapp.service.dao.util.valueobject.userprofile.UserClientConsentVO;
import com.wuapp.service.dao.util.valueobject.userprofile.UserFlagVO;
import com.wuapp.service.dao.util.valueobject.userprofile.UserInfoVO;
import com.wuapp.service.dao.util.valueobject.userprofile.YearOfEntryVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class UserProfilePayloadToClient implements GenericReturnType {

   //
   ArrayList<UserInfoVO> userInfoList = null;
   ArrayList<CountryListVO> countriesList = null;
   ArrayList<AppVersionVO> versionDetails = null;
   ArrayList<YearOfEntryVO> yearOfEntryList = null;
   ArrayList<PreviousQualVO> previousQualList = null;
   ArrayList<UserFlagVO> userFlagList = null;
   ArrayList<UserFlagVO> editUserInfoList = null;
   ArrayList<PrivacyCAFlagVO> privacyCaFlagList = null;
   ArrayList<QualificationVO> qualificationList = null;
   ArrayList<SchoolInfoVO> schoolList = null;
   String successMsg = null;
   private String showClearingSwitchFlag;
   private String guestUserFlag;
   // Added for showing client consent details in preference page by Hemalatha.K on 17_DEC_19_REL
   ArrayList<UserClientConsentVO> userClientConsentList = null; 

   public ArrayList<UserClientConsentVO> getUserClientConsentList() {
      return userClientConsentList;
   }
   
   public void setUserClientConsentList(ArrayList<UserClientConsentVO> userClientConsentList) {
      this.userClientConsentList = userClientConsentList;
   }
   
   public String getGuestUserFlag() {
      return guestUserFlag;
   }

   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }

   public String getShowClearingSwitchFlag() {
	return showClearingSwitchFlag;
   }

   public void setShowClearingSwitchFlag(String showClearingSwitchFlag) {
	this.showClearingSwitchFlag = showClearingSwitchFlag;
   }

   //     
   public ArrayList<UserInfoVO> getUserInfoList() {
	return userInfoList;
   }

   public ArrayList<UserFlagVO> getEditUserInfoList() {
	return editUserInfoList;
   }

   public void setEditUserInfoList(ArrayList<UserFlagVO> editUserInfoList) {
	this.editUserInfoList = editUserInfoList;
   }

   public ArrayList<SchoolInfoVO> getSchoolList() {
	return schoolList;
   }

   public void setSchoolList(ArrayList<SchoolInfoVO> schoolList) {
	this.schoolList = schoolList;
   }

   public ArrayList<QualificationVO> getQualificationList() {
	return qualificationList;
   }

   public void setQualificationList(ArrayList<QualificationVO> qualificationList) {
	this.qualificationList = qualificationList;
   }

   public String getSuccessMsg() {
	return successMsg;
   }

   public void setSuccessMsg(String successMsg) {
	this.successMsg = successMsg;
   }

   public void setUserInfoList(ArrayList<UserInfoVO> userInfoList) {
	this.userInfoList = userInfoList;
   }

   public ArrayList<CountryListVO> getCountriesList() {
	return countriesList;
   }

   public void setCountriesList(ArrayList<CountryListVO> countriesList) {
	this.countriesList = countriesList;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }

   public ArrayList<YearOfEntryVO> getYearOfEntryList() {
	return yearOfEntryList;
   }

   public void setYearOfEntryList(ArrayList<YearOfEntryVO> yearOfEntryList) {
	this.yearOfEntryList = yearOfEntryList;
   }

   public ArrayList<PreviousQualVO> getPreviousQualList() {
	return previousQualList;
   }

   public void setPreviousQualList(ArrayList<PreviousQualVO> previousQualList) {
	this.previousQualList = previousQualList;
   }

   public ArrayList<UserFlagVO> getUserFlagList() {
	return userFlagList;
   }

   public void setUserFlagList(ArrayList<UserFlagVO> userFlagList) {
	this.userFlagList = userFlagList;
   }

   public ArrayList<PrivacyCAFlagVO> getPrivacyCaFlagList() {
	return privacyCaFlagList;
   }

   public void setPrivacyCaFlagList(ArrayList<PrivacyCAFlagVO> privacyCaFlagList) {
	this.privacyCaFlagList = privacyCaFlagList;
   }
}
