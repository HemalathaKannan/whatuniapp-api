package com.wuapp.service.pojo.json.coursedetials;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.ApplicantsDataVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.AssessedCourseVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.CourseInfoVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.EnquiryInfoVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.EntryReqVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.FeesDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.KeyStatsInfoVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.ModuleDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.OpportunityDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.OppurtunityEntryReqVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.PreviousSubjectVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.ReviewDetailsVO;
import com.wuapp.service.dao.util.valueobject.coursedetails.UniRankingVO;
import com.wuapp.service.dao.util.valueobject.profile.LoadYearListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class CourseDetailsPayloadToClient implements GenericReturnType {

   private String advertiserFlag;
   private String opportunityId;
   private String oppDetails;
   private String moduleInfo;
   private String universityCourseReviewURL;
   private ArrayList<CourseInfoVO> courseInfo = null;
   private ArrayList<EnquiryInfoVO> enquiryInfo = null;
   private ArrayList<ModuleDetailsVO> moduleDetails = null;
   private ArrayList<ReviewDetailsVO> reviewList = null;
   private ArrayList<OpportunityDetailsVO> opportunityDetails = null;
   private ArrayList<KeyStatsInfoVO> keyStatsInfo = null;
   private ArrayList<UniRankingVO> uniRankingDetails = null;
   private ArrayList<AssessedCourseVO> assessedCourse = null;
   private ArrayList<CourseInfoVO> otherCoursesPod = null;
   private ArrayList<ApplicantsDataVO> applicantsData = null;
   private ArrayList<PreviousSubjectVO> previousSubject = null;
   private ArrayList<EntryReqVO> entryRequirementList = null;
   private ArrayList<AppVersionVO> versionDetails = null;
   private ArrayList<LoadYearListVO> loadYearList = null;
   private String openDayFlag = null;
   private String clearingResultFlag = null;
   private String uniDefaultBgImgPath;
   private ArrayList<OppurtunityEntryReqVO> oppurtunityEntryReqList = null;
   private ArrayList<FeesDetailsVO> feesDetailsList = null;
   private String userId = null;
   private String videoUrl = null;
   private String applyNowUrl = null;
   private String guestUserFlag = null;
   private String thumbnailPath = null;
   private String interstitialFlag = null;
   
   
   
 



   
   
   
   public String getInterstitialFlag() {
      return interstitialFlag;
   }





   
   public void setInterstitialFlag(String interstitialFlag) {
      this.interstitialFlag = interstitialFlag;
   }





   public String getThumbnailPath() {
      return thumbnailPath;
   }




   
   public void setThumbnailPath(String thumbnailPath) {
      this.thumbnailPath = thumbnailPath;
   }




   public String getGuestUserFlag() {
      return guestUserFlag;
   }



   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }



   public String getApplyNowUrl() {
      return applyNowUrl;
   }


   
   public void setApplyNowUrl(String applyNowUrl) {
      this.applyNowUrl = applyNowUrl;
   }


   public String getUserId() {
      return userId;
   }

   
   public void setUserId(String userId) {
      this.userId = userId;
   }

   
   public String getVideoUrl() {
      return videoUrl;
   }

   
   public void setVideoUrl(String videoUrl) {
      this.videoUrl = videoUrl;
   }

   public ArrayList<OppurtunityEntryReqVO> getOppurtunityEntryReqList() {
	return oppurtunityEntryReqList;
   }

   public void setOppurtunityEntryReqList(ArrayList<OppurtunityEntryReqVO> oppurtunityEntryReqList) {
	this.oppurtunityEntryReqList = oppurtunityEntryReqList;
   }

   public ArrayList<FeesDetailsVO> getFeesDetailsList() {
	return feesDetailsList;
   }

   public void setFeesDetailsList(ArrayList<FeesDetailsVO> feesDetailsList) {
	this.feesDetailsList = feesDetailsList;
   }

   public String getClearingResultFlag() {
	return clearingResultFlag;
   }

   public void setClearingResultFlag(String clearingResultFlag) {
	this.clearingResultFlag = clearingResultFlag;
   }

   public String getOpenDayFlag() {
	return openDayFlag;
   }

   public void setOpenDayFlag(String openDayFlag) {
	this.openDayFlag = openDayFlag;
   }

   public ArrayList<LoadYearListVO> getLoadYearList() {
	return loadYearList;
   }

   public void setLoadYearList(ArrayList<LoadYearListVO> loadYearList) {
	this.loadYearList = loadYearList;
   }

   public String getModuleInfo() {
	return moduleInfo;
   }

   public void setModuleInfo(String moduleInfo) {
	this.moduleInfo = moduleInfo;
   }

   public String getUniversityCourseReviewURL() {
	return universityCourseReviewURL;
   }

   public void setUniversityCourseReviewURL(String universityCourseReviewURL) {
	this.universityCourseReviewURL = universityCourseReviewURL;
   }

   public ArrayList<EntryReqVO> getEntryRequirementList() {
	return entryRequirementList;
   }

   public void setEntryRequirementList(ArrayList<EntryReqVO> entryRequirementList) {
	this.entryRequirementList = entryRequirementList;
   }

   public ArrayList<PreviousSubjectVO> getPreviousSubject() {
	return previousSubject;
   }

   public void setPreviousSubject(ArrayList<PreviousSubjectVO> previousSubject) {
	this.previousSubject = previousSubject;
   }

   public String getAdvertiserFlag() {
	return advertiserFlag;
   }

   public void setAdvertiserFlag(String advertiserFlag) {
	this.advertiserFlag = advertiserFlag;
   }

   public String getOpportunityId() {
	return opportunityId;
   }

   public void setOpportunityId(String opportunityId) {
	this.opportunityId = opportunityId;
   }

   public String getOppDetails() {
	return oppDetails;
   }

   public void setOppDetails(String oppDetails) {
	this.oppDetails = oppDetails;
   }

   public ArrayList<CourseInfoVO> getCourseInfo() {
	return courseInfo;
   }

   public void setCourseInfo(ArrayList<CourseInfoVO> courseInfo) {
	this.courseInfo = courseInfo;
   }

   public ArrayList<EnquiryInfoVO> getEnquiryInfo() {
	return enquiryInfo;
   }

   public void setEnquiryInfo(ArrayList<EnquiryInfoVO> enquiryInfo) {
	this.enquiryInfo = enquiryInfo;
   }

   public ArrayList<ModuleDetailsVO> getModuleDetails() {
	return moduleDetails;
   }

   public void setModuleDetails(ArrayList<ModuleDetailsVO> moduleDetails) {
	this.moduleDetails = moduleDetails;
   }

   public ArrayList<ReviewDetailsVO> getReviewList() {
	return reviewList;
   }

   public void setReviewList(ArrayList<ReviewDetailsVO> reviewList) {
	this.reviewList = reviewList;
   }

   public ArrayList<OpportunityDetailsVO> getOpportunityDetails() {
	return opportunityDetails;
   }

   public void setOpportunityDetails(ArrayList<OpportunityDetailsVO> opportunityDetails) {
	this.opportunityDetails = opportunityDetails;
   }

   public ArrayList<KeyStatsInfoVO> getKeyStatsInfo() {
	return keyStatsInfo;
   }

   public void setKeyStatsInfo(ArrayList<KeyStatsInfoVO> keyStatsInfo) {
	this.keyStatsInfo = keyStatsInfo;
   }

   public ArrayList<UniRankingVO> getUniRankingDetails() {
	return uniRankingDetails;
   }

   public void setUniRankingDetails(ArrayList<UniRankingVO> uniRankingDetails) {
	this.uniRankingDetails = uniRankingDetails;
   }

   public ArrayList<AssessedCourseVO> getAssessedCourse() {
	return assessedCourse;
   }

   public void setAssessedCourse(ArrayList<AssessedCourseVO> assessedCourse) {
	this.assessedCourse = assessedCourse;
   }

   public ArrayList<CourseInfoVO> getOtherCoursesPod() {
	return otherCoursesPod;
   }

   public void setOtherCoursesPod(ArrayList<CourseInfoVO> otherCoursesPod) {
	this.otherCoursesPod = otherCoursesPod;
   }

   public ArrayList<ApplicantsDataVO> getApplicantsData() {
	return applicantsData;
   }

   public void setApplicantsData(ArrayList<ApplicantsDataVO> applicantsData) {
	this.applicantsData = applicantsData;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }

   public String getUniDefaultBgImgPath() {
	return uniDefaultBgImgPath;
   }

   public void setUniDefaultBgImgPath(String uniDefaultBgImgPath) {
	this.uniDefaultBgImgPath = uniDefaultBgImgPath;
   }
}
