package com.wuapp.service.pojo.json.coursedetials;

import lombok.Data;

@Data
public class CourseDetailsPayloadFromClient {

   private String userId;
   private String collegeId;
   private String courseId;
   private String opportunityId;
   private String userAgent;
   private String trackSessionId;
   private String region;
   private String Subject;
   private String qualification;
   private String regionFlag;
   private String studyMode;
   private String previousQual;
   private String previousQualGrade;
   private String locationType;
   private String appVersion;
   private String affiliateId;
   private String accessToken;
   private String assessmentType;
   private String searchKeyword;
   private String reviewSubjectOne;
   private String reviewSubjectTwo;
   private String reviewSubjectThree;
   private String userLatitude;
   private String userLongitude;

   public String getSubject() {
	return Subject;
   }

   public void setSubject(String subject) {
	Subject = subject;
   }
}
