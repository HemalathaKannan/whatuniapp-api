package com.wuapp.service.pojo.json.enquiry;

import com.wuapp.service.pojo.json.common.GenericReturnType;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class EnquiryFormPayloadFromClient implements GenericReturnType {

   //
   private String userId;
   private String institutionId;
   private String courseId;
   private String enquiryType;
   private String appVersion;
   private String affiliateId;
   private String accessToken;
   //
   private String yearOfEntry;
   private String userIP;
   private String userAgent;
   private String message;
   private String addressLineOne;
   private String addressLineTwo;
   private String city;
   private String countyState;
   private String postCode;
   private String suborderItemId;
   private String categoryId;
   private String categoryCode;
   private String qualification;
   private String searchKeyword;
   private String locationType;
   private String region;
   private String regionFlag;
   private String studyMode;
   private String previousQualification;
   private String previousQualificationGrade;
   private String assessmentType;
   private String reviewSubjectOne;
   private String reviewSubjectTwo;
   private String reviewSubjectThree;
   //
   private String openDate;
   private String eventId;
   //
   private String jsLog;
   private String profileId;
   private String typeName;
   private String activity;
   private String extraText;
   private String logStatusId;
   // Added for client lead consent by Hemalatha.K on 17_DEC_19_REL
   private String clientConsentFlag;   
   private String latitude;
   private String longitude;

   
   public String getLatitude() {
      return latitude;
   }

   
   public void setLatitude(String latitude) {
      this.latitude = latitude;
   }

   
   public String getLongitude() {
      return longitude;
   }

   
   public void setLongitude(String longitude) {
      this.longitude = longitude;
   }

   public String getClientConsentFlag() {
      return clientConsentFlag;
   }
   
   public void setClientConsentFlag(String clientConsentFlag) {
      this.clientConsentFlag = clientConsentFlag;
   }
   //
   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getInstitutionId() {
	return institutionId;
   }

   public void setInstitutionId(String institutionId) {
	this.institutionId = institutionId;
   }

   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }

   public String getEnquiryType() {
	return enquiryType;
   }

   public void setEnquiryType(String enquiryType) {
	this.enquiryType = enquiryType;
   }

   public String getYearOfEntry() {
	return yearOfEntry;
   }

   public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
   }

   public String getUserIP() {
	return userIP;
   }

   public void setUserIP(String userIP) {
	this.userIP = userIP;
   }

   public String getUserAgent() {
	return userAgent;
   }

   public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
   }

   public String getMessage() {
	return message;
   }

   public void setMessage(String message) {
	this.message = message;
   }

   public String getAddressLineOne() {
	return addressLineOne;
   }

   public void setAddressLineOne(String addressLineOne) {
	this.addressLineOne = addressLineOne;
   }

   public String getAddressLineTwo() {
	return addressLineTwo;
   }

   public void setAddressLineTwo(String addressLineTwo) {
	this.addressLineTwo = addressLineTwo;
   }

   public String getCity() {
	return city;
   }

   public void setCity(String city) {
	this.city = city;
   }

   public String getPostCode() {
	return postCode;
   }

   public void setPostCode(String postCode) {
	this.postCode = postCode;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getCategoryId() {
	return categoryId;
   }

   public void setCategoryId(String categoryId) {
	this.categoryId = categoryId;
   }

   public String getCategoryCode() {
	return categoryCode;
   }

   public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
   }

   public String getQualification() {
	return qualification;
   }

   public void setQualification(String qualification) {
	this.qualification = qualification;
   }

   public String getLocationType() {
	return locationType;
   }

   public void setLocationType(String locationType) {
	this.locationType = locationType;
   }

   public String getRegion() {
	return region;
   }

   public void setRegion(String region) {
	this.region = region;
   }

   public String getRegionFlag() {
	return regionFlag;
   }

   public void setRegionFlag(String regionFlag) {
	this.regionFlag = regionFlag;
   }

   public String getStudyMode() {
	return studyMode;
   }

   public void setStudyMode(String studyMode) {
	this.studyMode = studyMode;
   }

   public String getPreviousQualification() {
	return previousQualification;
   }

   public void setPreviousQualification(String previousQualification) {
	this.previousQualification = previousQualification;
   }

   public String getPreviousQualificationGrade() {
	return previousQualificationGrade;
   }

   public void setPreviousQualificationGrade(String previousQualificationGrade) {
	this.previousQualificationGrade = previousQualificationGrade;
   }

   public String getSearchKeyword() {
	return searchKeyword;
   }

   public void setSearchKeyword(String searchKeyword) {
	this.searchKeyword = searchKeyword;
   }

   public String getOpenDate() {
	return openDate;
   }

   public void setOpenDate(String openDate) {
	this.openDate = openDate;
   }

   public String getEventId() {
	return eventId;
   }

   public void setEventId(String eventId) {
	this.eventId = eventId;
   }

   public String getCountyState() {
	return countyState;
   }

   public void setCountyState(String countyState) {
	this.countyState = countyState;
   }

   public String getJsLog() {
	return jsLog;
   }

   public void setJsLog(String jsLog) {
	this.jsLog = jsLog;
   }

   public String getProfileId() {
	return profileId;
   }

   public void setProfileId(String profileId) {
	this.profileId = profileId;
   }

   public String getTypeName() {
	return typeName;
   }

   public void setTypeName(String typeName) {
	this.typeName = typeName;
   }

   public String getActivity() {
	return activity;
   }

   public void setActivity(String activity) {
	this.activity = activity;
   }

   public String getExtraText() {
	return extraText;
   }

   public void setExtraText(String extraText) {
	this.extraText = extraText;
   }

   public String getLogStatusId() {
	return logStatusId;
   }

   public void setLogStatusId(String logStatusId) {
	this.logStatusId = logStatusId;
   }

   public String getAssessmentType() {
	return assessmentType;
   }

   public void setAssessmentType(String assessmentType) {
	this.assessmentType = assessmentType;
   }

   public String getReviewSubjectOne() {
	return reviewSubjectOne;
   }

   public void setReviewSubjectOne(String reviewSubjectOne) {
	this.reviewSubjectOne = reviewSubjectOne;
   }

   public String getReviewSubjectTwo() {
	return reviewSubjectTwo;
   }

   public void setReviewSubjectTwo(String reviewSubjectTwo) {
	this.reviewSubjectTwo = reviewSubjectTwo;
   }

   public String getReviewSubjectThree() {
	return reviewSubjectThree;
   }

   public void setReviewSubjectThree(String reviewSubjectThree) {
	this.reviewSubjectThree = reviewSubjectThree;
   }
}
