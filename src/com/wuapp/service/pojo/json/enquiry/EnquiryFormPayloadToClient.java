package com.wuapp.service.pojo.json.enquiry;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.enquiry.AjaxUserAddressVO;
import com.wuapp.service.dao.util.valueobject.enquiry.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.enquiry.OtherCoursesVO;
import com.wuapp.service.dao.util.valueobject.enquiry.OtherInstitutionsVO;
import com.wuapp.service.dao.util.valueobject.enquiry.UserAddressVO;
import com.wuapp.service.dao.util.valueobject.enquiry.YearListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class EnquiryFormPayloadToClient implements GenericReturnType {

   //
   ArrayList<InstitutionVO> institutionDetailsList;
   ArrayList<UserAddressVO> userAddressList;
   ArrayList<YearListVO> yearList;
   ArrayList<AppVersionVO> versionDetails;
   ArrayList<OtherInstitutionsVO> otherInstitutionsList;
   ArrayList<OtherCoursesVO> otherCoursesList;
   ArrayList<AjaxUserAddressVO> ajaxAddressList;
   String yearOfEntry;
   String returnFlag;
   String guestUserFlag;
   // Added for client lead consent by Hemalatha.K on 17_DEC_19_REL
   private String clientConsentFlag;
   private String clientConsentText;

   public String getClientConsentFlag() {
      return clientConsentFlag;
   }
   
   public void setClientConsentFlag(String clientConsentFlag) {
      this.clientConsentFlag = clientConsentFlag;
   }
   
   public String getClientConsentText() {
      return clientConsentText;
   }
   
   public void setClientConsentText(String clientConsentText) {
      this.clientConsentText = clientConsentText;
   }
   public String getGuestUserFlag() {
      return guestUserFlag;
   }

   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }

   //   
   public ArrayList<InstitutionVO> getInstitutionDetailsList() {
	return institutionDetailsList;
   }

   public void setInstitutionDetailsList(ArrayList<InstitutionVO> institutionDetailsList) {
	this.institutionDetailsList = institutionDetailsList;
   }

   public ArrayList<UserAddressVO> getUserAddressList() {
	return userAddressList;
   }

   public void setUserAddressList(ArrayList<UserAddressVO> userAddressList) {
	this.userAddressList = userAddressList;
   }

   public ArrayList<YearListVO> getYearList() {
	return yearList;
   }

   public void setYearList(ArrayList<YearListVO> yearList) {
	this.yearList = yearList;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }

   public String getYearOfEntry() {
	return yearOfEntry;
   }

   public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
   }

   public ArrayList<OtherInstitutionsVO> getOtherInstitutionsList() {
	return otherInstitutionsList;
   }

   public void setOtherInstitutionsList(ArrayList<OtherInstitutionsVO> otherInstitutionsList) {
	this.otherInstitutionsList = otherInstitutionsList;
   }

   public ArrayList<OtherCoursesVO> getOtherCoursesList() {
	return otherCoursesList;
   }

   public void setOtherCoursesList(ArrayList<OtherCoursesVO> otherCoursesList) {
	this.otherCoursesList = otherCoursesList;
   }

   public ArrayList<AjaxUserAddressVO> getAjaxAddressList() {
	return ajaxAddressList;
   }

   public void setAjaxAddressList(ArrayList<AjaxUserAddressVO> ajaxAddressList) {
	this.ajaxAddressList = ajaxAddressList;
   }

   public String getReturnFlag() {
	return returnFlag;
   }

   public void setReturnFlag(String returnFlag) {
	this.returnFlag = returnFlag;
   }
}
