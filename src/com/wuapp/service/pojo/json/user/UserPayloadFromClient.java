package com.wuapp.service.pojo.json.user;

import com.wuapp.service.pojo.json.common.GenericReturnType;

/**
 * 
 * @since wuapp1.0_ - Initial draft
 * @version 1.0
 * Class : UserPayloadFromClient.java
 * Description : This class used to get user details for registration.
 * Change Log :
 * *************************************************************************************************************
 * Author        Modification Id     Modified On                 Modification Details Change
 * *************************************************************************************************************
 * Hemalatha K       1.1             31_MAR_2020             Added latitude and longitude for location tracking
 */

public class UserPayloadFromClient implements GenericReturnType {

   private String userId;
   private String firstName;
   private String lastName;
   private String email;
   private String password;
   private String appVersion;
   private String affiliateId;
   private String accessToken;
   private String userIp;
   private String userAgent;
   private String ageRange;
   private String locale;
   private String userFriendList;
   private String fbUserId;
   private String latitude;
   private String longitude;

   //
   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getFirstName() {
	return firstName;
   }

   public void setFirstName(String firstName) {
	this.firstName = firstName;
   }

   public String getLastName() {
	return lastName;
   }

   public void setLastName(String lastName) {
	this.lastName = lastName;
   }

   public String getEmail() {
	return email;
   }

   public void setEmail(String email) {
	this.email = email;
   }

   public String getPassword() {
	return password;
   }

   public void setPassword(String password) {
	this.password = password;
   }

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }

   public String getUserIp() {
	return userIp;
   }

   public void setUserIp(String userIp) {
	this.userIp = userIp;
   }

   public String getUserAgent() {
	return userAgent;
   }

   public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
   }

   public String getAgeRange() {
	return ageRange;
   }

   public void setAgeRange(String ageRange) {
	this.ageRange = ageRange;
   }

   public String getLocale() {
	return locale;
   }

   public void setLocale(String locale) {
	this.locale = locale;
   }

   public String getUserFriendList() {
	return userFriendList;
   }

   public void setUserFriendList(String userFriendList) {
	this.userFriendList = userFriendList;
   }

   public String getFbUserId() {
	return fbUserId;
   }

   public void setFbUserId(String fbUserId) {
	this.fbUserId = fbUserId;
   }
   
   public String getLatitude() {
      return latitude;
   }
   
   public void setLatitude(String latitude) {
      this.latitude = latitude;
   }
   
   public String getLongitude() {
      return longitude;
   }
   
   public void setLongitude(String longitude) {
      this.longitude = longitude;
   }   
}
