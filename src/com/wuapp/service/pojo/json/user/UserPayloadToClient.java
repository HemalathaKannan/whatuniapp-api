package com.wuapp.service.pojo.json.user;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.user.UserDetailsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Data;

@Data
public class UserPayloadToClient implements GenericReturnType {

   private String userId;
   private String responseCode;
   private String errorMessage;
   private String password;
   private String userExistFlag;
   private ArrayList<AppVersionVO> versionDetails = null;
   private ArrayList<UserDetailsVO> userDetails = null;
   private String appClearingFlag;
   private String guestUserFlag;
   private String interstitialFlag;
   private String appClearingBanner = null;
   private String appClearingurl = null;
}
