package com.wuapp.service.pojo.json.user;

import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Data;

@Data
public class GuestUserPayLoadToClient implements GenericReturnType {
   private String guestUserId;
   private String guestUserFlag;
   private String clearingFlag;
   private String interstitialFlag;
   private String appClearingBanner = null;
   private String appClearingurl = null;
}
