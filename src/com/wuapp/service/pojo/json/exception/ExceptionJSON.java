package com.wuapp.service.pojo.json.exception;

import com.wuapp.service.pojo.json.common.GenericReturnType;

/**
 * Producing exception related details to client
 *
 * @author Bhoovi arashan P
 * @since wuapp1.0_20170315 - initial draft
 * @version 1.1
 */
public class ExceptionJSON implements GenericReturnType {

   private String status;
   private String message;
   private String exception;

   public String toString() {
	StringBuffer sf = new StringBuffer();
	sf.append("\n ExceptionJSON.status = ");
	sf.append(status);
	sf.append("\n ExceptionJSON.message = ");
	sf.append(message);
	sf.append("\n ExceptionJSON.exception = ");
	sf.append(exception);
	//
	return sf.toString();
   }

   public String getStatus() {
	return status;
   }

   public void setStatus(String status) {
	this.status = status;
   }

   public String getMessage() {
	return message;
   }

   public void setMessage(String message) {
	this.message = message;
   }

   public String getException() {
	return exception;
   }

   public void setException(String exception) {
	this.exception = exception;
   }
}
