package com.wuapp.service.pojo.json.provider;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.filters.AssesmentTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.PreviousQualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.QualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.ReviewCategoryFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.StudyModeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.SubjectFilterVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.dao.util.valueobject.provider.InstitutionDetailsVO;
import com.wuapp.service.dao.util.valueobject.provider.ProviderResultsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class ProviderResultsPayloadToClient implements GenericReturnType {

   private ArrayList<ProviderResultsVO> searchResults = null;
   private ArrayList<InstitutionDetailsVO> institutionDetails = null;
   private ArrayList<AppVersionVO> versionDetails = null;
   private ArrayList<QualificationFilterVO> qualificationFilters = null;
   private ArrayList<SubjectFilterVO> subjectfilters = null;
   private ArrayList<LocationFilterVO> locationfilters = null;
   private ArrayList<LocationTypeFilterVO> locationTypeFilters = null;
   private ArrayList<StudyModeFilterVO> studyModeFilters = null;
   private ArrayList<PreviousQualificationFilterVO> previousQualificationfilters = null;
   private ArrayList<AssesmentTypeFilterVO> assessmentTypeFilterDetails = null;
   private ArrayList<ReviewCategoryFilterVO> reviewCategoryFilterDetails = null;
   private ArrayList<SubjectVO> orderByName = null;
   private String advertFlag;
   private String overAllCourseCount;
   private String uniDefaultBgImgPath;
   private String clearingResultFlag;
   private String guestUserFlag;

   
   public String getGuestUserFlag() {
      return guestUserFlag;
   }

   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }

   public String getClearingResultFlag() {
	return clearingResultFlag;
   }

   public void setClearingResultFlag(String clearingResultFlag) {
	this.clearingResultFlag = clearingResultFlag;
   }

   public ArrayList<ReviewCategoryFilterVO> getReviewCategoryFilterDetails() {
	return reviewCategoryFilterDetails;
   }

   public void setReviewCategoryFilterDetails(ArrayList<ReviewCategoryFilterVO> reviewCategoryFilterDetails) {
	this.reviewCategoryFilterDetails = reviewCategoryFilterDetails;
   }

   public String getAdvertFlag() {
	return advertFlag;
   }

   public void setAdvertFlag(String advertFlag) {
	this.advertFlag = advertFlag;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }

   public String getOverAllCourseCount() {
	return overAllCourseCount;
   }

   public void setOverAllCourseCount(String overAllCourseCount) {
	this.overAllCourseCount = overAllCourseCount;
   }

   public ArrayList<SubjectVO> getOrderByName() {
	return orderByName;
   }

   public void setOrderByName(ArrayList<SubjectVO> orderByName) {
	this.orderByName = orderByName;
   }

   public ArrayList<ProviderResultsVO> getSearchResults() {
	return searchResults;
   }

   public void setSearchResults(ArrayList<ProviderResultsVO> searchResults) {
	this.searchResults = searchResults;
   }

   public ArrayList<InstitutionDetailsVO> getInstitutionDetails() {
	return institutionDetails;
   }

   public void setInstitutionDetails(ArrayList<InstitutionDetailsVO> institutionDetails) {
	this.institutionDetails = institutionDetails;
   }

   public ArrayList<QualificationFilterVO> getQualificationFilters() {
	return qualificationFilters;
   }

   public void setQualificationFilters(ArrayList<QualificationFilterVO> qualificationFilters) {
	this.qualificationFilters = qualificationFilters;
   }

   public ArrayList<SubjectFilterVO> getSubjectfilters() {
	return subjectfilters;
   }

   public void setSubjectfilters(ArrayList<SubjectFilterVO> subjectfilters) {
	this.subjectfilters = subjectfilters;
   }

   public ArrayList<LocationFilterVO> getLocationfilters() {
	return locationfilters;
   }

   public void setLocationfilters(ArrayList<LocationFilterVO> locationfilters) {
	this.locationfilters = locationfilters;
   }

   public ArrayList<LocationTypeFilterVO> getLocationTypeFilters() {
	return locationTypeFilters;
   }

   public void setLocationTypeFilters(ArrayList<LocationTypeFilterVO> locationTypeFilters) {
	this.locationTypeFilters = locationTypeFilters;
   }

   public ArrayList<StudyModeFilterVO> getStudyModeFilters() {
	return studyModeFilters;
   }

   public void setStudyModeFilters(ArrayList<StudyModeFilterVO> studyModeFilters) {
	this.studyModeFilters = studyModeFilters;
   }

   public ArrayList<PreviousQualificationFilterVO> getPreviousQualificationfilters() {
	return previousQualificationfilters;
   }

   public void setPreviousQualificationfilters(ArrayList<PreviousQualificationFilterVO> previousQualificationfilters) {
	this.previousQualificationfilters = previousQualificationfilters;
   }

   public ArrayList<AssesmentTypeFilterVO> getAssessmentTypeFilterDetails() {
	return assessmentTypeFilterDetails;
   }

   public void setAssessmentTypeFilterDetails(ArrayList<AssesmentTypeFilterVO> assessmentTypeFilterDetails) {
	this.assessmentTypeFilterDetails = assessmentTypeFilterDetails;
   }

   public String getUniDefaultBgImgPath() {
	return uniDefaultBgImgPath;
   }

   public void setUniDefaultBgImgPath(String uniDefaultBgImgPath) {
	this.uniDefaultBgImgPath = uniDefaultBgImgPath;
   }
}
