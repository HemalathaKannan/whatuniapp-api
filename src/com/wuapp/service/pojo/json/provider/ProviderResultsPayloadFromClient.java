package com.wuapp.service.pojo.json.provider;

import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Data;

@Data
public class ProviderResultsPayloadFromClient implements GenericReturnType {

   private String x;
   private String y;
   private String searchWhat;
   private String phraseSearch;
   private String institutionId;
   private String qualification;
   private String city;
   private String postCode;
   private String studyMode;
   private String searchHow;
   private String searchCategory;
   private String searchCategoryId;
   private String userAgent;
   private String pageNo;
   private String fromPage;
   private String toPage;
   private String pageName;
   private String entryLevel;
   private String entryPoints;
   private String universityLocationType;
   private String searchUrl;
   private String trackSessionId;
   private String networkId;
   private String affiliateId;
   private String accessToken;
   private String locationType;
   private String region;
   private String regionFlag;
   private String appVersion;
   private String userId;
   private String orderBy;
   private String assessmentType;
   private String reviewSubjectOne;
   private String reviewSubjectTwo;
   private String reviewSubjectThree;
   private String jacsCode;
   private String userIp;
   private String latitude;
   private String longitude;
}
