package com.wuapp.service.pojo.json.interaction;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class GetClearingFlagPayloadToClient implements GenericReturnType {

   private String clearingFlag;

   public String getClearingFlag() {
	return clearingFlag;
   }

   public void setClearingFlag(String clearingFlag) {
	this.clearingFlag = clearingFlag;
   }
}
