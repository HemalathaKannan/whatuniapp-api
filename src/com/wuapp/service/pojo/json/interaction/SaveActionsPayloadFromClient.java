package com.wuapp.service.pojo.json.interaction;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class SaveActionsPayloadFromClient implements GenericReturnType {

   private String userId = null;
   private String actionType = null;
   private String actionDate = null;
   private String actionStatus = null;
   private String actionUrl = null;
   private String actionKeyId = null;
   private String actionDetails = null;
   private String trackSessionId = null;
   private String accessToken = null;
   private String affiliateId = null;
   private String appVersion = null;

   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getActionType() {
	return actionType;
   }

   public void setActionType(String actionType) {
	this.actionType = actionType;
   }

   public String getActionDate() {
	return actionDate;
   }

   public void setActionDate(String actionDate) {
	this.actionDate = actionDate;
   }

   public String getActionStatus() {
	return actionStatus;
   }

   public void setActionStatus(String actionStatus) {
	this.actionStatus = actionStatus;
   }

   public String getActionUrl() {
	return actionUrl;
   }

   public void setActionUrl(String actionUrl) {
	this.actionUrl = actionUrl;
   }

   public String getActionKeyId() {
	return actionKeyId;
   }

   public void setActionKeyId(String actionKeyId) {
	this.actionKeyId = actionKeyId;
   }

   public String getActionDetails() {
	return actionDetails;
   }

   public void setActionDetails(String actionDetails) {
	this.actionDetails = actionDetails;
   }

   public String getTrackSessionId() {
	return trackSessionId;
   }

   public void setTrackSessionId(String trackSessionId) {
	this.trackSessionId = trackSessionId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }
}
