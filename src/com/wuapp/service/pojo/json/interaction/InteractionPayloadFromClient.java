package com.wuapp.service.pojo.json.interaction;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class InteractionPayloadFromClient implements GenericReturnType {

   private String userId;
   private String collegeId;
   private String courseId;
   private String userIp;
   private String userAgent;
   private String jsLog;
   private String suborderItemId;
   private String profileId;
   private String typeName;
   private String activity;
   private String extraText;
   private String appVersion;
   private String affiliateId;
   private String accessToken;
   private String logStatusId;
   private String studyMode; 
   private String latitude;
   private String longitude;
   
   public String getLatitude() {
      return latitude;
   }
   
   public void setLatitude(String latitude) {
      this.latitude = latitude;
   }
   
   public String getLongitude() {
      return longitude;
   }
   
   public void setLongitude(String longitude) {
      this.longitude = longitude;
   }
   
   public String getStudyMode() {
      return studyMode;
   }
   
   public void setStudyMode(String studyMode) {
      this.studyMode = studyMode;
   }

   public String getExtraText() {
	return extraText;
   }

   public void setExtraText(String extraText) {
	this.extraText = extraText;
   }

   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getCourseId() {
	return courseId;
   }

   public void setCourseId(String courseId) {
	this.courseId = courseId;
   }

   public String getUserIp() {
	return userIp;
   }

   public void setUserIp(String userIp) {
	this.userIp = userIp;
   }

   public String getUserAgent() {
	return userAgent;
   }

   public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
   }

   public String getJsLog() {
	return jsLog;
   }

   public void setJsLog(String jsLog) {
	this.jsLog = jsLog;
   }

   public String getSuborderItemId() {
	return suborderItemId;
   }

   public void setSuborderItemId(String suborderItemId) {
	this.suborderItemId = suborderItemId;
   }

   public String getProfileId() {
	return profileId;
   }

   public void setProfileId(String profileId) {
	this.profileId = profileId;
   }

   public String getTypeName() {
	return typeName;
   }

   public void setTypeName(String typeName) {
	this.typeName = typeName;
   }

   public String getActivity() {
	return activity;
   }

   public void setActivity(String activity) {
	this.activity = activity;
   }

   public String getLogStatusId() {
	return logStatusId;
   }

   public void setLogStatusId(String logStatusId) {
	this.logStatusId = logStatusId;
   }

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }
}
