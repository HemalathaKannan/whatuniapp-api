package com.wuapp.service.pojo.json.interaction;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class GetClearingFlagPayloadFromClient implements GenericReturnType {

   private String appVersion;
   private String affiliateId;
   private String accessToken;
   private String flagName;

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }

   public String getFlagName() {
	return flagName;
   }

   public void setFlagName(String flagName) {
	this.flagName = flagName;
   }
}
