package com.wuapp.service.pojo.json.course;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class SearchAjaxInstitutionPayloadToClient implements GenericReturnType {

   private ArrayList<InstitutionVO> recentInstitutions = null;
   private ArrayList<InstitutionVO> institutionsCloseToYou = null;
   private ArrayList<InstitutionVO> suggestedInstitutions = null;
   private ArrayList<AppVersionVO> versionDetails = null;

   public ArrayList<InstitutionVO> getRecentInstitutions() {
	return recentInstitutions;
   }

   public void setRecentInstitutions(ArrayList<InstitutionVO> recentInstitutions) {
	this.recentInstitutions = recentInstitutions;
   }

   public ArrayList<InstitutionVO> getInstitutionsCloseToYou() {
	return institutionsCloseToYou;
   }

   public void setInstitutionsCloseToYou(ArrayList<InstitutionVO> institutionsCloseToYou) {
	this.institutionsCloseToYou = institutionsCloseToYou;
   }

   public ArrayList<InstitutionVO> getSuggestedInstitutions() {
	return suggestedInstitutions;
   }

   public void setSuggestedInstitutions(ArrayList<InstitutionVO> suggestedInstitutions) {
	this.suggestedInstitutions = suggestedInstitutions;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }
}
