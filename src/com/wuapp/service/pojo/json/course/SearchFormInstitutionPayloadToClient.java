package com.wuapp.service.pojo.json.course;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class SearchFormInstitutionPayloadToClient implements GenericReturnType {

   private String userId;
   private String userName;
   private ArrayList<InstitutionVO> recentInstitutions = null;
   private ArrayList<InstitutionVO> institutionsCloseToYou = null;
   private ArrayList<AppVersionVO> versionDetails = null;

   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getUserName() {
	return userName;
   }

   public void setUserName(String userName) {
	this.userName = userName;
   }

   public ArrayList<InstitutionVO> getRecentInstitutions() {
	return recentInstitutions;
   }

   public void setRecentInstitutions(ArrayList<InstitutionVO> recentInstitutions) {
	this.recentInstitutions = recentInstitutions;
   }

   public ArrayList<InstitutionVO> getInstitutionsCloseToYou() {
	return institutionsCloseToYou;
   }

   public void setInstitutionsCloseToYou(ArrayList<InstitutionVO> institutionsCloseToYou) {
	this.institutionsCloseToYou = institutionsCloseToYou;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }
}
