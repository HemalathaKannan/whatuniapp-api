package com.wuapp.service.pojo.json.course;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class SearchFormSubjectPayloadToClient implements GenericReturnType {

   private String userId;
   private String userName;
   private ArrayList<SubjectVO> recentSubjects = null;
   private ArrayList<SubjectVO> nearSubjects = null;
   private ArrayList<AppVersionVO> versionDetails = null;

   //
   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getUserName() {
	return userName;
   }

   public void setUserName(String userName) {
	this.userName = userName;
   }

   public ArrayList<SubjectVO> getRecentSubjects() {
	return recentSubjects;
   }

   public void setRecentSubjects(ArrayList<SubjectVO> recentSubjects) {
	this.recentSubjects = recentSubjects;
   }

   public ArrayList<SubjectVO> getNearSubjects() {
	return nearSubjects;
   }

   public void setNearSubjects(ArrayList<SubjectVO> nearSubjects) {
	this.nearSubjects = nearSubjects;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }
}
