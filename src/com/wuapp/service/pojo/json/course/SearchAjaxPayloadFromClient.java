package com.wuapp.service.pojo.json.course;

import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Data;

@Data
public class SearchAjaxPayloadFromClient implements GenericReturnType {

   private String userId;
   private String searchKeyword;
   private String qualificationCode;
   private String accessToken;
   private String affiliateId;
   private String latitude;
   private String longitude;
   private String appVersion;
}
