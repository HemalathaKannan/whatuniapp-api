package com.wuapp.service.pojo.json.course;

import lombok.Data;

@Data
public class SearchResultsPayloadFromClient {

   private String userId;
   private String categoryCode;
   private String categoryId;
   private String qualification;
   private String recordCountPerPage;
   private String pageno;
   private String appVersion;
   private String affiliateId;
   private String accessToken;
   private String keywordSearch;
   private String userAgent;
   private String searchUrl;
   private String orderBy;
   private String latitude;
   private String longtitude;
   private String locationType;
   private String region;
   private String fromPageNo;
   private String toPageNo;
   private String regionFlag;
   private String studyMode;
   private String previousQualification;
   private String previousQualificationGrade;
   private String institutionId;
   private String assessmentType;
   private String reviewSubjectOne;
   private String reviewSubjectTwo;
   private String reviewSubjectThree;
   private String jacsCode;
}
