package com.wuapp.service.pojo.json.course;

import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Data;

@Data
public class SearchFormPayloadFromClient implements GenericReturnType {

   private String userId;
   private String latitude;
   private String longitude;
   private String recentSubjectCount;
   private String recentInstitutionCount;
   private String institutionsCloseToYouCount;
   private String appVersion;
   private String accessToken;
   private String affiliateId;
   private String qualificationCode;
}
