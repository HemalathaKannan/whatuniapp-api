package com.wuapp.service.pojo.json.course;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class SearchAjaxSubjectPayloadToClient implements GenericReturnType {

   private ArrayList<SubjectVO> recentSubjects = null;
   private ArrayList<SubjectVO> suggestedSubjects = null;
   private ArrayList<AppVersionVO> versionDetails = null;

   public ArrayList<SubjectVO> getRecentSubjects() {
	return recentSubjects;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }

   public void setRecentSubjects(ArrayList<SubjectVO> recentSubjects) {
	this.recentSubjects = recentSubjects;
   }

   public ArrayList<SubjectVO> getSuggestedSubjects() {
	return suggestedSubjects;
   }

   public void setSuggestedSubjects(ArrayList<SubjectVO> suggestedSubjects) {
	this.suggestedSubjects = suggestedSubjects;
   }
}
