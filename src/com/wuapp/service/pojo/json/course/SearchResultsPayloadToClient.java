package com.wuapp.service.pojo.json.course;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.filters.AssesmentTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.LocationTypeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.PreviousQualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.QualificationFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.ReviewCategoryFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.StudyModeFilterVO;
import com.wuapp.service.dao.util.valueobject.filters.SubjectFilterVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.dao.util.valueobject.searchresults.SearchResultsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class SearchResultsPayloadToClient implements GenericReturnType {

   private String userId;
   private String subjectTitle;
   private String subjectTileMediaPath;
   private String subjectTileMediaId;
   private String subjectMediaPath;
   private String iconMediaPath;
   private String totalCourseCount;
   private ArrayList<SearchResultsVO> searchResults = null;
   private ArrayList<AppVersionVO> versionDetails = null;
   private ArrayList<QualificationFilterVO> qualificationFilters = null;
   private ArrayList<SubjectFilterVO> subjectfilters = null;
   private ArrayList<LocationFilterVO> locationfilters = null;
   private ArrayList<LocationTypeFilterVO> locationTypeFilters = null;
   private ArrayList<StudyModeFilterVO> studyModeFilters = null;
   private ArrayList<PreviousQualificationFilterVO> previousQualificationfilters = null;
   private ArrayList<AssesmentTypeFilterVO> assessmentTypeFilterDetails = null;
   private ArrayList<ReviewCategoryFilterVO> reviewCategoryFilterDetails = null;
   private String totalUniversityCount;
   private ArrayList<SubjectVO> orderByName;
   private String uniDefaultBgImgPath;
   private String clearingResultFlag = null;
   private String guestUserFlag = null;

   
   public String getGuestUserFlag() {
      return guestUserFlag;
   }

   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }

   public String getClearingResultFlag() {
	return clearingResultFlag;
   }

   public void setClearingResultFlag(String clearingResultFlag) {
	this.clearingResultFlag = clearingResultFlag;
   }

   public ArrayList<SubjectVO> getOrderByName() {
	return orderByName;
   }

   public void setOrderByName(ArrayList<SubjectVO> orderByName) {
	this.orderByName = orderByName;
   }

   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getSubjectTitle() {
	return subjectTitle;
   }

   public void setSubjectTitle(String subjectTitle) {
	this.subjectTitle = subjectTitle;
   }

   public void setSearchResults(ArrayList<SearchResultsVO> searchResults2) {
	this.searchResults = searchResults2;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }

   public String getSubjectTileMediaPath() {
	return subjectTileMediaPath;
   }

   public void setSubjectTileMediaPath(String subjectTileMediaPath) {
	this.subjectTileMediaPath = subjectTileMediaPath;
   }

   public String getTotalCourseCount() {
	return totalCourseCount;
   }

   public void setTotalCourseCount(String totalCourseCount) {
	this.totalCourseCount = totalCourseCount;
   }

   public String getSubjectTileMediaId() {
	return subjectTileMediaId;
   }

   public void setSubjectTileMediaId(String subjectTileMediaId) {
	this.subjectTileMediaId = subjectTileMediaId;
   }

   public String getIconMediaPath() {
	return iconMediaPath;
   }

   public void setIconMediaPath(String iconMediaPath) {
	this.iconMediaPath = iconMediaPath;
   }

   public void setQualificationFilters(ArrayList<QualificationFilterVO> qualificationDetails) {
	this.qualificationFilters = qualificationDetails;
   }

   public void setSubjectfilters(ArrayList<SubjectFilterVO> subjectDetails) {
	this.subjectfilters = subjectDetails;
   }

   public void setLocationfilters(ArrayList<LocationFilterVO> locationDetails) {
	this.locationfilters = locationDetails;
   }

   public void setLocationTypeFilters(ArrayList<LocationTypeFilterVO> locationTypeDetails) {
	this.locationTypeFilters = locationTypeDetails;
   }

   public void setStudyModeFilters(ArrayList<StudyModeFilterVO> studyModeDetails) {
	this.studyModeFilters = studyModeDetails;
   }

   public void setPreviousQualificationfilters(ArrayList<PreviousQualificationFilterVO> previousQualificationDetails) {
	this.previousQualificationfilters = previousQualificationDetails;
   }

   public String getTotalUniversityCount() {
	return totalUniversityCount;
   }

   public void setTotalUniversityCount(String totalUniversityCount) {
	this.totalUniversityCount = totalUniversityCount;
   }

   public ArrayList<SearchResultsVO> getSearchResults() {
	return searchResults;
   }

   public ArrayList<QualificationFilterVO> getQualificationFilters() {
	return qualificationFilters;
   }

   public ArrayList<SubjectFilterVO> getSubjectfilters() {
	return subjectfilters;
   }

   public ArrayList<LocationFilterVO> getLocationfilters() {
	return locationfilters;
   }

   public ArrayList<LocationTypeFilterVO> getLocationTypeFilters() {
	return locationTypeFilters;
   }

   public ArrayList<StudyModeFilterVO> getStudyModeFilters() {
	return studyModeFilters;
   }

   public ArrayList<PreviousQualificationFilterVO> getPreviousQualificationfilters() {
	return previousQualificationfilters;
   }

   public ArrayList<AssesmentTypeFilterVO> getAssessmentTypeFilterDetails() {
	return assessmentTypeFilterDetails;
   }

   public void setAssessmentTypeFilterDetails(ArrayList<AssesmentTypeFilterVO> assessmentTypeFilterDetails) {
	this.assessmentTypeFilterDetails = assessmentTypeFilterDetails;
   }

   public ArrayList<ReviewCategoryFilterVO> getReviewCategoryFilterDetails() {
	return reviewCategoryFilterDetails;
   }

   public void setReviewCategoryFilterDetails(ArrayList<ReviewCategoryFilterVO> reviewCategoryFilterDetails) {
	this.reviewCategoryFilterDetails = reviewCategoryFilterDetails;
   }

   public String getSubjectMediaPath() {
	return subjectMediaPath;
   }

   public void setSubjectMediaPath(String subjectMediaPath) {
	this.subjectMediaPath = subjectMediaPath;
   }

   public String getUniDefaultBgImgPath() {
	return uniDefaultBgImgPath;
   }

   public void setUniDefaultBgImgPath(String uniDefaultBgImgPath) {
	this.uniDefaultBgImgPath = uniDefaultBgImgPath;
   }
}
