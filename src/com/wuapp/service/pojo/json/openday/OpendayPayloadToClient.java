package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.MoreOpendayVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendayVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class OpendayPayloadToClient implements GenericReturnType {

   ArrayList<OpendayVO> opendayList;
   ArrayList<MoreOpendayVO> moreOpendayList;

   public ArrayList<OpendayVO> getOpendayList() {
	return opendayList;
   }

   public void setOpendayList(ArrayList<OpendayVO> opendayList) {
	this.opendayList = opendayList;
   }

   public ArrayList<MoreOpendayVO> getMoreOpendayList() {
	return moreOpendayList;
   }

   public void setMoreOpendayList(ArrayList<MoreOpendayVO> moreOpendayList) {
	this.moreOpendayList = moreOpendayList;
   }
}
