package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.EventTypeListVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchLandingVO;
import com.wuapp.service.dao.util.valueobject.openday.RegionListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY SEARCH LANDING PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchLandingPayloadToClient implements GenericReturnType {

   ArrayList<OpendaySearchLandingVO> virtualOpendaysList = null;
   ArrayList<OpendaySearchLandingVO> upcomingOpendaysList = null;
   ArrayList<OpendaySearchLandingVO> regionOpendaysList = null;
   ArrayList<OpendaySearchLandingVO> eventTypeOpendaysList = null;
   ArrayList<RegionListVO> regionList =null;
   private String regionName = null;
   ArrayList<EventTypeListVO> eventTypeList =null;
   private String eventTypeName = null;
   private String eventCategoryId = null;
}
