package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchAjaxVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY SEARCH AJAX PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchAjaxPayloadToClient implements GenericReturnType {

   ArrayList<OpendaySearchAjaxVO> opendaySearchAjaxList;
}