package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.EventListVO;
import com.wuapp.service.dao.util.valueobject.openday.EventTypeListVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendayMonthListVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendaySearchLandingVO;
import com.wuapp.service.dao.util.valueobject.openday.RegionListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY SEARCH RESULTS PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchResultsPayloadToClient implements GenericReturnType {

   private String location = null;
   ArrayList<OpendaySearchLandingVO> viewAllOpendaysList = null;
   private String monthName = null;
   ArrayList<OpendayMonthListVO> opendayMonthList = null;
   ArrayList<RegionListVO> opendayRegionList = null;
   private String eventTypeName = null;
   private String eventCategoryId = null;
   ArrayList<EventTypeListVO> eventTypeList = null;
   ArrayList<EventListVO> eventList = null;
}
