package com.wuapp.service.pojo.json.openday;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class OpendayPayloadFromClient implements GenericReturnType {

   private String eventId = null;
   private String collegeId = null;
   private String opendayDate = null;
   private String appVersion = null;
   private String affiliateId = null;
   private String accessToken = null;

   public String getEventId() {
	return eventId;
   }

   public void setEventId(String eventId) {
	this.eventId = eventId;
   }

   public String getCollegeId() {
	return collegeId;
   }

   public void setCollegeId(String collegeId) {
	this.collegeId = collegeId;
   }

   public String getOpendayDate() {
	return opendayDate;
   }

   public void setOpendayDate(String opendayDate) {
	this.opendayDate = opendayDate;
   }

   public String getAppVersion() {
	return appVersion;
   }

   public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }
}
