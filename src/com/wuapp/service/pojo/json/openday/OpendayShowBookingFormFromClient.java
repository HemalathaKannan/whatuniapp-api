package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY BOOKING FORM
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayShowBookingFormFromClient {
   
  private String affiliateId = null;
  private String accessToken = null;
  private String userId = null;
  private String collegeId = null;
  private String eventId = null;
  private String suborderItemId = null;
}
