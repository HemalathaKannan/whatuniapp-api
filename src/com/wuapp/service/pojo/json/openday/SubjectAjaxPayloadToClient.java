package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.SubjectAjaxListVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * SUBJECT AJAX PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class SubjectAjaxPayloadToClient implements GenericReturnType {

   ArrayList<SubjectAjaxListVO> subjectAjaxList;
}