package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY SEARCH RESULTS PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchResultsPayloadFromClient {
   
   private String affiliateId =null;
   private String accessToken = null;
   private String appVersion = null;
   private String pageName = null;
   private String monthValue = null;
   private String location = null;
   private String monthName = null;
   private String eventTypeName = null;
   private String eventCategoryId = null;
   private String sessionId = null;
   private String fromPageNo = null;
   private String toPageNo = null;
}
