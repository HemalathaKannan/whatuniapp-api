package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * SUBJECT AJAX PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class SubjectAjaxPayloadFromClient {

   private String affiliateId = null;
   private String accessToken = null;
   private String keyword = null;
   private String qualCode = null;
   private String categoryId = null;
}
