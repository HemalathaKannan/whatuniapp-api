package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY PROVIDER LANDING PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayProviderLandingPayloadFromClient {   

   private String affiliateId = null;
   private String accessToken = null;
   private String userId = null;
   private String collegeId = null;
   private String eventId = null;
   private String appVersion = null;
   private String totalOpendayCount = null;  
   private String bookingFormFlag = null;
   private String sessionId = null;
}
