package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.EventDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.OtherOdPodVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY BOOKING SUBMIT FORM
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayBookingFormSubmitToClient implements GenericReturnType {
   
  private String sessionId = null;
  private String userId = null;
  private String userSent = null;
  private ArrayList<EventDetailsVO> eventDetails = null;
  private ArrayList<OtherOdPodVO> otherUniOpendays = null;
  private String fbOdSuccessMsg = null;
  private String twitterOdSuccessMsg = null;
  private String userBooked = null;
  private String emailExistsFlag = null;
}

