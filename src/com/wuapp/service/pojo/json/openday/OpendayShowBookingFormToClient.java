package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.openday.ClientConsentVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendaysDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.ProviderDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.QualificationDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.StudyModeVO;
import com.wuapp.service.dao.util.valueobject.openday.UserDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.YearOfEntryVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY BOOKING FORM
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayShowBookingFormToClient implements GenericReturnType {
   
   ArrayList<ProviderDetailsVO> providerDetails = null;
   ArrayList<OpendaysDetailsVO> openDaysDetails = null;
   ArrayList<UserDetailsVO> userDetails = null;
   ArrayList<QualificationDetailsVO> qualificationDetails = null;
   ArrayList<StudyModeVO> studyModeDetails = null;
   ArrayList<ClientConsentVO> clientConsentDetails = null;
   private String odMaxPeopleCount = null;
   ArrayList<YearOfEntryVO> yearOfEntryDetails = null;
   private String userBooked = null;
}
