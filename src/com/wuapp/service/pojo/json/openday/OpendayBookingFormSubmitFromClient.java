package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY BOOKING SUBMIT FORM
 *
 * @author Hema S
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayBookingFormSubmitFromClient {
   
  private String affiliateId = null;
  private String accessToken = null;
  private String eventId = null;
  private String foreName = null;
  private String surName = null;
  private String userEmail = null;
  private String password = null;
  private String mobileNumber = null;
  private String userAgent = null;
  private String userIp = null;
  private String requestURL = null;
  private String refererURL = null;
  private String suborderItemId = null;
  private String networkId = null;
  private String widgetId = null;
  private String levelOfStudy = null;
  private String peopleAttendCount = null;
  private String studyingLevel = null;
  private String studyMode = null;
  private String interestedSubjectArray = null;
  private String courseStartMonth = null;
  private String courseStartYear = null;
  private String specialRegFlag = null;
  private String specialRegMessage = null;
  private String mobileFlag = null;
  private String trackSession = null;
  private String marketingEmailFlag = null;
  private String solusEmailFlag = null;
  private String surveyEmailFlag = null;
  private String screenWidth = null;
  private String clientConsentFlag = null;
  private String latitude = null;
  private String longitude = null; 
  private String sessionId = null; 
  private String userId = null; 
  private String collegeId = null;
}
