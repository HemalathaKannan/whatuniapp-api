package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY SEARCH AJAX PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchAjaxPayloadFromClient {

   private String affiliateId = null;
   private String accessToken = null;
   private String keywordText = null;
}
