package com.wuapp.service.pojo.json.openday;

import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY SEARCH LANDING PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendaySearchLandingPayloadFromClient {
   
   private String affiliateId =null;
   private String accessToken = null;
   private String appVersion = null;
   private String userId = null;
   private String locationMode = null;
   private String regionName = null;
   private String eventTypeName = null;
   private String eventCategoryId = null;
   private String sessionId = null;
   private String userIp = null;
}
