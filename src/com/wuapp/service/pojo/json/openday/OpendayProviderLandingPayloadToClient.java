package com.wuapp.service.pojo.json.openday;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.openday.AdditionalResourcesVO;
import com.wuapp.service.dao.util.valueobject.openday.AddressVO;
import com.wuapp.service.dao.util.valueobject.openday.ContentSectionVO;
import com.wuapp.service.dao.util.valueobject.openday.GallerySectionVO;
import com.wuapp.service.dao.util.valueobject.openday.OpendayListVO;
import com.wuapp.service.dao.util.valueobject.openday.SelectedEventDetailsVO;
import com.wuapp.service.dao.util.valueobject.openday.TestimonialSectionVO;
import com.wuapp.service.dao.util.valueobject.openday.UniInfoVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Getter;
import lombok.Setter;

/**
 * OPENDAY PROVIDER LANDING PAGE
 *
 * @author Hemalatha K
 * @since 06_MAY_2020
 * @version 1.0
 */

@Getter @Setter
public class OpendayProviderLandingPayloadToClient implements GenericReturnType {
   
   ArrayList<UniInfoVO> uniDetails = null;
   ArrayList<OpendayListVO> opendayList = null;
   ArrayList<GallerySectionVO> gallerySection = null;
   ArrayList<ContentSectionVO> contentSection = null;
   ArrayList<AdditionalResourcesVO> additionalResource = null;
   ArrayList<AddressVO> uniAddress = null;
   ArrayList<AppVersionVO> appversionDetails = null;  
   ArrayList<TestimonialSectionVO> testimonialSection = null;
   private String mapBoxKey = null;
   ArrayList<SelectedEventDetailsVO> eventDetails = null;
}
