package com.wuapp.service.pojo.json.common;

/**
 * Kind of marker interface - to achive common return type from Services
 * Service can which will return two different TYPE based on success and failure.
 * Success - result JSON
 * Failure - exception JSON
 * 
 * @author Mohamed.Syed
 * @since wuapp1.0_20170315 - initial draft
 */
public interface GenericReturnType {
}
