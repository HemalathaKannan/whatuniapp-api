package com.wuapp.service.pojo.json.favourites;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.favourites.ShortlistDetailsVO;
import com.wuapp.service.dao.util.valueobject.favourites.SuggestionDetailsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class FavouritesFormPayloadToClient implements GenericReturnType {

   ArrayList<ShortlistDetailsVO> shortlistDetails;
   ArrayList<SuggestionDetailsVO> suggestionDetails;
   String shortlistExistFlag;
   String reorderReturnFlag;
   String returnFlag;
   String guestUserFlag;

   
   public String getGuestUserFlag() {
      return guestUserFlag;
   }

   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }

   //
   public ArrayList<ShortlistDetailsVO> getShortlistDetails() {
	return shortlistDetails;
   }

   public void setShortlistDetails(ArrayList<ShortlistDetailsVO> shortlistDetails) {
	this.shortlistDetails = shortlistDetails;
   }

   public ArrayList<SuggestionDetailsVO> getSuggestionDetails() {
	return suggestionDetails;
   }

   public void setSuggestionDetails(ArrayList<SuggestionDetailsVO> suggestionDetails) {
	this.suggestionDetails = suggestionDetails;
   }

   public String getShortlistExistFlag() {
	return shortlistExistFlag;
   }

   public void setShortlistExistFlag(String shortlistExistFlag) {
	this.shortlistExistFlag = shortlistExistFlag;
   }

   public String getReorderReturnFlag() {
	return reorderReturnFlag;
   }

   public void setReorderReturnFlag(String reorderReturnFlag) {
	this.reorderReturnFlag = reorderReturnFlag;
   }

   public String getReturnFlag() {
	return returnFlag;
   }

   public void setReturnFlag(String returnFlag) {
	this.returnFlag = returnFlag;
   }
}
