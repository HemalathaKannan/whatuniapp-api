package com.wuapp.service.pojo.json.favourites;

import com.wuapp.service.pojo.json.common.GenericReturnType;

public class FavouritesFormPayloadFromClient implements GenericReturnType {

   private String userId;
   private String subject;
   private String qualification;
   private String latitude;
   private String longitude;
   private String locationType;
   private String region;
   private String regionFlag;
   private String studyMode;
   private String previousQualification;
   private String previousQualificationGrade;
   private String assessmentType;
   private String reviewSubjectOne;
   private String reviewSubjectTwo;
   private String reviewSubjectThree;
   private String shortlistExistFlag;
   private String affiliateId;
   private String accessToken;
   private String finalChoiceIds;
   private String institutionIds;
   private String courseIds;
   private String basketContentIds;
   private String addTo;
   private String removeFrom;
   private String userIP;
   private String userAgent;
   private String jacsCode;

   public String getUserId() {
	return userId;
   }

   public void setUserId(String userId) {
	this.userId = userId;
   }

   public String getLatitude() {
	return latitude;
   }

   public void setLatitude(String latitude) {
	this.latitude = latitude;
   }
   
   public String getLongitude() {
      return longitude;
   }
   
   public void setLongitude(String longitude) {
      this.longitude = longitude;
   }
   
   public String getLocationType() {
	return locationType;
   }

   public void setLocationType(String locationType) {
	this.locationType = locationType;
   }

   public String getRegion() {
	return region;
   }

   public void setRegion(String region) {
	this.region = region;
   }

   public String getRegionFlag() {
	return regionFlag;
   }

   public void setRegionFlag(String regionFlag) {
	this.regionFlag = regionFlag;
   }

   public String getStudyMode() {
	return studyMode;
   }

   public void setStudyMode(String studyMode) {
	this.studyMode = studyMode;
   }

   public String getPreviousQualification() {
	return previousQualification;
   }

   public void setPreviousQualification(String previousQualification) {
	this.previousQualification = previousQualification;
   }

   public String getPreviousQualificationGrade() {
	return previousQualificationGrade;
   }

   public void setPreviousQualificationGrade(String previousQualificationGrade) {
	this.previousQualificationGrade = previousQualificationGrade;
   }

   public String getAssessmentType() {
	return assessmentType;
   }

   public void setAssessmentType(String assessmentType) {
	this.assessmentType = assessmentType;
   }

   public String getReviewSubjectOne() {
	return reviewSubjectOne;
   }

   public void setReviewSubjectOne(String reviewSubjectOne) {
	this.reviewSubjectOne = reviewSubjectOne;
   }

   public String getReviewSubjectTwo() {
	return reviewSubjectTwo;
   }

   public void setReviewSubjectTwo(String reviewSubjectTwo) {
	this.reviewSubjectTwo = reviewSubjectTwo;
   }

   public String getReviewSubjectThree() {
	return reviewSubjectThree;
   }

   public void setReviewSubjectThree(String reviewSubjectThree) {
	this.reviewSubjectThree = reviewSubjectThree;
   }

   public String getShortlistExistFlag() {
	return shortlistExistFlag;
   }

   public void setShortlistExistFlag(String shortlistExistFlag) {
	this.shortlistExistFlag = shortlistExistFlag;
   }

   public String getAffiliateId() {
	return affiliateId;
   }

   public void setAffiliateId(String affiliateId) {
	this.affiliateId = affiliateId;
   }

   public String getAccessToken() {
	return accessToken;
   }

   public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
   }

   public String getFinalChoiceIds() {
	return finalChoiceIds;
   }

   public void setFinalChoiceIds(String finalChoiceIds) {
	this.finalChoiceIds = finalChoiceIds;
   }

   public String getInstitutionIds() {
	return institutionIds;
   }

   public void setInstitutionIds(String institutionIds) {
	this.institutionIds = institutionIds;
   }

   public String getCourseIds() {
	return courseIds;
   }

   public void setCourseIds(String courseIds) {
	this.courseIds = courseIds;
   }

   public String getBasketContentIds() {
	return basketContentIds;
   }

   public void setBasketContentIds(String basketContentIds) {
	this.basketContentIds = basketContentIds;
   }

   public String getAddTo() {
	return addTo;
   }

   public void setAddTo(String addTo) {
	this.addTo = addTo;
   }

   public String getRemoveFrom() {
	return removeFrom;
   }

   public void setRemoveFrom(String removeFrom) {
	this.removeFrom = removeFrom;
   }

   public String getUserIP() {
	return userIP;
   }

   public void setUserIP(String userIP) {
	this.userIP = userIP;
   }

   public String getUserAgent() {
	return userAgent;
   }

   public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
   }

   public String getSubject() {
	return subject;
   }

   public void setSubject(String subject) {
	this.subject = subject;
   }

   public String getQualification() {
	return qualification;
   }

   public void setQualification(String qualification) {
	this.qualification = qualification;
   }

   public String getJacsCode() {
	return jacsCode;
   }

   public void setJacsCode(String jacsCode) {
	this.jacsCode = jacsCode;
   }
}
