package com.wuapp.service.pojo.json.home;

import lombok.Data;

@Data
public class HomePayloadFromClient {

   private String userId;
   private String latitude;
   private String longitude;
   private String locationMode;
   private String recentSubjectCount;
   private String otherSubjectCount;
   private String institutionsCloseToYouCount;
   private String institutionsWithUpCommingOpendaysCount;
   private String appVersion;
   private String accessToken;
   private String affiliateId;
}
