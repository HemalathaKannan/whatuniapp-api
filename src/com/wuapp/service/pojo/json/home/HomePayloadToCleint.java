package com.wuapp.service.pojo.json.home;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.home.AddressVO;
import com.wuapp.service.dao.util.valueobject.home.FeaturedProviderVO;
import com.wuapp.service.dao.util.valueobject.home.FeaturedSlotsVO;
import com.wuapp.service.dao.util.valueobject.home.InstitutionVO;
import com.wuapp.service.dao.util.valueobject.home.SubjectVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;
import lombok.Data;

@Data
public class HomePayloadToCleint implements GenericReturnType {

   private String userId;
   private String userName;
   private ArrayList<SubjectVO> recentSubjects = null;
   private ArrayList<SubjectVO> otherSubjects = null;
   private ArrayList<InstitutionVO> institutionsCloseToYou = null;
   private ArrayList<InstitutionVO> institutionsWithUpCommingOpendays = null;
   private ArrayList<AddressVO> address = null;
   private ArrayList<AppVersionVO> versionDetails = null;
   private String postcode;
   private String uniDefaultBgImgPath;
   private String showClearingSwitchFlag;
   private String appClearingResultFlag;
   private String currentYear;
   private ArrayList<FeaturedProviderVO> featuredProviderList = null;
   private String guestUserFlag;
   private String interstitialApplyFlag;
   //
   // Added for showing featured university, course and openday pods in home page by Hemalatha.K on 04_FEB_2019_REL 
   private ArrayList<FeaturedSlotsVO> featuredUniversity = null;
   private ArrayList<FeaturedSlotsVO> featuredCourse = null;
   private ArrayList<FeaturedSlotsVO> featuredOpenDay = null; 
   //Added for COVID related article changes by Hema.S on 21_APR_2020_REL
   private String articleFlag = null;
   private String articleContent = null;   
   private String appClearingBanner = null;
   private String appClearingurl = null;
}
