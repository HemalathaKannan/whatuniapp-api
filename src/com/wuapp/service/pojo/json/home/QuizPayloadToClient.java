package com.wuapp.service.pojo.json.home;

import java.util.ArrayList;
import com.wuapp.service.dao.util.valueobject.common.AppVersionVO;
import com.wuapp.service.dao.util.valueobject.quiz.CourseAjaxVO;
import com.wuapp.service.dao.util.valueobject.quiz.JobIndustryAjaxVO;
import com.wuapp.service.dao.util.valueobject.quiz.PreviousQualVO;
import com.wuapp.service.dao.util.valueobject.quiz.QualSubjectAjaxVO;
import com.wuapp.service.dao.util.valueobject.quiz.QuestionDetailsVO;
import com.wuapp.service.pojo.json.common.GenericReturnType;

public class QuizPayloadToClient implements GenericReturnType {

   private ArrayList<QuestionDetailsVO> questionDetailsList = null;
   private ArrayList<PreviousQualVO> previousQualList = null;
   private ArrayList<CourseAjaxVO> courseList = null;
   private ArrayList<AppVersionVO> versionDetails = null;
   private ArrayList<JobIndustryAjaxVO> jobIndustryList = null;
   private ArrayList<QualSubjectAjaxVO> qualSubjectList = null;
   private String courseCount = null;
   private String selectedFilterUrl = null;
   private String guestUserFlag = null;

   
   
   public String getGuestUserFlag() {
      return guestUserFlag;
   }


   
   public void setGuestUserFlag(String guestUserFlag) {
      this.guestUserFlag = guestUserFlag;
   }


   public String getSelectedFilterUrl() {
      return selectedFilterUrl;
   }

   
   public void setSelectedFilterUrl(String selectedFilterUrl) {
      this.selectedFilterUrl = selectedFilterUrl;
   }

   public ArrayList<QualSubjectAjaxVO> getQualSubjectList() {
	return qualSubjectList;
   }

   public void setQualSubjectList(ArrayList<QualSubjectAjaxVO> qualSubjectList) {
	this.qualSubjectList = qualSubjectList;
   }

   public ArrayList<JobIndustryAjaxVO> getJobIndustryList() {
	return jobIndustryList;
   }

   public void setJobIndustryList(ArrayList<JobIndustryAjaxVO> jobIndustryList) {
	this.jobIndustryList = jobIndustryList;
   }

   public String getCourseCount() {
	return courseCount;
   }

   public void setCourseCount(String courseCount) {
	this.courseCount = courseCount;
   }

   public ArrayList<QuestionDetailsVO> getQuestionDetailsList() {
	return questionDetailsList;
   }

   public void setQuestionDetailsList(ArrayList<QuestionDetailsVO> questionDetailsList) {
	this.questionDetailsList = questionDetailsList;
   }

   public ArrayList<PreviousQualVO> getPreviousQualList() {
	return previousQualList;
   }

   public void setPreviousQualList(ArrayList<PreviousQualVO> previousQualList) {
	this.previousQualList = previousQualList;
   }

   public ArrayList<CourseAjaxVO> getCourseList() {
	return courseList;
   }

   public void setCourseList(ArrayList<CourseAjaxVO> courseList) {
	this.courseList = courseList;
   }

   public ArrayList<AppVersionVO> getVersionDetails() {
	return versionDetails;
   }

   public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
	this.versionDetails = versionDetails;
   }
}
