package com.wuapp.service.business;

import java.util.Map;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadFromClient;

public interface IProviderBusiness {

   public Map<String, Object> getProviderResults(ProviderResultsPayloadFromClient providerResultsPayloadFromClient);
}
