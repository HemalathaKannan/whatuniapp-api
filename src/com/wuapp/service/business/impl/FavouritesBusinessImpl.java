package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IFavouritesBusiness;
import com.wuapp.service.dao.IFavouritesDAO;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;

/**
 * FAVOURITES/FINAL 5 PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class FavouritesBusinessImpl implements IFavouritesBusiness {

   public Map<String, Object> getShortlistDetials(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	Map<String, Object> resultMap = favouritesDAO.getShortlistDetials(favouritesFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> reorderChoices(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	Map<String, Object> resultMap = favouritesDAO.reorderChoices(favouritesFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> addOrRemoveShortlist(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient) {
	Map<String, Object> resultMap = favouritesDAO.addOrRemoveShortlist(favouritesFormPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public IFavouritesDAO favouritesDAO = null;

   public IFavouritesDAO getFavouritesDAO() {
	return favouritesDAO;
   }

   public void setFavouritesDAO(IFavouritesDAO favouritesDAO) {
	this.favouritesDAO = favouritesDAO;
   }
}