package com.wuapp.service.business.impl;

import java.util.HashMap;
import java.util.Map;
import com.wuapp.service.business.ICommonBusiness;
import com.wuapp.service.dao.ICommonDAO;
import com.wuapp.service.pojo.json.interaction.InteractionPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.SaveActionsPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadFromClient;

public class CommonBusinessImpl implements ICommonBusiness {

   public Map<String, Object> preLoadStaticData(HashMap<String, String> parameters) {
	return commonDAO.preLoadStaticData(parameters);
   }

   public Map<String, Object> dbStatsLogging(InteractionPayloadFromClient interactionPayloadFromClient) {
	Map<String, Object> resultMap = commonDAO.dbStatsLogging(interactionPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserActions(SaveActionsPayloadFromClient saveActionsPayloadFromClient) {
	Map<String, Object> resultMap = commonDAO.saveUserActions(saveActionsPayloadFromClient);
	return resultMap;
   }
   
   public Map<String, Object> getClearingFlag(GetClearingFlagPayloadFromClient getClearingFlagPayloadFromClient) {
	Map<String, Object> resultMap = commonDAO.getClearingFlag(getClearingFlagPayloadFromClient);
	return resultMap;
   }

   /*****************************************************************************
   *
   */
   public ICommonDAO commonDAO = null;

   public ICommonDAO getCommonDAO() {
	return commonDAO;
   }

   public void setCommonDAO(ICommonDAO commonDAO) {
	this.commonDAO = commonDAO;
   }
}
