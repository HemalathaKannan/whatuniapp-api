package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IProfileBusiness;
import com.wuapp.service.dao.IProfileDAO;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadFromClient;

public class ProfileBusinessImpl implements IProfileBusiness {

   public Map<String, Object> getProfileDetails(ProfileDetailsPayloadFromClient profileDetailsPayloadFromClient) {
	return profileDAO.getProfileDetails(profileDetailsPayloadFromClient);
   }

   /*****************************************************************************
   *
   */
   public IProfileDAO profileDAO = null;

   public IProfileDAO getProfileDAO() {
	return profileDAO;
   }

   public void setProfileDAO(IProfileDAO profileDAO) {
	this.profileDAO = profileDAO;
   }
}
