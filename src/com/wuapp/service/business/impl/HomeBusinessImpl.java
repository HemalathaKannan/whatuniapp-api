package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IHomeBusiness;
import com.wuapp.service.dao.IHomeDAO;
import com.wuapp.service.pojo.json.home.HomePayloadFromClient;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;

public class HomeBusinessImpl implements IHomeBusiness {

   public Map<String, Object> getBrowseResults(HomePayloadFromClient homePayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.getBrowseResults(homePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getQuizDetails(QuizPayloadFromClient quizPayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.getQuizDetails(quizPayloadFromClient);
	return resultMap;
   }
  //Added for DESKTOP chatbot- Jeyalakshmi 08/Aug/2019
   public Map<String, Object> getDesktopQuizDetails(QuizPayloadFromClient quizPayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.getDesktopQuizDetails(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getCourseList(QuizPayloadFromClient quizPayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.getCourseList(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getJobIndustryList(QuizPayloadFromClient quizPayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.getJobIndustryList(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getQualSubjectList(QuizPayloadFromClient quizPayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.getQualSubjectList(quizPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserActions(QuizPayloadFromClient quizPayloadFromClient) {
	Map<String, Object> resultMap = homeDAO.saveUserActions(quizPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public IHomeDAO homeDAO = null;

   public IHomeDAO getHomeDAO() {
	return homeDAO;
   }

   public void setHomeDAO(IHomeDAO homeDAO) {
	this.homeDAO = homeDAO;
   }
}
