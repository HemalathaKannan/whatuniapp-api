package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IEnquiryBusiness;
import com.wuapp.service.dao.IEnquiryDAO;
import com.wuapp.service.pojo.json.enquiry.EnquiryFormPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayPayloadFromClient;

/**
 * ENQUIRY FORM PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public class EnquiryBusinessImpl implements IEnquiryBusiness {

   //
   public Map<String, Object> getEnquiryFormList(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Map<String, Object> resultMap = enquiryDAO.getEnquiryFormList(enquiryFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> submitEnquiryForm(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Map<String, Object> resultMap = enquiryDAO.submitEnquiryForm(enquiryFormPayloadFromClient);
	return resultMap;
   }

   // getAjaxAddressList
   public Map<String, Object> saveOpenDays(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Map<String, Object> resultMap = enquiryDAO.saveOpenDays(enquiryFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> getAjaxAddressList(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Map<String, Object> resultMap = enquiryDAO.getAjaxAddressList(enquiryFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> openDaysStatsLog(EnquiryFormPayloadFromClient enquiryFormPayloadFromClient) {
	Map<String, Object> resultMap = enquiryDAO.openDaysStatsLog(enquiryFormPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> getOpendaysData(OpendayPayloadFromClient opendayPayloadFromClient) {
	Map<String, Object> resultMap = enquiryDAO.getOpendaysData(opendayPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public IEnquiryDAO enquiryDAO = null;

   public IEnquiryDAO getEnquiryDAO() {
	return enquiryDAO;
   }

   public void setEnquiryDAO(IEnquiryDAO enquiryDAO) {
	this.enquiryDAO = enquiryDAO;
   }
}
