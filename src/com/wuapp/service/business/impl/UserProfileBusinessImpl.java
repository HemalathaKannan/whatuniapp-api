package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IUserProfileBusiness;
import com.wuapp.service.dao.IUserProfileDAO;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;

public class UserProfileBusinessImpl implements IUserProfileBusiness {

   public Map<String, Object> getUserProfile(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Map<String, Object> resultMap = userProfileDAO.getUserProfile(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserProfileInfo(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Map<String, Object> resultMap = userProfileDAO.saveUserProfileInfo(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getUserPreference(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Map<String, Object> resultMap = userProfileDAO.getUserPreference(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> saveUserPreference(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Map<String, Object> resultMap = userProfileDAO.saveUserPreference(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getSchoolList(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Map<String, Object> resultMap = userProfileDAO.getSchoolList(userProfilePayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> sendEmailToWhatuni(UserProfilePayloadFromClient userProfilePayloadFromClient) {
	Map<String, Object> resultMap = userProfileDAO.sendEmailToWhatuni(userProfilePayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public IUserProfileDAO userProfileDAO = null;

   //
   public IUserProfileDAO getUserProfileDAO() {
	return userProfileDAO;
   }

   public void setUserProfileDAO(IUserProfileDAO userProfileDAO) {
	this.userProfileDAO = userProfileDAO;
   }
}
