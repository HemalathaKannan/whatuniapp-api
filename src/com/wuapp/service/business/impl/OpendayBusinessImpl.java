package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IOpendayBusiness;
import com.wuapp.service.dao.IOpendayDAO;
import com.wuapp.service.pojo.json.openday.OpendayBookingFormSubmitFromClient;
import com.wuapp.service.pojo.json.openday.OpendayProviderLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchLandingPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendaySearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.openday.OpendayShowBookingFormFromClient;
import com.wuapp.service.pojo.json.openday.SubjectAjaxPayloadFromClient;

/**
 * @author : Hemalatha K
 * @since : 06_MAY_2020
 * @version : 1.0
 * Class : OpendayBusinessImpl.java  
 * Description : This class is used to getting provider opendays info 
 * Change Log :
 * *************************************************************************************************************
 * Author       Modification Id       Modified On          Modification Details Change
 * *************************************************************************************************************
 * Hema.S            1.1             06_MAY_2020           Added booking form show and submit method
 */

public class OpendayBusinessImpl implements IOpendayBusiness {

   @Override
   public Map<String, Object> getOpendayProviderDetails(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient) {
	Map<String, Object> resultMap = opendayDAO.getOpendayProviderDetails(opendayProviderLandingPayloadFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getMoreOpendayList(OpendayProviderLandingPayloadFromClient opendayProviderLandingPayloadFromClient) {
	Map<String, Object> resultMap = opendayDAO.getMoreOpendayList(opendayProviderLandingPayloadFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getBookingFormSubmitList(OpendayBookingFormSubmitFromClient opendayBookingFormSubmitFromClient) {
	Map<String, Object> resultMap = opendayDAO.getBookingFormSubmitList(opendayBookingFormSubmitFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getShowBookingFormList(OpendayShowBookingFormFromClient opendayShowBookingFormFromClient) {
	Map<String, Object> resultMap = opendayDAO.getShowBookingFormList(opendayShowBookingFormFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getSubjectAjaxList(SubjectAjaxPayloadFromClient subjectAjaxPayloadFromClient) {
	Map<String, Object> resultMap = opendayDAO.getSubjectAjaxList(subjectAjaxPayloadFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getOpendaySearchList(OpendaySearchLandingPayloadFromClient opendaySearchLandingPayloadFromClient) {
	Map<String, Object> resultMap = opendayDAO.getOpendaySearchList(opendaySearchLandingPayloadFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getOpendaySearchAjaxList(OpendaySearchAjaxPayloadFromClient opendaySearchAjaxPayloadFromClient) {
	Map<String, Object> resultMap = opendayDAO.getOpendaySearchAjaxList(opendaySearchAjaxPayloadFromClient);
	return resultMap;
   }

   @Override
   public Map<String, Object> getOpendaySearchResults(OpendaySearchResultsPayloadFromClient opendaySearchResultsPayloadFromClient) {
	Map<String, Object> resultMap = opendayDAO.getOpendaySearchResults(opendaySearchResultsPayloadFromClient);
	return resultMap;
   }

   private IOpendayDAO opendayDAO = null;

   public IOpendayDAO getOpendayDAO() {
	return opendayDAO;
   }

   public void setOpendayDAO(IOpendayDAO opendayDAO) {
	this.opendayDAO = opendayDAO;
   }
}
