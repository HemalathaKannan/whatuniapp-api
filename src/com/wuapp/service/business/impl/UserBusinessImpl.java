package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IUserBusiness;
import com.wuapp.service.dao.IUserDAO;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;

public class UserBusinessImpl implements IUserBusiness {

   //
   public Map<String, Object> userRegistration(UserPayloadFromClient userPayloadFromClient) {
	Map<String, Object> resultMap = userDAO.userRegistration(userPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> userLogin(UserPayloadFromClient userPayloadFromClient) {
	Map<String, Object> resultMap = userDAO.userLogin(userPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> checkUserExisted(UserPayloadFromClient userPayloadFromClient) {
	Map<String, Object> resultMap = userDAO.checkUserExisted(userPayloadFromClient);
	return resultMap;
   }

   //
   public Map<String, Object> userForgotPassword(UserPayloadFromClient userPayloadFromClient) {
	Map<String, Object> resultMap = userDAO.userForgotPassword(userPayloadFromClient);
	return resultMap;
   }
   //
   public Map<String, Object> getGuestUserId(){
	Map<String, Object> resultMap = userDAO.getGuestUserId();
	return resultMap;
   }
   //
   public IUserDAO userDAO = null;

   public IUserDAO getUserDAO() {
	return userDAO;
   }

   public void setUserDAO(IUserDAO userDAO) {
	this.userDAO = userDAO;
   }
}
