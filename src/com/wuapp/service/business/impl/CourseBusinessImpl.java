package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.ICourseBusiness;
import com.wuapp.service.dao.ICourseDAO;
import com.wuapp.service.pojo.json.course.SearchAjaxPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchFormPayloadFromClient;
import com.wuapp.service.pojo.json.course.SearchResultsPayloadFromClient;
import com.wuapp.service.pojo.json.coursedetials.CourseDetailsPayloadFromClient;

public class CourseBusinessImpl implements ICourseBusiness {

   public Map<String, Object> getSearchResults(SearchResultsPayloadFromClient searchPayloadFromClient) {
	Map<String, Object> resultMap = courseDAO.getSearchResults(searchPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getSearchFormList(SearchFormPayloadFromClient searchFormPayloadFromClient) {
	Map<String, Object> resultMap = courseDAO.getSearchFormList(searchFormPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getSearchAjaxList(SearchAjaxPayloadFromClient searchAjaxPayloadFromClient) {
	Map<String, Object> resultMap = courseDAO.getSearchAjaxList(searchAjaxPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getCourseCount(SearchResultsPayloadFromClient searchPayloadFromClient) {
	Map<String, Object> resultMap = courseDAO.getCourseCount(searchPayloadFromClient);
	return resultMap;
   }

   public Map<String, Object> getCourseDetials(CourseDetailsPayloadFromClient courseDetailsPayloadFromClient) {
	Map<String, Object> resultMap = courseDAO.getCourseDetials(courseDetailsPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
   * 
   */
   public ICourseDAO courseDAO = null;

   public ICourseDAO getCourseDAO() {
	return courseDAO;
   }

   public void setCourseDAO(ICourseDAO courseDAO) {
	this.courseDAO = courseDAO;
   }
}
