package com.wuapp.service.business.impl;

import java.util.Map;
import com.wuapp.service.business.IProviderBusiness;
import com.wuapp.service.dao.IProviderDAO;
import com.wuapp.service.pojo.json.provider.ProviderResultsPayloadFromClient;

public class ProviderBusinessImpl implements IProviderBusiness {

   public Map<String, Object> getProviderResults(ProviderResultsPayloadFromClient providerResultsPayloadFromClient) {
	Map<String, Object> resultMap = providerDAO.getProviderResults(providerResultsPayloadFromClient);
	return resultMap;
   }

   /**************************************************************************
    * 
    */
   public IProviderDAO providerDAO = null;

   public IProviderDAO getProviderDAO() {
	return providerDAO;
   }

   public void setProviderDAO(IProviderDAO providerDAO) {
	this.providerDAO = providerDAO;
   }
}
