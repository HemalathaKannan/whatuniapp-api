package com.wuapp.service.business;

import java.util.Map;
import com.wuapp.service.pojo.json.userprofile.UserProfilePayloadFromClient;

public interface IUserProfileBusiness {

   public Map<String, Object> getUserProfile(UserProfilePayloadFromClient userProfilePayloadFromClient);

   public Map<String, Object> saveUserProfileInfo(UserProfilePayloadFromClient userProfilePayloadFromClient);

   public Map<String, Object> getUserPreference(UserProfilePayloadFromClient userProfilePayloadFromClient);

   public Map<String, Object> saveUserPreference(UserProfilePayloadFromClient userProfilePayloadFromClient);

   public Map<String, Object> getSchoolList(UserProfilePayloadFromClient userProfilePayloadFromClient);

   public Map<String, Object> sendEmailToWhatuni(UserProfilePayloadFromClient userProfilePayloadFromClient);
}
