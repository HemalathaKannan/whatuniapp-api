package com.wuapp.service.business;

import java.util.Map;
import com.wuapp.service.pojo.json.user.UserPayloadFromClient;

public interface IUserBusiness {

   public Map<String, Object> userRegistration(UserPayloadFromClient userPayloadFromClient);

   public Map<String, Object> userLogin(UserPayloadFromClient userPayloadFromClient);

   public Map<String, Object> checkUserExisted(UserPayloadFromClient userPayloadFromClient);

   public Map<String, Object> userForgotPassword(UserPayloadFromClient userPayloadFromClient);
   
   public Map<String, Object> getGuestUserId();
}
