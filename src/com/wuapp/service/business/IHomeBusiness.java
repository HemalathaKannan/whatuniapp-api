package com.wuapp.service.business;

import java.util.Map;
import com.wuapp.service.pojo.json.home.HomePayloadFromClient;
import com.wuapp.service.pojo.json.home.QuizPayloadFromClient;

public interface IHomeBusiness {

   public Map<String, Object> getBrowseResults(HomePayloadFromClient homePayloadFromClient);

   public Map<String, Object> getQuizDetails(QuizPayloadFromClient quizPayloadFromClient);
   
   public Map<String, Object> getDesktopQuizDetails(QuizPayloadFromClient quizPayloadFromClient); //Added for DESKTOP chatbot- Jeyalakshmi 08/Aug/2019

   public Map<String, Object> getCourseList(QuizPayloadFromClient quizPayloadFromClient);

   public Map<String, Object> getJobIndustryList(QuizPayloadFromClient quizPayloadFromClient);

   public Map<String, Object> getQualSubjectList(QuizPayloadFromClient quizPayloadFromClient);

   public Map<String, Object> saveUserActions(QuizPayloadFromClient quizPayloadFromClient);
}
