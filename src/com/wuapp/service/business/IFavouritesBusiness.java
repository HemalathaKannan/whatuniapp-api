package com.wuapp.service.business;

import java.util.Map;
import com.wuapp.service.pojo.json.favourites.FavouritesFormPayloadFromClient;

/**
 * FAVOURITES/FINAL 5 PAGE
 *
 * @author Keerthika P
 * @since wuapp1.0_ - Initial draft
 * @version 1.1
 */
public interface IFavouritesBusiness {

   public Map<String, Object> getShortlistDetials(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient);

   //
   public Map<String, Object> reorderChoices(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient);

   //
   public Map<String, Object> addOrRemoveShortlist(FavouritesFormPayloadFromClient favouritesFormPayloadFromClient);
}
