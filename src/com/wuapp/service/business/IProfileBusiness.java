package com.wuapp.service.business;

import java.util.Map;
import com.wuapp.service.pojo.json.profile.ProfileDetailsPayloadFromClient;

public interface IProfileBusiness {

   public Map<String, Object> getProfileDetails(ProfileDetailsPayloadFromClient profileDetailsPayloadFromClient);
}
