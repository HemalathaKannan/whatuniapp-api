package com.wuapp.service.business;

import java.util.HashMap;
import java.util.Map;
import com.wuapp.service.pojo.json.interaction.InteractionPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.SaveActionsPayloadFromClient;
import com.wuapp.service.pojo.json.interaction.GetClearingFlagPayloadFromClient;

public interface ICommonBusiness {

   public Map<String, Object> preLoadStaticData(HashMap<String, String> parameters);

   public Map<String, Object> dbStatsLogging(InteractionPayloadFromClient interactionPayloadFromClient);

   public Map<String, Object> saveUserActions(SaveActionsPayloadFromClient saveActionsPayloadFromClient);
   
   public Map<String, Object> getClearingFlag(GetClearingFlagPayloadFromClient getClearingFlagPayloadFromClient);
}
