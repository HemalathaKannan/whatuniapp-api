package com.wuapp.util.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Encode request & respose to UTF-8 friendly to support non-engish languages
 *
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public class RequestEncodeFilter implements Filter {

   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	//
	// Setting the CHARACTER_ENCODING for the request
	//
	request.setCharacterEncoding("UTF-8");
	//
	// Pass the request on 
	//
	chain.doFilter(request, response);
	//
	// Setting the CONTENT_TYPE for response 
	//
	response.setContentType("text/html; charset=UTF-8");
   }

   public void init(FilterConfig filterConfig) {
	this.filterConfig = filterConfig;
   }

   public void destroy() {
	this.filterConfig = null;
   }

   /*****************************************************************************
    *
    */
   private FilterConfig filterConfig = null;

   public FilterConfig getFilterConfig() {
	return filterConfig;
   }

   public void setFilterConfig(FilterConfig filterConfig) {
	this.filterConfig = filterConfig;
   }
}
