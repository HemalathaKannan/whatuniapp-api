package com.wuapp.util.developer;

public class TrackingPOJO {

   private String url = null;
   private String queryString = null;
   private String actionClass = null;
   private String actionStartTime = null;
   private String actionEndTime = null;
   private String spDetails = null;
   private String spExecutuionTime = null;
   private String jspName = null;

   public void setUrl(String url) {
	this.url = url;
   }

   public String getUrl() {
	return url;
   }

   public void setQueryString(String queryString) {
	this.queryString = queryString;
   }

   public String getQueryString() {
	return queryString;
   }

   public void setActionClass(String actionClass) {
	this.actionClass = actionClass;
   }

   public String getActionClass() {
	return actionClass;
   }

   public void setSpDetails(String spDetails) {
	this.spDetails = spDetails;
   }

   public String getSpDetails() {
	return spDetails;
   }

   public void setSpExecutuionTime(String spExecutuionTime) {
	this.spExecutuionTime = spExecutuionTime;
   }

   public String getSpExecutuionTime() {
	return spExecutuionTime;
   }

   public void setJspName(String jspName) {
	this.jspName = jspName;
   }

   public String getJspName() {
	return jspName;
   }

   public void setActionStartTime(String actionStartTime) {
	this.actionStartTime = actionStartTime;
   }

   public String getActionStartTime() {
	return actionStartTime;
   }

   public void setActionEndTime(String actionEndTime) {
	this.actionEndTime = actionEndTime;
   }

   public String getActionEndTime() {
	return actionEndTime;
   }
}
