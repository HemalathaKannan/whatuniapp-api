package com.wuapp.util.developer;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.exception.ExceptionUtils;
import com.wuapp.service.util.common.CommonUtilities;
import com.wuapp.service.util.common.DateUtilities;
import com.wuapp.util.environment.Environment;
import com.wuapp.util.environment.StaticDataPreLoader;

/**
 * Singleton which contains debug/track flags (URL/REQUEST/DB-PRAM,TIME/PAGE-TIME)
 *
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public class Debugger {

   /*****************************************************************************
    * 
    */
   // Private default-constructor to avoid multiple object creation to this class
   private Debugger() {
   }

   // Factory to create a final instance
   private static class Factory {

	private static final Debugger INSTANCE = new Debugger();
   }

   // Public factory method which will return the object which is created only once.
   public static Debugger getInstance() {
	return Factory.INSTANCE;
   }

   // override clone() to avoid multiple object creation
   public Object clone() {
	return getInstance();
   }

   /*****************************************************************************
    * debug related flags
    */
   // PAGE: Time and resource trackings flags
   private boolean debugPageTime = false;
   // DB: debug details (execution-time/parameters)
   private boolean debugDbDetails = false;
   // Request: debug details (execution-time/parameters)
   private boolean debugRequest = false;
   // URL: tracking the URL_PATTERN
   private boolean debugUrl = false;

   /*****************************************************************************
    * method will initialize the variables based on ENVIRONMENT
    */
   public void init() {
	//
	if (StaticDataPreLoader.getInstance().getEnvironment().equals("DEVELOPMENT_ECLIPSE")) {
	   resetAllFlags(true);
	} else {
	   resetAllFlags(false); //TODO make it to false when we settle down with debugging & testing
	}
	//
	// PRINT all values 
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n 01) " + this.getClass().toString() + " --> init() > debugPageTime: " + debugPageTime);
	buffer.append("\n 02) " + this.getClass().toString() + " --> init() > debugDbDetails: " + debugDbDetails);
	buffer.append("\n 03) " + this.getClass().toString() + " --> init() > debugUrl: " + debugUrl);
	buffer.append("\n 04) " + this.getClass().toString() + " --> init() > debugRequest: " + debugRequest);
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   public void refresh() {
	flush();
	init();
   }

   private void flush() {
	resetAllFlags(false);
   }

   public void storeTimeAndResourceDetails(HttpServletRequest request, String actionClassName, String startActionTime, String spDetails, String spExecutionTime) {
	//
	TrackingPOJO trackingPOJO = new TrackingPOJO();
	trackingPOJO.setUrl(request.getRequestURL().toString());
	trackingPOJO.setQueryString(request.getQueryString());
	trackingPOJO.setActionClass(actionClassName);
	trackingPOJO.setActionStartTime(startActionTime);
	trackingPOJO.setActionEndTime((new Long(System.currentTimeMillis()).toString()));
	trackingPOJO.setSpDetails(spDetails);
	trackingPOJO.setSpExecutuionTime(spExecutionTime);
	request.setAttribute("TRACKER_DETAILS", trackingPOJO);
	//
   }

   public void printDbDetails(String spClass, String spName, HashMap<String, String> spInParam, Long timeTaken) {
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n 1) SP_JAVA_CLASS:     " + spClass);
	buffer.append("\n 2) SP_ORACLE_PRC:     " + (spName + "(" + spInParam.size() + "), " + spInParam));
	buffer.append("\n 3) SP_EXECUTION_TIME: " + timeTaken + " ms");
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   public void printRequestDetails(String serilaNumber, String className, HttpServletRequest request) {
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n " + serilaNumber + className);
	buffer.append("\n getPathInfo: " + request.getPathInfo());
	buffer.append("\n getRemoteAddr: " + request.getRemoteAddr());
	buffer.append("\n getRemoteHost: " + request.getRemoteHost());
	buffer.append("\n getRemoteUser: " + request.getRemoteUser());
	buffer.append("\n getRequestURI: " + request.getRequestURI());
	buffer.append("\n getRemoteUser: " + request.getRemoteUser());
	buffer.append("\n getRequestURL: " + request.getRequestURL());
	buffer.append("\n getQueryString: " + request.getQueryString());
	buffer.append("\n getUserPrincipal: " + request.getUserPrincipal());
	buffer.append("\n getServerName: " + request.getServerName());
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   public void printUrlDetails(String actionClass, HttpServletRequest request) {
	String urlString = request.getRequestURI();
	String urlArray[] = urlString.split("/");
	String currentURL = request.getRequestURL().toString() + "?" + request.getQueryString();
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n\n");
	buffer.append("\n CLASS: " + actionClass);
	buffer.append("\n URL: " + currentURL);
	buffer.append("\n MAPPING: " + urlString);
	buffer.append("\n urlArray.length: " + urlArray.length);
	for (int i = 0; i < urlArray.length; i++) {
	   buffer.append("\n urlArray[" + i + "]: " + urlArray[i]);
	}
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   public String getExceptionDetails(Exception exception) {
	String exceptionTraces = ExceptionUtils.getStackTrace(exception); //.replaceAll("\r\n\t", "");
	return exceptionTraces;
   }

   public void printExceptionDetails(Exception exception, HttpServletRequest request, String controllerName) {
	//
	String exceptionTraces = ExceptionUtils.getStackTrace(exception);
	request.setAttribute("customException", (exceptionTraces.replaceAll("at ", " at <br />").replaceAll("\"", "'")));
	request.setAttribute("REASON", exception.toString());
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n URL:\t" + (request.getRequestURL() + "?" + request.getQueryString()));
	buffer.append("\n Class:\t" + controllerName);
	buffer.append("\n Time:\t" + new DateUtilities().getFullDate());
	buffer.append("\n User IP:\t" + new CommonUtilities().getUserIp(request));
	buffer.append("\n -----");
	buffer.append("\n Robot?\t" + "on demand");
	buffer.append("\n Cookies list:\t" + "on demand"); //new CookieManager().getAllCookieDetails(request));
	buffer.append("\n Session variables:\t" + "on demand"); //new CommonUtil().getSessionDetails(request));
	buffer.append("\n Request variables:\t" + "on demand"); //new CommonUtil().getRequestDetails(request));
	buffer.append("\n -----");
	buffer.append("\n Exception traces:");
	buffer.append("\n ----- \n" + exceptionTraces);
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
   }

   public void printExceptionDetails(Exception exception, String className) {
	//
	String exceptionTraces = ExceptionUtils.getStackTrace(exception);
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n URL:\t N/A");
	buffer.append("\n Class:\t" + className);
	buffer.append("\n Time:\t" + new DateUtilities().getFullDate());
	buffer.append("\n User IP:\t N/A");
	buffer.append("\n -----");
	buffer.append("\n Robot?\t N/A");
	buffer.append("\n Cookies list:\t N/A");
	buffer.append("\n Session variables:\t N/A");
	buffer.append("\n Request variables:\t N/A");
	buffer.append("\n -----");
	buffer.append("\n Exception traces:");
	buffer.append("\n ----- \n" + exceptionTraces);
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
   }

   public void resetPageTimeFlags(boolean flag) {
	this.debugPageTime = flag;
   }

   public void resetDbDetailsFlags(boolean flag) {
	this.debugDbDetails = flag;
   }

   public void resetRequestDetailsFlags(boolean flag) {
	this.debugRequest = flag;
   }

   public void resetUrlFlags(boolean flag) {
	this.debugUrl = flag;
   }

   public void resetAllFlags(boolean flag) {
	resetPageTimeFlags(flag);
	resetDbDetailsFlags(flag);
	resetRequestDetailsFlags(flag);
	resetUrlFlags(flag);
   }

   public void setDebugPageTime(boolean debugPageTime) {
	this.debugPageTime = debugPageTime;
   }

   public boolean isDebugPageTime() {
	return debugPageTime;
   }

   public void setDebugUrl(boolean debugUrl) {
	this.debugUrl = debugUrl;
   }

   public boolean isDebugUrl() {
	return debugUrl;
   }

   public void setDebugDbDetails(boolean debugDbDetails) {
	this.debugDbDetails = debugDbDetails;
   }

   public boolean isDebugDbDetails() {
	return debugDbDetails;
   }

   public void setDebugRequest(boolean debugRequest) {
	this.debugRequest = debugRequest;
   }

   public boolean isDebugRequest() {
	return debugRequest;
   }
}
