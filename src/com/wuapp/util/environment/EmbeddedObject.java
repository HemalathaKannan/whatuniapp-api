package com.wuapp.util.environment;

import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import com.wuapp.util.developer.Debugger;

/**
 * Singleton which serves JS/IMG/HTML/CSS paths.
 * 
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public class EmbeddedObject {

   /*****************************************************************************
    * 
    */
   // Private default-constructor to avoid multiple object creation to this class
   private EmbeddedObject() {
   }

   // Factory to create a final instance
   private static class Factory {

	private static final EmbeddedObject INSTANCE = new EmbeddedObject();
   }

   // Public factory method which will return the object which is created only once.
   public static EmbeddedObject getInstance() {
	return Factory.INSTANCE;
   }

   // override clone() to avoid multiple object creation
   public Object clone() {
	return getInstance();
   }

   /*****************************************************************************
    * variable to hold fully qualified JS/IMG/HTML/CSS paths
    */
   private String environment = null;
   //
   private String cssPath = null;
   private String jsPath = null;
   private String imgPath = null;
   private String uploadedImgPath = null;
   //
   private String cssFolder = null;
   private String jsFolder = null;
   private String imgFolder = null;
   private String imgFolderSubjects = null;
   private String imgFolderUniLogos = null;
   private String imgFolderUniImages = null;
   private String protocol = null;
   //
   private String imgDomainCount = null;

   /*****************************************************************************
    * method will initialize the variables based on ENVIRONMENT
    */
   public void init() {
	//    
	EstablishEnvironment established = EstablishEnvironment.getInstance();
	//
	// DEFAULT to refer from actual CDN domains. (mainly when the application in server, i.e DEV/TEST/LIVE/FAILOVER)
	//
	environment = StaticDataPreLoader.getInstance().getEnvironment();
	//
	imgDomainCount = established.getBundle().getString("cdn.img.count");
	uploadedImgPath = established.getBundle().getString("cdn.domain.img");
	cssPath = established.getBundle().getString("cdn.domain.css") + established.getBundle().getString("cont.folder.css");
	jsPath = established.getBundle().getString("cdn.domain.js") + established.getBundle().getString("cont.folder.js");
	imgPath = established.getBundle().getString("cdn.domain.img") + established.getBundle().getString("cont.folder.img");
	cssFolder = established.getBundle().getString("cont.folder.css");
	jsFolder = established.getBundle().getString("cont.folder.js");
	imgFolder = established.getBundle().getString("cont.folder.img");
	imgFolderSubjects = established.getBundle().getString("cont.folder.img.subjects");
	imgFolderUniLogos = established.getBundle().getString("cont.folder.img.unis.logos");
	imgFolderUniImages = established.getBundle().getString("cont.folder.img.unis.images");
	protocol = Environment.SSL_PROTOCOL;
	//
	// OVERWRITE the default values to refer from WAR (when we run the application in IDE)
	//
	if ("DEVELOPMENT_ECLIPSE".equals(environment)) {
	   cssPath = established.getContextPath() + "/embeddedobject/css";
	   jsPath = established.getContextPath() + "/embeddedobject/script";
	   imgPath = established.getContextPath() + "/embeddedobject/img";
	}
	if ("DEVELOPMENT_ECLIPSE".equals(environment) || "DEVELOPMENT_APP_SERVER".equals(environment)) {
	   protocol = Environment.DEFAUL_PROTOCOL;
	}
	//
	// PRINT all values 
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n 01) " + this.getClass().toString() + " --> init() > cssPath: " + cssPath);
	buffer.append("\n 02) " + this.getClass().toString() + " --> init() > imgPath: " + imgPath);
	buffer.append("\n 03) " + this.getClass().toString() + " --> init() > jsPath: " + jsPath);
	buffer.append("\n 04) " + this.getClass().toString() + " --> init() > cssFolder: " + cssFolder);
	buffer.append("\n 05) " + this.getClass().toString() + " --> init() > jsFolder: " + jsFolder);
	buffer.append("\n 06) " + this.getClass().toString() + " --> init() > imgFolder: " + imgFolder);
	buffer.append("\n 07) " + this.getClass().toString() + " --> init() > uploadedImgPath: " + uploadedImgPath);
	buffer.append("\n 08) " + this.getClass().toString() + " --> init() > imgDomainCount: " + imgDomainCount);
	buffer.append("\n 09) " + this.getClass().toString() + " --> init() > protocol: " + protocol);
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   public void refresh() {
	flush();
	init();
   }

   private void flush() {
	cssPath = null;
	imgPath = null;
	jsPath = null;
	cssFolder = null;
	jsFolder = null;
	imgFolder = null;
	imgDomainCount = null;
	uploadedImgPath = null;
	protocol = null;
   }

   public boolean isThisMainDomain(String serverName) {
	if (EstablishEnvironment.getInstance().getMainDomain().indexOf(serverName) < 0) {
	   return false;
	}
	return true;
   }

   public String getCssPath(HttpServletRequest request, String fileName) {
	String serverName = request.getServerName();
	//
	// going to load the CSS from CDN domain - if we base URL is main-domain
	//
	if (isThisMainDomain(serverName) || "DEVELOPMENT_ECLIPSE".equals(environment)) {
	   return (cssPath + fileName);
	}
	//
	// going to load the CSS from mdev/mtest domain - if the base URL is mdev/mtest
	//
	else {
	   return (protocol + serverName + cssFolder + fileName);
	}
   }

   public String getJsPath(HttpServletRequest request, String fileName) {
	String serverName = request.getServerName();
	//
	// going to load the JS from CDN domain - if we base URL is main-domain
	//
	if (isThisMainDomain(serverName) || "DEVELOPMENT_ECLIPSE".equals(environment)) {
	   return (jsPath + fileName);
	}
	//
	// going to load the JS from mdev/mtest domain - if the base URL is mdev/mtest
	//
	else {
	   return (protocol + serverName + jsFolder + fileName);
	}
   }

   public String getImgPath(HttpServletRequest request, int imgDomainNumber, String fileName) {
	String serverName = request.getServerName();
	//
	// going to load the SITE_IMAGE from CDN domain - if we base URL is main-domain
	//
	if (isThisMainDomain(serverName)) {
	   if ("DEVELOPMENT_ECLIPSE".equals(environment)) {
		return (getImgDomain(request, 0) + fileName);
	   } else {
		return (getImgDomain(request, imgDomainNumber) + imgFolder + fileName);
	   }
	}
	//
	// going to load the SITE_IMAGE from mdev/mtest domain - if the base URL is mdev/mtest
	// 
	else {
	   return (protocol + serverName + imgFolder + fileName);
	}
   }

   public String getUploadedImagePath(HttpServletRequest request, String fileName) {
	String serverName = request.getServerName();
	//
	// going to load the UPLOADED_IMAGES from CDN domain - if we base URL is main-domain
	//
	if (isThisMainDomain(serverName) || "DEVELOPMENT_ECLIPSE".equals(environment)) {
	   return (getImgDomain(request, 0) + fileName);
	}
	//
	// going to load the UPLOADED_IMAGES from mdev/mtest domain - if the base URL is mdev/mtest
	// 
	else {
	   return (protocol + serverName + fileName);
	}
   }

   /**
    * Will return the image domain
    * i > 0 && i <= 4 will return same number x.
    * i <= 0 || i > 4 will return random number between 1-4
    *
    * @param imgDomainNumber
    * @return
    */
   public String getImgDomain(HttpServletRequest request, int imgDomainNumber) {
	Debugger debug = Debugger.getInstance();
	int tmpImgDomainNumber = 0;
	int idc = Integer.parseInt(imgDomainCount);
	String tpmImgDomain = null;
	tpmImgDomain = EstablishEnvironment.getInstance().getBundle().getString("cdn.domain.img");
	try {
	   if (imgDomainNumber > 0 && imgDomainNumber <= idc) {
		tmpImgDomainNumber = imgDomainNumber;
	   } else {
		Random rnd = new Random();
		int i = rnd.nextInt(idc) + 1;
		tmpImgDomainNumber = i;
	   }
	   tpmImgDomain = tpmImgDomain.replaceAll("#", Integer.toString(tmpImgDomainNumber));
	} catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	}
	return tpmImgDomain;
   }
}
