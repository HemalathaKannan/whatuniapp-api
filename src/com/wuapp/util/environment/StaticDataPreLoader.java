package com.wuapp.util.environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.wuapp.config.SpringConfigurations;
import com.wuapp.config.SpringContextInjector;
import com.wuapp.service.business.ICommonBusiness;
import com.wuapp.service.dao.util.valueobject.common.AccessTokenVO;

/**
 * Signlton which loads static data (from DB) which are all normally used in AJAX call.
 *
 * @author Mohamed Syed
 * @since whatuniapp1.0_20160315 - intial draft
 * @version 1.1
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	         Modified On     	  Modification Details 	                
 * *************************************************************************************************************************
 * Hemalatha.K            2.0               25_JUN_2020           Removed clearing related changes
 */
 
public class StaticDataPreLoader {

   /*****************************************************************************
    * 
    */
   // Private default-constructor to avoid multiple object creation to this class
   private StaticDataPreLoader() {
   }

   // Factory to create a final instance
   private static class Factory {

	private static final StaticDataPreLoader INSTANCE = new StaticDataPreLoader();
   }

   // Public factory method which will return the object which is created only once.
   public static StaticDataPreLoader getInstance() {
	return Factory.INSTANCE;
   }

   // override clone() to avoid multiple object creation
   public Object clone() {
	return getInstance();
   }

   /*****************************************************************************
    * variable to hold static data
    */
   private String environment = null;
   private String appClearingFlag = null;
   private HashMap<String, String> accessTokens = null;

   /*****************************************************************************
    * init static data
    */
   public void init() {
	//
	ICommonBusiness commonBusiness = (ICommonBusiness) SpringContextInjector.getBeanFromSpringContext(SpringConfigurations.COMMON_BUSINESS);
	HashMap<String, String> parameters = new HashMap<String, String>();
	parameters.put("NAME", "ENVIRONMENT");
	Map<String, Object> staticDataMap = commonBusiness.preLoadStaticData(parameters);
	//
	ArrayList accessTokenList = (ArrayList) staticDataMap.get("PC_ACCESS_TOKEN");
	if (accessTokenList != null && accessTokenList.size() > 0) {
	   generateAccessTokens(accessTokenList);
	}
	//
	environment = (String) staticDataMap.get("P_VALUE");
	//
	//
	if (environment == null || environment.trim().length() == 0) {
	   environment = "DEVELOPMENT";
	} else {
	   environment = environment.trim();
	}
	//
	if (environment.equals("DEVELOPMENT")) {
	   String osName = System.getProperty("os.name");
	   String userName = System.getProperty("user.name");
	   //
	   if ((userName.indexOf("oracle") != -1) && (osName.indexOf("Linux") != -1)) {
		environment = "DEVELOPMENT_APP_SERVER";
	   } else {
		environment = "DEVELOPMENT_ECLIPSE";
	   }
	   //
	   osName = null;
	   userName = null;
	}
	//
	// PRINT all values 
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n 01) " + this.getClass().toString() + " --> init() > environment: " + environment);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n 01) " + this.getClass().toString() + " --> init() > accessTokens: " + accessTokens);
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   // refresh static data
   public void refresh() {
	flush();
	init();
   }

   private void generateAccessTokens(ArrayList accessTokenList) {
	AccessTokenVO autoCompleteVO = new AccessTokenVO();
	accessTokens = new HashMap<String, String>();
	for (int i = 0; i < accessTokenList.size(); i++) {
	   autoCompleteVO = (AccessTokenVO) accessTokenList.get(i);
	   accessTokens.put(autoCompleteVO.getAffiliatedId(), autoCompleteVO.getAccessToken());
	}
   }

   public boolean isValidAccessToken(String affiliateId, String accessToken) {
	if (accessToken == null || accessToken.trim().length() == 0) {
	   return false;
	}
	String tmpToken = accessTokens.get(affiliateId);
	if (accessToken.equals(tmpToken)) {
	   return true;
	}
	return false;
   }

   private void flush() {
	environment = null;
   }

   public String getEnvironment() {
	return environment;
   }

   public String getAppClearingFlag() {
	return appClearingFlag;
   }
}
