package com.wuapp.util.environment;

/**
 * Interface which holds basic application variables.
 * 
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public interface Environment {

   public static final String MY_PORT_NUMBER = ":7001";
   public static final String CONTEXT_PATH = "/wuappws";
   //
   public static final String PASS_THROUGH_CONTEXT_PATH = "";
   public static final String PASS_THROUGH_MAPPING_PATH = "/";
   //
   public static final String DEFAULT_LANGUAGE = "en";
   public static final String DEFAULT_COUNTRY = "UK";
   //
   public static final String DEFAUL_PROTOCOL = "http://";
   public static final String SSL_PROTOCOL = "https://";
   //
   public static final String PROJECT_NAME = "WHATUNIAPP - API";
   //
}
