package com.wuapp.util.environment;

import java.util.ResourceBundle;

/**
 * Singleton which initializes/contains basic environment specific variables
 *
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public class EstablishEnvironment {

   /*****************************************************************************
    * 
    */
   // Private default-constructor to avoid multiple object creation to this class
   private EstablishEnvironment() {
   }

   // Factory to create a final instance
   private static class Factory {

	private static final EstablishEnvironment INSTANCE = new EstablishEnvironment();
   }

   // Public factory method which will return the object which is created only once.
   public static EstablishEnvironment getInstance() {
	return Factory.INSTANCE;
   }

   // override clone() to avoid multiple object creation
   public Object clone() {
	return getInstance();
   }

   /*****************************************************************************
    * variables to hold some needed components like domain, portnumber, contextpath etc
    */
   private final String BUNDLE_SOURCE_PATH = "com.wuapp.util.resources.ApplicationResources";
   private ResourceBundle bundle = null;
   private String portNumber = null;
   private String contextPath = null;
   private String mainDomain = null;

   /*****************************************************************************
    * method will initialize the variables based on ENVIRONMENT
    */
   public void init() {
	//
	// DEFAULT to refer for APP_SERVER (mainly when the application in server, i.e DEV/TEST/LIVE/FAILOVER) 
	//
	bundle = ResourceBundle.getBundle(BUNDLE_SOURCE_PATH);
	portNumber = "";
	contextPath = Environment.CONTEXT_PATH;
	mainDomain = bundle.getString("domain.live");
	//
	if (StaticDataPreLoader.getInstance().getEnvironment().equals("DEVELOPMENT_ECLIPSE")) {
	   mainDomain = mainDomain.replaceAll("https", "http");
	   portNumber = Environment.MY_PORT_NUMBER;
	}
	//
	// PRINT all values 
	//
	StringBuffer buffer = new StringBuffer();
	buffer.append("\n\n **********************************************************************************************************");
	buffer.append("\n PROJECT:\t" + Environment.PROJECT_NAME);
	buffer.append("\n ----------------------------------------------------------------------------------------------------------");
	buffer.append("\n 01) " + this.getClass().toString() + " --> init() > bundle: " + bundle);
	buffer.append("\n 02) " + this.getClass().toString() + " --> init() > portNumber: " + portNumber);
	buffer.append("\n 03) " + this.getClass().toString() + " --> init() > contextPath: " + contextPath);
	buffer.append("\n 04) " + this.getClass().toString() + " --> init() > mainDomain: " + mainDomain);
	buffer.append("\n 05) " + this.getClass().toString() + " --> init() > bundleSourcePath: " + BUNDLE_SOURCE_PATH);
	buffer.append("\n **********************************************************************************************************\n\n");
	System.out.println(buffer);
	// nullify
	if (buffer != null) {
	   buffer.setLength(0);
	}
	buffer = null;
	//
   }

   public void refresh() {
	flush();
	init();
   }

   private void flush() {
	bundle = null;
	portNumber = null;
	contextPath = null;
	mainDomain = null;
   }

   public ResourceBundle getBundle() {
	return bundle;
   }

   public String getPortNumber() {
	return portNumber;
   }

   public String getContextPath() {
	return contextPath;
   }

   public String getMainDomain() {
	return mainDomain;
   }
}
