package com.wuapp.util.environment;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.wuapp.util.developer.Debugger;

/**
 * Servlet which prepares all environment related details whenever the application is going to start
 * (WAR deployment / container restart / server restart)
 *
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public class EnvironmentInaugurationServlet extends HttpServlet {

   private static final long serialVersionUID = 1L;

   public void init() throws ServletException {
	StaticDataPreLoader.getInstance().init();
	EstablishEnvironment.getInstance().init();
	EmbeddedObject.getInstance().init();
	Debugger.getInstance().init();
   }

   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//
	// ---(A)--- DECLARE local objects & varialbes
	//
	PrintWriter out = null;
	Debugger debug = Debugger.getInstance();
	//
	// ---(B)--- Do the core business logic.
	//
	try {
	   response.setContentType("text/html; charset=UTF-8");
	   out = response.getWriter();
	   //
	   StaticDataPreLoader.getInstance().refresh();
	   out.println("StaticDataPreLoader.getInstance().refresh() - DONE <br />");
	   //
	   EstablishEnvironment.getInstance().refresh();
	   out.println("EstablishEnvironment.getInstance().refresh() - DONE <br />");
	   //  
	   EmbeddedObject.getInstance().refresh();
	   out.println("EmbeddedObject.getInstance().refresh() - DONE <br />");
	   //  
	   Debugger.getInstance().refresh();
	   out.println("Debugger.getInstance().refresh() - DONE <br />");
	   //
	   //
	}
	//
	// ---(C)--- Print the exception traces with URL & CLASS details.
	//
	catch (Exception e) {
	   debug.printExceptionDetails(e, request, (this.getClass().toString()));
	}
	//
	// ---(Y)--- Nullify all unused local objects for GC
	//
	finally {
	   if (out != null) {
		out.close();
	   }
	   out = null;
	   debug = null;
	}
   }

   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
   }
}
