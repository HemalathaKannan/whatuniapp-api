package com.wuapp.util.exception;

/**
 * Custom Exception to show dummy traces
 *
 * @author Mohamed Syed
 * @since wuapp1.0_20160315 - intial draft
 * @version 1.1
 */
public class WuAppException extends Throwable {

   private static final long serialVersionUID = 1L;

   public WuAppException() {
   }

   public String toString() {
	return "";
   }

   public void printStackTrace() {
   }
}
